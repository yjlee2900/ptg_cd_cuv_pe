/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : BuzzerPWM.C
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : PWM
**     Version   : Bean 02.207, Driver 01.14, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**         This bean implements a pulse-width modulation generator
**         that generates signal with variable duty and fixed cycle. 
**     Settings  :
**         Used output pin             : 
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       4             |  PP0_KWP0_PWM0
**             ----------------------------------------------------
**
**         Timer name                  : PWM0 [8-bit]
**         Counter                     : PWMCNT0   [172]
**         Mode register               : PWMCTL    [165]
**         Run register                : PWME      [160]
**         Prescaler                   : PWMPRCLK  [163]
**         Compare 1 register          : PWMPER0   [178]
**         Compare 2 register          : PWMDTY0   [184]
**         Flip-flop 1 register        : PWMPOL    [161]
**
**         User handling procedure     : not specified
**
**         Output pin
**
**         Port name                   : P
**         Bit number (in port)        : 0
**         Bit mask of the port        : 1
**         Port data register          : PTP       [600]
**         Port control register       : DDRP      [602]
**
**         Runtime setting period      : none
**         Runtime setting ratio       : calculated
**         Initialization:
**              Aligned                : Left
**              Output level           : low
**              Timer                  : Enabled
**              Event                  : Enabled
**         High speed mode
**             Prescaler               : divide-by-1
**             Clock                   : 500000 Hz
**           Initial value of            period        pulse width (ratio 99.6%)
**             Xtal ticks              : 8000          7968
**             microseconds            : 500           498
**             seconds (real)          : 0.0005000     0.0004980
**
**     Contents  :
**         Enable    - byte BuzzerPWM_Enable(void);
**         Disable   - byte BuzzerPWM_Disable(void);
**         SetRatio8 - byte BuzzerPWM_SetRatio8(byte Ratio);
**         SetDutyUS - byte BuzzerPWM_SetDutyUS(word Time);
**         SetDutyMS - byte BuzzerPWM_SetDutyMS(word Time);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/


/* MODULE BuzzerPWM. */

#include "BuzzerPWM.h"

#pragma DATA_SEG BuzzerPWM_DATA
#pragma CODE_SEG BuzzerPWM_CODE

static bool EnUser;                    /* Enable/Disable device by user */
static word RatioStore;                /* Ratio of L-level to H-level */


/*
** ===================================================================
**     Method      :  HWEnDi (bean PWM)
**
**     Description :
**         Enables or disables the peripheral(s) associated with the bean.
**         The method is called automatically as a part of the Enable and 
**         Disable methods and several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void HWEnDi(void)
{
  if (EnUser) {                        /* Enable device? */
    PWME_PWME0 = 1;                    /* Run counter */
  } else {                             /* Disable device? */
    PWME_PWME0 = 0;                    /* Stop counter */
    PWMCNT0 = 0;                       /* Reset counter */
  }
}

/*
** ===================================================================
**     Method      :  SetRatio (bean PWM)
**
**     Description :
**         The method reconfigures the compare and modulo registers of 
**         the peripheral(s) when the speed mode changes. The method is 
**         called automatically as a part of the bean 
**         SetHigh/SetLow/SetSlow methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void SetRatio(void)
{
  PWMDTY0 = (byte)(((250 * (dword)RatioStore) + 32768) >> 16); /* Calculate new value according to the given ratio */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_Enable (bean PWM)
**
**     Description :
**         This method enables the signal generation 
**     Parameters  : None
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte BuzzerPWM_Enable(void)
{
  if (!EnUser) {                       /* Is the device disabled by user? */
    EnUser = TRUE;                     /* If yes then set the flag "device enabled" */
    HWEnDi();                          /* Enable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_Disable (bean PWM)
**
**     Description :
**         Disables the bean - it stops the signal generation and events calling.
**     Parameters  : None
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte BuzzerPWM_Disable(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    EnUser = FALSE;                    /* If yes then set the flag "device disabled" */
    HWEnDi();                          /* Disable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_SetRatio8 (bean PWM)
**
**     Description :
**         This method sets a new duty-cycle ratio.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Ratio      - Ratio is expressed as an 8-bit unsigned integer
**                      number. 0 - 255 value is proportional
**                      to ratio 0 - 100%
**         Note: Calculated duty depends on the timer possibilities
**               and on the selected period.
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte BuzzerPWM_SetRatio8(byte Ratio)
{
  RatioStore = (((word)Ratio << 8) + 128); /* Store new value of the ratio */
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_SetDutyUS (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal. The
**         duty is expressed in microseconds as a 16-bit unsigned integer
**         number.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Time       - Duty to set [in microseconds]
**                      (0 to 500 us in high speed mode)
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte BuzzerPWM_SetDutyUS(word Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  if (Time > 500) {                    /* Is the given value out of range? */
    return ERR_RANGE;                  /* If yes then error */
  }
  PE_Timer_LngMul((dword)Time,(dword)2199023256,&rtval); /* Multiply given value and high speed CPU mode coefficient */
  if (PE_Timer_LngHi3(rtval[0],rtval[1],&RatioStore)) { /* Is the result greater or equal than 65536 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_SetDutyMS (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal. The
**         duty is expressed in milliseconds as a 16-bit unsigned integer
**         number.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Time       - Duty to set [in milliseconds]
**         Note: The period is too short. The method will return
**               just the error code in high speed mode.
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte BuzzerPWM_SetDutyMS(word Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  /* Period is too little. Method 'SetDutyMS' will return only error code in high speed CPU mode. */
  return ERR_MATH;                     /* Calculation error */
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  BuzzerPWM_Init (bean PWM)
**
**     Description :
**         Initializes the associated peripheral(s) and the bean's 
**         internal variables. The method is called automatically as a 
**         part of the application initialization code.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
void BuzzerPWM_Init(void)
{
  /* PWMCNT0: BIT7=0,BIT6=0,BIT5=0,BIT4=0,BIT3=0,BIT2=0,BIT1=0,BIT0=0 */
  setReg8(PWMCNT0, 0);                 /* Reset Counter */ 
  /* PWMSDN: PWMIF=1,PWMIE=0,PWMRSTRT=0,PWMLVL=0,??=0,PWM5IN=0,PWM5INL=0,PWM5ENA=0 */
  setReg8(PWMSDN, 128);                /* Emergency shutdown feature settings */ 
  RatioStore = 65273;                  /* Store initial value of the ratio */
  EnUser = TRUE;                       /* Enable device */
  /* PWMDTY0: BIT7=1,BIT6=1,BIT5=1,BIT4=1,BIT3=1,BIT2=0,BIT1=0,BIT0=1 */
  setReg8(PWMDTY0, 249);               /* Store initial value to the duty-compare register */ 
  /* PWMPER0: BIT7=1,BIT6=1,BIT5=1,BIT4=1,BIT3=1,BIT2=0,BIT1=1,BIT0=0 */
  setReg8(PWMPER0, 250);               /* and to the period register */ 
  /* PWMCLK: PCLK0=1 */
  setReg8Bits(PWMCLK, 1);              /* Select clock source */ 
  HWEnDi();                            /* Enable/disable device according to status flags */
}

/* END BuzzerPWM. */

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
