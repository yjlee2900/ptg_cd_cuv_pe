/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : HallRH_CH1.C
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : BitIO
**     Version   : Bean 02.072, Driver 03.11, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**         This bean "BitIO" implements an one-bit input/output.
**         It uses one bit/pin of a port.
**         Note: This bean is set to work in Input direction only.
**         Methods of this bean are mostly implemented as a macros
**         (if supported by target language and compiler).
**     Settings  :
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       42            |  PA1
**             ----------------------------------------------------
**
**         Port name                   : A
**
**         Bit number (in port)        : 1
**         Bit mask of the port        : 2
**
**         Initial direction           : Input (direction cannot be changed)
**         Initial output value        : 0
**         Initial pull option         : off
**
**         Port data register          : PORTA     [0]
**         Port control register       : DDRA      [2]
**
**         Optimization for            : speed
**     Contents  :
**         GetVal - bool HallRH_CH1_GetVal(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

/* MODULE HallRH_CH1. */

#include "HallRH_CH1.h"
  /* Including shared modules, which are used in the whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Cpu.h"

#pragma DATA_SEG HallRH_CH1_DATA
#pragma CODE_SEG HallRH_CH1_CODE
#pragma CONST_SEG HallRH_CH1_CONST     /* Constant section for this module */
/*
** ===================================================================
**     Method      :  HallRH_CH1_GetVal (bean BitIO)
**
**     Description :
**         This method returns an input value.
**           a) direction = Input  : reads the input value from the
**                                   pin and returns it
**           b) direction = Output : returns the last written value
**         Note: This bean is set to work in Input direction only.
**     Parameters  : None
**     Returns     :
**         ---             - Input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)

** ===================================================================
*/
/*
bool HallRH_CH1_GetVal(void)

**  This method is implemented as a macro. See HallRH_CH1.h file.  **
*/


/* END HallRH_CH1. */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
