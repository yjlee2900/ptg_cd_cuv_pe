/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : Illumination_Ctl_Out.H
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : BitIO
**     Version   : Bean 02.072, Driver 03.11, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**         This bean "BitIO" implements an one-bit input/output.
**         It uses one bit/pin of a port.
**         Note: This bean is set to work in Output direction only.
**         Methods of this bean are mostly implemented as a macros
**         (if supported by target language and compiler).
**     Settings  :
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       11            |  PT4_IOC4_PWM4
**             ----------------------------------------------------
**
**         Port name                   : T
**
**         Bit number (in port)        : 4
**         Bit mask of the port        : 16
**
**         Initial direction           : Output (direction cannot be changed)
**         Initial output value        : 0
**         Initial pull option         : off
**
**         Port data register          : PTT       [576]
**         Port control register       : DDRT      [578]
**
**         Optimization for            : speed
**     Contents  :
**         GetVal - bool Illumination_Ctl_Out_GetVal(void);
**         PutVal - void Illumination_Ctl_Out_PutVal(bool Val);
**         ClrVal - void Illumination_Ctl_Out_ClrVal(void);
**         SetVal - void Illumination_Ctl_Out_SetVal(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef Illumination_Ctl_Out_H_
#define Illumination_Ctl_Out_H_

/* MODULE Illumination_Ctl_Out. */

  /* Including shared modules, which are used in the whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Cpu.h"

#pragma CODE_SEG Illumination_Ctl_Out_CODE
/*
** ===================================================================
**     Method      :  Illumination_Ctl_Out_GetVal (bean BitIO)
**
**     Description :
**         This method returns an input value.
**           a) direction = Input  : reads the input value from the
**                                   pin and returns it
**           b) direction = Output : returns the last written value
**         Note: This bean is set to work in Output direction only.
**     Parameters  : None
**     Returns     :
**         ---             - Input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)

** ===================================================================
*/
#define Illumination_Ctl_Out_GetVal() ( \
    (bool)((getReg8(PTT) & 16))        /* Return port data */ \
  )

/*
** ===================================================================
**     Method      :  Illumination_Ctl_Out_PutVal (bean BitIO)
**
**     Description :
**         This method writes the new output value.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Val             - Output value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)
**     Returns     : Nothing
** ===================================================================
*/
void Illumination_Ctl_Out_PutVal(bool Val);

/*
** ===================================================================
**     Method      :  Illumination_Ctl_Out_ClrVal (bean BitIO)
**
**     Description :
**         This method clears (sets to zero) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define Illumination_Ctl_Out_ClrVal() ( \
    (void)clrReg8Bits(PTT, 16)         /* PTT4=0 */ \
  )

/*
** ===================================================================
**     Method      :  Illumination_Ctl_Out_SetVal (bean BitIO)
**
**     Description :
**         This method sets (sets to one) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define Illumination_Ctl_Out_SetVal() ( \
    (void)setReg8Bits(PTT, 16)         /* PTT4=1 */ \
  )

#pragma CODE_SEG DEFAULT

/* END Illumination_Ctl_Out. */
#endif /* #ifndef __Illumination_Ctl_Out_H_ */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
