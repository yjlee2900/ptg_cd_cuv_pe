/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : CAN_NSTB.H
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : BitIO
**     Version   : Bean 02.072, Driver 03.11, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**         This bean "BitIO" implements an one-bit input/output.
**         It uses one bit/pin of a port.
**         Note: This bean is set to work in Output direction only.
**         Methods of this bean are mostly implemented as a macros
**         (if supported by target language and compiler).
**     Settings  :
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       66            |  PS3
**             ----------------------------------------------------
**
**         Port name                   : S
**
**         Bit number (in port)        : 3
**         Bit mask of the port        : 8
**
**         Initial direction           : Output (direction cannot be changed)
**         Initial output value        : 0
**         Initial pull option         : off
**
**         Port data register          : PTS       [584]
**         Port control register       : DDRS      [586]
**
**         Optimization for            : speed
**     Contents  :
**         GetVal - bool CAN_NSTB_GetVal(void);
**         PutVal - void CAN_NSTB_PutVal(bool Val);
**         ClrVal - void CAN_NSTB_ClrVal(void);
**         SetVal - void CAN_NSTB_SetVal(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef CAN_NSTB_H_
#define CAN_NSTB_H_

/* MODULE CAN_NSTB. */

  /* Including shared modules, which are used in the whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Cpu.h"

#pragma CODE_SEG CAN_NSTB_CODE
/*
** ===================================================================
**     Method      :  CAN_NSTB_GetVal (bean BitIO)
**
**     Description :
**         This method returns an input value.
**           a) direction = Input  : reads the input value from the
**                                   pin and returns it
**           b) direction = Output : returns the last written value
**         Note: This bean is set to work in Output direction only.
**     Parameters  : None
**     Returns     :
**         ---             - Input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)

** ===================================================================
*/
#define CAN_NSTB_GetVal() ( \
    (bool)((getReg8(PTS) & 8))         /* Return port data */ \
  )

/*
** ===================================================================
**     Method      :  CAN_NSTB_PutVal (bean BitIO)
**
**     Description :
**         This method writes the new output value.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Val             - Output value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)
**     Returns     : Nothing
** ===================================================================
*/
void CAN_NSTB_PutVal(bool Val);

/*
** ===================================================================
**     Method      :  CAN_NSTB_ClrVal (bean BitIO)
**
**     Description :
**         This method clears (sets to zero) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define CAN_NSTB_ClrVal() ( \
    (void)clrReg8Bits(PTS, 8)          /* PTS3=0 */ \
  )

/*
** ===================================================================
**     Method      :  CAN_NSTB_SetVal (bean BitIO)
**
**     Description :
**         This method sets (sets to one) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define CAN_NSTB_SetVal() ( \
    (void)setReg8Bits(PTS, 8)          /* PTS3=1 */ \
  )

#pragma CODE_SEG DEFAULT

/* END CAN_NSTB. */
#endif /* #ifndef __CAN_NSTB_H_ */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
