/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : AD1.C
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : ADC
**     Version   : Bean 01.489, Driver 01.15, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**         This device "ADC" implements an A/D converter,
**         its control methods and interrupt/event handling procedure.
**     Settings  :
**         AD control register         : ATDCTL4     [116]
**         Interrupt name              : Vatd
**         Interrupt enable reg.       : ATDCTL23    [114]
**         Priority                    : 
**         User handling procedure     : AD1_OnEnd
**         Number of conversions       : 1
**         AD resolution               : 10-bit
**
**         Input pins
**
**              Port name              : ADL
**              Bit number (in port)   : 0
**              Bit mask of the port   : 1
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 1
**              Bit mask of the port   : 2
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 2
**              Bit mask of the port   : 4
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 3
**              Bit mask of the port   : 8
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 5
**              Bit mask of the port   : 32
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 6
**              Bit mask of the port   : 64
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADL
**              Bit number (in port)   : 7
**              Bit mask of the port   : 128
**              Port data register     : PT1AD       [625]
**              Port control register  : DDR1AD      [627]
**
**              Port name              : ADH
**              Bit number (in port)   : 0
**              Bit mask of the port   : 1
**              Port data register     : PT0AD       [624]
**              Port control register  : DDR0AD      [626]
**
**              Port name              : ADH
**              Bit number (in port)   : 1
**              Bit mask of the port   : 2
**              Port data register     : PT0AD       [624]
**              Port control register  : DDR0AD      [626]
**
**         Initialization:
**              Conversion             : Enabled
**              Event                  : Enabled
**         High speed mode
**             Prescaler               : divide-by-2
**     Contents  :
**         Enable     - byte AD1_Enable(void);
**         Disable    - byte AD1_Disable(void);
**         Measure    - byte AD1_Measure(bool WaitForResult);
**         GetValue16 - byte AD1_GetValue16(word *Values);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/


/* MODULE AD1. */

#pragma MESSAGE DISABLE C5703          /* Disable warning C5703 "Parameter is not referenced" */
#pragma MESSAGE DISABLE C4002          /* Disable warning C4002 "Result not used is ignored" */
#pragma MESSAGE DISABLE C12056         /* Disable warning C12056 "SP debug info incorrect because of optimization or inline assembler" */

#include "Events.h"
#include "AD1.h"

#pragma DATA_SEG AD1_DATA
#pragma CODE_SEG AD1_CODE
#pragma CONST_SEG AD1_CONST            /* Constant section for this module */

#define STOP            0              /* STOP state           */
#define MEASURE         1              /* MESURE state         */
#define CONTINUOUS      2              /* CONTINUOUS state      */
#define SINGLE          3              /* SINGLE state         */

static const byte Channels[] = {       /* Contents for the device control register */
0,1,2,3,5,6,7,8,9};
static bool EnUser;                    /* Enable/Disable device */
static bool OutFlg;                    /* Measurement finish flag */
static byte SumChan;                   /* Number of measured channels */
volatile static byte ModeFlg;          /* Current state of device */
static word AD1_OutV[9];               /* Array of measured values */
/*
** ===================================================================
**     Method      :  AD1_Interrupt (bean ADC)
**
**     Description :
**         The method services the interrupt of the selected peripheral(s)
**         and eventually invokes event(s) of the bean.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
ISR(AD1_Interrupt)
{
  AD1_OutV[SumChan] = ATDDR0;          /* Save measured value */
  SumChan++;                           /* Number of measurement */
  if (SumChan == 9) {                  /* Is number of measurement equal to the number of channels? */
    OutFlg = TRUE;                     /* Measured values are available */
    AD1_OnEnd();                       /* Invoke user event */
    ModeFlg = STOP;                    /* Set the device to the stop mode */
    return;                            /* Return from interrupt */
  }
  ATDCTL5 = Channels[SumChan];         /* Start measurement of next channel */
}

#pragma CODE_SEG AD1_CODE
/*
** ===================================================================
**     Method      :  HWEnDi (bean ADC)
**
**     Description :
**         Enables or disables the peripheral(s) associated with the bean.
**         The method is called automatically as a part of the Enable and 
**         Disable methods and several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void HWEnDi(void)
{
  if (EnUser) {                        /* Enable device? */
    if (ModeFlg) {                     /* Start or stop measurement? */
      OutFlg = FALSE;                  /* Output values aren't available */
      SumChan = 0;                     /* Set the number of measured channels to 0 */
      /* ATDCTL5: ??=0,SC=0,SCAN=0,MULT=0,CD=0,CC=0,CB=0,CA=0 */
      ATDCTL5 = 0;                     /* Start the conversion */
    }
    else {
      /* ATDCTL3: DJM=1,S8C=0,S4C=0,S2C=0,S1C=1,FIFO=0,FRZ1=0,FRZ0=0 */
      ATDCTL3 = 136;                   /* Abort current measurement */
    }
  }
}

/*
** ===================================================================
**     Method      :  AD1_Enable (bean ADC)
**
**     Description :
**         Enables A/D converter bean. <Events> may be generated
**         (<DisableEvent>/<EnableEvent>). If possible, this method
**         switches on A/D converter device, voltage reference, etc.
**     Parameters  : None
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte AD1_Enable(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    return ERR_OK;                     /* If yes then set the flag "device enabled" */
  }
  EnUser = TRUE;                       /* Set the flag "device enabled" */
  HWEnDi();                            /* Enable the device */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  AD1_Disable (bean ADC)
**
**     Description :
**         Disables A/D converter bean. No <events> will be generated.
**         If possible, this method switches off A/D converter device,
**         voltage reference, etc.
**     Parameters  : None
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte AD1_Disable(void)
{
  if (!EnUser) {                       /* Is the device disabled by user? */
    return ERR_OK;                     /* If yes then OK */
  }
  EnUser = FALSE;                      /* If yes then set the flag "device disabled" */
  HWEnDi();                            /* Enable the device */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  AD1_Measure (bean ADC)
**
**     Description :
**         This method performs one measurement on all channels that
**         are set in the bean inspector. (Note: If the <number of
**         conversions> is more than one the conversion of A/D channels
**         is performed specified number of times.)
**     Parameters  :
**         NAME            - DESCRIPTION
**         WaitForResult   - Wait for a result of a
**                           conversion. If <interrupt service> is
**                           disabled, A/D peripheral doesn't support
**                           measuring all channels at once or Autoscan
**                           mode property isn't enabled and at the same
**                           time the <number of channel> is greater
**                           than 1, then the WaitForResult parameter is
**                           ignored and the method waits for each
**                           result every time. If the <interrupt
**                           service> is disabled and a <number of
**                           conversions> is greater than 1, the
**                           parameter is ignored and the method also
**                           waits for each result every time.
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_DISABLED - Device is disabled
**                           ERR_BUSY - A conversion is already running
** ===================================================================
*/
byte AD1_Measure(bool WaitForResult)
{
  if (!EnUser) {                       /* Is the device disabled by user? */
    return ERR_DISABLED;               /* If yes then error */
  }
  if (ModeFlg != STOP) {               /* Is the device in different mode than "stop"? */
    return ERR_BUSY;                   /* If yes then error */
  }
  ModeFlg = MEASURE;                   /* Set state of device to the measure mode */
  HWEnDi();                            /* Enable the device */
  if (WaitForResult) {                 /* Is WaitForResult TRUE? */
    while (ModeFlg == MEASURE) {}      /* If yes then wait for end of measurement */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  AD1_GetValue16 (bean ADC)
**
**     Description :
**         This method returns the last measured values of all channels
**         justified to the left. Compared with <GetValue> method this
**         method returns more accurate result if the <number of
**         conversions> is greater than 1 and <AD resolution> is less
**         than 16 bits. In addition, the user code dependency on <AD
**         resolution> is eliminated.
**     Parameters  :
**         NAME            - DESCRIPTION
**       * Values          - Pointer to the array that contains
**                           the measured data.
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_NOTAVAIL - Requested value not
**                           available
**                           ERR_OVERRUN - External trigger overrun flag
**                           was detected after the last value(s) was
**                           obtained (for example by GetValue). This
**                           error may not be supported on some CPUs
**                           (see generated code).
** ===================================================================
*/
byte AD1_GetValue16(word *Values)
{
  if (!OutFlg) {                       /* Is measured value(s) available? */
    return ERR_NOTAVAIL;               /* If no then error */
  }
  /* Note: Next 9 lines are speed optimized */
  *Values++ = AD1_OutV[0] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[1] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[2] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[3] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[4] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[5] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[6] << 6;        /* Save measured values to the output buffer */
  *Values++ = AD1_OutV[7] << 6;        /* Save measured values to the output buffer */
  *Values = AD1_OutV[8] << 6;          /* Save measured values to the output buffer */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  AD1_Init (bean ADC)
**
**     Description :
**         Initializes the associated peripheral(s) and the bean's 
**         internal variables. The method is called automatically as a 
**         part of the application initialization code.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
void AD1_Init(void)
{
  EnUser = TRUE;                       /* Enable device */
  OutFlg = FALSE;                      /* No measured value */
  ModeFlg = STOP;                      /* Device isn't running */
  /* ATDCTL4: SMP2=1,SMP1=1,SMP0=1,PRS4=0,PRS3=0,PRS2=0,PRS1=0,PRS0=0 */
  ATDCTL4 = 224;                       /* Set sample time and prescaler */
  /* ATDCTL3: DJM=1,S8C=0,S4C=0,S2C=0,S1C=1,FIFO=0,FRZ1=0,FRZ0=0 */
  ATDCTL3 = 136;                       /* Set ATD control register 3 */
  /* ATDCTL0: ??=0,??=0,??=0,??=0,WRAP3=1,WRAP2=1,WRAP1=1,WRAP0=1 */
  ATDCTL0 = 15;                        /* Set wrap around */
  /* ATDCTL1: ETRIGSEL=0,SRES1=0,SRES0=1,SMP_DIS=0,ETRIGCH3=1,ETRIGCH2=1,ETRIGCH1=1,ETRIGCH0=1 */
  ATDCTL1 = 47;                        /* Set resolution and discharge */
  /* ATDCTL2: ??=0,AFFC=1,ICLKSTP=0,ETRIGLE=0,ETRIGP=0,ETRIGE=0,ASCIE=1,ACMPIE=0 */
  ATDCTL2 = 66;                        /* Set ATD control register 2 */
}


/* END AD1. */

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
