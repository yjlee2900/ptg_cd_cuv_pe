/** ###################################################################
**     Filename  : Events.H
**     Project   : QL_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : Events
**     Version   : Driver 01.04
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2014-06-13, ���� 3:11
**     Abstract  :
**         This is user's event module.
**         Put your event handler code here.
**     Settings  :
**     Contents  :
**         No public methods
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef __Events_H
#define __Events_H
/* MODULE Events */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PE_Timer.h"
#include "AS1.h"
#include "AD1.h"
#include "Latch_Lock_SW.h"
#include "Latch_Primary_SW.h"
#include "Latch_Secondary_SW.h"
#include "PCU_HomePos_SW.h"
#include "TG_Consol_SW.h"
#include "TG_Inner_SW.h"
#include "N_GEAR_Position.h"
#include "HallLH_CH1.h"
#include "HallLH_CH2.h"
#include "HallRH_CH1.h"
#include "HallRH_CH2.h"
#include "AntiPinchPowerOut.h"
#include "Illumination_Ctl_Out.h"
#include "VNQ500_CE_Out.h"
#include "Hall_Power_LH_Out.h"
#include "Hall_Power_RH_Out.h"
#include "Watchdog_Out.h"
#include "SPD_LH_EN_A.h"
#include "SPD_LH_EN_B.h"
#include "SPD_RH_EN_A.h"
#include "SPD_RH_EN_B.h"
#include "PCU_EN_A.h"
#include "PCU_EN_B.h"
#include "SPD_LH_IN_B.h"
#include "SPD_LH_IN_A.h"
#include "SPD_RH_IN_B.h"
#include "SPD_RH_IN_A.h"
#include "PCU_IN_B.h"
#include "PCU_IN_A.h"
#include "SPD_LH_PWM.h"
#include "SPD_RH_PWM.h"
#include "PCU_PWM.h"
#include "MSCAN1.h"
#include "CAN_EN.h"
#include "CAN_EN.h"
#include "CAN_NSTB.h"
#include "CanNError_In.h"
#include "Timer100us.h"
#include "Timer1ms.h"
#include "IEE1.h"
#include "Internal_WatchDog.h"
#include "BuzzerPWM.h"
#include "PullUp_Control.h"
#include "RTI1.h"
#include "SPD_LH_Multisense_EN.h"
#include "SPD_RH_Multisense_EN.h"
#include "PCU_Multisense_EN.h"
#include "Buzzer_FeedBackIn.h"

#pragma CODE_SEG DEFAULT


void Timer1ms_OnInterrupt(void);
/*
** ===================================================================
**     Event       :  Timer1ms_OnInterrupt (module Events)
**
**     From bean   :  Timer1ms [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the bean is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void Timer100us_OnInterrupt(void);
/*
** ===================================================================
**     Event       :  Timer100us_OnInterrupt (module Events)
**
**     From bean   :  Timer100us [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the bean is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void AD1_OnEnd(void);
/*
** ===================================================================
**     Event       :  AD1_OnEnd (module Events)
**
**     From bean   :  AD1 [ADC]
**     Description :
**         This event is called after the measurement (which consists
**         of <1 or more conversions>) is/are finished.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/* END Events */
#endif /* __Events_H*/

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
