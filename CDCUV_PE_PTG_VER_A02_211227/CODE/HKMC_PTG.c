/** ###################################################################
**     Filename  : HKMC_PTG.C
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Version   : Driver 01.13
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2015-03-16, ���� 11:29
**     Abstract  :
**         Main module.
**         This module contains user's application code.
**     Settings  :
**     Contents  :
**         No public methods
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/
/* MODULE HKMC_PTG */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "AS1.h"
#include "AD1.h"
#include "Latch_Lock_SW.h"
#include "Latch_Primary_SW.h"
#include "Latch_Secondary_SW.h"
#include "PCU_HomePos_SW.h"
#include "TG_Consol_SW.h"
#include "TG_Inner_SW.h"
#include "N_GEAR_Position.h"
#include "HallLH_CH1.h"
#include "HallLH_CH2.h"
#include "HallRH_CH1.h"
#include "HallRH_CH2.h"
#include "AntiPinchPowerOut.h"
#include "Illumination_Ctl_Out.h"
#include "VNQ500_CE_Out.h"
#include "Hall_Power_LH_Out.h"
#include "Hall_Power_RH_Out.h"
#include "Watchdog_Out.h"
#include "SPD_LH_EN_A.h"
#include "SPD_LH_EN_B.h"
#include "SPD_RH_EN_A.h"
#include "SPD_RH_EN_B.h"
#include "PCU_EN_A.h"
#include "PCU_EN_B.h"
#include "SPD_LH_IN_B.h"
#include "SPD_LH_IN_A.h"
#include "SPD_RH_IN_B.h"
#include "SPD_RH_IN_A.h"
#include "PCU_IN_B.h"
#include "PCU_IN_A.h"
#include "SPD_LH_PWM.h"
#include "SPD_RH_PWM.h"
#include "PCU_PWM.h"
#include "MSCAN1.h"
#include "CAN_EN.h"
#include "CAN_EN.h"
#include "CAN_NSTB.h"
#include "CanNError_In.h"
#include "Timer100us.h"
#include "Timer1ms.h"
#include "IEE1.h"
#include "Internal_WatchDog.h"
#include "BuzzerPWM.h"
#include "PullUp_Control.h"
#include "RTI1.h"
#include "SPD_LH_Multisense_EN.h"
#include "SPD_RH_Multisense_EN.h"
#include "PCU_Multisense_EN.h"
#include "Buzzer_FeedBackIn.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

/* Application Code include */
#include "Task_Scheduling.h"
#include "EEPROM_Control.h"
#include "CAN_Drive_App.h"
#include "Parameterization.h"
#include "Status_Detect.h"
#include "Digital_Input_Chattering.h"
#include "Analog_conversion.h"
#include "Obstacle_Detect.h"
#include "Thermal_Protect.h"
#include "PCU_Sequence.h"
#include "PID_Control.h"
#include "Spindle_Sync_Control.h"
#include "Control_Command.h"
#include "User_Logic_Control.h"
#include "Sleep_Wakeup_Control.h"
#include "MCU_Fail_Check.h"


static void s_Value_Init(void);

/********************************************************************************/
/*   name     : s_Value_Init                                                    */
/*   function : function vaile intit                                            */
/*                                                                              */
/********************************************************************************/
static void s_Value_Init(void)
{
	  g_DrvCoreInit();
    g_Param_Init();
    g_StatusDetect_Init();
    g_Switch_value_Init();
    g_ADC_value_Init();
    g_Obstacle_Value_Init();
    g_PCU_Sequenc_Init();
    g_PID_Tartget_Position_Init();
    g_PID_Value_Init();
    g_Spindle_Sync_Init();
    g_Control_Command_Init();
    g_User_Logic_Init();
    g_Thermal_Status_Init();
}


void main(void)
{
  /* Write your local variable definition here */
  Cpu_DisableInt()
  /* READ RESET FLAG */
  CPMUFLG = 0x44; 
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  IVBR = 0x7F; 
  _DISABLE_COP();
  
  g_TriggerWDT();
  
  g_check_battAttach();
  g_BCAN_Init();
  g_TASK_Init();
  g_Read_All_EEP();
  s_Value_Init();
  g_Sleep_Wakeup_Drive(WAKE_UP_MODE);
  g_DrvCoreRegCheck();
  
  Cpu_EnableInt()
    
  /* Test Mode CAN Transiver Enable for Power On */
  /*s_CAN_Transiver_Control(TRS_INIT);*/
  /***********************************************/
  
  while(1) 
  {
      g_Task_Scheduling();  
  }       
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END HKMC_PTG */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/

/* END HKMC_PTG */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
