/** ###################################################################
**     Filename  : Events.C
**     Project   : QL_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : Events
**     Version   : Driver 01.04
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2014-06-13, ���� 3:11
**     Abstract  :
**         This is user's event module.
**         Put your event handler code here.
**     Settings  :
**     Contents  :
**         No public methods
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/
/* MODULE Events */


#include "Cpu.h"
#include "Events.h"
#include "Read_Hall_Sensor.h"  /* app program */
#include "Task_Scheduling.h"

#pragma CODE_SEG DEFAULT

/*
** ===================================================================
**     Event       :  Timer1ms_OnInterrupt (module Events)
**
**     From bean   :  Timer1ms [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the bean is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void Timer1ms_OnInterrupt(void)
{
  /* Write your code here ... */
  g_Task_Timer_Count_1msEvent();   /* Task timer counting  */  
}


/*
** ===================================================================
**     Event       :  Timer100us_OnInterrupt (module Events)
**
**     From bean   :  Timer100us [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the bean is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void Timer100us_OnInterrupt(void)
{
  /* Write your code here ... */
  g_Encoder_Scan();  /* polling hall sensor */
}


/*
** ===================================================================
**     Event       :  AD1_OnEnd (module Events)
**
**     From bean   :  AD1 [ADC]
**     Description :
**         This event is called after the measurement (which consists
**         of <1 or more conversions>) is/are finished.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void AD1_OnEnd(void)
{
  /* Write your code here ... */
}

#pragma CODE_SEG __NEAR_SEG NON_BANKED
/*
** ===================================================================
**     Interrupt handler : RTI_ISR
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
ISR(RTI_ISR)
{
  /* Write your interrupt code here ... */
  CPMUFLG = 128;
}
#pragma CODE_SEG DEFAULT



/* END Events */

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
