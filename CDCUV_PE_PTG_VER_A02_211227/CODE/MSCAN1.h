/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : MSCAN1.H
**     Project   : HKMC_PTG
**     Processor : MC9S12P64MQK
**     Beantype  : Init_MSCAN
**     Version   : Bean 01.063, Driver 01.09, CPU db: 3.00.002
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 2018-12-17, ���� 9:23
**     Abstract  :
**          This file implements the MSCAN (MSCAN) module initialization
**          according to the Peripheral Initialization Bean settings,
**          and defines interrupt service routines prototypes.
**          The Motorola Scalable Controller Area Network (MSCAN) definition
**          is based on the MSCAN12 definition which is the specific
**          implementation of the Motorola Scalable CAN concept targeted
**          for the Freescale MC68HC12 Microcontroller Family.
**
**          The basic features of the MSCAN are as follows:
**          - Implementation of the CAN protocol - Version 2.0A/B
**          � Standard and extended data frames
**          � 0 - 8 bytes data length
**          � Programmable bit rate up to 1 Mbps1
**          � Support for remote frames
**          � 5 receive buffers with FIFO storage scheme
**          - 3 transmit buffers with internal prioritization using a �local
**          priority concept
**          - Flexible maskable identifier filter supports two full size
**          extended identifier filters (two 32-bit) or four 16-bit filters
**          or eight 8-bit filters
**          - Programmable wake-up functionality with integrated low-pass
**          filter
**          - Programmable loop back mode supports self-test operation
**          - Programmable listen-only mode for monitoring of CAN bus
**          - Separate signalling and interrupt capabilities for all CAN
**          receiver and transmitter error states
**          (Warning, Error Passive, Bus-Off)
**          - Programmable MSCAN clock source either Bus Clock or Oscillator
**          Clock
**          - Internal timer for time-stamping of received and transmitted
**          messages
**          - Three low power modes: Sleep, Power Down and MSCAN Enable
**          - Global initialization of configuration registers
**     Settings  :
**          Bean name                                      : MSCAN1
**          Device                                         : MSCAN
**          Clock Source                                   : Oscillator Clock
**          Baud Rate Prescaler                            : 1
**          Synchr. Jump Width                             : 1
**          Sampling                                       : One sample per bit
**          Time Segment 1                                 : 1
**          Time Segment 2                                 : 1
**          CAN frequency                                  : 16 MHz
**          Time quantum                                   : 62.5 ns
**          Bit rate                                       : 5333.33 kbit/s
**          CAN Stops in Wait Mode                         : no
**          Wake-Up Mode                                   : None
**          Loop Back Test Mode                            : Disabled
**          Listen Only Mode                               : Activated
**          Sleep Mode Request                             : Disabled
**          Time Stamp                                     : Disabled
**          Acceptance mode                                : Two 32 bit Acceptance Filters
**          Rx acceptance ID(1st bank)                     : 0
**          Rx acceptance ID(2nd bank)                     : 0
**          Rx acceptance ID mask (1st bank)               : 0
**          Rx acceptance ID mask (2nd bank)               : 0
**          RXCAN pin                                      : PM0_RXCAN
**          RXCAN pin signal                               : 
**          TXCAN pin                                      : PM1_TXCAN
**          TXCAN pin signal                               : 
**          Wake up                                        : 
**          Wake up                                        : Disabled
**          Interrupt                                      : Vcanwkup
**          Wake interrupt priority                        : 1
**          ISR name                                       : CanWakeUpInterrupt_0
**          Error                                          : 
**          Error Interrupt                                : Vcanerr
**          Status Change Interrupt                        : Disabled
**          Receiver Status Change                         : do not generate
**          Transmitt. Status Change                       : do not generate
**          Overrun Interrupt                              : Disabled
**          Error interrupt priority                       : 1
**          ISR name                                       : CanErrorInterrupt_0
**          Receiver Full                                  : 
**          Receiver Full                                  : Disabled
**          Receiver Interrupt                             : Vcanrx
**          Rx interrupt priority                          : 1
**          ISR name                                       : CanRxInterrupt_0
**          Transmitter empty                              : 
**          Transmitter Interrupt                          : Vcantx
**          Tx Empty Interrupt 0                           : Disabled
**          Tx Empty Interrupt 1                           : Disabled
**          Tx Empty Interrupt 2                           : Disabled
**          Tx interrupt priority                          : 1
**          ISR name                                       : CanTxInterrupt_0
**          Call Init in CPU init. code                    : no
**          CAN Enable                                     : yes
**     Contents  :
**         Init - void MSCAN1_Init(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef __MSCAN1
#define __MSCAN1

/* MODULE MSCAN1. */

/*Include shared modules, which are used for whole project*/
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
/* Include inherited beans */

#include "Cpu.h"




void MSCAN1_Init(void);
/*
** ===================================================================
**     Method      :  MSCAN1_Init (bean Init_MSCAN)
**
**     Description :
**         This method initializes registers of the CAN module
**         according to this Peripheral Initialization Bean settings.
**         Call this method in user code to initialize the module.
**         By default, the method is called by PE automatically; see
**         "Call Init method" property of the bean for more details.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

#pragma CODE_SEG __NEAR_SEG NON_BANKED
/*
** ===================================================================
** The interrupt service routine must be implemented by user in one
** of the user modules (see MSCAN1.c file for more information).
** ===================================================================
*/
__interrupt void CanWakeUpInterrupt_0(void);
__interrupt void CanRxInterrupt_0(void);
__interrupt void CanErrorInterrupt_0(void);
__interrupt void CanTxInterrupt_0(void);
#pragma CODE_SEG DEFAULT

/* END MSCAN1. */

#endif /* ifndef __MSCAN1 */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.99 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
