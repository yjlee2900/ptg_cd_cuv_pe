/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-09 PM.  5:51:12                                   */
/*  Last update     : 2013-05-09 PM.  5:51:15                                   */
/*  File Name       : Digital_Input_Chattering.c                                */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define DIGITAL_INPUT_CHATTERING_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Digital_Input_Chattering.h"
#include "Latch_Lock_SW.h"
#include "Latch_Primary_SW.h"
#include "Latch_Secondary_SW.h"
#include "PCU_HomePos_SW.h"
#include "TG_Consol_SW.h"
#include "TG_Inner_SW.h"
#include "N_GEAR_Position.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
/* static U08 ; */

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_ioChattering_Check(stSwInput* stSW, U08 u08_t_base_Chatt );
static void s_io_ChattCount_5ms(void);
static void s_io_ChattCount_1ms(void);
static void s_CheckDigitalInputPeriod5ms(void);
static void s_CheckDigitalInputPeriod1ms(void);
static void s_io_FaultCheckPeriod5ms(void);
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/

/*#define u08_s_PORT_INPUT_LOW            FALSE */    /* switch input low of digital port  */
/*#define u08_s_PORT_INPUT_HIGH           TRUE  */   /* switch input high of digital port */

/*#define u08_s_TIME_CHATTERING_10ms_1T       10*/  /* 1ms  x 10 = 10ms */
#define u08_s_TIME_CHATTERING_20ms_1T       20  /* 1ms  x 20 = 20ms */
/*#define u08_s_TIME_CHATTERING_30ms_5T        6*/  /* 5ms  x 6  = 30ms */
#define u08_s_TIME_CHATTERING_50ms_5T       10  /* 5ms  x 10 = 50ms */
#define u08_s_TIME_CHATTERING_150ms_5T     30  /* 5ms  x 30 = 150ms */

/********************************************************************************/
/*   name     : g_ioChattering_Check                                            */
/*   function : io switch chattering detect                                     */
/*                                                                              */
/********************************************************************************/
static void s_ioChattering_Check(stSwInput* stSW, U08 u08_t_base_Chatt )
{
    if (stSW->swRead == enON)
    {
        if (stSW->swPrev == enOFF)
        {
            /* System wake up chattering is different */
            stSW->nCount = u08_t_base_Chatt;
        }
        stSW->swPrev = enON;
        if (stSW->nCount == 0)
        {
            if (stSW->swInput == enOFF)
            {
                stSW->swEvent = enON;         
            }
            stSW->swInput = enON; /* value is changed */
        }
    }
    else
    {
        /* switch is released */
        if (stSW->swPrev == enON)
        {
            /* System wake up chattering is different */
            stSW->nCount = u08_t_base_Chatt;
        }
        stSW->swPrev = enOFF;
        if (stSW->nCount == 0)
        {
            if (stSW->swInput == enON)        
            {   
                stSW->swEvent = enON;      
            }
            stSW->swInput = enOFF;   /* value is changed */
        }
    }
}
/********************************************************************************/
/*   name     : g_Switch_value_Init                                             */
/*   function : io switch value init                                            */
/*                                                                              */
/********************************************************************************/
void g_Switch_value_Init(void)
{
    /* st_g_swinput.swCPadTailgate.swRead  = enOFF; */
    st_g_swinput.swCPadTailgate.nCount  = 0;
    st_g_swinput.swCPadTailgate.swInput = enOFF;
    st_g_swinput.swCPadTailgate.swPrev  = enOFF;
    st_g_swinput.swCPadTailgate.swEvent = enOFF;
    
    /* st_g_swinput.swInnerOpenClose.swRead  = enOFF; */
    st_g_swinput.swInnerOpenClose.nCount  = 0;
    st_g_swinput.swInnerOpenClose.swInput = enOFF;
    st_g_swinput.swInnerOpenClose.swPrev  = enOFF;
    st_g_swinput.swInnerOpenClose.swEvent = enOFF;    
    
    /* st_g_swinput.inLatchPrimary.swRead  = enOFF; */
    st_g_swinput.inLatchPrimary.nCount  = 0;
    st_g_swinput.inLatchPrimary.swInput = enOFF;
    st_g_swinput.inLatchPrimary.swPrev  = enOFF;
    st_g_swinput.inLatchPrimary.swEvent = enOFF;   
    
    /* st_g_swinput.inLatchSecondary.swRead  = enOFF; */
    st_g_swinput.inLatchSecondary.nCount  = 0;
    st_g_swinput.inLatchSecondary.swInput = enOFF;
    st_g_swinput.inLatchSecondary.swPrev  = enOFF;
    st_g_swinput.inLatchSecondary.swEvent = enOFF;   
    
    /* st_g_swinput.inLatchLock.swRead  = enOFF; */
    st_g_swinput.inLatchLock.nCount  = 0;
    st_g_swinput.inLatchLock.swInput = enOFF;
    st_g_swinput.inLatchLock.swPrev  = enOFF;
    st_g_swinput.inLatchLock.swEvent = enOFF;    
    
    /* st_g_swinput.inLatchLock.swRead  = enOFF; */
    st_g_swinput.inHomePosition.nCount  = 0;
    st_g_swinput.inHomePosition.swInput = enOFF;
    st_g_swinput.inHomePosition.swPrev  = enOFF;
    st_g_swinput.inHomePosition.swEvent = enOFF;   
     
    /* st_g_swinput.swGearN_Position.swRead  = enOFF; */
    st_g_swinput.swGearN_Position.nCount  = 0;
    st_g_swinput.swGearN_Position.swInput = enOFF;
    st_g_swinput.swGearN_Position.swPrev  = enOFF;
    st_g_swinput.swGearN_Position.swEvent = enOFF;     
     
}

/********************************************************************************/
/*   name     : s_io_ChattCount_1ms                                           */
/*   function : io chattering down count 1ms period                             */
/*                                                                              */
/********************************************************************************/
static void s_io_ChattCount_1ms(void)
{
    if( st_g_swinput.inLatchPrimary.nCount > 0 )
    {
        st_g_swinput.inLatchPrimary.nCount--;
    }
    if( st_g_swinput.inLatchSecondary.nCount > 0 )
    {
        st_g_swinput.inLatchSecondary.nCount--;
    }
    if( st_g_swinput.inLatchLock.nCount > 0 )
    {    
      st_g_swinput.inLatchLock.nCount--;
    }
    if( st_g_swinput.inHomePosition.nCount > 0 )
    {
        st_g_swinput.inHomePosition.nCount--;
    }
}

/********************************************************************************/
/*   name     : s_io_ChattCount_5ms                                           */
/*   function : io chattering down count 5ms period                             */
/*                                                                              */
/********************************************************************************/
static void s_io_ChattCount_5ms(void)
{
    if( st_g_swinput.swCPadTailgate.nCount > 0 )
    {
        st_g_swinput.swCPadTailgate.nCount--;
    }
    if( st_g_swinput.swInnerOpenClose.nCount > 0 )
    {
        st_g_swinput.swInnerOpenClose.nCount--;
    }
    if( st_g_swinput.swGearN_Position.nCount > 0 )
    {
        st_g_swinput.swGearN_Position.nCount--;
    }    
}


/********************************************************************************/
/*   name     : s_CheckDigitalInputPeriod1ms                                    */
/*   function : 5ms task io chattering check                                    */
/*                                                                              */
/********************************************************************************/
static void s_CheckDigitalInputPeriod1ms(void)
{
    U08 Read_Buff;
    /* Latch Primary Switch chattering  */
    Read_Buff = Latch_Primary_SW_GetVal();
    if( Read_Buff == 0 )
    {
        st_g_swinput.inLatchPrimary.swRead = enON;
    }
    else
    {
        st_g_swinput.inLatchPrimary.swRead = enOFF; 
    }        
    s_ioChattering_Check(&(st_g_swinput.inLatchPrimary), u08_s_TIME_CHATTERING_20ms_1T );
    
    /* Latch Secondary Switch chattering  */
    Read_Buff = Latch_Secondary_SW_GetVal();
    if( Read_Buff == 0 )
    {
        st_g_swinput.inLatchSecondary.swRead = enON;
    }
    else
    {
        st_g_swinput.inLatchSecondary.swRead = enOFF;
    }    
    s_ioChattering_Check(&(st_g_swinput.inLatchSecondary), u08_s_TIME_CHATTERING_20ms_1T );
    
    /* Latch Lock Switch chattering  */
    Read_Buff = Latch_Lock_SW_GetVal();
    if( Read_Buff == 0 )
    {
        st_g_swinput.inLatchLock.swRead = enON;
    }
    else
    {
        st_g_swinput.inLatchLock.swRead = enOFF;
    }    
    s_ioChattering_Check(&(st_g_swinput.inLatchLock), u08_s_TIME_CHATTERING_20ms_1T );
    
    /* Latch HomePosition Switch chattering */
    Read_Buff = PCU_HomePos_SW_GetVal();
    if( Read_Buff != 0 )
    {
        st_g_swinput.inHomePosition.swRead = enON;
    }
    else
    {
        st_g_swinput.inHomePosition.swRead = enOFF;
    }    
    s_ioChattering_Check(&(st_g_swinput.inHomePosition), u08_s_TIME_CHATTERING_20ms_1T );
}

/********************************************************************************/
/*   name     : g_CheckDigitalInputPeriod5ms                                    */
/*   function : 5ms task io chattering check                                    */
/*                                                                              */
/********************************************************************************/
static void s_CheckDigitalInputPeriod5ms(void)
{
    U08 Read_Buff2;
    
    /* Latch Primary Switch chattering  */
    Read_Buff2 = TG_Consol_SW_GetVal();
    if( Read_Buff2 != 0 )
    {
        st_g_swinput.swCPadTailgate.swRead = enON;
    }
    else
    {
        st_g_swinput.swCPadTailgate.swRead = enOFF;
    }        
    s_ioChattering_Check(&(st_g_swinput.swCPadTailgate), u08_s_TIME_CHATTERING_150ms_5T );
    
    /* Latch Secondary Switch chattering  */
    Read_Buff2 = TG_Inner_SW_GetVal();
    if( Read_Buff2 != 0 )
    {
        st_g_swinput.swInnerOpenClose.swRead = enON;
    }
    else
    {
        st_g_swinput.swInnerOpenClose.swRead = enOFF;
    }    
    s_ioChattering_Check(&(st_g_swinput.swInnerOpenClose), u08_s_TIME_CHATTERING_50ms_5T );


    /* Gear 'N' Position Switch chattering  */
    Read_Buff2 = N_GEAR_Position_GetVal();
    /*if( Read_Buff2 != 0 )*/
    if( Read_Buff2 == 0 ) /* CD PTG Manual Gear Position Revese */
    {
        st_g_swinput.swGearN_Position.swRead = enON;
    }
    else
    {
        st_g_swinput.swGearN_Position.swRead = enOFF;
    }    
    s_ioChattering_Check(&(st_g_swinput.swGearN_Position), u08_s_TIME_CHATTERING_50ms_5T );
        
    /* for parameterization Reset */
    if( st_g_swinput.swInnerOpenClose.swInput == ON )
    {
        if( u16_g_InnerSW_On_t < 2000 )
        {
            u16_g_InnerSW_On_t++;
        }    
    } 
}

/********************************************************************************/
/*   name     : s_io_FaultCheckPeriod5ms                                        */
/*   function : Input Fault check                                               */
/*                                                                              */
/********************************************************************************/
static void s_io_FaultCheckPeriod5ms( void )
{
	  static U08 u08_t_IOCheckCnt = 0;
	  static U08 u08_t_IOHealCnt = 0;
	  
    if( (st_g_swinput.swCPadTailgate.swRead == enON) || (st_g_swinput.swInnerOpenClose.swRead == enON) )
    {
        if(u08_t_IOCheckCnt < 4)
        {
            u08_t_IOCheckCnt++;
            if( (st_g_swinput.swCPadTailgate.swRead == enON) && (st_g_swinput.swInnerOpenClose.swRead == enON) )
            {
                u08_g_IO_InputFault = TRUE;	
                u08_t_IOHealCnt = 0;
            }	
        }
    }
    else
    {
        if(u08_g_IO_InputFault == TRUE)
        {
            if(u08_t_IOHealCnt < 10)
            {
                u08_t_IOHealCnt++;
            }
            else
            {
                u08_t_IOCheckCnt = 0U;
			          u08_g_IO_InputFault = FALSE;
            }
        }
        else
        {
            u08_t_IOCheckCnt = 0U;
        }
    }	
}
/********************************************************************************/
/*   name     : g_IO_InputHandlerPeriod1ms                                      */
/*   function : 1ms task io input handler                                       */
/*                                                                              */
/********************************************************************************/
void g_IO_InputHandlerPeriod1ms( void )
{
	  s_io_ChattCount_1ms();
	  s_CheckDigitalInputPeriod1ms();
}

/********************************************************************************/
/*   name     : g_IO_InputHandlerPeriod5ms                                      */
/*   function : 5ms task io input handler                                       */
/*                                                                              */
/********************************************************************************/
void g_IO_InputHandlerPeriod5ms( void )
{
	  s_io_ChattCount_5ms();
    s_CheckDigitalInputPeriod5ms();
    s_io_FaultCheckPeriod5ms();
}
