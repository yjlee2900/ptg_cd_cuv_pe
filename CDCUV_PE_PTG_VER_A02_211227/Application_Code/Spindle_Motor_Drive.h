/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-30 PM.  6:31:47                                   */
/*	Last update     : 2013-04-30 PM.  6:31:51                                   */
/*	File Name       : LID_Motor_Drive.c                                         */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		LID_MOTOR_DRIVE_H
#define		LID_MOTOR_DRIVE_H

#include "Basic_Type_Define.h"

#ifndef  	LID_MOTOR_DRIVE_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define STOP_TAILGATE    0U
#define CLOSE_TAILGATE   1U
#define OPEN_TAILGATE    2U
#define BREAK_TAILGATE   3U
#define INIT_TAILGATE    4U

#define u08_FULL_BRIT_OFF      0U
#define u08_FULL_BRIT_ON       1U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_SpindlePowerMov_Direction; 
EXTERN U08  u08_g_LH_Ratio;
EXTERN U08  u08_g_RH_Ratio;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_Spind_MTDrive_Out( U08 u08_MotorOut );
EXTERN	void g_Spindle_MT_OutPWM(/*U08 u08_RH_Ratio,*/ U08 u08_LH_Ratio );

#undef EXTERN

#endif
