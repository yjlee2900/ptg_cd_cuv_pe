/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-23 AM.  9:25:42                                   */
/*  Last update     : 2013-05-23 AM.  9:25:45                                   */
/*  File Name       : Sensor_Power_Control.c                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define SENSOR_POWER_CONTROL_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Sensor_Power_Control.h"
#include "Analog_conversion.h"
#include "Status_Detect.h"
#include "Hall_Power_LH_Out.h"
#include "Hall_Power_RH_Out.h"
#include "AntiPinchPowerOut.h"
#include "Sleep_Wakeup_Control.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U08 u08_s_Hall_PowerSt_LH = ON;
static U08 u08_s_Hall_PowerSt_RH = ON;
static U08 u08_s_HallPwrOffCheckDly_LH = 0;
static U08 u08_s_HallPwrOffCheckDly_RH = 0;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void Hall_Power_out_control_LH( void );
/*static void Hall_Power_out_control_RH( void );*/
static void s_HallPowerOnFaultDetection_LH( void );
/* static void s_HallPowerOnFaultDetection_RH( void );*/
static void s_HallPowerOffFaultDetection_LH( void );
/* static void s_HallPowerOffFaultDetection_RH( void );*/
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define RETRY_ON_DELAY   50  /* 10ms * 50 = 500ms */
#define RETRY_OFF_DELAY  20
const U08 u08_t_HallVoltCmpnstnTable[21] = {
    /* Battery Volt                   0     1     2     3     4     5     6     7     8     9     */
                                     0U,   0U,   0U,   0U,   0U,   0U,  39U,  45U,  51U,  57U,
    /* Battery Volt                   10    11    12    13    14    15    16    17    18    19     */
                                     64U,  70U,  74U,  76U,  76U,  71U,  63U,  51U,  36U,   0U,
    /* Battery Volt                   20                                                  */
                                      0U };
                                      
/********************************************************************************/
/*   name     : Hall_Power_out_control_LH                                       */
/*   function : Hall sensor LH Out put Control                                  */
/********************************************************************************/
static void Hall_Power_out_control_LH(void)
{                                        
    static U08 u08_s_Retry_DelayTime_LH = 0;
    static U08 u08_s_Hall_PowerRetry_Cnt_LH = 0; 
    
    switch( u08_s_Hall_PowerSt_LH )
    {
        case ON :
            if( (u08_g_HallPowerOnErr_LH == 1) || (u08_g_HallPowerOffErr_LH == 1) ) 
            {
            	  
            	  if( u08_s_Retry_DelayTime_LH < RETRY_ON_DELAY )
                {
                    u08_s_Retry_DelayTime_LH++;
                }
                else
                {	
                    if( u08_g_clacBatteryVolt > 6 )
                    {
                        g_Hall_Power_Out_LH( OFF );
                    } 
                    u08_s_Retry_DelayTime_LH = 0;
                }    
            }
            else
            {
            	  if( u08_g_HallPwrOffCheckCmd_LH == 1 )
            	  {
            	  	  u08_g_HallPwrOffCheckCmd_LH = 0;
            	  	  if(u08_g_clacBatteryVolt >= 18)
            	  	  {
            	  	      g_Hall_Power_Out_LH(OVER_VOLTAGE_OFF);	
            	  	  }
            	  	  else
            	  	  {
            	  	      g_Hall_Power_Out_LH( OFF );	
            	  	  }		 
            	  }	
            	  u08_s_Retry_DelayTime_LH = 0;
            	  u08_s_Hall_PowerRetry_Cnt_LH = 0;
            }    
            break;
        case OFF :
                    	
        	  if( (u08_g_HallPowerOnErr_LH == 1) || (u08_g_HallPowerOffErr_LH == 1)  )
        	  {	
                if( u08_s_Hall_PowerRetry_Cnt_LH < 3 )
                {
                    if( u08_s_Retry_DelayTime_LH < RETRY_OFF_DELAY )
                    {
                        u08_s_Retry_DelayTime_LH++;
                    }
                    else
                    {             
                        g_Hall_Power_Out_LH( ON );
                        u08_s_Hall_PowerRetry_Cnt_LH++;
                        if( u08_g_HallPowerOnErr_LH == 1 )
                        {	
                           u08_s_Retry_DelayTime_LH = 0; /* ON to Off Delay 500ms*/
                        }
                        else
                        {
                        	 u08_s_Retry_DelayTime_LH = 45; /* 450ms set / ON to Off Delay 50ms*/
                        }	   
                    }        
                }
                else
                {
                    g_Hall_Power_Out_LH(NO_RETRY_OFF);
                    u08_s_Retry_DelayTime_LH = 0;
                }
            }
            else
            {
            	  if( (u08_s_HallPwrOffCheckDly_LH == 2) || (u08_g_LatchSt != LATCH_LOCK) )
            	  {	
                    g_Hall_Power_Out_LH( ON );
                    u08_s_Hall_PowerRetry_Cnt_LH = 0;
                }
            }	
            break;
        case OVER_VOLTAGE_OFF :
            if( u08_g_LatchSt != LATCH_LOCK )
            {
                g_Hall_Power_Out_LH( ON );
            }     
            break;
        default :    
            g_Hall_Power_Out_LH( NO_RETRY_OFF );
            break;        
    }
}

/********************************************************************************/
/*   name     : Hall_Power_out_control_RH                                       */
/*   function : Hall sensor RH Out put Control                                  */
/********************************************************************************/
#if 0
static void Hall_Power_out_control_RH(void)
{                                     

}
#endif


/********************************************************************************/
/*   name     : s_Hall_Power_Fault_Detection_LH                                 */
/*   function : Hall Power Fault Detection LH                                   */
/********************************************************************************/
static void s_HallPowerOnFaultDetection_LH( void )
{
	  static U08 u08_s_HallPwrCmpDlyCnt_LH = 0;
	   
	  S16 s16_t_ErrValue_LH_Hall = 0;  
    S16 s16_t_mDtctCmpVal_LH_Hall = 0;


    if( (st_g_ADCData.HallPower_LH.Valid == ON) && (u08_g_HallPowerOffErr_LH == 0) && 
    	  (u08_s_Hall_PowerSt_LH == ON) && (u08_g_ECU_Power_Mode == ECU_NORAM_MODE) && (u08_g_LatchSt == LATCH_OPEN) )
    {
        s16_t_mDtctCmpVal_LH_Hall = st_g_ADCData.BattMon.Value - (st_g_ADCData.HallPower_LH.Value + u08_t_HallVoltCmpnstnTable[u08_g_clacBatteryVolt]);	
        s16_t_ErrValue_LH_Hall = (st_g_ADCData.BattMon.Value / 7);

        if ( (s16_t_mDtctCmpVal_LH_Hall > s16_t_ErrValue_LH_Hall) || (s16_t_mDtctCmpVal_LH_Hall < (s16_t_ErrValue_LH_Hall * -1)) ) 
        {
            if( (u08_s_HallPwrCmpDlyCnt_LH >= 3U) && (u08_g_clacBatteryVolt > 6U) )
            {
                u08_g_HallPowerOnErr_LH = 1U;
                u08_s_HallPwrCmpDlyCnt_LH = 0U;
            }
            else
            {
                u08_s_HallPwrCmpDlyCnt_LH++;        
            }
        }
        else
        {
            u08_g_HallPowerOnErr_LH = 0U;
            u08_s_HallPwrCmpDlyCnt_LH = 0U;
        }
    }
    else
    {   
        s16_t_ErrValue_LH_Hall = 0;
        u08_s_HallPwrCmpDlyCnt_LH = 0U;
        if( u08_g_LatchSt == LATCH_LOCK )
        {
        	  u08_g_HallPowerOnErr_LH = 0;
        }        	  
    }
}

/********************************************************************************/
/*   name     : s_Hall_Power_Fault_Detection_RH                                 */
/*   function : Hall Power Fault Detection RH                                   */
/********************************************************************************/
#if 0
static void s_HallPowerOnFaultDetection_RH( void )
{
	
}
#endif

/********************************************************************************/
/*   name     : s_HallPwrOffFaultDetection_LH                                   */
/*   function : Hall Power Fault Detection LH                                   */
/********************************************************************************/
static void s_HallPowerOffFaultDetection_LH( void )
{
	  
    if( (st_g_ADCData.HallPower_LH.Valid == ON) && (u08_s_Hall_PowerSt_LH == OFF) &&
    	  (u08_g_HallPowerOnErr_LH == 0) && (u08_g_LatchSt == LATCH_LOCK) )
    {
    	  if( u08_s_HallPwrOffCheckDly_LH >= 1 )
    	  {
    	  	  u08_s_HallPwrOffCheckDly_LH = 2;
    	  	  
    	      if( st_g_ADCData.HallPower_LH.Value > 100 )
    	      {
    	          u08_g_HallPowerOffErr_LH = 1;	
    	      }	
    	      else
    	      {
    	          u08_g_HallPowerOffErr_LH = 0;
    	      }	   	  
    	  }	
    	  else
    	  {
    	  	  u08_s_HallPwrOffCheckDly_LH = 1; 
    	  }	
    }
    else
    {
    	 u08_s_HallPwrOffCheckDly_LH = 0; 
    	 if(u08_g_LatchSt != LATCH_LOCK) 
    	 {
    	 	  u08_g_HallPowerOffErr_LH = 0;
    	 }	
    }		  
}

/********************************************************************************/
/*   name     : s_HallPwrOffFaultDetection_RH                                   */
/*   function : Hall Power Fault Detection RH                                   */
/********************************************************************************/ 
#if 0
static void s_HallPowerOffFaultDetection_RH( void )
{
	  
}
#endif
/********************************************************************************/
/*   name     : g_AntiSensor_Power_Out                                          */
/*   function : Anti-pinchi Sensor Power Output Set                             */
/********************************************************************************/
#if 0
void g_AntiSensor_Power_Out(U08 OnOffSt )
{
    AntiPinchPowerOut_PutVal(OnOffSt);
    u08_g_AntiSensor_Power_St = OnOffSt;
}
#endif
/********************************************************************************/
/*   name     : g_Hall_Power_Out_LH                                             */
/*   function : Hall sensor LH Out put                                          */
/********************************************************************************/
void g_Hall_Power_Out_LH(U08 OnOffStL )
{
    if(OnOffStL >= 2) /* sleep off : do not retry on */
    {
        Hall_Power_LH_Out_PutVal(OFF);
    }
    else
    {
        Hall_Power_LH_Out_PutVal(OnOffStL);
    }    
    
    u08_s_Hall_PowerSt_LH = OnOffStL;
}

/********************************************************************************/
/*   name     : g_Hall_Power_Out_LH                                             */
/*   function : Hall sensor LH Out put                                          */
/********************************************************************************/
#if 0
void g_Hall_Power_Out_RH(U08 OnOffStR )
{
    if( OnOffStR >= 2 ) /* sleep off : do not retry on */
    {
        Hall_Power_RH_Out_PutVal(OFF);
    }
    else
    {
        Hall_Power_RH_Out_PutVal(OnOffStR);
    }
    
    u08_s_Hall_PowerSt_RH = OnOffStR;
}
#endif
/********************************************************************************/
/*   name     : g_Sensor_Power_Control                                          */
/*   function : hall sensor Power LH,RH Out & Antipinchi Sensor Power Out       */
/*              Out Control 10ms Task                                           */
/********************************************************************************/
void g_SensorPowerControl_10msPeroid(void)
{
    Hall_Power_out_control_LH(); 
    /*Hall_Power_out_control_RH(); */
}

/********************************************************************************/
/*   name     : g_Sensor_Power_Control                                          */
/*   function : hall sensor Power LH,RH Out & Antipinchi Sensor Power Out       */
/*              Out Control 10ms Task                                           */
/********************************************************************************/
void g_SensorPowerControl_100msPeroid(void)
{
    s_HallPowerOnFaultDetection_LH(); 
    /*s_HallPowerOnFaultDetection_RH(); */
    s_HallPowerOffFaultDetection_LH(); 
    /*s_HallPowerOffFaultDetection_RH(); */    
}
