/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-09 PM.  2:22:24                                   */
/*	Last Update     : 2013-04-09 PM.  2:22:24                                   */
/*	File Name       : Read_Hall_Sensor.h                                        */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/


/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		EEPROM_CONTROL_H
#define		EEPROM_CONTROL_H

#include "Basic_Type_Define.h"
#include "IEE1.h"
#include <string.h>
#include <stdlib.h>

#ifndef  	EEPROM_CONTROL_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define EEP_PARA_ADDRESS        0x4400L /* EEPROM_BASE+0x0L */
#define EEP_DIAG_ADDRESS        0x4500L /* EEPROM_BASE+0x100L */
#define RESERVE_ADDRESS         0x4600L /* EEPROM_BASE+0x200L */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
/* EXTERN U08  u08_g_PoDSFFSDFSDAF; */

typedef struct
{
    U16   Start;
    S16   LearnedMechEndPos;
    S16   MaxBasicPos;
    S16   MaxPowerOpenPos;
    S16   UserSetPosition;
    S16   ZONE3Pos;
    S16   ZONE2Pos;
    U16   User_Hight_SetValue;
    U16   EndCRC;
}EEP_PTG_PARA_St;

typedef struct
{
    U16   Start;
    U16   PGTM_Enable; 
    U16   EEPCanLineErrorB;
    U16   EEPCanBusOffB;
    U16   EEPIGNCount;
    U16   InLine_Mode;
    U16   Pre_InLine_Mode;  
    U16   InnerSw_Short;
    U16   OS_HDLSw_Short;
    U16   ConsoleSw_Short;      
    U16   Serial[11]; 
    U16   HallError_LH;
    U16   HallError_RH;
    U16   APS_Error_LH;
    U16   APS_Error_RH;
    U16   UnLatch_Error;
    U16   PastFailure[3];
    U16   PastFailure_Index;   
    U16   Speed_Set_Value;
    U16   IO_InputFault;
    U16   EndCRC;
}EEP_PTG_Diag_St;


EXTERN EEP_PTG_PARA_St u16_g_EEP_Para;
EXTERN EEP_PTG_Diag_St u16_g_EEP_Diag;
EXTERN U08 u08_g_Init_Param;
EXTERN U08 u08_g_EEP_Write_Event;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN U08 u08_g_Write_Para_Sector(void);
EXTERN U08 u08_g_Write_Diag_Sector(void);
EXTERN void g_Read_All_EEP(void);

#undef EXTERN

#endif