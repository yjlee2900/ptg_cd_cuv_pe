/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-07 AM.  9:20:25                                   */
/*  Last update     : 2013-06-07 PM.  4:00:27                                   */
/*  File Name       : CAN_Drive_App.c                                           */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		CAND_DRIVE_APP_H_EXTERN
#define		CAND_DRIVE_APP_H_EXTERN

#include "Basic_Type_Define.h"
#include "can_inc.h"
#include "can_par.h"	/* paramters */
#include "v_inc.h"		/* osek network manager module */
#include "v_cfg.h"
#include "nm_osek.h"	/* osek network manager module */
#include "il_inc.h"       
#include "ccp.h"
#include "desc.h"

#ifndef  	CAN_DRIVE_AP_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define    INIT_SLEEP_DEL_T      500   /* 500 * 10ms = 5000ms */ 

#define    TRS_INIT     0U
#define    TRS_SLEEP    1U
#define    TRS_NOMAL    2U
#define    TRS_STANDBY  3U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
/* EXTERN U16 	u16_g_XX_XXXXXXXXX; */

typedef	struct	/* PT function control */
{
    U08     ADMTimeOutTm;
    U08     DDMTimeOutTm;
    U08     EreaseCount;
    U16     count_Bus_Off;
    U16     CanLineErrCount;
    U16     CanLineError        :1;      /* now error_ */
    U16     CanBusOff           :1;      /* now error_ */
    U16     CanLineErrorB_Old   :1;
    U16     CanLineErrorB       :1;
    U16     CanBusOffB_Old      :1;
    U16     CanBusOffB          :1;
    U16     CanBusOffSet        :1;
    U08     EcuResetTm;  
    U16     EcuResetID;     
    U16     SleepFlag           :1;
    U16     IlCanDisableFlag    :1;
    U16     kDescCommControlStateDisableFlag    : 1;
    U16     Pre_Inline_Mode     : 1;         
    U16     HallError_LH        :1; 
    U16     HallError_RH        :1;               
    U16     APS_Error_LH        :1;       
    U16     APS_Error_RH        :1;
    U16     UnLatch_Error       :1;     

} stTrans;

EXTERN stTrans PtlTrans;
EXTERN U08 u08_g_CanSleep_flag;   
EXTERN U08 u08_g_Recheck_IGNSt_flag;
EXTERN U16 u16_g_InitSleepDelTm_Att;
EXTERN U08 u08_g_CANFatalError;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_BCAN_Init( void );
/* EXTERN	void s_CAN_Transiver_Control(U08 TRS_Mode); */
EXTERN  void g_check_battAttach( void );
EXTERN  void g_BCAN_Handle_10msPeriod_1( void );
EXTERN  void g_BCAN_Handle_10msPeriod_2( void );
#undef EXTERN

#endif