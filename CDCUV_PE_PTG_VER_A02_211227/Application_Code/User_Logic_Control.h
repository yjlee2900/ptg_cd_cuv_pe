/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-07 AM.  9:20:25                                   */
/*  Last update     : 2013-06-07 AM.  9:21:04                                   */
/*  File Name       : User_Logic_Control.c                                      */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		USER_LOGIC_H_EXTERN
#define		USER_LOGIC_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	USER_LOGIC_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define TG_OUTER_SW         1U  /* CAN Signal : Tailgate outer release Switch  */
#define TG_INNER_SW         2U  /* hardwire   : Tailgate inner Open/close Switch  */
#define TG_CPAD_SW          3U  /* hardwire   : inside console pad tailgate Switch  */
#define TG_RKE_SW           4U  /* CAN Signal : BCM or SKM Tailgate Open/Close Signal  */
#define TG_PASSIVE          5U  /* CAN Signal : SKM Tailgate Passive detect Signal  */
#define TG_AVNCMD           6U  /* CAN Signal : AVN Voice Open/Close Command Signal  */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_User_Tailgate_Cmd;
EXTERN U08  u08_g_Wait_Unlatch_T;
EXTERN U08  u08_g_Tx_ReleaseRly;       /* PTGMmsg05 : 0:Off, 1:On */
EXTERN U08  u08_g_PTGM_Power_Mode;
EXTERN U08  u08_g_Moving_Delay_T;
EXTERN U08  u08_g_User_Input_On;
EXTERN U08  u08_g_Signal_UpDate_check;
EXTERN U08  u08_g_Diag_Write_Cmd; 
EXTERN U08  u08_g_Consol_SW_On_t;
EXTERN U08  u08_g_Switch_Short_Event;
EXTERN U08  u08_g_RKE_Close_St;
EXTERN U08  u08_g_CPAD_Close_St;     
EXTERN U08  u08_g_Zone2_Open;
EXTERN U08  u08_g_User_Position_set;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_User_Logic_10msPeriod(void);
EXTERN  void g_User_Logic_Init(void);
#undef EXTERN

#endif
