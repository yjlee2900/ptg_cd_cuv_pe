/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-05-09 PM.  6:31:01                                   */
/*	Last Update     : 2013-05-09 PM.  6:31:04                                   */
/*	File Name       : Digital_Input_Chattering.h                                */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		AP_DATATYPE_H_EXTERN
#define		AP_DATATYPE_H_EXTERN

typedef	enum	/* enumerate switch value with positive logic */
{
	enOFF	    = 0,	
	enON	    = 1
} enumSw;
typedef	enum
{
	enLOW		= 0,
	enHIGH		= 1
} enumLogic;

typedef enum
{
    PARAM_NONE   =  0,
    PARAM_WAIT   =  1,
    PARAM_RUN    =  2,
    PARAM_CHECK  =  3,
    PARAM_SAVE   =  4,
    PARAM_ERROR  =  5
}enumParam;


#endif
