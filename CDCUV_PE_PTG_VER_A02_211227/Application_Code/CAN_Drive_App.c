/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-07 AM.  9:20:25                                   */
/*  Last update     : 2013-06-07 PM.  4:00:27                                   */
/*  File Name       : CAN_Drive_App.c                                           */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define CAN_DRIVE_AP_EXTERN

#include "CAN_Drive_App.h"    
#include "CAN_Signal_Handling.h"
#include "ccp.h"
#include "Status_Detect.h" 
#include "Thermal_Protect.h"
#include "Sleep_Wakeup_Control.h"
#include "PCU_Sequence.h"
#include "CAN_NSTB.h"
#include "CAN_EN.h"
#include "CanNError_In.h"
#include "Task_Scheduling.h"
#include "User_Logic_Control.h"
#include "TG_Consol_SW.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U08 u08_s_noSleep = 0; 
static U08 u08_s_BattAttach = 0;
static U16 u16_s_InitSleepDelTm = 0;
static U08 u08_s_IlTx_Delay_T = 0;
static U16 u16_s_LatchOnDelTm = 0;
/*static U08 u08_s_Switch_Wake_Check = 0;*/
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_BCAN_Tx_Disable( U08 Tx_Control ); 
static void s_BCAN_InitPowerOn( void );
static void s_BCAN_StartIlTask( void );
static void s_CAN_Sleep_Check( void );
static void s_CanTransceiverError( void );
static void s_IGOn_Alive_Out( void );
static void s_Latch_LockOn_Check_Delay( void );
static void s_CANSignal_Recheck( void ); 
static void s_CAN_Transiver_Control(U08 TRS_Mode);
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/


/********************************************************************************/
/*   name     : ApplCanBusOff                                                   */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void C_CALLBACK_2 ApplCanBusOff(CAN_CHANNEL_CANTYPE_ONLY)
{
/*    PtlTrans.CanBusOffSet = TRUE; */
;    
}

/********************************************************************************/
/*   name     : ApplCanTimerStart                                               */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void     C_CALLBACK_2 ApplCanTimerStart(CAN_CHANNEL_CANTYPE_FIRST vuint8 source)
{
	;	/* no description */	
}

/********************************************************************************/
/*   name     : ApplCanTimerLoop                                                */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 vuint8 C_CALLBACK_2 ApplCanTimerLoop (CAN_CHANNEL_CANTYPE_FIRST vuint8 source)
{
	U08	nResult = 0;
	;	/* no description */
	return nResult;
}

/********************************************************************************/
/*   name     : ApplCanTimerEnd                                                 */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void     C_CALLBACK_2 ApplCanTimerEnd  (CAN_CHANNEL_CANTYPE_FIRST vuint8 source)
{
	;	/* no description */	
}

/********************************************************************************/
/*   name     : ApplVStdFatalError                                              */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
VSTD_CALLBACK_0 void VSTD_CALLBACK_1 ApplVStdFatalError(vuint8 nErrorNumber)
{
	;
}  


/********************************************************************************/
/*   name     : ApplCanFatalError                                               */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void C_CALLBACK_2 ApplCanFatalError(CAN_CHANNEL_CANTYPE_FIRST vuint8 errorNumber)
{
	if( (errorNumber == kErrorTxHandleWrong) || (errorNumber == kErrorRxHandleWrong) )
	{
      u08_g_CANFatalError++;
	}
} 

/********************************************************************************/
/*   name     : ApplCanTxConfirmation                                           */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void C_CALLBACK_2 ApplCanTxConfirmation(CanTxInfoStructPtr txStruct)
{
/*	if ( PtlTrans.count_Bus_Off != 0) */
/*	    {PtlTrans.count_Bus_Off--;}  */ /*->can_drv.c*/

    PtlTrans.CanBusOffSet = FALSE;
    /*if( u08_s_Switch_Wake_Check == 1 )*/ /* PTG Tx First = ECU WAKE Source is Switch Input change */
    /*{
        u08_s_Switch_Wake_Check = 0;
    }*/       
}

/********************************************************************************/
/*   name     : ApplTpFatalError                                                */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void TP_API_CALLBACK_TYPE ApplTpFatalError(canuint8 errorNumber) 
{
;
}

/********************************************************************************/
/*   name     : ApplNmDirOsekFatalError                                         */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmDirOsekFatalError( vuint8 error )
{
	;
}

/********************************************************************************/
/*   name     : ApplIlFatalError                                                */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void ApplIlFatalError(vuint8 errorNumber)
{
   ;
}

/********************************************************************************/
/*   name     : ApplCanWakeUp                                                   */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
C_CALLBACK_1 void C_CALLBACK_2 ApplCanWakeUp(CAN_CHANNEL_CANTYPE_ONLY)
{
	/* Callback function at the transition from SleepMode to sleep indication recommended */
	/* call-back function at the transition from sleep-mode to sleep indication state, OsekNM */
    s_CAN_Transiver_Control(TRS_NOMAL);	    /* switch bus transceiver to normal mode */
    NmOsekInit(NM_SLEEPIND);	            /* init. osek nm */
    u08_g_Shutdown_Delay = 0;
    /* can wake up -> illumination On Control */
}

/********************************************************************************/
/*   name     : ApplCanMsgReceived                                              */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
canuint8 DRV_API_CALLBACK_TYPE ApplCanMsgReceived(CanRxInfoStructPtr rxStruct)
{   
    /*U08 U08_t_check_switch;*/
    
    /*if( u08_s_Switch_Wake_Check == 1 )*/ /* PTG RX First = ECU WAKE Source is CAN Message */
    /*{    
        u08_s_Switch_Wake_Check = 0;
        
        U08_t_check_switch = TG_Consol_SW_GetVal();
    
        if( U08_t_check_switch != 0 )
        {
            u08_g_Consol_SW_On_t = 0xFFU;
        }
        else
        {
            u08_g_Consol_SW_On_t = 0;
        }     
    } */
      
    return kCanCopyData; 
}

/********************************************************************************/
/*   name     : ApplNmBusOffEnd                                                 */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmBusOffEnd( NM_CHANNEL_APPLTYPE_ONLY )
{
    /* handle the IL */
    IlTxRelease();
}

/********************************************************************************/
/*   name     : ApplNmBusOff                                                    */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmBusOff( NM_CHANNEL_APPLTYPE_ONLY )
{
	/*	the function is normally called from the can error isr and indicates a bus off state.
		but the bus off handling is again highly dependent on your hardware.	*/
    PtlTrans.CanBusOffSet = TRUE;
    IlTxWait();
}

/********************************************************************************/
/*   name     : ApplNmCanNormal                                                 */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmCanNormal( NM_CHANNEL_APPLTYPE_ONLY )
{
    
	s_CAN_Transiver_Control(TRS_NOMAL);	         /* switch bus transceiver to normal mode */
	(void)CanWakeUp( NM_CHANNEL_CANPARA_ONLY );	 /* switch can controller to normal mode */

    /* switch IL to active mode */
    IlRxStart();
    IlTxStart();
;
}

/********************************************************************************/
/*   name     : ApplNmWaitBusSleepCancel                                        */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmWaitBusSleepCancel( NM_CHANNEL_APPLTYPE_ONLY )
{
    /* handle the IL */
    IlRxStart();
    IlTxStart();
;
}

/********************************************************************************/
/*   name     : ApplNmWaitBusSleep                                                 */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmWaitBusSleep( NM_CHANNEL_APPLTYPE_ONLY )
{

    /* handle the IL */
    IlRxStop();
    IlTxStop();

}

/********************************************************************************/
/*   name     : ApplNmBusStart                                                  */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmBusStart( NM_CHANNEL_APPLTYPE_ONLY )
{
	;
}

/********************************************************************************/
/*   name     : ApplNmCanSleep                                                  */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmCanSleep( NM_CHANNEL_APPLTYPE_ONLY )
{
	U08	returnCode;

	returnCode = CanSleep( NM_CHANNEL_CANPARA_ONLY );
	if (returnCode == kCanFailed)
	{
		u08_s_noSleep = 1; /* global variable */
	}
	else
	{
		u08_s_noSleep = 0; /* global variable */
		/* switch can controller to standby mode */
		s_CAN_Transiver_Control(TRS_STANDBY);
	}
}

/********************************************************************************/
/*   name     : ApplNmCanBusSleep                                               */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmCanBusSleep( NM_CHANNEL_APPLTYPE_ONLY )
{
	if (u08_s_noSleep == 1)
	{
		u08_s_noSleep = 0;
		GotoMode (Awake);
		u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T; /* 141002 */
		u08_g_CanSleep_flag = FALSE;
	}
	else
	{
		/* switch can controller to sleep mode */
		/*BusTransceiverToSleep();*/
		if( (u08_g_LatchSt == LATCH_LOCK) && (u16_s_LatchOnDelTm == 0) && (u08_g_C_IGNSw == KEY_OFF) ) /* latch lock on delay 3sec */
	    {
	        if( (u08_g_Latch_Unexpect == FALSE) && (u16_g_InitSleepDelTm_Att == 0) && (u08_g_PCU_Mode == STANDBY) &&
	            (PtlTrans.CanBusOff != TRUE) /*&& (PtlTrans.CanLineError != TRUE)*/ ) 
	        {
	            u08_g_CanSleep_flag = TRUE;
	        }                                                                                              
	    } 
	    
	    u08_g_Signal_UpDate_check = 0;
	    /* else {  ;  } */ /* QAC_correct_jaeho */
	}
}

/********************************************************************************/
/*   name     : ApplNmIndRingData                                               */
/*   function :                                                                 */
/*   vector callback function                                                   */
/********************************************************************************/
void NM_API_CALLBACK_TYPE ApplNmIndRingData(NM_CHANNEL_APPLTYPE_ONLY)
{
	vuint8	ringData[6];

/* #if defined (NM_COPY_RINGDATA_USED)*/
	if (ReadRingData(ringData) == E_OK)
	{
		/* ring data test, clear received pattern */
		ringData[0] = 0x00;
		ringData[1] = 0x00;
		ringData[2] = 0x00;
		ringData[3] = 0x00;
		ringData[4] = 0xFF;
		ringData[5] = 0xFF;

		(void)TransmitRingData(ringData);
	}
/* #else */
    else {
        
	/* only transmission possible, no itnerpretation of the ring data required */
		ringData[0] = 0x00;
		ringData[1] = 0x00;
		ringData[2] = 0x00;
		ringData[3] = 0x00;
		ringData[4] = 0xFF;
		ringData[5] = 0xFF;
    }

		(void)TransmitRingData(ringData);
/* #endif	 NM_RINGDATA_ACCESS_USED  */
}

/********************************************************************************/
/*   name     : s_BCAN_Tx_Disable                                               */
/*   function : BCAN TX Disable Enable                                          */
/********************************************************************************/
static void s_BCAN_Tx_Disable( U08 Tx_Control )
{
    if( Tx_Control == TRUE )
    {
        IlTxStop();
        StopNM();
    }    
    else
    {
        IlTxStart();
        StartNM();        
        NmOsekInit( NM_NORMAL );                 
        u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T;        
    }    
}

/********************************************************************************/
/*   name     : s_BCAN_InitPowerOn ,                                            */
/*   function : CAN Init set                                                    */
/********************************************************************************/
static void s_BCAN_InitPowerOn( void )
{
    u08_g_CanSleep_flag = FALSE;
    PtlTrans.SleepFlag  = 0;
    u08_s_IlTx_Delay_T = 11;
    u08_g_CANFatalError = 0;
    
    CanInitPowerOn(); /* To start the CAN Controller and the CAN Driver */
    IlInitPowerOn();  /* Initialize the interaction layer (Il_Vector) */
    ccpInit();  
    
    if (u08_s_BattAttach == TRUE)
    {  
        u08_s_BattAttach = FALSE; 
        NmOsekInit( NM_NORMAL );                 /* according to vector manual, gong */
        u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T;
    }
    else 
    { 
        NmOsekInit( NM_SLEEPIND );      /* HMC CANGeny Demo */
        u16_s_InitSleepDelTm = (INIT_SLEEP_DEL_T + 20);    /* silent power-on operation */   
    }                                     
                                                
    TpInitPowerOn();
    (void)DescInitPowerOn(0);  /*( kDescPowerOnInitParam );*/
    
}

/********************************************************************************/
/*   name     : s_BCAN_StartIlTask                                              */
/*   function : Il task start                                                   */
/*                                                                              */
/********************************************************************************/
static void s_BCAN_StartIlTask( void )
{
    IlRxStart();
    IlTxStart();
}

/********************************************************************************/
/*   name     : s_CAN_Sleep_Check                                               */
/*   function : CAN Transiver sleep check                                       */
/*                                                                              */
/********************************************************************************/
static void s_CAN_Sleep_Check( void )
{
    
    /*U08 t_sleepInd_flag;*/
    /*U08 t_Limphome_flag;*/
                                 
    if( u08_g_CanSleep_flag == TRUE )
    {
        g_ECU_Sleep();
        u08_g_CanSleep_flag = FALSE;
    }    
                             
    if ( (NmStateActive(NmGetStatus()) != 0) || 
         ( (u16_s_InitSleepDelTm < (INIT_SLEEP_DEL_T - 100)) && (u08_g_LatchSt == LATCH_OPEN))  )
    { 
        u16_s_InitSleepDelTm = 0;   
    }
    
    if( u16_s_InitSleepDelTm != 0 )
    {
        u16_s_InitSleepDelTm--;
        if( u16_s_InitSleepDelTm == 0 )
        {
            g_ECU_Sleep();
        }    
    }    
    
    /* statePT.CanActiveState = NmGetStatus();*/     /* NOMAL STATUS CHECK */
#if 0    
    t_sleepInd_flag = (U08)NmStateBusSleepInd(NmGetStatus());
    t_Limphome_flag = (U08)NmStateLimphome(NmGetStatus());    
    
    if(t_Limphome_flag == TRUE)    
    {                           
        if( (t_sleepInd_flag == TRUE) && (u08_g_LatchSt == LATCH_LOCK) && (u08_g_PCU_Mode == STANDBY) &&
            (u08_g_Latch_Unexpect == FALSE) && (u16_s_InitSleepDelTm == 0) && (u08_g_Shutdown_Delay == 0))
        {
             g_ECU_Sleep();
        }
    }    
#endif 
    
   /*  #ifndef NONCAN_TEST_MODE  */
    
    if( u08_g_Shutdown_Delay != 0 )
    {
        u08_g_Shutdown_Delay--; 
        if( (NmStateBusSleep(NmGetStatus()) != 0) /*&& ( CanNError_In_GetVal() != 0 ) */)
        {    
            if( u08_g_Shutdown_Delay == 0  )
            {
                s_CAN_Transiver_Control(TRS_SLEEP);
            }
        }
        else
        {
            u08_g_Shutdown_Delay = 0;
        }    
    }    
}
   
/********************************************************************************/
/*   name     : s_CanTransceiverError                                           */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
static void s_CanTransceiverError( void )
{
    if( CanNError_In_GetVal() == 0 )  
    {   
        if (PtlTrans.CanLineErrCount <500)
        { 
            PtlTrans.CanLineErrCount++; 
        } 
        PtlTrans.EreaseCount = 5 ;          
    }
    else
    { 
        if (PtlTrans.CanLineErrCount> 40)
        { 
            PtlTrans.CanLineErrCount -= 40;  
        }
        else 
        { 
            PtlTrans.CanLineErrCount = 0; 
        }

        if (PtlTrans.EreaseCount != 0)
        { 
            PtlTrans.EreaseCount--;
        }
    }
    
    if ( PtlTrans.CanLineErrCount >= 200) 
    {
        PtlTrans.CanLineError = TRUE; 
        PtlTrans.CanLineErrorB = TRUE;    
    }

    if (( PtlTrans.CanLineErrCount == 0) || ( PtlTrans.EreaseCount == 0))
    { 
        PtlTrans.CanLineError = FALSE ; 
        PtlTrans.CanLineErrCount = 0;
        PtlTrans.EreaseCount = 0;
    }

    /* Can Bus Off check */        
    if (PtlTrans.CanBusOffSet == TRUE)
    { 
        if (PtlTrans.count_Bus_Off < 255)
        { 
            PtlTrans.count_Bus_Off ++; 
        }
        else 
        { 
            PtlTrans.CanBusOff = TRUE; 
            PtlTrans.CanBusOffB = TRUE;
        }
    }
    else 
    { 
        PtlTrans.count_Bus_Off = 0 ;
        PtlTrans.CanBusOff = FALSE;   
    }
}
                
/********************************************************************************/
/*   name     : s_IGOn_Alive_Out                                                */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/                
static void s_IGOn_Alive_Out( void )
{                                      
    static U08 u08_s_Pre_IGN_flag = 0;
    
    if(u08_g_C_IGNSw >= KEY_IGN) 
    { 
        u08_g_CanSleep_flag = FALSE;
        u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T; /* 500ms  2014.02.08 */
        
        if( u08_s_Pre_IGN_flag == FALSE ) 
        {
            GotoMode (Awake);
            u08_s_Pre_IGN_flag = TRUE;
        }
    }
    else
    {
        u08_s_Pre_IGN_flag = FALSE;  
    }
}
                
/********************************************************************************/
/*   name     : s_CANSignal_Recheck                                             */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/                
static void s_CANSignal_Recheck( void )                
{                              
    static U08 u08_s_Busoff_check_T = 0;
    
     /* IGn after can Bus off check */   
    if ((PtlTrans.CanBusOff == TRUE )||(PtlTrans.CanLineError == TRUE))
    {   
        u08_s_Busoff_check_T = 210;       /* 2 sec setting */
        if( PtlTrans.CanBusOff == TRUE )  /* 141029 */  /* for in to Sleep mode when can line error occur  */
        {    
            u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T ;
        }
    }
    else 
    {
        if(u08_s_Busoff_check_T != 0)          
        {   
            u08_s_Busoff_check_T--;
            if( u08_s_Busoff_check_T == 0 )
            {
                /* u08_g_C_IGNSw = KEY_OFF; */
                u08_g_Recheck_IGNSt_flag = TRUE;              
            }
        }
    } 
}            

/********************************************************************************/
/*   name     : s_Latch_LockOn_Check_Delay                                      */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/ 
static void s_Latch_LockOn_Check_Delay( void )
{
    if( u08_g_LatchSt != LATCH_LOCK )
    {
        /* latch lock on check delay for can sleep */
        u16_s_LatchOnDelTm = 300U;  /* 300 * 10ms : 3000ms */  
    }
    else
    {
        if( u16_s_LatchOnDelTm != 0 )
        {
            u16_s_LatchOnDelTm--;
        }    
    }
}    
/********************************************************************************/
/*   name     : g_check_battAttach                                              */
/*   function : battery attach check                                            */
/*                                                                              */
/********************************************************************************/
void g_check_battAttach( void )
{
    U08 u08_t_delay_T = 0;
    U08 u08_t_get_err;
    
    s_CAN_Transiver_Control(TRS_INIT);
    /*u08_s_Switch_Wake_Check = 1;*/
    
    do
    {
        u08_t_delay_T++;
    }while(u08_t_delay_T < 200);
    
    u08_t_get_err = CanNError_In_GetVal();
    if( u08_t_get_err != 0 )
    {
        u08_s_BattAttach = FALSE;
    } 
    else
    {
        u08_s_BattAttach = TRUE;
    }    
       
    PtlTrans.SleepFlag = 0;
    u08_g_Recheck_IGNSt_flag = 0;
}

/********************************************************************************/
/*   name     : g_BCAN_Init                                                     */
/*   function : Power On Reset BCAN Init                                        */
/*                                                                              */
/********************************************************************************/
void g_BCAN_Init( void )
{
    s_BCAN_InitPowerOn();
    s_BCAN_StartIlTask();
}

/********************************************************************************/
/*   name     : s_CAN_Transiver_Control                                         */
/*   function : CAN Transiver Contorl function                                  */
/*                                                                              */
/********************************************************************************/
static void s_CAN_Transiver_Control(U08 TRS_Mode)
{
    U16 u16_t_delay_T;
    static U08 u08_s_sleep_cancel = 0;
    
    switch( TRS_Mode )
    {
        case TRS_INIT : 
            CAN_NSTB_PutVal(HIGH); /* To read power-on flag of TJA1055(Battety Attach) */
            CAN_EN_PutVal(LOW);    /* To read power-on flag of TJA1055(Battety Attach) */
            break;
        case TRS_SLEEP :
            PtlTrans.SleepFlag = 1;
            u08_s_sleep_cancel = 0;
	        CAN_NSTB_PutVal(LOW);
	        CAN_EN_PutVal(HIGH);
	        /* make sure this loop is not removed by compiler ooptimization */
	        for (u16_t_delay_T = 0; u16_t_delay_T < 1000; u16_t_delay_T++)
	        {
	        	; /* nothing to do - wait 50us */  
	        } 
	        if( u08_s_sleep_cancel == 0 ) /* check can interrupt wake */
	        {     
	            CAN_EN_PutVal(LOW);  
	        }          
            break;
        case TRS_NOMAL :
            CAN_NSTB_PutVal(HIGH); 
	        CAN_EN_PutVal(HIGH);
	        PtlTrans.SleepFlag = 0;
	        u08_s_sleep_cancel = 1;
	        u08_g_CanSleep_flag = FALSE;
	        g_Sleep_Wakeup_Drive( WAKE_UP_MODE );  /* 151214 */
            break;
        case TRS_STANDBY :
            CAN_NSTB_PutVal(LOW); 
	        CAN_EN_PutVal(LOW);
	        PtlTrans.SleepFlag = 1;
            break;
        default :
            break;
                            
    }
} 

/********************************************************************************/
/*   name     : g_BCAN_Handle_10msPeriod                                        */
/*   function : BCAN handle 10ms call task function                             */
/*                                                                              */
/********************************************************************************/
void g_BCAN_Handle_10msPeriod_1( void )
{
    static U08 u08_s_pre_CAN_Disable = 0;
    
    if (PtlTrans.SleepFlag != 0)    /*  can sleep check */
    {   
        u08_s_IlTx_Delay_T = 11;             /* 80 msec */    
    }
                                       
    if(st_g_Vehiclest.bBatVoltCAN_Disable != u08_s_pre_CAN_Disable)  
    {
        if( st_g_Vehiclest.bBatVoltCAN_Disable == TRUE )
        {
            s_BCAN_Tx_Disable( TRUE );
        }    
        else
        {
            s_BCAN_Tx_Disable( FALSE );
        }    
        
        u08_s_pre_CAN_Disable = st_g_Vehiclest.bBatVoltCAN_Disable;
    }
    
    if (u08_s_IlTx_Delay_T == 0)
    { 
        IlTxTask(); 
    }
    else
    {
        u08_s_IlTx_Delay_T--;
    }    
        
    IlRxTask();
    TpRxTask();
    (void)DescTask();     /* QAC_correct_jaeho */
    TpTxTask();
    
    ccpDaq(1); 
    ccpDaq(2); 
    ccpDaq(3); 
    
    if(PtlTrans.IlCanDisableFlag == FALSE ) 
    {
        NmTask();
    }
    
    if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (st_g_Vehiclest.bBatVoltCAN_Disable == 0))
    {        
        s_CanTransceiverError();
    }            
    
    
    if (u16_g_InitSleepDelTm_Att != 0) /* battery attach can wake to sleep */
    {
        u16_g_InitSleepDelTm_Att--;
        if( u16_g_InitSleepDelTm_Att == 0 )
        {            
            GotoMode(BusSleep); /* HMC CANGeny Demo */
            /*NmOsekInit( NM_SLEEPIND );*/              
        }
    }
}

/********************************************************************************/
/*   name     : g_BCAN_Handle_10msPeriod                                        */
/*   function : BCAN handle 10ms call task function                             */
/*                                                                              */
/********************************************************************************/
void g_BCAN_Handle_10msPeriod_2( void )
{
    s_IGOn_Alive_Out();  /* 2014.02.08 */
    s_CANSignal_Recheck();
    s_Latch_LockOn_Check_Delay();
    s_CAN_Sleep_Check();
}
