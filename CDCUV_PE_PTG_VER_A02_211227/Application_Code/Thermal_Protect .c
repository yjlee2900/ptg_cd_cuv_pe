/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-24 AM.  9:10:07                                   */
/*  Last update     : 2013-05-24 AM.  9:10:10                                   */
/*  File Name       : Thermal_Protect.c                                         */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define THERMAL_PROTECT_EXTERN
#include "Basic_Type_Define.h"
#include "AP_DataType.h"
#include "Thermal_Protect.h"
#include "Spindle_Motor_Drive.h"
#include "PCU_Motor_Drive.h"
#include "Hazard_Buzzer_Control.h"

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define SPINDLE_THERMAL_UP_CNT    6
#define SPINDLE_THERMAL_DOWN_CNT  4
#define SPINDLE_THERAML_MAX       30000 /* 50 Sec */  
#define SPINDLE_THERAML_MIN       27000 

#define PCU_COOLDOWN_TIME         2500  /* 2500 * 10 msec =25 sec  */                                    
#define PCU_THERMAL_ON_TIME       2500  /* 2500 * 10 msec = 25 sec */   
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/



/********************************************************************************/
/*   name     : g_Spindle_Thermal_10msPeriod                                    */
/*   function :                                                                 */
/*   spindle Motor thermal check , Protect      10ms task                       */
/********************************************************************************/
void g_Spindle_Thermal_10msPeriod(void)
{
    if ((u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE)||(u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE))
    {    
        u16_g_Spindle_Thermal_Cnt += SPINDLE_THERMAL_UP_CNT;
    }
    else 
    {
        if(u16_g_Spindle_Thermal_Cnt >= SPINDLE_THERMAL_DOWN_CNT) 
        { 
            u16_g_Spindle_Thermal_Cnt -= SPINDLE_THERMAL_DOWN_CNT; 
        }
        else 
        {
            u16_g_Spindle_Thermal_Cnt = 0;
        }
    }
        
    if (u16_g_Spindle_Thermal_Cnt >= SPINDLE_THERAML_MAX)
    {   
        u08_g_Spindle_Theraml_St = THERMAL_PROTECT_SET; 
    }
    if (u16_g_Spindle_Thermal_Cnt < SPINDLE_THERAML_MIN)    
    {
        u08_g_Spindle_Theraml_St = THERMAL_PROTECT_CLEAR; 
    }                      
}

/********************************************************************************/
/*   name     : g_PCU_Thermal_10msPeriod                                        */
/*   function :                                                                 */
/*   PCU Motor thermal check , Protect                                          */
/********************************************************************************/
void g_PCU_Thermal_10msPeriod(void)
{
    U16 Cnt_Sum;
    static U16 u16_s_PCU_Cooldown_Cnt = 0;
    static U16 u16_s_PCU_Thermal_Off_Time = 0;
    static U16 u16_s_PCU_Thermal_Buff[5] = { PCU_THERMAL_ON_TIME,PCU_THERMAL_ON_TIME,PCU_THERMAL_ON_TIME,PCU_THERMAL_ON_TIME,PCU_THERMAL_ON_TIME }; 
    static U08 u08_s_PCU_Inx = 0; 
    static U08 u08_s_Pre_PCU_St = 0;   
    
    if( u16_s_PCU_Cooldown_Cnt < PCU_COOLDOWN_TIME )
    {   
        u16_s_PCU_Cooldown_Cnt++;     /* time 10 msec increse */
    }           
                                                
    if(u16_s_PCU_Thermal_Off_Time != 0 )
    {  
        u16_s_PCU_Thermal_Off_Time--;
    }
    else 
    { 
        u08_g_PCU_Theraml_St = THERMAL_PROTECT_CLEAR ; 
    }

    if(u08_g_PCU_MotorSt == CINCHING_PCU)
    {  
        if(u08_s_Pre_PCU_St != ON)
        {   
            u08_s_Pre_PCU_St = ON;
            
            switch (u08_s_PCU_Inx) 
            {
                case 0U:
                    u16_s_PCU_Thermal_Buff[0] = u16_s_PCU_Cooldown_Cnt;
                    u08_s_PCU_Inx++;
                    break;
                case 1U:
                    u16_s_PCU_Thermal_Buff[1] = u16_s_PCU_Cooldown_Cnt;
                    u08_s_PCU_Inx++;
                    break;
                case 2U:
                    u16_s_PCU_Thermal_Buff[2] = u16_s_PCU_Cooldown_Cnt;
                    u08_s_PCU_Inx++;
                    break;
                case 3U:
                    u16_s_PCU_Thermal_Buff[3] = u16_s_PCU_Cooldown_Cnt;
                    u08_s_PCU_Inx++;
                    break;
                case 4U:
                    u16_s_PCU_Thermal_Buff[4] = u16_s_PCU_Cooldown_Cnt;
                    u08_s_PCU_Inx = 0;
                    break;
                default :
                    u08_s_PCU_Inx = 0;
                    break;    
            }
            
            u16_s_PCU_Cooldown_Cnt = 0; 

            Cnt_Sum = u16_s_PCU_Thermal_Buff[0]+u16_s_PCU_Thermal_Buff[1]+u16_s_PCU_Thermal_Buff[2]   
                      +u16_s_PCU_Thermal_Buff[3]+u16_s_PCU_Thermal_Buff[4];
                        
            if(Cnt_Sum < PCU_COOLDOWN_TIME) 
            { 
                if (u08_g_PCU_Theraml_St != THERMAL_PROTECT_SET)
                {
                    /*g_SetBuzzHazardControl( BUZZONLY_ON, 6 );*/
                    u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    u08_g_Buzzer_Hazard_Count = 6;           
                }
                u08_g_PCU_Theraml_St = THERMAL_PROTECT_SET; 
                u16_s_PCU_Thermal_Off_Time = PCU_THERMAL_ON_TIME;       /* 25 sec AntiPaly time set */
            }    
        }
    }
    else 
    {
        u08_s_Pre_PCU_St = OFF;
    }
}

/********************************************************************************/
/*   name     : g_Thermal_Status_Init                                           */
/*   function :                                                                 */
/*   Thermal check Value Init                                                   */
/********************************************************************************/
void g_Thermal_Status_Init(void)
{
    u08_g_Spindle_Theraml_St = THERMAL_PROTECT_CLEAR;
    u08_g_PCU_Theraml_St = THERMAL_PROTECT_CLEAR;
    u16_g_Spindle_Thermal_Cnt = 0;
}
