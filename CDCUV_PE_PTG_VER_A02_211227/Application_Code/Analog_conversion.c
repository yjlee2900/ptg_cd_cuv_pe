/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-14 AM.  11:00:07                                  */
/*  Last update     : 2013-05-23 PM.  6:51:53                                   */
/*  File Name       : Analog_conversion.c                                       */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define ANALOG_CONVERSION_EXTERN
#include "Basic_Type_Define.h"
#include "Analog_conversion.h"   
#include "AD1.h"
#include "Spindle_Motor_Drive.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/


/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Get_Average_AD( stAdcSignal* stAD, U16 u16_s_getValue , U08 u08_s_average_cnt );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define u08_AVERAGE_CNT_SPINDLE    10      
#define u08_AVERAGE_CNT_PCU         3 
#define u08_AVERAGE_CNT_HALL        5 
/*#define u08_AVERAGE_CNT_ANTIPIN     3 */
#define u08_AVERAGE_CNT_THERMIST   10 
#define u08_AVERAGE_CNT_BATT        3 

#define u08_ADCH_SPIN_LH            0
#define u08_ADCH_BATT               1
#define u08_ADCH_THERMIST           2
/*#define u08_ADCH_SPIN_RH            3*/
#define u08_ADCH_PCU                4 
#define u08_ADCH_HALL_LH            5
/*#define u08_ADCH_HALL_RH            6*/
/*#define u08_ADCH_ANTI_LH            7*/
/*#define u08_ADCH_ANTI_RH            8*/


/********************************************************************************/
/*   name     : s_Get_Average_AD ,                                              */
/*   function : calcuration ADC average value                                   */
/********************************************************************************/
static void s_Get_Average_AD( stAdcSignal* stAD, U16 u16_s_getValue , U08 u08_s_average_cnt )
{
    if (stAD->Valid == 0)  
    {
        stAD->Read += (u16_s_getValue>>6);
        stAD->Count++; 
        
        if (stAD->Count >= u08_s_average_cnt) 
        {
            stAD->Valid = ON;
            stAD->Value = (U16)(stAD->Read / u08_s_average_cnt);
        } 
    } 
    else 
    {
        /* Subtract old sum vale to Avg. old value */
        stAD->Read -= stAD->Value;    
        stAD->Read += (u16_s_getValue>>6);
        stAD->Value = (U16)(stAD->Read / u08_s_average_cnt);
    }    
}
/********************************************************************************/
/*   name     : g_ADC_value_Init                                                */
/*   function : adc value init                                                  */
/*                                                                              */
/********************************************************************************/
void g_ADC_value_Init(void)
{
    st_g_ADCData.BattMon.Value = 0;
    st_g_ADCData.BattMon.Valid = 0;
    st_g_ADCData.BattMon.Read  = 0;
    st_g_ADCData.BattMon.Count = 0;
    
    st_g_ADCData.TempMon.Value = 0;
    st_g_ADCData.TempMon.Valid = 0;
    st_g_ADCData.TempMon.Read  = 0;
    st_g_ADCData.TempMon.Count = 0;
    
    st_g_ADCData.DriveVoltage.Value = 0;
    st_g_ADCData.DriveVoltage.Valid = 0;
    st_g_ADCData.DriveVoltage.Read  = 0;
    st_g_ADCData.DriveVoltage.Count = 0;    
    
    st_g_ADCData.SpindleCur_LH.Value = 0;
    st_g_ADCData.SpindleCur_LH.Valid = 0;
    st_g_ADCData.SpindleCur_LH.Read  = 0;
    st_g_ADCData.SpindleCur_LH.Count = 0;

    st_g_ADCData.SpindleCur_RH.Value = 0;
    st_g_ADCData.SpindleCur_RH.Valid = 0;
    st_g_ADCData.SpindleCur_RH.Read  = 0;
    st_g_ADCData.SpindleCur_RH.Count = 0;
   
    st_g_ADCData.PCUCurrent.Value = 0; 
    st_g_ADCData.PCUCurrent.Valid = 0; 
    st_g_ADCData.PCUCurrent.Read  = 0; 
    st_g_ADCData.PCUCurrent.Count = 0;
    
    st_g_ADCData.HallPower_LH.Value = 0;
    st_g_ADCData.HallPower_LH.Valid = 0;
    st_g_ADCData.HallPower_LH.Read  = 0;
    st_g_ADCData.HallPower_LH.Count = 0;
   
    st_g_ADCData.HallPower_RH.Value = 0;
    st_g_ADCData.HallPower_RH.Valid = 0;
    st_g_ADCData.HallPower_RH.Read  = 0; 
    st_g_ADCData.HallPower_RH.Count = 0;
    
    st_g_ADCData.Antipinch_LH.Value = 0;
    st_g_ADCData.Antipinch_LH.Valid = 0;
    st_g_ADCData.Antipinch_LH.Read  = 0;
    st_g_ADCData.Antipinch_LH.Count = 0;
    
    st_g_ADCData.Antipinch_RH.Value = 0;
    st_g_ADCData.Antipinch_RH.Valid = 0;
    st_g_ADCData.Antipinch_RH.Read  = 0;
    st_g_ADCData.Antipinch_RH.Count = 0;
    
}



/********************************************************************************/
/*   name     : g_ADC_ConversionPeriod5ms                                       */
/*   function : ADC value average claulation & valid check                      */
/*   5ms time polling task                                                      */
/********************************************************************************/
void g_ADC_ConversionPeriod5ms(void)
{
    static U16 u16_s_ADC_Result[9] = {0,0,0,0,0,0,0,0,0};
    static U08 u08_s_Check_count = 0;
    
    if( AD1_GetValue16(&(u16_s_ADC_Result[0])) != ERR_OK )  {}
    if( AD1_Measure(FALSE) != ERR_OK )    {}
    
    if( u08_s_Check_count == 0 )
    {            
        u08_s_Check_count++;
        if( u08_g_SpindlePowerMov_Direction == STOP_TAILGATE )
        {
        	  s_Get_Average_AD( &(st_g_ADCData.DriveVoltage) , u16_s_ADC_Result[u08_ADCH_SPIN_LH]  , u08_AVERAGE_CNT_SPINDLE ); 
        	  st_g_ADCData.SpindleCur_LH.Valid = 0;
        	  st_g_ADCData.SpindleCur_LH.Count = 0;
        	  st_g_ADCData.SpindleCur_LH.Read = 0;
        	  st_g_ADCData.SpindleCur_LH.Value = 0;
        }
        else	
        {	
            s_Get_Average_AD( &(st_g_ADCData.SpindleCur_LH) , u16_s_ADC_Result[u08_ADCH_SPIN_LH]  , u08_AVERAGE_CNT_SPINDLE );
            st_g_ADCData.DriveVoltage.Valid = 0;    
            st_g_ADCData.DriveVoltage.Count = 0;
            st_g_ADCData.DriveVoltage.Read = 0;
            /*st_g_ADCData.DriveVoltage.Value = 0;*/ /* Not Clear : Old value set */
        }
        /*s_Get_Average_AD( &(st_g_ADCData.SpindleCur_RH) , u16_s_ADC_Result[u08_ADCH_SPIN_RH]  , u08_AVERAGE_CNT_SPINDLE );*/
        s_Get_Average_AD( &(st_g_ADCData.BattMon      ) , u16_s_ADC_Result[u08_ADCH_BATT]     , u08_AVERAGE_CNT_BATT );    
        s_Get_Average_AD( &(st_g_ADCData.PCUCurrent   ) , u16_s_ADC_Result[u08_ADCH_PCU]      , u08_AVERAGE_CNT_PCU );
    }
    else
    {   
        u08_s_Check_count = 0;
        s_Get_Average_AD( &(st_g_ADCData.TempMon      ) , u16_s_ADC_Result[u08_ADCH_THERMIST] , u08_AVERAGE_CNT_THERMIST );	
        s_Get_Average_AD( &(st_g_ADCData.HallPower_LH ) , u16_s_ADC_Result[u08_ADCH_HALL_LH]  , u08_AVERAGE_CNT_HALL );
        /*s_Get_Average_AD( &(st_g_ADCData.HallPower_RH ) , u16_s_ADC_Result[u08_ADCH_HALL_RH]  , u08_AVERAGE_CNT_HALL );*/
        /*s_Get_Average_AD( &(st_g_ADCData.Antipinch_LH ) , u16_s_ADC_Result[u08_ADCH_ANTI_LH]  , u08_AVERAGE_CNT_ANTIPIN );*/
        /*s_Get_Average_AD( &(st_g_ADCData.Antipinch_RH ) , u16_s_ADC_Result[u08_ADCH_ANTI_RH]  , u08_AVERAGE_CNT_ANTIPIN );*/
    }
}
