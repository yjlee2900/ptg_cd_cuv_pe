/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-06-16 PM.  3:44:48                                   */
/*	Last Update     : 2013-06-16 PM.  3:44:50                                   */
/*	File Name       : Read_Hall_Sensor.h                                        */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define EEPROM_CONTROL_EXTERN
#include <string.h> 
#include "EEPROM_Control.h"       
#include "Sleep_Wakeup_Control.h"     
#include "CAN_Drive_App.h"
#include "User_Logic_Control.h"     
#include "Status_Detect.h"
#include "Parameterization.h"
#include "Digital_Input_Chattering.h"
#include "IEE1.h"
#include "STDLIB.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static U16 u16_s_Data_Checksum( U08 size, U08* pData );
static U08 u08_s_SET_SECTOR(IEE1_TAddress Addr, U16* Data, U08 size );
static U08 u08_s_GET_SECTOR(IEE1_TAddress Addr, U16* Data, U08 size );
static U08 u08_s_Read_PARA_EEP(void);
static U08 u08_s_Read_Diag_EEP(void);
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
/*#define u08_XXX_TABLE_CNT 4*/
/********************************************************************************/
/*   name     : u08_s_SET_SECTOR ,                                              */
/*   function :                                                                 */
/********************************************************************************/
static U08 u08_s_SET_SECTOR(IEE1_TAddress Addr, U16* Data, U08 size )
{
    /*U16 i;*/
    U08 i;
    U08 err = ERR_OK;
    IEE1_TAddress SecAddr;               /* EEPROM Sector address */ 
    static U16 BackupData[128];
    
    if((Addr < IEE1_AREA_START) || (Addr > IEE1_AREA_END)) 
    { /* Is given address out of EEPROM area array ? */
        err = ERR_RANGE;                  /* If yes then error */
    }
    else if(FSTAT_CCIF == 0) 
    {                    /* Is reading from EEPROM possible? */
        err = ERR_BUSY;                   /* If no then error */
    }
    else
    {    
        SecAddr = (IEE1_TAddress)((U32)Addr & 16776960); /* Sector Aligned address */
        
        for( i=0; i<(size/2); i++)
        {
            BackupData[i] = *Data; 
            Data++;
        }  
        g_TriggerWDT();
        
        err = IEE1_EraseSector(Addr);   /* Erase sector */
        
        g_TriggerWDT();
        
        if(err == ERR_OK) 
        {
            err = WriteBlock(SecAddr, 0, 256,BackupData); /* Restore sector, IEE1.h Rewite extern function */
        }
        g_TriggerWDT();
    }
    
    return(err);
}

/********************************************************************************/
/*   name     : u16_s_Data_Checksum ,                                           */
/*   function : get check sum data                                              */
/********************************************************************************/
static U08 u08_s_GET_SECTOR(IEE1_TAddress Addr, U16* Data, U08 size )
{
    U08 i;
    U08 err2 = ERR_OK;
    IEE1_TAddress SecAddr;
    IEE1_TAddress tempAddr; 
    
    if((Addr < IEE1_AREA_START) || (Addr > IEE1_AREA_END)) 
    { /* Is given address out of EEPROM area array ? */
      err2 = ERR_RANGE;                  /* If yes then error */
    }
    else if(FSTAT_CCIF == 0) 
    {                    /* Is reading from EEPROM possible? */
      err2 = ERR_BUSY;                   /* If no then error */
    }
    else
    {    
        /* tempAddr = (IEE1_TAddress)((dword)Addr & 16776960) - 0x4000 ;*/ /* Sector Aligned address */
        tempAddr = (IEE1_TAddress)((dword)Addr & 7936); /* Sector Aligned address */
        
        for( i=0; i<(size/2); i++)
        {
            SecAddr = (IEE1_TAddress)(tempAddr + i);
            *Data = *((word *far)SecAddr);
            
            Data++;       
        }
    }    

    return(err2);                       /* OK */
}

/********************************************************************************/
/*   name     : u08_s_Read_PARA_EEP                                             */
/*   function : Read parameter EEP data                                         */
/*                                                                              */
/********************************************************************************/
static U08 u08_s_Read_PARA_EEP(void)
{
    U08 Err_Cnt_R = 10;
    U08 Resert_Read;
    
    do
    {
        Resert_Read = u08_s_GET_SECTOR((IEE1_TAddress)EEP_PARA_ADDRESS, (U16*)&u16_g_EEP_Para.Start, sizeof(EEP_PTG_PARA_St));
        Err_Cnt_R--;
        if(Err_Cnt_R == 0)
        {
            break;
        }
        g_TriggerWDT();
    }while(Resert_Read != ERR_OK);
    
    return( Resert_Read );
}

/********************************************************************************/
/*   name     : u08_s_Read_Diag_EEP                                             */
/*   function : Read diag EEP data                                              */
/*                                                                              */
/********************************************************************************/
static U08 u08_s_Read_Diag_EEP(void)
{
    U08 Err_Cnt_d = 10;
    U08 Resert_Read1;
    
    do
    {
        Resert_Read1 = u08_s_GET_SECTOR((IEE1_TAddress)EEP_DIAG_ADDRESS, (U16*)&u16_g_EEP_Diag.Start, sizeof(EEP_PTG_Diag_St));
        Err_Cnt_d--;
        if(Err_Cnt_d == 0)
        {
            break;
        }   
        g_TriggerWDT();
    }while(Resert_Read1 != ERR_OK);
    
    return( Resert_Read1 );
}

/********************************************************************************/
/*   name     : u16_s_Data_Checksum ,                                           */
/*   function : get check sum data                                              */
/********************************************************************************/
static U16 u16_s_Data_Checksum( U08 size, U08* pData )
{
    U08 i;
    U16 CheckSumBuff = 0;
    
    for( i=0; i<size; i++)
    {
        CheckSumBuff += *pData;
        pData++;
    }
    CheckSumBuff ^= 0xFFFFU;
    
    return( CheckSumBuff );   
}

/********************************************************************************/
/*   name     : g_Read_All_EEP ,                                                */
/*   function : get check sum data                                              */
/********************************************************************************/
void g_Read_All_EEP( void )
{
    U08 Resert_Read3;
    U16 Resert_CRC;

    u08_g_EEP_Write_Event = FALSE;    

    Resert_Read3 = u08_s_Read_PARA_EEP();
    Resert_CRC = u16_s_Data_Checksum( sizeof(EEP_PTG_PARA_St)-2, (U08 *)&u16_g_EEP_Para.Start );
    
    if( (Resert_Read3 != ERR_OK) || ( u16_g_EEP_Para.EndCRC != Resert_CRC ) || 
        (u16_g_EEP_Para.Start != 'P' ) || (u16_g_EEP_Para.MaxBasicPos >= SPINDLE_MAX_MECH_LIMIT) || (u16_g_EEP_Para.MaxBasicPos < 100)  )
    {
        u08_g_Init_Param = OFF;
        u16_g_EEP_Para.User_Hight_SetValue = 4; /* 0: Default -> 4 : 100% avn menu setting */
    }
    else
    {
        u08_g_Init_Param = ON;
    }         
    
    Resert_Read3 = u08_s_Read_Diag_EEP();
    Resert_CRC = u16_s_Data_Checksum( sizeof(EEP_PTG_Diag_St)-2, (U08 *)&u16_g_EEP_Diag.Start );
    if( (Resert_Read3 != ERR_OK) || ( u16_g_EEP_Diag.EndCRC != Resert_CRC ) || (u16_g_EEP_Diag.Start != 'P' ) )
    {
        PtlTrans.CanBusOffB = FALSE;
        PtlTrans.CanBusOffB_Old = TRUE; /* for rewirte */
        PtlTrans.CanLineErrorB = FALSE;
        PtlTrans.CanLineErrorB_Old = TRUE; /* for rewirte */
        u16_g_EEP_Diag.PGTM_Enable = TRUE;
        u08_g_PTGM_Power_Mode = TRUE;
        u16_g_EEP_Diag.EEPIGNCount = 0;
        u16_g_EEP_Diag.InnerSw_Short = FALSE;
        u16_g_EEP_Diag.OS_HDLSw_Short = FALSE;        
        u16_g_EEP_Diag.ConsoleSw_Short = FALSE;
        u08_g_IO_InputFault = 0U;
        u16_g_EEP_Diag.IO_InputFault = 0;
        u08_g_IGN_Count = 0;
        u16_g_EEP_Diag.InLine_Mode = 0;
        u16_g_EEP_Diag.Pre_InLine_Mode = 0;
        u16_g_EEP_Diag.HallError_LH = 0; 
        u16_g_EEP_Diag.HallError_RH = 0; 
        u16_g_EEP_Diag.APS_Error_LH = 0;
        u16_g_EEP_Diag.APS_Error_RH = 0;
        u16_g_EEP_Diag.UnLatch_Error = 0;
        u16_g_EEP_Diag.PastFailure_Index = 0;
        u16_g_EEP_Diag.PastFailure[0] = 0;
        u16_g_EEP_Diag.PastFailure[1] = 0;
        u16_g_EEP_Diag.PastFailure[2] = 0;
        u16_g_EEP_Diag.Speed_Set_Value = 1; /* default : Fast */
        u16_g_EEP_Diag.Start =(U08)'P';    
    } 
    else
    {
        /* check diagnostic data init */
        if( u16_g_EEP_Diag.EEPCanLineErrorB == FALSE )
        {    
            PtlTrans.CanLineErrorB = TRUE;
            PtlTrans.CanLineErrorB_Old = TRUE;
        }
        else
        {
            PtlTrans.CanLineErrorB = FALSE;
            PtlTrans.CanLineErrorB_Old = FALSE;
        }
        
        if( u16_g_EEP_Diag.EEPCanBusOffB == FALSE )
        {
            PtlTrans.CanBusOffB = TRUE;
            PtlTrans.CanBusOffB_Old = TRUE;
        }
        else
        {
            PtlTrans.CanBusOffB = FALSE;
            PtlTrans.CanBusOffB_Old = FALSE;
        }
        
        u08_g_IGN_Count = (U08)u16_g_EEP_Diag.EEPIGNCount;  
        if( u16_g_EEP_Diag.PGTM_Enable != 0 )
        {      
            u08_g_PTGM_Power_Mode = TRUE;
        }
        else
        {
            u08_g_PTGM_Power_Mode = FALSE;  
        }
            
        /*if( u16_g_EEP_Diag.Pre_InLine_Mode == TRUE )
        {
            PtlTrans.Pre_Inline_Mode = TRUE;
        }
        else
        {
            PtlTrans.Pre_Inline_Mode = FALSE;
        }*/        
        
        u08_g_IO_InputFault = (U08)u16_g_EEP_Diag.IO_InputFault;
    }       
}

/********************************************************************************/
/*   name     : g_Write_Para_Sector                                             */
/*   function : Write eeprom parameter data                                     */
/*                                                                              */
/********************************************************************************/
U08 u08_g_Write_Para_Sector(void)
{
    U08 Err_Cnt = 10;
    U08 Resert_eep = 0;
    
    u16_g_EEP_Para.EndCRC = u16_s_Data_Checksum( sizeof(EEP_PTG_PARA_St)-2, (U08 *)&u16_g_EEP_Para.Start );
    
    do
    {
         Resert_eep = u08_s_SET_SECTOR((IEE1_TAddress)EEP_PARA_ADDRESS, (U16*)&u16_g_EEP_Para.Start, sizeof(EEP_PTG_PARA_St));
         /* clear watch dog */
         Err_Cnt--;
         if(Err_Cnt == 0)
         {
            break;
         }     
                
    }while( Resert_eep != ERR_OK );
    
    return(Resert_eep); 
}

/********************************************************************************/
/*   name     : u08_g_Write_Diag_Sector                                         */
/*   function : Write eeprom Diagnositc data                                    */
/*                                                                              */
/********************************************************************************/
U08 u08_g_Write_Diag_Sector(void)
{
    U08 Err_Cnt1 = 10;
    U08 Resert_eep1 = 0;
    
    u16_g_EEP_Diag.EndCRC = u16_s_Data_Checksum( sizeof(EEP_PTG_Diag_St)-2, (U08 *)&u16_g_EEP_Diag.Start );
    
    do
    {
         Resert_eep1 = u08_s_SET_SECTOR((IEE1_TAddress)EEP_DIAG_ADDRESS, (U16*)&u16_g_EEP_Diag.Start, sizeof(EEP_PTG_Diag_St));
         /* clear watch dog */
         Err_Cnt1--;
         if(Err_Cnt1 == 0)
         {
            break;
         }     
                
    }while( Resert_eep1 != ERR_OK );
    
    return(Resert_eep1); 
}
