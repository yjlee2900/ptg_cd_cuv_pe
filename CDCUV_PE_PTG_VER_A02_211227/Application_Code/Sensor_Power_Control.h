/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-23 AM.  9:25:42                                   */
/*  Last update     : 2013-05-23 AM.  9:25:45                                   */
/*  File Name       : Sensor_Power_Control.h                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		SENSOR_POWER_H_EXTERN
#define		SENSOR_POWER_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	SENSOR_POWER_CONTROL_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define NO_RETRY_OFF  2U
#define OVER_VOLTAGE_OFF  3U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 u08_g_AntiSensor_Power_St;
EXTERN U08 u08_g_HallPowerOnErr_LH;
/*EXTERN U08 u08_g_HallPowerOnErr_RH;*/
EXTERN U08 u08_g_HallPowerOffErr_LH;
/*EXTERN U08 u08_g_HallPowerOffErr_RH;*/
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
/*EXTERN  void g_AntiSensor_Power_Out( U08 OnOffSt );*/
/*EXTERN  void g_Hall_Power_Out_RH(U08 OnOffStR );*/
EXTERN  void g_Hall_Power_Out_LH(U08 OnOffStL );
EXTERN	void g_SensorPowerControl_10msPeroid( void );
EXTERN  void g_SensorPowerControl_100msPeroid( void );

#undef EXTERN

#endif
