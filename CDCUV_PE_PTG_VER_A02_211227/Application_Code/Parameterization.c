/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-20 PM.  2:22:59                                   */
/*  Last update     : 2013-12-12 AM.  11:23:07                                  */
/*  File Name       : Parameterization.c                                        */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define PARAMETERIZAITON_EXTERN
#include "Basic_Type_Define.h"
#include "AP_DataType.h"
#include "Parameterization.h"
#include "EEPROM_Control.h"
#include "Read_Hall_Sensor.h"
#include "Digital_Input_Chattering.h"
#include "Status_Detect.h"
#include "Hazard_Buzzer_Control.h"
#include "Spindle_Motor_Drive.h"
#include "Control_Command.h"      
#include "PID_Control.h"
#include "IEE1.h"
#include "User_Logic_Control.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U08 u08_s_User_Position_Buzzer = 0;

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
/*static void s_Get_Local_Temp( void );*/

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define PARA_SCAN_MAX_TIME        1200 /* 10ms task * 1200 = 12Sec */

/*#define SYNC_MECH_LIMIT             80*/  /* Tunning point */
/********************************************************************************/
/*   name     : s_Get_XXXXXX ,                                                  */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
/*static void s_Get_Local_Temp( void )
{
    u08_s_local_value = 0;
}*/

/********************************************************************************/
/*   name     : g_Param_Init                                                     */
/*   function :                                                                 */
/*   Parameterization value init                                                */
/********************************************************************************/
void g_Param_Init(void)
{
    S16 Temp_Buff2 = 0; /* 140515 SQA */
    
    u08_g_ParamStatus = PARAM_NONE;
    u16_g_ParamerInitTimer = 0;
    
    if( u08_g_Init_Param == OFF )
    {
        u16_g_EEP_Para.LearnedMechEndPos = MECHANICAL_END_CONST;
        Temp_Buff2 = (u16_g_EEP_Para.LearnedMechEndPos/20); /* 5% */
        u16_g_EEP_Para.MaxBasicPos = ( u16_g_EEP_Para.LearnedMechEndPos - Temp_Buff2 ); /* 95% */  
        u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.MaxBasicPos; 
        u16_g_EEP_Para.UserSetPosition = u16_g_EEP_Para.MaxBasicPos;
        Temp_Buff2 = (u16_g_EEP_Para.MaxPowerOpenPos / 10 );
        u16_g_EEP_Para.ZONE3Pos = Temp_Buff2; 
        u16_g_EEP_Para.ZONE2Pos = ( Temp_Buff2 / 2 );
        u08_g_Init_Param = OFF;     
        u08_g_ParamStatus = PARAM_NONE;
    }
        
    s16_g_LH_Position_Cnt = 0; /* u16_g_EEP_Para.MaxPowerOpenPos; */
    s16_g_RH_Position_Cnt = 0; /* u16_g_EEP_Para.MaxPowerOpenPos; */
}

/********************************************************************************/
/*   name     : g_User_Open_Position                                            */
/*   function :                                                                 */
/*   user setting Power Open Position                                           */
/********************************************************************************/
void g_User_Open_Position(void)
{
    if( (u08_g_Init_Param == ON) && (u08_g_Power_OnReset == 0) )
    {    
        if( s16_g_LH_Position_Cnt  > (u16_g_EEP_Para.ZONE3Pos + 25) )
        {
            if( s16_g_LH_Position_Cnt >= u16_g_EEP_Para.LearnedMechEndPos )
            {    
                u16_g_EEP_Para.MaxPowerOpenPos = (u16_g_EEP_Para.LearnedMechEndPos - ((u16_g_EEP_Para.LearnedMechEndPos/20)-20));
                u16_g_EEP_Para.UserSetPosition = u16_g_EEP_Para.MaxPowerOpenPos;
            }
            else
            {        
                u16_g_EEP_Para.MaxPowerOpenPos = s16_g_LH_Position_Cnt;
                u16_g_EEP_Para.MaxPowerOpenPos -= 20;
                u16_g_EEP_Para.UserSetPosition = u16_g_EEP_Para.MaxPowerOpenPos;
            }
            u16_g_EEP_Para.User_Hight_SetValue = 0x06; /* user position Set value */
            u08_g_ParamStatus = PARAM_SAVE;
            u08_s_User_Position_Buzzer = 1;
        }
    }
}

/********************************************************************************/
/*   name     : g_Parameterization_10msPeriod                                   */
/*   function :                                                                 */
/*   Parameterization function 10ms polling                                     */
/********************************************************************************/
void g_Parameterization_10msPeriod(void)
{
    S16 Temp_Buff; /* 140515 SQA */
    /*S16 Temp_Buff2;*/ /* 140515 SQA */
    U08 Dflash_Write_St;
    
    switch( u08_g_ParamStatus )
    {
        case PARAM_NONE : 
            if( u08_g_Init_Param == OFF )
            {
                if( u08_g_LatchSt == LATCH_OPEN  )
                {
                    u08_g_ParamStatus = PARAM_WAIT;
                }    
            }    
            break;
        case PARAM_WAIT : 
            if( u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE )
            {
                u16_g_ParamerInitTimer = PARA_SCAN_MAX_TIME;
                /*g_SetBuzzHazardControl( BUZZONLY_ON, 30 );*/
                
                u08_g_ParamStatus = PARAM_RUN;
            }    
            break;
        case PARAM_RUN : 
            if( u16_g_ParamerInitTimer > 0 )
            {
                u16_g_ParamerInitTimer--;
                if( u16_g_ParamerInitTimer == 1050 )
                {
                    if( u16_g_EEP_Diag.InLine_Mode == TRUE )
                    {    
                        u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;  
                    }
                    else
                    {
                        u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    }    
                    u08_g_Buzzer_Hazard_Count = 30;                 	
                }	
                if( u08_g_SpindlePowerMov_Direction == STOP_TAILGATE )
                {
                    /*g_SetBuzzHazardControl( BUZZHAZA_OFF, 0 );*/
                    u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF;  
                    u08_g_Buzzer_Hazard_Count = 0; 
                    
                    u08_g_ParamStatus = PARAM_CHECK;
                }
                
                if( u16_g_ParamerInitTimer == 0 )     
                {
                    u08_g_Tailgate_Stop_Cmd = ON;
                    u08_g_ParamStatus = PARAM_ERROR;
                    /*g_SetBuzzHazardControl( BUZZONLY_ON, 6 );*/
                    u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    u08_g_Buzzer_Hazard_Count = 6;
                }
            }    
            break;
        case PARAM_CHECK : 
            if( (s16_g_LH_Position_Cnt < SPINDLE_MAX_MECH_LIMIT) && (s16_g_LH_Position_Cnt > SPINDLE_MIN_MECH_LIMIT) )
            {
                u16_g_EEP_Para.LearnedMechEndPos = s16_g_LH_Position_Cnt;
                u16_g_EEP_Para.Start = (U08)'P'; /* P : 0x50 Parameter eep Set check */
                Temp_Buff = (u16_g_EEP_Para.LearnedMechEndPos/20); /* 5% */
                u16_g_EEP_Para.MaxBasicPos = ( u16_g_EEP_Para.LearnedMechEndPos - (Temp_Buff+20) ); /* 90% */  
                u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.MaxBasicPos;
                u16_g_EEP_Para.UserSetPosition = u16_g_EEP_Para.MaxBasicPos;
                Temp_Buff = (u16_g_EEP_Para.MaxPowerOpenPos / 10 );
                u16_g_EEP_Para.ZONE3Pos = Temp_Buff; 
                u16_g_EEP_Para.ZONE2Pos = ( Temp_Buff / 2 ); 
                
                if( u08_g_User_Position_set != 1 )
                {
                    u08_g_ParamStatus = PARAM_SAVE;
                    u16_g_EEP_Para.User_Hight_SetValue = 4;
                }
                else
                {
                	u08_g_Buzzer_Hazard_Mode = HAZAONLY_ON;
                    u08_g_Buzzer_Hazard_Count = 4;    
                    u08_g_Init_Param = ON;                   
                }
            }
            else
            {
                u08_g_ParamStatus = PARAM_ERROR;
            }        
            break;
        case PARAM_SAVE : 
            if( (st_g_BuzzHaza.CountTick == 0) && (st_g_BuzzHaza.TimeTick == 0) )
            {
                Cpu_DisableInt();
                Dflash_Write_St = u08_g_Write_Para_Sector();
                if( Dflash_Write_St != ERR_OK )
                {    
                    Dflash_Write_St = u08_g_Write_Para_Sector();
                }
                                  
                Cpu_EnableInt();      /* QAC_correct_jaeho */                                   
                if( Dflash_Write_St == ERR_OK )
                {
                    u08_g_ParamStatus = PARAM_NONE;
                    u08_g_Init_Param = ON;
                    g_PID_Tartget_Position_Init();
                    /*g_SetBuzzHazardControl( BUZZHAZA_ON, 4);*/  /* 1.5 SEC Delay and buzzer & hazard */
                    if (u08_g_User_Position_set != 1)
                    {
                        if( u08_s_User_Position_Buzzer == 1 )
                        {    
                            u08_g_Buzzer_Hazard_Mode = SHORT_BUZZER;  
                        }
                        else
                        {
                            u08_g_Buzzer_Hazard_Mode = HAZAONLY_ON;
                        }   
                        u08_g_Buzzer_Hazard_Count = 4;
                    }
                    u08_g_User_Position_set = 0;
                }
                else
                {
                    u08_g_ParamStatus = PARAM_ERROR;       
               	    /*g_SetBuzzHazardControl( BUZZONLY_ON, 6);*/
                    u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    u08_g_Buzzer_Hazard_Count = 6;
                }        
                u08_s_User_Position_Buzzer = 0; 
            } 
            break;
        case PARAM_ERROR : 
            u16_g_EEP_Para.LearnedMechEndPos = MECHANICAL_END_CONST;
            Temp_Buff = (u16_g_EEP_Para.LearnedMechEndPos/20); /* 5% */
            u16_g_EEP_Para.MaxBasicPos = ( u16_g_EEP_Para.LearnedMechEndPos - Temp_Buff ); /* 95% */  
            u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.MaxBasicPos; 
            u16_g_EEP_Para.UserSetPosition = u16_g_EEP_Para.MaxBasicPos;
            Temp_Buff = (u16_g_EEP_Para.MaxPowerOpenPos / 10 );
            u16_g_EEP_Para.ZONE3Pos = Temp_Buff; 
            u16_g_EEP_Para.ZONE2Pos = ( Temp_Buff / 2 ); 
            u08_g_Init_Param = OFF;     
            u08_g_ParamStatus = PARAM_NONE;
            break;  
        default :
            break;                                                          
    }
}

/********************************************************************************/
/*   name     : g_Parameter_Reset                                               */
/*   function :                                                                 */
/*   Parameterization value User Reset                                          */
/********************************************************************************/
void g_Parameter_Reset(void)
{
    U08 u08_s_check = 0;                                 
    
    u16_g_EEP_Para.LearnedMechEndPos = 0;
    u16_g_EEP_Para.MaxBasicPos = MECHANICAL_END_CONST; 
    u16_g_EEP_Para.MaxPowerOpenPos = MECHANICAL_END_CONST;
    u16_g_EEP_Para.UserSetPosition = MECHANICAL_END_CONST;
    u16_g_EEP_Para.ZONE3Pos = 0;  
    u16_g_EEP_Para.ZONE2Pos = 0;  
    u16_g_EEP_Para.User_Hight_SetValue = 4;
    u08_g_Init_Param = OFF;     
    u08_g_ParamStatus = PARAM_NONE;    
    
    s16_g_LH_Position_Cnt = u16_g_EEP_Para.MaxBasicPos;
    s16_g_RH_Position_Cnt = u16_g_EEP_Para.MaxBasicPos;

    Cpu_DisableInt();                  

    u08_s_check = u08_g_Write_Para_Sector();

    if (u08_s_check != ERR_OK) 
    {
        u08_s_check = u08_g_Write_Para_Sector();
        
        Cpu_EnableInt();
        if (u08_s_check != ERR_OK) 
        {
            /*g_SetBuzzHazardControl( BUZZONLY_ON, 6); */
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6;           
        } 
        else 
        {
            /*g_SetBuzzHazardControl( BUZZONLY_ON, 2);    */
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 2;
        } 
    }
    else
    {
        Cpu_EnableInt();
        /*g_SetBuzzHazardControl( BUZZONLY_ON, 2);*/
        u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
        u08_g_Buzzer_Hazard_Count = 2;
    } 
    
}
