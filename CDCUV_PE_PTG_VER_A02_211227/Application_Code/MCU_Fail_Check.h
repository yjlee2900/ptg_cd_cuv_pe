/*=============================================================================*/
/*                                                                             */
/* Project         : PTG(Power Tailgate ) for CD                               */
/* Copyright       : MOTOTECH CO. LTD.                                         */
/* Author          : HeeSub-Song                                               */
/* Created date    : 2018-01-11 PM.  09:15:07                                  */
/* Last update     : 2018-01-11 PM.  09:15:07                                  */
/* File Name       : DrvCore.h                                                 */
/* Processor       : MC9S12P64                                                 */
/* Version         :                                                           */
/* Compiler        : HI-WIRE                                                   */
/* IDE             : CodeWarrior IDE VERSION 5.1                               */
/*                                                                             */
/*=============================================================================*/

/*=============================================================================*/
/*                        MODULE DEFINITION                                    */
/*=============================================================================*/
#ifndef     DRV_CORE_H_EXTERN
#define     DRV_CORE_H_EXTERN

#include "Basic_Type_Define.h"

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

#ifndef     DRV_CORE_EXTERN
#define     EXTERN     extern
#else
#define     EXTERN
#endif


/*=============================================================================*/
/*  Types                                                                      */
/*=============================================================================*/


/*=============================================================================*/
/*  Macro                                                                      */
/*=============================================================================*/
#define CORE_SEQUENCE_1		0U
#define CORE_SEQUENCE_2		1U
#define CORE_SEQUENCE_3		2U
#define CORE_SEQUENCE_4		3U
#define CORE_SEQUENCE_5		4U
#define CORE_SEQUENCE_6		5U

#define CORE_SEQUENCE_SUM	15U


/*=============================================================================*/
/*  Global Variable                                                            */
/*=============================================================================*/
EXTERN U08 u08_g_CoreFaultSts;
EXTERN U08 u08_g_TaskCheckIndex;


/*=============================================================================*/
/*  extern function                                                            */
/*=============================================================================*/
EXTERN void g_MCU_FailCheckPeriod500ms(void);
EXTERN void g_MCU_FailCheckPeriod100ms(void);
EXTERN void g_DrvCoreRegCheck(void);
EXTERN void g_DrvCoreInit(void);
EXTERN void g_DrvCoreSquenceCheck(U08 Sequence_Num );

#undef EXTERN

#endif

/*================================================================================*/
/*================================================================================*/
/*================================================================================*/
/*================================================================================*/



