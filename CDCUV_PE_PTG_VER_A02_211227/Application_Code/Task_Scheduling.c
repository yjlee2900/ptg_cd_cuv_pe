/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-15 AM.  10:31:03                                  */
/*  Last update     : 2014-03-11 PM.   2:58:16                                  */
/*  File Name       : Task_Scheduling.c                                         */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define TASK_SCHEDULING_EXTERN

#include "Basic_Type_Define.h"
#include "Task_Scheduling.h"
#include "CAN_Drive_App.h"
#include "CAN_Signal_Handling.h"
#include "Digital_Input_Chattering.h"
#include "Analog_Conversion.h"
#include "Sensor_Power_Control.h"
#include "Read_Hall_Sensor.h"
#include "Status_Detect.h"
#include "Parameterization.h"
#include "Hazard_Buzzer_Control.h"
#include "Illumination_Control.h"
#include "Thermal_Protect.h" 
#include "Obstacle_Detect.h"
#include "Spindle_Sync_Control.h"
#include "Control_Command.h"
#include "PCU_Sequence.h"
#include "User_Logic_Control.h"
#include "Sleep_Wakeup_Control.h"
#include "MCU_Fail_Check.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
/* static U08 u08_s_local_value; */

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_TASK_Periodic1ms( void ); 
static void s_TASK_Periodic5ms( void );
static void s_TASK_Periodic10ms_1( void );
static void s_TASK_Periodic10ms_2( void );
static void s_TASK_Periodic10ms_3( void );
static void s_TASK_Periodic20ms( void );
static void s_TASK_Periodic100ms( void );
static void s_TASK_Periodic500ms( void );
/* static void s_TASK_Periodic1Sec( void ); */
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
/* #define u08_XXX_TABLE_CNT 4 */

/********************************************************************************/
/*   name     : s_TASK_Periodic1ms                                              */
/*   function : 1ms polling tesk                                                */
/********************************************************************************/
static void s_TASK_Periodic1ms( void )
{
    
    g_Get_Motor_RPM_1msPeriod();
    g_Spindle_Moving_1msPeriod();
    g_IO_InputHandlerPeriod1ms();
    g_PCU_Rewind_Break_1msperiod();
} 
/********************************************************************************/
/*   name     : s_TASK_Periodic5ms                                              */
/*   function : 5ms polling tesk                                                */
/********************************************************************************/
static void s_TASK_Periodic5ms( void )
{
    g_IO_InputHandlerPeriod5ms();
    g_ADC_ConversionPeriod5ms();
    g_StatusDetect_Period5ms();
    g_Obstacle_Sensor_5msPeriod();
    
}
/********************************************************************************/
/*   name     : s_TASK_Periodic10ms                                             */
/*   function : 10ms polling tesk                                               */
/********************************************************************************/
static void s_TASK_Periodic10ms_1( void )
{                    
    g_TriggerWDT();
    g_BCAN_Handle_10msPeriod_1();
    g_BCAN_Handle_10msPeriod_2();
    g_CAN_Signal_Handling1_10msPeriod_TX();
    g_CAN_Signal_Handling1_10msPeriod_1();
    g_CAN_Signal_Handling2_10msPeriod_2();
    g_CAN_Signal_Handling3_10msPeriod_3();
    g_StatusDetect_Period10ms();

}
/********************************************************************************/
/*   name     : s_TASK_Periodic10ms_2                                             */
/*   function : 10ms polling tesk                                               */
/********************************************************************************/
static void s_TASK_Periodic10ms_2( void )
{
    g_PCU_SequenceControl_10msPeriod();
    g_HallDirectionFaultCheck_10msPeriod();
    g_User_Logic_10msPeriod();
    g_Tailgate_Control_10msPeriod();
    g_Spindle_MoveStart_10msPeriod();
    g_SensorPowerControl_10msPeroid();
    g_Obstacle_Spindle_10msPeriod();
    g_Spindle_Thermal_10msPeriod();
    
}
/********************************************************************************/
/*   name     : s_TASK_Periodic10ms_2                                             */
/*   function : 10ms polling tesk                                               */
/********************************************************************************/
static void s_TASK_Periodic10ms_3( void )
{
	  g_PCU_Thermal_10msPeriod();
    g_Parameterization_10msPeriod();
    g_BuzzHazardOut10msPeriod();
    g_Illumination_Control_10msPeriod();
    g_SleepWake_Control_10msPeriod();
    /* */
    /* */
}

/********************************************************************************/
/*   name     : s_TASK_Periodic20ms                                             */
/*   function : 20ms polling tesk                                               */
/********************************************************************************/
static void s_TASK_Periodic20ms( void )
{                      
    /*g_Spindle_SyncControl_20msPeriod(); */
}
/********************************************************************************/
/*   name     : s_TASK_Periodic100ms                                            */
/*   function : 100ms polling tesk                                              */
/********************************************************************************/
static void s_TASK_Periodic100ms( void )
{
    g_DTC_Clear_by_IGN();
    g_StatusDetet_Period100ms();
    g_SensorPowerControl_100msPeroid();
    g_MCU_FailCheckPeriod100ms();
    g_PTGOpnClsStUpdate_Period100ms();
}
/********************************************************************************/
/*   name     : s_TASK_Periodic500ms                                            */
/*   function : 500ms polling tesk                                              */
/********************************************************************************/
static void s_TASK_Periodic500ms( void )
{
	  g_StatusDetet_Period500ms();
	  g_MCU_FailCheckPeriod500ms();
}
/********************************************************************************/
/*   name     : g_TASK_Init                                                     */
/*   function : task scheduling count, event value initialize                   */
/*                                                                              */
/********************************************************************************/
void g_TASK_Init(void)
{
    st_g_TimeEvent.bOn1ms        =    0;                      /* 1ms x 1    */
    st_g_TimeEvent.bOn5ms        =    0;                      /* 1ms x 5    */
    st_g_TimeEvent.bOn10ms       =    0;                      /* 1ms x 10   */
    st_g_TimeEvent.bOn20ms       =    0;                      /* 1ms x 20   */
    st_g_TimeEvent.bOn100ms      =    0;                      /* 100ms x 1  */
    st_g_TimeEvent.bOn500ms      =    0;                      /* 100ms x 2  */
    st_g_TimeEvent.bOn1S         =    0;                      /* 100ms x 10 */

    /* set task starting time */
    /* Being counted in the 1msec timer */
    st_g_Timer1ms.nCount1ms      =    0;
    st_g_Timer1ms.nCount5ms      =    0;
    st_g_Timer1ms.nCount10ms     =    0;
    st_g_Timer1ms.nCount20ms     =    0;

    /* Being counted in the 100msec timer */
    st_g_Timer100ms.nCount100ms  =    0;
    st_g_Timer100ms.nCount500ms  =    0;
    st_g_Timer100ms.nCount1S     =    0;
    st_g_Timer100ms.nCount10S    =    0;
 
}
        
/********************************************************************************/
/*   name     : g_Task_Timer_Count_1msEvent                                     */
/*   function : task time check 1ms timer Event function call                   */
/*                                                                              */
/********************************************************************************/         
void g_Task_Timer_Count_1msEvent( void )
{
    st_g_TimeEvent.bOn1ms = 1;
    st_g_Timer1ms.nCount1ms++;
    
    if( st_g_Timer1ms.nCount1ms >= 5 )
    {
        st_g_Timer1ms.nCount1ms = 0;
        st_g_TimeEvent.bOn5ms = 1;
        st_g_Timer1ms.nCount5ms++;
        
        if( st_g_Timer1ms.nCount5ms >= 2 )
        {
            st_g_Timer1ms.nCount5ms = 0;
            st_g_TimeEvent.bOn10ms = 1;
            st_g_Timer1ms.nCount10ms++;
        }
    }
    
    if( st_g_Timer1ms.nCount10ms >= 2 )
    {
        st_g_Timer1ms.nCount10ms = 0;
        st_g_TimeEvent.bOn20ms = 1;
        st_g_Timer1ms.nCount20ms++;
        
        if( st_g_Timer1ms.nCount20ms >= 5 )
        {
            st_g_Timer1ms.nCount20ms = 0;
            st_g_TimeEvent.bOn100ms = 1;
            st_g_Timer100ms.nCount100ms++;
            if( st_g_Timer100ms.nCount100ms >= 5 )
            {
                st_g_Timer100ms.nCount100ms = 0;
                st_g_TimeEvent.bOn500ms = 1;
                st_g_Timer100ms.nCount500ms++;
                if( st_g_Timer100ms.nCount500ms >= 2 )
                {	
                    st_g_Timer100ms.nCount500ms = 0;
                    st_g_TimeEvent.bOn1S = 1;
                }
            }    
        }
    }
}       
        
/********************************************************************************/
/*   name     : g_Task_Scheduling                                               */
/*   function : main task Operating function                                    */
/*                                                                              */
/********************************************************************************/        
void g_Task_Scheduling( void )
{
    if( st_g_TimeEvent.bOn1ms == 1 )
    {
        st_g_TimeEvent.bOn1ms = 0;
        s_TASK_Periodic1ms();
        g_DrvCoreSquenceCheck(CORE_SEQUENCE_1);
    } 
    
    if( st_g_TimeEvent.bOn5ms == 1 )
    {
        st_g_TimeEvent.bOn5ms = 0;
        s_TASK_Periodic5ms();   
        g_DrvCoreSquenceCheck(CORE_SEQUENCE_2);
    }
    
    if( st_g_TimeEvent.bOn10ms == 1 )
    {
        st_g_TimeEvent.bOn10ms = 0;
        s_TASK_Periodic10ms_1();
        s_TASK_Periodic10ms_2();
        s_TASK_Periodic10ms_3();
        g_DrvCoreSquenceCheck(CORE_SEQUENCE_3);
    }
    
    if( st_g_TimeEvent.bOn20ms == 1 )
    {
        st_g_TimeEvent.bOn20ms = 0;
        s_TASK_Periodic20ms();
        g_DrvCoreSquenceCheck(CORE_SEQUENCE_4);
    }    
    
    if( st_g_TimeEvent.bOn100ms == 1 )
    {
        st_g_TimeEvent.bOn100ms = 0;
        s_TASK_Periodic100ms();
        g_DrvCoreSquenceCheck(CORE_SEQUENCE_5);
    }
    
    if( st_g_TimeEvent.bOn500ms == 1 )
    {
    	  st_g_TimeEvent.bOn500ms = 0;
    	  s_TASK_Periodic500ms();
    	  g_DrvCoreSquenceCheck(CORE_SEQUENCE_6);
    }	
}        
