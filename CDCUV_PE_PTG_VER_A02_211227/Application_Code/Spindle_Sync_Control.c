/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-31 PM. 3:24:11                                    */
/*  Last update     : 2013-06-03 PM. 10:05:31                                   */
/*  File Name       : Spindle_Sync_Control.c                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define SPINDLE_SYNC_EXTERN
#include "Basic_Type_Define.h"
#include "Spindle_Sync_Control.h"
#include "Spindle_Motor_Drive.h"
#include "Read_Hall_Sensor.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U08 u08_s_SyncMode = 0;
static U16 u16_s_PositionGap = 0;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Get_Spindle_PositionGap( void );

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define RH_FIT_POS           0U
#define RH_LOW_POS           1U
#define RH_HI_POS            2U

#define SYNC_FAIL_LIMIT     75  /* Tunning Point */
#define SYNC_CORRECT_MIN    10   /* Tunning Point */

#define MAX_INCREMENT_RPM   10    /* Tunning Point */
#define MAX_DECREMENT_RPM   10   /* Tunning Point */
#define INCREMNET_RPM_RATIO  1    /* Tunning Point */
#define DECREMNET_RPM_RATIO  1    /* Tunning Point */
/********************************************************************************/
/*   name     : s_Get_Spindle_PositionGap                                       */
/*   function : spindle Motor Hall count LH, RH Gap check                       */
/********************************************************************************/
static void s_Get_Spindle_PositionGap( void )
{
    
    if( s16_g_LH_Position_Cnt > s16_g_RH_Position_Cnt )
    {
        u16_s_PositionGap = (U08)( s16_g_LH_Position_Cnt - s16_g_RH_Position_Cnt );        
        
        if( u16_s_PositionGap > SYNC_CORRECT_MIN )
        {
            if( u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE ) 
            {
                u08_s_SyncMode = RH_HI_POS;
            }
            else
            {
                u08_s_SyncMode = RH_LOW_POS; 
            }    
        }    
        else
        {
            u08_s_SyncMode = RH_FIT_POS;
        }    
    }
    else if(  s16_g_LH_Position_Cnt < s16_g_RH_Position_Cnt )
    {
        u16_s_PositionGap = (U08)( s16_g_RH_Position_Cnt - s16_g_LH_Position_Cnt );
        if( u16_s_PositionGap > SYNC_CORRECT_MIN )
        {
            if( u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE )
            {    
                u08_s_SyncMode = RH_LOW_POS;
            }
            else
            {
                u08_s_SyncMode = RH_HI_POS;  
            }    
        }    
        else
        {
            u08_s_SyncMode = RH_FIT_POS;
        } 
    }
    else
    {
        u08_s_SyncMode = RH_FIT_POS;
    }
}

/********************************************************************************/
/*   name     : g_Spindle_SyncControl_20msPeriod                                */
/*   function : LH,RH Hall count Gap compensation control                       */
/*                                                                              */
/********************************************************************************/
void g_Spindle_SyncControl_20msPeriod(void)
{
    if( (u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) ||  
        (u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE )     )   
    {
        s_Get_Spindle_PositionGap();
        
        switch( u08_s_SyncMode )
        {
            case RH_FIT_POS :
                if( s08_g_RH_Sync_Ratio != 0 )
                {
                    if( s08_g_RH_Sync_Ratio > 0 )
                    {
                        s08_g_RH_Sync_Ratio--;
                    }
                    else
                    {
                        s08_g_RH_Sync_Ratio++;
                    }            
                }    
                break;
            case RH_LOW_POS :
                if( u16_s_PositionGap > SYNC_FAIL_LIMIT )
                {
                    if( u08_g_Sync_Fail_Stop == OFF )
                    {
                        u08_g_Sync_Fail_Stop = ON;
                    }
                }
                else
                {
                    /*if( s08_g_RH_Sync_Ratio > (-MAX_INCREMENT_RPM) )*/
                    if( s08_g_RH_Sync_Ratio < MAX_INCREMENT_RPM ) 
                    {
                        s08_g_RH_Sync_Ratio = (s08_g_RH_Sync_Ratio + INCREMNET_RPM_RATIO); 
                        /*s08_g_RH_Sync_Ratio = (s08_g_RH_Sync_Ratio - DECREMNET_RPM_RATIO);*/
                    }
                }
                break;
            case RH_HI_POS :
                if( u16_s_PositionGap > SYNC_FAIL_LIMIT )
                {
                    if( u08_g_Sync_Fail_Stop == OFF )
                    {
                        u08_g_Sync_Fail_Stop = ON;
                    }
                }
                else
                {
                    /*if( s08_g_RH_Sync_Ratio < MAX_DECREMENT_RPM )*/
                    if( s08_g_RH_Sync_Ratio > (-MAX_DECREMENT_RPM) ) 
                    {
                        s08_g_RH_Sync_Ratio = (s08_g_RH_Sync_Ratio - DECREMNET_RPM_RATIO);
                        /*s08_g_RH_Sync_Ratio = (s08_g_RH_Sync_Ratio + INCREMNET_RPM_RATIO); */
                    }
                }                 
                break;
            default :
                s08_g_RH_Sync_Ratio = 0;
                u08_s_SyncMode = RH_FIT_POS;
                break;                                    
        }
    }
    else
    {
        s08_g_RH_Sync_Ratio = 0;
        u08_g_Sync_Fail_Stop = 0;
    }
}

/********************************************************************************/
/*   name     : g_Spindle_Sync_Init                                             */
/*   function : sync control value initialization                               */
/*                                                                              */
/********************************************************************************/
void g_Spindle_Sync_Init(void)
{
    u08_s_SyncMode = 0;
    u16_s_PositionGap = 0;
    s08_g_RH_Sync_Ratio = 0;
    u08_g_Sync_Fail_Stop = 0;
}
