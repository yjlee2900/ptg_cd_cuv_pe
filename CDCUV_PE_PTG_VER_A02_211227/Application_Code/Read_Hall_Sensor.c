/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-09 PM.  2:22:24                                   */
/*	Last Update     : 2013-04-09 PM.  2:22:24                                   */
/*	File Name       : Read_Hall_Sensor.c                                        */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define READ_HALL_SENSOR_EXTERN
#include "Basic_Type_Define.h"
#include "Read_Hall_Sensor.h"
#include "Status_Detect.h"
#include "Parameterization.h"
#include "Sleep_Wakeup_Control.h"
#include "Spindle_Motor_Drive.h"
#include "HallLH_CH1.h"
#include "HallLH_CH2.h"
#include "HallRH_CH1.h"
#include "HallRH_CH2.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/


static U16 u16_s_LH_EncoderPeriod;
static U16 u16_s_RH_EncoderPeriod;
static U08 u08_s_LH_Hall_Mode;
static U08 u08_s_RH_Hall_Mode;
static U08 u08_s_LH_NewPeriod;
static U08 u08_s_RH_NewPeriod;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_hall_decoder_LH( void );
/*static void s_hall_decoder_RH( void );*/
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define u08_HALL_TABLE_CNT 4
#define u08_CWCCW_IND  2
#define u08_MOTOR_CW   0 
#define u08_MOTOR_CCW  1
#define u08_MOTOR_HOLD 3
#define u08_HALL_END   3

static U08 u08_s_Hall_Table[u08_CWCCW_IND][u08_HALL_TABLE_CNT] = 
{
    { 0, 1, 3, 2 },
    { 0, 2, 3, 1 }
};

/********************************************************************************/
/*   name     : s_Get_LH_Hall ,  s_Get_RH_Hall                                  */
/*   function : channel hall value read                                         */
/********************************************************************************/
U08 g_Get_LH_Hall( void )
{
    U08 Value_Buff_L = 0;
    
    if( HallLH_CH1_GetVal() != 0 )
    {
       Value_Buff_L |= 1;
    }   
     
    if( HallLH_CH2_GetVal() != 0 )
    {
        Value_Buff_L |= 2;
    }   
    
    return(Value_Buff_L);
}

U08 g_Get_RH_Hall( void )
{
    U08 Value_Buff_R = 0;         
    
    if( HallRH_CH1_GetVal() != 0 )
    {
       Value_Buff_R |= 1;
    }   
     
    if( HallRH_CH2_GetVal() != 0 )
    {
        Value_Buff_R |= 2;
    }         
    
    return(Value_Buff_R);
}
/********************************************************************************/
/*   name     : s_hall_decoder_LH                                               */
/*   function : hall counting , time                                            */
/*   CW hall value : 0 -> 1 -> 3 -> 2 -> 0                                      */
/*   CCW hall value : 0 -> 2 -> 3 -> 1 -> 0                                     */
/********************************************************************************/
static void s_hall_decoder_LH( void )
{
    U08 u08_t_LH_Hall_value = 0;
    static U08 u08_s_LH_PreValue = 0;
    static U08 u08_s_LH_HallErrCnt = 0;
    
    if( u16_s_LH_EncoderPeriod < u16_MAX_HALL_DELAY_TIME )  /* Hall count timeout */
    {
        u16_s_LH_EncoderPeriod++;
    }    
    else
    {
        u16_g_LH_EncoderPeriod = u16_MAX_HALL_DELAY_TIME;
    }    
               
    u08_t_LH_Hall_value = g_Get_LH_Hall();

    if( (u08_t_LH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CW][u08_s_LH_Hall_Mode]) || 
        (u08_t_LH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CCW][u08_s_LH_Hall_Mode])  )
    {
        u08_s_LH_Hall_Mode++;
        u08_s_LH_HallErrCnt = 0;
        
        if( u08_s_LH_Hall_Mode == u08_HALL_TABLE_CNT )
        {
            u16_g_LH_EncoderPeriod = u16_s_LH_EncoderPeriod;
            u16_s_LH_EncoderPeriod = 0;
            u08_s_LH_Hall_Mode = 0;
            u08_s_LH_NewPeriod = 1;
            
            if( u08_t_LH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CW][u08_HALL_END] )
            {
                s16_g_LH_Position_Cnt++;
                u08_g_LH_Motor_Dir = u08_MOTOR_CW;
            }
            else
            {
                s16_g_LH_Position_Cnt--;
                u08_g_LH_Motor_Dir = u08_MOTOR_CCW;
            }    
        }
    }
    else
    {
    	  if( (u08_s_LH_PreValue != u08_t_LH_Hall_value) && (u08_g_ECU_Power_Mode == ECU_NORAM_MODE) )
    	  {	
            if( u08_s_LH_HallErrCnt > 50 )
            {
                u08_g_LH_HallPatternErrSt = 1;
            }	
            else
            {
                u08_s_LH_HallErrCnt++;	
            }	
        }    
    }	 
    
    u08_s_LH_PreValue = u08_t_LH_Hall_value;
}
/********************************************************************************/
/*   name     : s_hall_decoder_RH                                               */
/*   function : hall counting , time                                            */
/*   CW hall value : 0 -> 1 -> 3 -> 2 -> 0                                      */
/*   CCW hall value : 0 -> 2 -> 3 -> 1 -> 0                                     */
/********************************************************************************/
#if 0
static void s_hall_decoder_RH( void )
{               
    U08 u08_t_RH_Hall_value;
    
    if( u16_s_RH_EncoderPeriod < u16_MAX_HALL_DELAY_TIME ) /* Hall count timeout */
    {
        u16_s_RH_EncoderPeriod++;
    }
    else
    {
        u16_g_RH_EncoderPeriod = u16_MAX_HALL_DELAY_TIME;
    }    
          
    u08_t_RH_Hall_value = g_Get_RH_Hall();
          
    if( (u08_t_RH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CW][u08_s_RH_Hall_Mode]) || 
        (u08_t_RH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CCW][u08_s_RH_Hall_Mode])  )
    {
        u08_s_RH_Hall_Mode++;
        
        if( u08_s_RH_Hall_Mode == u08_HALL_TABLE_CNT )
        {
            u16_g_RH_EncoderPeriod = u16_s_RH_EncoderPeriod;
            u16_s_RH_EncoderPeriod = 0;
            u08_s_RH_Hall_Mode = 0;
            u08_s_RH_NewPeriod = 1;
            
            if( u08_t_RH_Hall_value == u08_s_Hall_Table[u08_MOTOR_CW][u08_HALL_END] )
            {
                s16_g_RH_Position_Cnt++;
                u08_g_RH_Motor_Dir = u08_MOTOR_CW;
            }
            else
            {
                s16_g_RH_Position_Cnt--;
                u08_g_RH_Motor_Dir = u08_MOTOR_CCW;
            }
        }
    }
}
#endif

/********************************************************************************/
/*   name     : g_HallDirectionFaultCheck_10msPeriod                            */
/*   function : Hall input Pattern check for motor direction                    */
/*   Hall Direction Fault Chekc                                                 */
/********************************************************************************/
void g_HallDirectionFaultCheck_10msPeriod(void)
{
	  static U08 u08_s_DirectionError_Cnt = 0;
	  
    if( (u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE) || (u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) )
    {             
        if( ((u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE) && (u08_g_LH_Motor_Dir == u08_MOTOR_CCW)) ||
        	  ((u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) && (	u08_g_LH_Motor_Dir == u08_MOTOR_CW))    )
        {
            if( u08_s_DirectionError_Cnt > 50 )
            {	
            	  u08_g_LH_HallPatternErrSt = 1;
            }
            else
            {
                u08_s_DirectionError_Cnt++;	          
            }
        }
        else
        {
            u08_s_DirectionError_Cnt = 0;	
        }	
    }
    else
    {
        u08_s_DirectionError_Cnt = 0;
    }	
}

/********************************************************************************/
/*   name     : g_Encoder_Scan                                                  */
/*   function : hall counting , time                                            */
/*   100us time event task                                                      */
/********************************************************************************/
void g_Encoder_Scan(void)
{
    if( (u08_g_LatchSt == LATCH_OPEN) || (u08_g_Latch_Unexpect == ON) )
    {                       
        s_hall_decoder_LH();
        
        /*s_hall_decoder_RH();*/
    }
}

/********************************************************************************/
/*   name     : g_Encoder_Init                                                  */
/*   function : Encoder value initialization                                    */
/*                                                                              */
/********************************************************************************/
void g_Encoder_Init(void)
{
    u16_g_LH_EncoderPeriod = 0;
    u16_g_RH_EncoderPeriod = 0;
    u16_s_LH_EncoderPeriod = 0;
    u16_s_RH_EncoderPeriod = 0;
    u08_s_LH_Hall_Mode = 0;
    u08_s_RH_Hall_Mode = 0;
    u08_g_LH_Motor_Dir = u08_MOTOR_HOLD;
    u08_g_RH_Motor_Dir = u08_MOTOR_HOLD;
    u08_s_LH_NewPeriod = 0;
    u08_s_RH_NewPeriod = 0; 
    u16_g_LH_MotorRPM = 0;
    u16_g_RH_MotorRPM = 0;
    u08_g_LH_HallPatternErrSt = 0;
    /*u08_g_RH_HallPatternErrSt = 0;*/
    u08_g_PowerMoveInit1_NoHallData_LH = ON;
    u08_g_PowerMoveInit2_NoHallData_RH = ON;
    
}

/****************************************************************************************/
/*   name     : g_Get_Motor_RPM_1msPeriod                                               */
/*   function : spindle Motor RPM calculation                                           */
/*   RPM = revolutions per minute                                                       */
/*   RPM = 600000(*100us) / motor 1 rotation time(us)                                   */
/*   RPM = 600000(*100us) / ( EncodirPriod time(*100us) * 5 ) 5 priod time = 1 rotation */
/*   RPM = 60000(ms)      / ((EncodirPriod time(*100us)/10)(ms) * 5))                   */
/*   RPM = 60000(ms)      / (EncodirPriod time(*100us)/2)                               */
/*   RPM = 120000(ms)     / (EncodirPriod time(*100us)                                  */
/****************************************************************************************/
void g_Get_Motor_RPM_1msPeriod(void)
{
    /*U16 u16_t_Buff = 0;*/
    U16 u16_t_Buff2 = 0;
    
    if( u08_s_LH_NewPeriod == 1 ) 
    {
        u08_s_LH_NewPeriod = 0;
        u08_g_PowerMoveInit1_NoHallData_LH = 0;
        /*u16_t_Buff = (u16_g_LH_EncoderPeriod / 2);
        u16_t_Buff2 = (60000U / u16_t_Buff); */
        u16_t_Buff2 = (U16)(120000U / u16_g_LH_EncoderPeriod); 
        u16_g_LH_MotorRPM = (u16_t_Buff2 + u16_g_LH_MotorRPM)/2;
    }
    else
    {
        if( u16_s_LH_EncoderPeriod >= 1000 ) /* 120 RPM Under = 0 RPM */   
        {
            u16_g_LH_MotorRPM = 0;
            u08_g_LH_Motor_Dir = u08_MOTOR_HOLD;
        }
    }
#if 0    
    if( u08_s_RH_NewPeriod == 1 ) 
    {
        u08_s_RH_NewPeriod = 0;
        u08_g_PowerMoveInit2_NoHallData_RH = 0;
        u16_t_Buff = (u16_g_RH_EncoderPeriod / 2);
        u16_t_Buff2 = (60000U / u16_t_Buff);
        u16_g_RH_MotorRPM = (u16_t_Buff2 + u16_g_RH_MotorRPM)/2;
    }
    else
    {
        if( u16_s_RH_EncoderPeriod >= 1000 ) /* 120 RPM Under = 0 RPM */   
        {
            u16_g_RH_MotorRPM = 0;
            u08_g_RH_Motor_Dir = u08_MOTOR_HOLD;
        }
    }
#endif    
}
