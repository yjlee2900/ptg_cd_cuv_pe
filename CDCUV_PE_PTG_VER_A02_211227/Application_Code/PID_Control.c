/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-29 AM.  10:11:04                                  */
/*  Last update     : 2014-06-24 PM. 7:05:58                                    */
/*  File Name       : PID_Control.c                                             */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define PID_CONTROL_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "PID_Control.h"
#include "EEPROM_Control.h"
#include "Parameterization.h"
#include "Read_Hall_Sensor.h"
#include "Spindle_Motor_Drive.h" 
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static S16 u16_s_PID_CloseZone1_Pos = 0;
static S16 u16_s_PID_CloseZone2_Pos = 0;
static S16 u16_s_PID_CloseZone3_Pos = 0;
/* static U16 u16_s_PID_CloseZone4_Pos = 0; */

static S16 u16_s_PID_OpenZone1_Pos = 0;
static S16 u16_s_PID_OpenZone2_Pos = 0;
static S16 u16_s_PID_OpenZone3_Pos = 0;
/* static U16 u16_s_PID_OpenZone4_Pos = 0; */
 
static U16 u16_s_NowPos   = 0;
static S16 s16_s_PreI     = 0;
static S16 s16_s_PreERR   = 0;

static S16 s16_s_PreERR1  = 0;
static S16 s16_s_PreERR2  = 0;

static U08 u08_s_PID_Period_T     = 0;
static U08 u08_s_Propoti_Period_T = 0;

static S16 Test_ERR = 0;
static S16 Test_P = 0;
static S16 Test_I = 0;
static S16 Test_D = 0;
static S16 Test_SUM = 0;
static S16 Test_Return_PID = 0;

static S16  u16_s_Open_Zone1_Target_RPM = 0;
static S16  u16_s_Open_Zone2_Target_RPM = 0;
static S16  u16_s_Close_Zone1_Target_RPM = 0;
static S16  u16_s_Close_Zone2_Target_RPM = 0;

static S16 s16_t_UsrSetOpnZn1Pos = 0;
static S16 s16_t_UsrSetClsZn1Pos = 0;
static S16 s16_t_UsrSetOpnZn2Pos = 0;
static S16 s16_t_UsrSetClsZn2Pos = 0;
static S16 s16_t_UsrSetOpnZn3Pos = 0;
static S16 s16_t_UsrSetClsZn3Pos = 0;

static S16 u16_t_UsrSetOpnZn2PosGp = 0;
static S16 u16_t_UsrSetOpnZn3PosGp = 0;
static S16 u16_t_UsrSetClsZn0PosGp = 0;
static S16 u16_t_UsrSetClsZn1PosGp = 0; 

static S16 s16_t_OpnInGradient = 0;
static S16 s16_t_OpnDeGradient = 0;
static S16 s16_t_OpnInIntercept = 0;
static S16 s16_t_OpnDeIntercept = 0;

static S16 s16_t_ClsInGradient = 0;
static S16 s16_t_ClsDeGradient = 0;
static S16 s16_t_ClsInIntercept = 0;
static S16 s16_t_ClsDeIntercept = 0;


/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static S08 s08_s_PID_Limit_check( S16 Calc_PID );
static S16 s16_s_Get_P_Calc( S16 s16_t_Err );
static S16 s16_s_Get_I_Calc( S16 s16_t_Err );
static S16 s16_s_Get_D_Calc( S16 s16_t_Err );
static U16 u16_s_Close_Target_RPM_Caculation( void );
static U16 u16_s_Open_Target_RPM_Caculation( void );
static void s_Get_PID_Target_RPM( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/

#define PROPORTIONAL_COE   13  /*10*/ /* 15 */ /* 1 : 0.05 */ /* tunning point */
#define INTEGRAL_COE       2  /*2 */ /* 1  */ /* 1 : 0.05 */ /* tunning point */
#define DIFFERRENTIAL_COE  16 /*15*/ /* 20 */ /* 1 : 0.05 */ /* tunning point */

#define MAX_CAL_VALUE      200    /* tunning point */
#define MIDDLE_CAL_VALUE   150
/*#define MIN_CAL_VALUE      100 */   /* tunning point */

#define MAX_PID_VALUE      3    /* tunning point */
#define MIDDEL_PID_VALUE   2    /* tunning point */
#define MIN_PID_VALUE      1    /* tunning point */

#define PID_PERIOD_T       20 /* 1ms * 20 = 20ms */ /* tunning point */
#define PROPORTI_PERIOD_T  15  /* 1ms * 15 = 15ms */   /* tunning point */

/********************************************************************************/
/*   name     : s08_s_PID_Limit_check                                           */
/*   function : PID calc value limit check                                      */
/********************************************************************************/
static S08 s08_s_PID_Limit_check( S16 Calc_PID )
{
    S08 RetValue;
    
    /* if( Calc_PID > MAX_PID_VALUE )         */
    /* {                                      */
    /*     RetValue = MAX_PID_VALUE;          */
    /* }                                      */
    /* else if( Calc_PID < (-MIN_PID_VALUE) ) */
    /* {                                      */
    /*     RetValue = (-MIN_PID_VALUE);       */
    /* }                                      */
    /* else                                   */
    /* {                                      */
    /*     RetValue = (S08)Calc_PID;          */
    /* }                                      */
    if( Calc_PID > 1 )
    {   
        if( Calc_PID > MAX_CAL_VALUE )
        {
            RetValue = MAX_PID_VALUE;
        } 
        else if( Calc_PID > MIDDLE_CAL_VALUE )
        {
            RetValue = MIDDEL_PID_VALUE;
        }
        else
        {
            RetValue = MIN_PID_VALUE;
        }    
    }
    else
    {
        if( Calc_PID < (-MAX_CAL_VALUE) )
        {
            RetValue = (-MAX_PID_VALUE);
        }
        else if( Calc_PID < (-MIDDLE_CAL_VALUE) )
        {
            RetValue = (-MIDDEL_PID_VALUE);
        }        
        else if( Calc_PID < (-10) )
        {
            RetValue = (-MIN_PID_VALUE);
        } 
        else
        {
            RetValue = 0;
        }           
    }        
    
    return( RetValue );   
}

/********************************************************************************/
/*   name     : u16_s_Close_Target_RPM_Caculation                               */
/*   function : PID Position Target RPM caculation                              */
/********************************************************************************/
static U16 u16_s_Close_Target_RPM_Caculation( void )
{
    U16 u16_t_RetValue_C   = 0;
    U16 u16_t_PosGap_C;

    S16 s16_t_ClsGrdnt     = 0;
    S16 s16_t_ClsIntrctp   = 0;
    
    switch( u08_g_PID_Zone )
    {
        case PID_ZONE1 :
            if( u16_s_NowPos < u16_s_PID_CloseZone1_Pos )
            {
                u08_g_PID_Zone   = PID_ZONE2; 
                u16_t_RetValue_C = u16_s_Close_Zone2_Target_RPM;
            }
            else
            {
                if((U16)u16_g_EEP_Para.MaxPowerOpenPos >= (U16)u16_s_NowPos)
                {
                    s16_t_ClsGrdnt = (u16_s_Close_Zone1_Target_RPM - CLOSE_TARGET_RPM_START)/(u16_s_PID_CloseZone1_Pos - u16_g_EEP_Para.MaxPowerOpenPos);
                    s16_t_ClsIntrctp = (s16_t_ClsGrdnt * ((S16)u16_g_EEP_Para.MaxPowerOpenPos * (-1))) +  CLOSE_TARGET_RPM_START;
                    u16_t_RetValue_C = (s16_t_ClsGrdnt * u16_s_NowPos) + s16_t_ClsIntrctp;
                }
                else
                {
                    u16_t_RetValue_C = CLOSE_TARGET_RPM_START;
                }    
            }     
            break;
        case PID_ZONE2 :
            /* Close, Constant RPM */  
            u16_t_RetValue_C = u16_s_Close_Zone2_Target_RPM;
            
            if( u16_s_NowPos < u16_s_PID_CloseZone2_Pos )
            {
                u08_g_PID_Zone = PID_ZONE3;
            }     
            break;    
        case PID_ZONE3 :
            if( u16_s_NowPos < u16_s_PID_CloseZone3_Pos )
            {
                u08_g_PID_Zone   = PID_ZONE4;
                u16_t_RetValue_C = CLOSE_TARGET_RPM_ZONE3;
            }
            else
            {
                s16_t_ClsGrdnt = (CLOSE_TARGET_RPM_ZONE3 - u16_s_Close_Zone2_Target_RPM)/(u16_s_PID_CloseZone3_Pos - u16_s_PID_CloseZone2_Pos);
                s16_t_ClsIntrctp = (s16_t_ClsGrdnt * (u16_s_PID_CloseZone2_Pos * (-1))) + u16_s_Close_Zone2_Target_RPM;
                u16_t_RetValue_C = (s16_t_ClsGrdnt * u16_s_NowPos) + s16_t_ClsIntrctp;
            }        
            break;        
        case PID_ZONE4 :
            /* Close, Constant RPM */
            u16_t_RetValue_C = CLOSE_TARGET_RPM_ZONE4;
            
            if( u16_s_NowPos == 0 )
            {
                u08_g_PID_Zone = PID_ZONE1;
            }
            break;
        default :  /* reserching zone*/
            if( u16_s_NowPos > u16_s_PID_CloseZone1_Pos )
            {
                u08_g_PID_Zone = PID_ZONE1;
                u16_t_RetValue_C = CLOSE_TARGET_RPM_START;
            }
            else if( u16_s_NowPos > u16_s_PID_CloseZone2_Pos ) 
            {
                u08_g_PID_Zone = PID_ZONE2;
                u16_t_RetValue_C = u16_s_Close_Zone2_Target_RPM;
            }
            else if( u16_s_NowPos > u16_s_PID_CloseZone3_Pos ) 
            {
                u08_g_PID_Zone = PID_ZONE3;
                u16_t_RetValue_C = CLOSE_TARGET_RPM_ZONE3;
            }
            else 
            {
                u08_g_PID_Zone = PID_ZONE4;
                u16_t_RetValue_C = CLOSE_TARGET_RPM_ZONE4;
            }                    
            break;        
    }                    
    
    return(u16_t_RetValue_C);
}

/********************************************************************************/
/*   name     : u16_s_Open_Target_RPM_Caculation                                */
/*   function : PID Position Target RPM caculation                              */
/********************************************************************************/
static U16 u16_s_Open_Target_RPM_Caculation( void )
{
    U16 u16_t_RetValue_O   = 0;
    U16 u16_t_PosGap_O;

    S16 s16_t_OpnGrdnt     = 0;
    S16 s16_t_OpnIntrctp   = 0; 
    
    switch( u08_g_PID_Zone )
    {
        case PID_ZONE1 :
            s16_t_OpnGrdnt = (u16_s_Open_Zone1_Target_RPM - OPEN_TARGET_RPM_START)/u16_s_PID_OpenZone1_Pos;
            s16_t_OpnIntrctp = OPEN_TARGET_RPM_START;
            u16_t_RetValue_O = (s16_t_OpnGrdnt * u16_s_NowPos) + s16_t_OpnIntrctp;
   
            if( u16_s_NowPos > u16_s_PID_OpenZone1_Pos )
            {
                u08_g_PID_Zone   = PID_ZONE2;
            }
            break;
        case PID_ZONE2 :
            /* Open , Constant RPM */
            u16_t_RetValue_O = u16_s_Open_Zone2_Target_RPM;
            
            if( u16_s_NowPos > u16_s_PID_OpenZone2_Pos )
            {
                u08_g_PID_Zone = PID_ZONE3;
            }     
            break;    
        case PID_ZONE3 :
            /* Open, Decrement RPM */
            if( u16_s_NowPos > u16_s_PID_OpenZone3_Pos )
            {
                u08_g_PID_Zone   = PID_ZONE4;
                u16_t_RetValue_O = OPEN_TARGET_RPM_ZONE4;
            }         
            else
            {
                s16_t_OpnGrdnt = (OPEN_TARGET_RPM_ZONE3 - u16_s_Open_Zone2_Target_RPM)/(u16_s_PID_OpenZone3_Pos - u16_s_PID_OpenZone2_Pos);
                s16_t_OpnIntrctp = (s16_t_OpnGrdnt * (u16_s_PID_OpenZone2_Pos * (-1))) + u16_s_Open_Zone2_Target_RPM;
                u16_t_RetValue_O = (s16_t_OpnGrdnt * u16_s_NowPos) + s16_t_OpnIntrctp;
            }    
            break;        
        case PID_ZONE4 :
            /* Open , Constant RPM */
            u16_t_RetValue_O = OPEN_TARGET_RPM_ZONE4;
            
            if( u16_s_NowPos == 0 )
            {
                u08_g_PID_Zone = PID_ZONE1;
            }
            break;
        default :  /* reserching zone*/
            if( u16_s_NowPos < u16_s_PID_OpenZone1_Pos )
            {
                u08_g_PID_Zone = PID_ZONE1;
                u16_t_RetValue_O = OPEN_TARGET_RPM_START;
            }
            else if( u16_s_NowPos < u16_s_PID_OpenZone2_Pos ) 
            {
                u08_g_PID_Zone = PID_ZONE2;
                u16_t_RetValue_O = u16_s_Open_Zone2_Target_RPM;
            }
            else if( u16_s_NowPos < u16_s_PID_OpenZone3_Pos ) 
            {
                u08_g_PID_Zone = PID_ZONE3;
                u16_t_RetValue_O = OPEN_TARGET_RPM_ZONE3;
            }
            else 
            {
                u08_g_PID_Zone = PID_ZONE4;
                u16_t_RetValue_O = OPEN_TARGET_RPM_ZONE4;
            }                    
            break;        
    }
    
    return(u16_t_RetValue_O);
}

/********************************************************************************/
/*   name     : s_Get_PID_Target_RPM                                            */
/*   function : PID target RPM Calculation , call g_PID_Calculation function    */
/********************************************************************************/
static void s_Get_PID_Target_RPM( void )
{
    if( s16_g_LH_Position_Cnt < 0 )
    {
        u16_s_NowPos = 0;
    }
    else
    {
        u16_s_NowPos = (U16)s16_g_LH_Position_Cnt;
    }              
    
    if( u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE ) 
    {    
        u16_g_Target_RPM = u16_s_Close_Target_RPM_Caculation();
    }
    else 
    {                    
        u16_g_Target_RPM = u16_s_Open_Target_RPM_Caculation();    
    }
}

/********************************************************************************/
/*   name     : s16_s_Get_P_Calc                                                */
/*   function : Proportional Out value calculatio                               */
/********************************************************************************/
static S16 s16_s_Get_P_Calc( S16 s16_t_Err )
{
    S16 s16_t_RetP;
    s16_t_RetP = (S16)(s16_t_Err * u08_g_Kp);
    if( s16_t_RetP > 1500 )
    {
        s16_t_RetP = 1500;
    }    
    if( s16_t_RetP < -1500 )
    {
        s16_t_RetP = -1500;
    }
    return(s16_t_RetP);
}

/********************************************************************************/
/*   name     : s16_s_Get_I_Calc                                                */
/*   function : Proportional Out value calculatio                               */
/********************************************************************************/
static S16 s16_s_Get_I_Calc( S16 s16_t_Err )
{
    S16 s16_t_RetI;
    s16_t_RetI = (S16)(s16_t_Err * u08_g_Ki)/10;
    s16_t_RetI = s16_t_RetI + s16_s_PreI;
    if( s16_t_RetI > 600 )
    {
        s16_t_RetI = 600;
    }    
    if( s16_t_RetI < -350 )
    {
        s16_t_RetI = -350;
    }    
    s16_s_PreI = s16_t_RetI;
    return(s16_t_RetI);
}

/********************************************************************************/
/*   name     : s16_s_Get_D_Calc                                                */
/*   function : Proportional Out value calculatio                               */
/********************************************************************************/
static S16 s16_s_Get_D_Calc( S16 s16_t_Err )
{
    S16 s16_t_RetD;
    
    s16_t_RetD = (S16)(((s16_t_Err - s16_s_PreERR2) + (s16_s_PreERR - s16_s_PreERR1)) * u08_g_Kd);
    if( s16_t_RetD > 1500 )
    {
        s16_t_RetD = 1500;
    }    
    if( s16_t_RetD < -1500 )
    {
        s16_t_RetD = -1500;
    }
    
    s16_s_PreERR2 = s16_s_PreERR1;
    s16_s_PreERR1 = s16_s_PreERR;
    s16_s_PreERR = s16_t_Err;
    return(s16_t_RetD);
}

/********************************************************************************/
/*   name     : g_Proporti_PWM_Calculation                                       */
/*   function : PID Out value calculation                                       */
/********************************************************************************/
void g_Proporti_PWM_Calculation( void )
{
    U16 u16_t_Buff;
    
    if( u08_s_Propoti_Period_T != 0 )
    {
        u08_s_Propoti_Period_T--;
        if( u08_s_Propoti_Period_T == 0 )
        {
        	  if( u08_g_Init_Param == ON ) 
        	  {	
                s_Get_PID_Target_RPM();
            }
            
            if( u16_g_Target_RPM > u16_g_LH_MotorRPM )
            {   
                u16_t_Buff = ( u16_g_Target_RPM - u16_g_LH_MotorRPM ); 
                
                /*s08_g_Propoti_Ratio = (u16_t_Buff / 100);*/
                /*if( s08_g_Propoti_Ratio > 2 )            */
                /*{                                        */
                /*    s08_g_Propoti_Ratio = 2;             */
                /*}                                        */
                if( u16_t_Buff > 1200U )        
                {                             
                    s08_g_Propoti_Ratio = 3;  
                } 
                else if( u16_t_Buff > 900U )        
                {                             
                    s08_g_Propoti_Ratio = 2;  
                }                             
                else if( u16_t_Buff > 500U )    
                {                             
                    s08_g_Propoti_Ratio = 1;  
                }                             
                else                          
                {                             
                    s08_g_Propoti_Ratio = 0;  
                }                             
            }
            
            if( u16_g_Target_RPM < u16_g_LH_MotorRPM )
            {
                u16_t_Buff = ( u16_g_LH_MotorRPM - u16_g_Target_RPM );  
                /*s08_g_Propoti_Ratio = -(u16_t_Buff / 100);*/
                /*if( s08_g_Propoti_Ratio < -2 )            */
                /*{                                         */
                /*    s08_g_Propoti_Ratio = -2;             */
                /*}                                         */
                if( u16_t_Buff > 500U )         
                {                              
                    s08_g_Propoti_Ratio = -2;  
                }                              
                else if( u16_t_Buff > 200U )     
                {                              
                    s08_g_Propoti_Ratio = -1;  
                }                              
                else                           
                {                              
                    s08_g_Propoti_Ratio = 0;   
                }                              
            }
            
            u08_s_Propoti_Period_T = PROPORTI_PERIOD_T;
        }
        else
        {
            s08_g_Propoti_Ratio = 0;
        }        
    }
    else
    {
        u08_s_Propoti_Period_T = PROPORTI_PERIOD_T;
    }    
}

/********************************************************************************/
/*   name     : g_PID_Calculation                                                  */
/*   function : PID Out value calculation                                       */
/********************************************************************************/
void g_PID_Calculation( void )
{
    S16 s16_t_RPM_Err;
    S16 s16_t_P_OutRatio;
    S16 s16_t_I_OutRatio;
    S16 s16_t_D_OutRatio;
    S16 s16_t_calc_Buff;
    
    if( u08_s_PID_Period_T != 0 )
    {
        u08_s_PID_Period_T--;
        if( u08_s_PID_Period_T == 0 )
        {
            s_Get_PID_Target_RPM();
            
            s16_t_RPM_Err = (S16)(u16_g_Target_RPM - u16_g_LH_MotorRPM)/10;
            s16_t_P_OutRatio = s16_s_Get_P_Calc( s16_t_RPM_Err ); 
            s16_t_I_OutRatio = s16_s_Get_I_Calc( s16_t_RPM_Err ); 
            s16_t_D_OutRatio = s16_s_Get_D_Calc( s16_t_RPM_Err );
            
            Test_ERR = s16_t_RPM_Err; 
            Test_P = s16_t_P_OutRatio;
            Test_I = s16_t_I_OutRatio;
            Test_D = s16_t_D_OutRatio;
            
            s16_t_calc_Buff = (S16)((s16_t_P_OutRatio + s16_t_I_OutRatio + s16_t_D_OutRatio)/10);
            s08_g_PID_Radio = s08_s_PID_Limit_check( s16_t_calc_Buff );
            u08_s_PID_Period_T = PID_PERIOD_T;
            
            Test_SUM = s16_t_calc_Buff;
            Test_Return_PID = s08_g_PID_Radio;        
        }
        else
        {
            s08_g_PID_Radio = 0;
        }    
    }
    else
    {
        u08_s_PID_Period_T = PID_PERIOD_T;
    }    
        
}

/********************************************************************************/
/*   name     : g_PID_Value_Init                                                */
/*   function : PID Calculation value initialazation                            */
/*                                                                              */
/********************************************************************************/
void g_PID_Value_Init( void )
{              
    u08_g_PID_Zone  = RE_SERCHING_ZONE;
    s08_g_PID_Radio = 0;
    s08_g_Propoti_Ratio = 0;
    u16_s_NowPos   = 0;
    s16_s_PreI     = 0;
    s16_s_PreERR   = 0;
    s16_s_PreERR1  = 0;
    s16_s_PreERR2  = 0;    
                                 
    u08_s_PID_Period_T     = 0;
    u08_s_Propoti_Period_T = 0;
}

/********************************************************************************/
/*   name     : g_PID_Tartget_Position_Init                                     */
/*   function : when system power on, PID Tartget RPM Position Initialization   */
/*   xxxus time event task                                                      */
/********************************************************************************/
void g_PID_Tartget_Position_Init(void)
{
    U16 u16_t_5Per_Buff;
    U16 u16_t_User_Setting_gap;

    if( u16_g_EEP_Diag.Speed_Set_Value == 2 ) 
    {
        u16_s_Open_Zone1_Target_RPM = OPEN_TARGET_RPM_ZONE1;
        u16_s_Open_Zone2_Target_RPM = OPEN_TARGET_RPM_ZONE2;
        
        u16_s_Close_Zone1_Target_RPM = CLOSE_TARGET_RPM_ZONE1;
        u16_s_Close_Zone2_Target_RPM = CLOSE_TARGET_RPM_ZONE2;        
    }
    else
    {
        u16_s_Open_Zone1_Target_RPM = (OPEN_TARGET_RPM_ZONE1 + FAST_SPEED_TARGET_RPM);
        u16_s_Open_Zone2_Target_RPM = (OPEN_TARGET_RPM_ZONE2 + FAST_SPEED_TARGET_RPM);
        
        u16_s_Close_Zone1_Target_RPM = (CLOSE_TARGET_RPM_ZONE1 + FAST_SPEED_TARGET_RPM);
        u16_s_Close_Zone2_Target_RPM = (CLOSE_TARGET_RPM_ZONE2 + FAST_SPEED_TARGET_RPM);
        
    }

    u16_t_5Per_Buff = ( u16_g_EEP_Para.MaxBasicPos / 20U );
    // MaxbasicPos = Origin Open height

    u16_t_User_Setting_gap = (U16)( u16_g_EEP_Para.MaxBasicPos - u16_g_EEP_Para.MaxPowerOpenPos );
    // MaxPowerOpenPos = Target Open height
        
    u16_s_PID_OpenZone1_Pos = (U16)( u16_t_5Per_Buff * 4  );  /* 20%  Close : 0%, Max Open : 100% */
    s16_t_UsrSetOpnZn1Pos = u16_s_PID_OpenZone1_Pos;
    u16_s_PID_OpenZone2_Pos = (U16)( u16_t_5Per_Buff * 16 );  /* 75%  Close : 0%, Max Open : 100% */ 
    s16_t_UsrSetOpnZn2Pos = u16_s_PID_OpenZone2_Pos;
    u16_s_PID_OpenZone3_Pos = (U16)( u16_t_5Per_Buff * 19 );  /* 100%  Close : 0%, Max Open : 100% */ 
    s16_t_UsrSetOpnZn3Pos = u16_s_PID_OpenZone3_Pos; 
    /* u16_s_PID_OpenZone4_Pos = (U16)( u16_t_5Per_Buff * 18 );*/  /* 90%  Close : 0%, Max Open : 100% */ 
    
    u16_s_PID_CloseZone1_Pos = (U16)( u16_t_5Per_Buff * 17 ); /* 80%  Close : 100%, Max Open : 0% */
    s16_t_UsrSetClsZn1Pos = u16_s_PID_CloseZone1_Pos;
    u16_s_PID_CloseZone2_Pos = (U16)( u16_t_5Per_Buff * 6 ); /* 6 70%  Close : 100%, Max Open : 0% */
    s16_t_UsrSetClsZn2Pos = u16_s_PID_CloseZone2_Pos;
    u16_s_PID_CloseZone3_Pos = (U16)( u16_t_5Per_Buff * 2 ); /* 0 100%  Close : 100%, Max Open : 0% */
    s16_t_UsrSetClsZn3Pos = u16_s_PID_CloseZone3_Pos;
    /*u16_s_PID_CloseZone2_Pos = (U16)( u16_t_5Per_Buff * 2  );*/  /* 90%  Close : 100%, Max Open : 0% */

    u16_t_UsrSetOpnZn2PosGp = s16_t_UsrSetOpnZn2Pos - u16_t_User_Setting_gap;
    u16_t_UsrSetOpnZn3PosGp = s16_t_UsrSetOpnZn3Pos - u16_t_User_Setting_gap;

    u16_t_UsrSetClsZn0PosGp = u16_g_EEP_Para.MaxBasicPos - u16_t_User_Setting_gap;
    u16_t_UsrSetClsZn1PosGp = s16_t_UsrSetClsZn1Pos - u16_t_User_Setting_gap;

    if(u16_t_UsrSetOpnZn2PosGp <= s16_t_UsrSetOpnZn1Pos)
    {
        /* Open Target RPM */
        s16_t_OpnInGradient = (u16_s_Open_Zone1_Target_RPM - OPEN_TARGET_RPM_START) / s16_t_UsrSetOpnZn1Pos;
        s16_t_OpnDeGradient = (OPEN_TARGET_RPM_ZONE3 - u16_s_Open_Zone2_Target_RPM) / (u16_t_UsrSetOpnZn3PosGp - u16_t_UsrSetOpnZn2PosGp);
        s16_t_OpnInIntercept = OPEN_TARGET_RPM_START;
        s16_t_OpnDeIntercept = (s16_t_OpnDeGradient * (u16_t_UsrSetOpnZn2PosGp * (-1))) + u16_s_Open_Zone2_Target_RPM;

        u16_s_PID_OpenZone1_Pos = (s16_t_OpnDeIntercept - s16_t_OpnInIntercept) / ((s16_t_OpnDeGradient * (-1)) + s16_t_OpnInGradient);
        u16_s_PID_OpenZone2_Pos = u16_s_PID_OpenZone1_Pos;
        u16_s_PID_OpenZone3_Pos = u16_t_UsrSetOpnZn3PosGp;
        /* Close Target RPM */
        s16_t_ClsInGradient = (u16_s_Close_Zone1_Target_RPM - CLOSE_TARGET_RPM_START) / (u16_t_UsrSetClsZn1PosGp - u16_t_UsrSetClsZn0PosGp);
        s16_t_ClsDeGradient = (CLOSE_TARGET_RPM_ZONE3 - u16_s_Close_Zone2_Target_RPM)/(s16_t_UsrSetClsZn3Pos - s16_t_UsrSetClsZn2Pos);
        s16_t_ClsInIntercept = (s16_t_ClsInGradient * (u16_t_UsrSetClsZn1PosGp * (-1))) + CLOSE_TARGET_RPM_START;
        s16_t_ClsDeIntercept = (s16_t_ClsDeGradient * (s16_t_UsrSetClsZn2Pos*(-1))) + u16_s_Close_Zone2_Target_RPM;

        u16_s_PID_CloseZone1_Pos = (s16_t_ClsInIntercept - s16_t_ClsDeIntercept) / ((s16_t_ClsInGradient * (-1)) + s16_t_ClsDeGradient);
        u16_s_PID_CloseZone2_Pos = u16_s_PID_CloseZone1_Pos;
        u16_s_PID_CloseZone3_Pos = (u16_g_EEP_Para.MaxPowerOpenPos / 20) * 2;

    }
    else
    {
        u16_s_PID_OpenZone1_Pos = s16_t_UsrSetOpnZn1Pos;
        u16_s_PID_OpenZone2_Pos = u16_t_UsrSetOpnZn2PosGp;
        u16_s_PID_OpenZone3_Pos = u16_t_UsrSetOpnZn3PosGp;

        u16_s_PID_CloseZone1_Pos = u16_t_UsrSetClsZn1PosGp;
        u16_s_PID_CloseZone2_Pos = s16_t_UsrSetClsZn2Pos;
        u16_s_PID_CloseZone3_Pos = s16_t_UsrSetClsZn3Pos;
    }
    
    u08_g_Kp = PROPORTIONAL_COE; 
    u08_g_Ki = INTEGRAL_COE;
    u08_g_Kd = DIFFERRENTIAL_COE;
}
