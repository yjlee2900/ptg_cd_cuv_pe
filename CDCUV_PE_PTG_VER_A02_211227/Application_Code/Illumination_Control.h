/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-22 PM.  5:18:18                                   */
/*  Last update     : 2013-05-22 PM.  5:18:20                                   */
/*  File Name       : Illumination_Control.h                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		ILLUMINATION_H_EXTERN
#define		ILLUMINATION_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	ILLUMINATION_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define SLEEP_OFF   3U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 u08_g_Illumi_St;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_Illumination_Control_10msPeriod(void);

#undef EXTERN

#endif
