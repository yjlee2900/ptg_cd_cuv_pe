/********************************************************************************/   
/*                                                                              */   
/*  Project         : PTG(Power Tailgate ) for CK                               */   
/*  Copyright       : MOTOTECH CO. LTD.                                         */   
/*  Author          : Jae-Young Park                                            */   
/*  Created date    : 2013-06-17 PM.  8:16:38                                   */   
/*  Last update     : 2013-06-17 PM.  8:16:44                                   */   
/*  File Name       : Sleep_Wakeup_Control.h                                    */   
/*  Processor       : MC9S12P64                                                 */   
/*  Version         :                                                           */   
/*  Compiler        : HI-WIRE                                                   */   
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */   
/*                                                                              */   
/********************************************************************************/ 

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		SLEEP_WAKEUP_H_EXTERN
#define		SLEEP_WAKEUP_H_EXTERN

#ifndef  	SLEEP_WAKEUP_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define ECU_NORAM_MODE    1U
#define ECU_STANDBY_MODE  2U

#define SLEEP_MODE        1U
#define WAKE_UP_MODE      2U
#define WAKE_SERCH_MODE   3U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 u08_g_Shutdown_Delay;
EXTERN U08 u08_g_ECU_Power_Mode;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_TriggerWDT(void);
EXTERN  void g_ECU_Sleep( void );
EXTERN  void g_Sleep_Wakeup_Drive( U08 t_Mode );
EXTERN  void g_SleepWake_Control_10msPeriod( void );
#undef EXTERN

#endif
