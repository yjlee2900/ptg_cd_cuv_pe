/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-27 PM.  7:19:27                                   */
/*  Last update     : 2013-12-27 PM.  7:38:02                                   */
/*  File Name       : Obstacle Detection.c                                      */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define OBSTACLE_DETECT_EXTERN
#include "Basic_Type_Define.h"
#include "Obstacle_Detect.h"
#include "Status_Detect.h"
#include "Control_Command.h"
#include "Spindle_Motor_Drive.h"
#include "Read_Hall_Sensor.h"
#include "Analog_conversion.h"
#include "Sensor_Power_Control.h"
#include "Hazard_Buzzer_Control.h"
#include "PCU_Motor_Drive.h"
#include "Parameterization.h"
#include "PCU_Sequence.h"
#include "EEPROM_Control.h"
#include "Illumination_Control.h"
#include "CAN_Drive_App.h"
#include "PID_Control.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U16 u16_s_Spindle_Antipinch_On_T = 0;
static U08 u08_s_fill_Buffer_wait_T = 0;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Spindle_AntiPinch_10msPeriod( void );
static void s_Spindle_CurrentAntipinch( void );
static void s_HallSensor_Antipinch( void );
/*static void s_Antipinch_Sensor_Detect( void );*/
static void s_PCU_CurrentAntipinch_5msPeriod( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/ 
/* VNS5019A : Current monitor (v) = Output Current(mA)/6000 */
#define SPINDLE_OVER_CURRENT_LIMIT    900  /* K 4436 */
#define SPINDLE_UNDER_CURRENT_LIMIT     5    /* K 4436 */
#define SPINDLE_UNDER_CURRENT_ON_TIME  50   /* 10ms * 50 = 500ms */  
#define CLOSE_ANTI_TIME                31 //30  /* 20 : 160524 */ /* 10 * 10ms = 200ms */
#define CLOSE_ANTI_TIME_ZONE_2         70  /* 10 * 10ms = 200ms */
#define OPEN_ANTI_TIME                 40//20  /* 30 : 160524 */ /* 20 * 10ms = 200ms */
#define HI_TEMP_ANTI_TIME              70  /* 30 * 10ms = 300ms */
#define PCU_OVER_CURRENT_ON_TIME       16  /* 16 * 5ms  = 80ms */  
#define PCU_SHORT_CURRENT_ON_TIME       4  /*  4 * 5ms  = 20ms */ 
#define PCU_UNDER_CURRENT_ON_TIME      20  /* 20 * 5ms  = 100ms */  

#define PCU_OVER_CURRENT_AD     310  /* 6.6A */
#define PCU_SHORT_CURRENT_AD    900  /* 19A  */
#define PCU_UNDER_CURRENT_AD      5  /* 100mA */

/*#define ANTIPINCH_RPM_DOWN     120 */
/*#define ANTIPINCH_PWM_UP       4   */
/*#define ANTIPINCH_CURRENT_UP   10  */

/********************************************************************************/
/*   name     : g_Spindle_AntiPinch_10msPeriod                                  */
/*   function : pwm, rpm, current antipinch check                               */
/********************************************************************************/
static void s_Spindle_AntiPinch_10msPeriod( void )
{                      
    static U16 u16_s_CurrentBuff[5] = { 0,0,0,0,0 };
    static U16 u16_s_RPMBuff[5]     = { 0,0,0,0,0 };
    static U08 u08_s_PWMBuff[5]     = { 0,0,0,0,0 };
    
    u16_s_CurrentBuff[4] = u16_s_CurrentBuff[3];
    u16_s_CurrentBuff[3] = u16_s_CurrentBuff[2];
    u16_s_CurrentBuff[2] = u16_s_CurrentBuff[1];
    u16_s_CurrentBuff[1] = u16_s_CurrentBuff[0];
    u16_s_CurrentBuff[0] = st_g_ADCData.SpindleCur_LH.Value; 
    
    u08_s_PWMBuff[4] = u08_s_PWMBuff[3];
    u08_s_PWMBuff[3] = u08_s_PWMBuff[2];
    u08_s_PWMBuff[2] = u08_s_PWMBuff[1];
    u08_s_PWMBuff[1] = u08_s_PWMBuff[0];
    u08_s_PWMBuff[0] = u08_g_LH_Ratio;
                
    u16_s_RPMBuff[4] = u16_s_RPMBuff[3];           
    u16_s_RPMBuff[3] = u16_s_RPMBuff[2];
    u16_s_RPMBuff[2] = u16_s_RPMBuff[1];
    u16_s_RPMBuff[1] = u16_s_RPMBuff[0];
    u16_s_RPMBuff[0] = u16_g_LH_MotorRPM;            
    
    if( (u08_s_PWMBuff[4] <= u08_s_PWMBuff[3]) &&
        (u08_s_PWMBuff[3] <= u08_s_PWMBuff[2]) &&
        (u08_s_PWMBuff[2] <= u08_s_PWMBuff[1]) &&
        (u08_s_PWMBuff[1] <= u08_s_PWMBuff[0]) &&
        (u08_s_PWMBuff[4] <  u08_s_PWMBuff[0])    )
    {
        if( (u16_s_RPMBuff[4] >= u16_s_RPMBuff[3]) &&
            (u16_s_RPMBuff[3] >= u16_s_RPMBuff[2]) &&
            (u16_s_RPMBuff[2] >= u16_s_RPMBuff[1]) &&
            (u16_s_RPMBuff[1] >= u16_s_RPMBuff[0]) &&
            (u16_s_RPMBuff[4] >  u16_s_RPMBuff[0])    )
        {
            if( (u16_s_CurrentBuff[4] <= u16_s_CurrentBuff[3]) &&
                (u16_s_CurrentBuff[3] <= u16_s_CurrentBuff[2]) &&
                (u16_s_CurrentBuff[2] <= u16_s_CurrentBuff[1]) &&
                (u16_s_CurrentBuff[1] <= u16_s_CurrentBuff[0]) &&
                (u16_s_CurrentBuff[4] <  u16_s_CurrentBuff[0])    )
            {
                if( (u08_g_Init_Param == ON) && (st_g_Vehiclest.bSpeedOver3Km != ON) && (u16_s_Spindle_Antipinch_On_T == 0) )
                {
                    if( s08_g_Convert_Temp > 50 )
                    {
                        u16_s_Spindle_Antipinch_On_T = HI_TEMP_ANTI_TIME;
                    }
                    else
                    {    
                        if( u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE  ) 
                        {
                            if( s16_g_Tailgate_Position < u16_g_EEP_Para.ZONE2Pos )
                            {
                                if( s16_g_Tailgate_Position < 10 ) 
                                {
                                    u16_s_Spindle_Antipinch_On_T = 100;
                                } 
                                else 
                                {
                                    u16_s_Spindle_Antipinch_On_T = CLOSE_ANTI_TIME_ZONE_2;
                                }
                            }
                            else
                            {        
                                u16_s_Spindle_Antipinch_On_T = CLOSE_ANTI_TIME;
                            }
                        }    
                        else
                        {   
                            u16_s_Spindle_Antipinch_On_T = OPEN_ANTI_TIME;
                        }
                    }
                }
            }
        }
    }
    
    if( (u16_s_CurrentBuff[0] < u16_s_CurrentBuff[4]) || (u16_g_LH_MotorRPM > (u16_g_Target_RPM - 400)) ) 
    {
        u16_s_Spindle_Antipinch_On_T = 0;
    }
    
    if( u16_s_Spindle_Antipinch_On_T > 0 )
    {
        u16_s_Spindle_Antipinch_On_T--;
        if( (u16_s_Spindle_Antipinch_On_T == 0) && (st_g_Vehiclest.bSpeedOver3Km != ON) )
        {        
            u08_g_Spindle_Antipinch_St = ON;
        }
    }    
#if 0
    if( u08_s_fill_Buffer_wait_T > 5 )
    {    
        if( ((u16_s_CurrentBuff[4] + ANTIPINCH_CURRENT_UP) <= u16_s_CurrentBuff[0]) && 
              ((u08_s_PWMBuff[4] + ANTIPINCH_PWM_UP) <=  u08_s_PWMBuff[0]) && 
              ((u16_s_RPMBuff[0] + ANTIPINCH_RPM_DOWN) <= u16_s_RPMBuff[4]) &&
              ( s16_g_Tailgate_Position > 80 ) && (u16_g_LH_MotorRPM < (u16_g_Target_RPM - 200)) &&
              (st_g_Vehiclest.bSpeedOver3Km != ON) && ( s08_g_Convert_Temp < 40 ) ) 
        {                                                                    
            u16_s_Spindle_Antipinch_On_T = 0;
            u08_g_Spindle_Antipinch_St = ON;
        }
    }
    else
    {
        u08_s_fill_Buffer_wait_T++;
    }  
#endif
}

/********************************************************************************/
/*   name     : g_Spindle_CurrentAntipinch                                      */
/*   function : spindle Motor Current Antipinch                                 */
/*              Overcurrent 6.6A, short Current 16.6A                           */
/********************************************************************************/
static void s_Spindle_CurrentAntipinch(void)
{
	  static U08 u08_t_UnderCurrent_T = 0;
	
    /* Over Current Antipinch */   
    /*if((st_g_ADCData.SpindleCur_LH.Valid == ON)&&(st_g_ADCData.SpindleCur_RH.Valid == ON))*/
    if( st_g_ADCData.SpindleCur_LH.Valid == ON )	
    {    
        /*if( (st_g_ADCData.SpindleCur_LH.Value > SPINDLE_OVER_CURRENT_LIMIT) ||
            (st_g_ADCData.SpindleCur_RH.Value > SPINDLE_OVER_CURRENT_LIMIT)    )*/
        if( st_g_ADCData.SpindleCur_LH.Value > SPINDLE_OVER_CURRENT_LIMIT )
        {
            if( /*(st_g_Vehiclest.bSpeedOver3Km != ON) &&*/ (u08_g_AntiPinch_Start_Delay_T == 0) )
            {
                u08_g_Spindle_Antipinch_St = ON; 
                u08_g_SpindleCurrentFailCnt++;
                u08_g_SpindleOverUnderCurrentSt = 1;
            }
        }
        
        if( st_g_ADCData.SpindleCur_LH.Value < SPINDLE_UNDER_CURRENT_LIMIT )
        {
            u08_t_UnderCurrent_T++;
            if( u08_t_UnderCurrent_T > SPINDLE_UNDER_CURRENT_ON_TIME)
            {
            	  u08_g_SpindleCurrentFailCnt++;
                u08_g_SpindleOverUnderCurrentSt = 1;
            }	
        }
        else
        {
        	  u08_t_UnderCurrent_T = 0;
        }		
    }
    
    /*  Overcurrent limit value 16.6A for Parmeterization of the detection Mecanical End position  */
    if( ( ((u08_g_Init_Param == OFF) && ( s16_g_Tailgate_Position > 300 )) || (u16_g_EEP_Diag.InLine_Mode == TRUE) ) && 
        (u08_g_AntiPinch_Start_Delay_T == 0) )
    {    
        if( (st_g_ADCData.SpindleCur_LH.Value > 500) ||  
            (st_g_ADCData.SpindleCur_RH.Value > 500)    )
        {
            u08_g_Spindle_Antipinch_St = ON;
        }
    }    
}

/********************************************************************************/
/*   name     : s_HallSensor_Antipinch                                          */
/*   function : spindle Motor Hall Sensor Antipinch                             */
/*                                                                              */
/********************************************************************************/
static void s_HallSensor_Antipinch( void )
{
    /*if((u16_g_LH_EncoderPeriod >= u16_MAX_HALL_DELAY_TIME) || ( u16_g_RH_EncoderPeriod >= u16_MAX_HALL_DELAY_TIME))*/
    if( u16_g_LH_EncoderPeriod >= u16_MAX_HALL_DELAY_TIME)
    {
        if( /*(st_g_Vehiclest.bSpeedOver3Km != ON) &&*/ (u08_g_AntiPinch_Start_Delay_T == 0) )
        {
            u08_g_Spindle_Antipinch_St = ON;
            /*if( (u08_g_PowerMoveInit1_NoHallData_LH == ON) || (u08_g_PowerMoveInit2_NoHallData_RH == ON) )*/
            if( u08_g_PowerMoveInit1_NoHallData_LH == ON ) 
            {
                u08_g_Hall_Sensor_Error = ON;
                /*g_SetBuzzHazardControl( BUZZONLY_ON, 6);*/
                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                u08_g_Buzzer_Hazard_Count = 6; 
            }
        }
    }     
}

/********************************************************************************/
/*   name     : s_Antipinch_Sensor_Detect                                       */
/*   function : Tailgate Antipinch Sensor Detect                                */
/*              5ms task                                                        */
/********************************************************************************/
#if 0
static void s_Antipinch_Sensor_Detect( void )
{

    static U08 u08_t_APS_ON_Delay = 0; 
    static U08 u08_s_APS_Open_Delay_t = 0;
    
    const U16 AntiSensor_Tbl[21] = {
    /* Battery Volt                   0     1     2     3     4     5     6     7     8     9     */
                                    120,  120,  120,  120,  120,  120,  130,  130,  170,  190,
    /* Battery Volt                  10    11    12    13    14    15    16    17    18    19     */
                                    220,  250,  280,  310,  330,  360,  380,  380,  380,  380,
    /* Battery Volt                  20                                                  */
                                    380 }; 
   
    /* normal status : 9V-296, 10V-333, 11V-367, 12V-404, 13V-441, 14V-478, 15V-509, 16V-547, 17V-584 */ 
    
    const U16 AntiSensor_Tbl_Open[21] = {
    /* Battery Volt                   0     1     2     3     4     5     6     7     8     9     */
                                    /*120,  120,  120,  120,  120,  120,  240,  320,  330,  390,*/
                                    120,  120,  120,  120,  120,  120,  240,  340,  350,  410,
    /* Battery Volt                  10    11    12    13    14    15    16    17    18    19     */
                                    /*420,  470,  520,  580,  630,  680,  740,  800,  850,  900,*/
                                    420,  490,  540,  600,  650,  700,  760,  820,  850,  900,
    /* Battery Volt                  20                                                  */
                                    980 };                                      
    /* if( (u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) || (u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE) ) */
    if( (u08_g_LatchSt != LATCH_LOCK) && (u08_g_AntiSensor_Power_St == ON) )  /* Antipinch Sensor Detect Close Only */
    {
        if( (st_g_ADCData.Antipinch_LH.Valid == 1) && (st_g_ADCData.Antipinch_RH.Valid ==1) )
        {    
            if( (st_g_ADCData.Antipinch_LH.Value <= AntiSensor_Tbl[u08_g_clacBatteryVolt]) ||
                (st_g_ADCData.Antipinch_RH.Value <= AntiSensor_Tbl[u08_g_clacBatteryVolt])    )
            {
                if( (st_g_Vehiclest.bSpeedOver3Km != ON) && (u08_g_AntiPinch_Start_Delay_T == 0) )
                {   
                    if( u08_g_PCU_Mode == CINCHING_RUN )
                    {
                        u08_g_PCU_Antipinch_St = ANTIPINCH_PCU_UNLATCH;
                        u08_g_StopToMove_T = CINCHING_ANTI_OPEN_DELAY_T;
                        u08_g_Antipinch_Reverse_Mode = OPEN_TAILGATE;
                    }
                }
                
                u08_g_Antipinch_Sensor_St = ON;
                
                if(st_g_ADCData.Antipinch_LH.Value <= AntiSensor_Tbl[u08_g_clacBatteryVolt])
                {
                    u08_g_APS_LH_St = 1;
                }
                if(st_g_ADCData.Antipinch_RH.Value <= AntiSensor_Tbl[u08_g_clacBatteryVolt])
                {
                    u08_g_APS_RH_St = 1;
                }        
                
            }
            else if( (st_g_ADCData.Antipinch_LH.Value > AntiSensor_Tbl_Open[u08_g_clacBatteryVolt]) ||
                     (st_g_ADCData.Antipinch_RH.Value > AntiSensor_Tbl_Open[u08_g_clacBatteryVolt])    )
            {
            	  u08_s_APS_Open_Delay_t++;
            	  
            	  if( u08_s_APS_Open_Delay_t > 20 )
            	  {	
                    u08_g_Antipinch_Sensor_St = ON;
                }
                if(st_g_ADCData.Antipinch_LH.Value > AntiSensor_Tbl_Open[u08_g_clacBatteryVolt])
                {
                    u08_g_APS_LH_St = 3;
                    if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (u16_g_EEP_Diag.InLine_Mode != TRUE) && 
                    	  (u08_t_APS_ON_Delay >= 40) &&( u08_s_APS_Open_Delay_t > 20))
                    {     
                        if( u16_g_EEP_Diag.APS_Error_LH != TRUE )
                        {    
                            u16_g_EEP_Diag.APS_Error_LH = TRUE;
                            u08_g_EEP_Write_Event = TRUE; 
                        }
                        PtlTrans.APS_Error_LH = TRUE;
                    }
                }
                if(st_g_ADCData.Antipinch_RH.Value > AntiSensor_Tbl_Open[u08_g_clacBatteryVolt])
                {
                    u08_g_APS_RH_St = 3;
                    if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (u16_g_EEP_Diag.InLine_Mode != TRUE) && 
                    	  (u08_t_APS_ON_Delay >= 40) &&( u08_s_APS_Open_Delay_t > 20))
                    {     
                        if( u16_g_EEP_Diag.APS_Error_RH != TRUE )
                        {    
                            u16_g_EEP_Diag.APS_Error_RH = TRUE;
                            u08_g_EEP_Write_Event = TRUE; 
                        }
                        PtlTrans.APS_Error_RH = TRUE;
                    }
                }

            }
            else
            {
                u08_g_Antipinch_Sensor_St = OFF;
                u08_g_APS_LH_St = 0;
                u08_g_APS_RH_St = 0;
                PtlTrans.APS_Error_LH = FALSE;
                PtlTrans.APS_Error_RH = FALSE;
                u08_t_APS_ON_Delay = 0; 
                u08_s_APS_Open_Delay_t = 0;
            }
        } 

        if( u08_t_APS_ON_Delay < 40 )
        {
            u08_t_APS_ON_Delay++;
        }
    }
    else
    { 
        u08_g_Antipinch_Sensor_St = OFF;
        u08_g_APS_LH_St = 0;
        u08_g_APS_RH_St = 0;
        st_g_ADCData.Antipinch_LH.Valid = 0;
        st_g_ADCData.Antipinch_LH.Count = 0;
        st_g_ADCData.Antipinch_RH.Valid = 0;
        st_g_ADCData.Antipinch_RH.Count = 0;  
        u08_t_APS_ON_Delay = 0;  
        u08_s_APS_Open_Delay_t = 0;    
    }
}
#endif                            
/********************************************************************************/
/*   name     : s_PCU_CurrentAntipinch_5msPeriod                                */
/*   function : PCU Over/short Current Detect                                   */
/*              5ms task                                                        */
/********************************************************************************/
static void s_PCU_CurrentAntipinch_5msPeriod( void )
{                                    
    static U08 u08_s_OverCurrentON_Time       = 0;    
    static U08 u08_s_ShortCurrentON_Time      = 0; 
    static U08 u08_s_OpenCurrentON_Time       = 0;   
    
    if( (u08_g_PCU_MotorSt == CINCHING_PCU) || (u08_g_PCU_MotorSt == REWIND_PCU) )
    {
        if( st_g_ADCData.PCUCurrent.Valid == ON )
        {    
            if( st_g_ADCData.PCUCurrent.Value > PCU_OVER_CURRENT_AD )  
            {
                u08_s_OverCurrentON_Time++;
                
                if( st_g_ADCData.PCUCurrent.Value > PCU_SHORT_CURRENT_AD )
                {
                    u08_s_ShortCurrentON_Time++;
                }
                else
                {
                    u08_s_ShortCurrentON_Time = 0;
                } 
            }
            else
            {      
                u08_s_ShortCurrentON_Time = 0;
                u08_s_OverCurrentON_Time  = 0; 
                
                if( st_g_ADCData.PCUCurrent.Value < PCU_UNDER_CURRENT_AD )
                {
                    u08_s_OpenCurrentON_Time++;
                    if( u08_s_OpenCurrentON_Time > PCU_UNDER_CURRENT_ON_TIME )
                    {
                    	 u08_g_PCU_Antipinch_St = ON;
                    }	
                }
                else
                {
                    u08_s_OpenCurrentON_Time = 0;	
                }		
            }    
        }
        
        if( (u08_s_OverCurrentON_Time > PCU_OVER_CURRENT_ON_TIME) || (u08_s_ShortCurrentON_Time > PCU_SHORT_CURRENT_ON_TIME) ||
        	  ( u08_s_OpenCurrentON_Time > PCU_UNDER_CURRENT_ON_TIME ) )
        {
            u08_g_PCU_Antipinch_St = ON;
            u08_s_OverCurrentON_Time = 0;
            u08_s_ShortCurrentON_Time = 0; 
            u08_s_OpenCurrentON_Time = 0;
            /*g_SetBuzzHazardControl( BUZZONLY_ON, 6);*/
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6; 
        }        
    }
    else
    {   
        u08_s_OverCurrentON_Time  = 0;
        u08_s_ShortCurrentON_Time = 0; 
        u08_s_OpenCurrentON_Time = 0;
    }    
}

/********************************************************************************/
/*   name     : g_Obstacle_Spindle_10msPeriod                                   */
/*   function : Obstacle Antipinch tesk                                         */
/*   10ms period task                                                           */
/********************************************************************************/
void g_Obstacle_Spindle_10msPeriod(void)
{
     if( (u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) || (u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE) )
     {
         if( u08_g_AntiPinch_Start_Delay_T > 0 )
         {
             u08_g_AntiPinch_Start_Delay_T--;
             u08_s_fill_Buffer_wait_T =  0;
         }
         else
         {   
             s_Spindle_AntiPinch_10msPeriod();
         }
         
         s_Spindle_CurrentAntipinch();
         s_HallSensor_Antipinch();
     }
     else
     {
         u08_g_Spindle_Antipinch_St = OFF; 
         u16_s_Spindle_Antipinch_On_T = 0;
     }      
}

/********************************************************************************/
/*   name     : g_Obstacle_Sensor_5msPeriod                                     */
/*   function : Obstacle Antipinch tesk                                         */
/*   10ms period task                                                           */
/********************************************************************************/
void g_Obstacle_Sensor_5msPeriod(void)
{
    /*s_Antipinch_Sensor_Detect();*/
    s_PCU_CurrentAntipinch_5msPeriod();
}

/********************************************************************************/
/*   name     : g_Obstacle_Value_Init                                           */
/*   function : Obstacle check value initialization                             */
/*                                                                              */
/********************************************************************************/
void g_Obstacle_Value_Init(void)
{
    u08_g_Spindle_Antipinch_St = 0;    
    u08_g_Antipinch_Sensor_St = 0;
    u08_g_PCU_Antipinch_St = 0;        
    u08_g_AntiPinch_Start_Delay_T = 0; 
    u08_g_Hall_Sensor_Error = 0;       
    u08_g_AntiSensor_Power_St = OFF;
    u08_g_SpindleOverUnderCurrentSt = 0;  
    u08_g_SpindleCurrentFailCnt = 0;
}
