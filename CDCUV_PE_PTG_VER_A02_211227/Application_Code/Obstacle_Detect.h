/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-27 PM.  7:19:27                                   */
/*  Last update     : 2013-05-29 AM.  10:10:49                                  */
/*  File Name       : Obstacle Detection.c                                      */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		OBSTACLE_H_EXTERN
#define		OBSTACLE_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	OBSTACLE_DETECT_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define    ANTIPINCH_START_DELAY     70  /* 10ms task * 70 : 700ms  */
#define    ANTIPINCH_PCU_UNLATCH  2

/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_Spindle_Antipinch_St;
EXTERN U08 	u08_g_Antipinch_Sensor_St;
EXTERN U08 	u08_g_PCU_Antipinch_St;
EXTERN U08  u08_g_AntiPinch_Start_Delay_T;
EXTERN U08  u08_g_Hall_Sensor_Error;
EXTERN U08  u08_g_APS_LH_St;
EXTERN U08  u08_g_APS_RH_St;
EXTERN U08  u08_g_SpindleOverUnderCurrentSt;
EXTERN U08  u08_g_SpindleCurrentFailCnt;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_Obstacle_Spindle_10msPeriod(void); 
EXTERN  void g_Obstacle_Sensor_5msPeriod(void);
EXTERN  void g_Obstacle_Value_Init(void);
#undef EXTERN

#endif
