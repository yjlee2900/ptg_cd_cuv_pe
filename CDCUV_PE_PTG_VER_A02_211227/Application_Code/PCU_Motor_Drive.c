/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-30 PM.  6:31:47                                   */
/*	Last update     : 2013-05-23 PM.  6:51:43                                   */
/*	File Name       : Spindle_Motor_Drive.c                                     */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define PCU_MOTOR_DRIVE_EXTERN
#include "Basic_Type_Define.h"
#include "PCU_Motor_Drive.h"
#include "Status_Detect.h"
#include "PCU_EN_A.h"
#include "PCU_EN_B.h"
#include "PCU_IN_A.h"
#include "PCU_IN_B.h"
#include "PCU_PWM.h"
#include "PCU_Multisense_EN.h"
/* freescale bean full bridge ic add */
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_PCU_MT_OutPWM( U08 u08_Ratio );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define u08_FULL_BRIT_OFF      0
#define u08_FULL_BRIT_ON       1


/********************************************************************************/
/*   name     : s_PCU_MT_OutPWM                                                 */
/*   function : PCU Motor pwm Out Control                                       */
/*                                                                              */
/********************************************************************************/
static void s_PCU_MT_OutPWM( U08 u08_Ratio ) 
{
    u08_g_Result_call = PCU_PWM_SetRatio8( 255U - u08_Ratio ); 
}

/********************************************************************************/
/*   name     : g_PCU_MTDrive_Direction_Init                                    */
/*   function : full bridge IC Direction initial                                */
/********************************************************************************/
void g_PCU_MTDrive_Out( U08 u08_MotorOut )
{
    U08 t_duty = 0;
    U08 u08_EN_A = u08_FULL_BRIT_OFF;
    U08 u08_EN_B = u08_FULL_BRIT_OFF;
    U08 u08_IN_A = u08_FULL_BRIT_OFF;
    U08 u08_IN_B = u08_FULL_BRIT_OFF;
    U08 u08_PCU_MT_EN = u08_FULL_BRIT_OFF;
    
    const U08 Tbl_PCUPwmDuty[21] = {
    /* Battery Volt                   0    1    2    3    4    5    6    7    8    9     */
                                     250, 250, 250, 250, 250, 250, 250, 250, 250, 250,
    /* Battery Volt                  10   11   12   13   14   15   16   17   18   19     */
                                     250, 230, 230, 220, 210, 210, 200, 200, 200, 190,
    /* Battery Volt                  20                                                  */
                                     190 };  
                                     
    if( u08_MotorOut != OFF_PCU )
    {
        /*u08_EN_A = u08_FULL_BRIT_ON;*/
        /*u08_EN_B = u08_FULL_BRIT_ON;*/
        u08_PCU_MT_EN = u08_FULL_BRIT_ON;
    }    
    
    switch( u08_MotorOut )
    {
        case  OFF_PCU :
            t_duty = 0;
            break;
        case  CINCHING_PCU : 
            u08_EN_A = u08_FULL_BRIT_ON;
            u08_IN_A = u08_FULL_BRIT_ON;
            if( s08_g_Convert_Temp >= 40 ) /* 40th Over */
            {    
                t_duty = 250;
            }
            else
            {
              t_duty = Tbl_PCUPwmDuty[u08_g_clacBatteryVolt];
            }    
            break;
        case  REWIND_PCU :
            /*u08_EN_A = u08_FULL_BRIT_ON;*/
            u08_IN_B = u08_FULL_BRIT_ON;
            t_duty = 178;   /* 125 ( 50% out ) -> 178 ( 70% out ) -> 100% */
            break;
        case  BREAK_PCU :
            t_duty = 255;/* high out == Motor LOW */
            break;
        default :
            break;    
    }
    
    PCU_Multisense_EN_PutVal( u08_PCU_MT_EN );
    PCU_EN_A_PutVal( u08_EN_A ); /* Sel 0 */
    PCU_EN_B_PutVal( u08_EN_B ); /* Sel 1 */
    PCU_IN_A_PutVal( u08_IN_A ); 
    PCU_IN_B_PutVal( u08_IN_B ); 
    s_PCU_MT_OutPWM( t_duty );
    u08_g_PCU_MotorSt = u08_MotorOut;
}


