/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-09 PM.  2:22:24                                   */
/*	Last Update     : 2013-04-09 PM.  2:22:24                                   */
/*	File Name       : Read_Hall_Sensor.h                                        */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		READ_HALL_SENSOR_H
#define		READ_HALL_SENSOR_H

#include "Basic_Type_Define.h"

#ifndef  	READ_HALL_SENSOR_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define u16_MAX_HALL_DELAY_TIME    5000  /* 100us * 5000 = 500ms */
#define u08_MOTOR_CW   0
#define u08_MOTOR_CCW  1
#define u08_MOTOR_HOLD 3
 
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U16 	u16_g_LH_EncoderPeriod;
EXTERN U16 	u16_g_RH_EncoderPeriod;
EXTERN S16 	s16_g_LH_Position_Cnt;
EXTERN S16 	s16_g_RH_Position_Cnt;
EXTERN U08 	u08_g_LH_Motor_Dir;
EXTERN U08 	u08_g_RH_Motor_Dir;
EXTERN U16  u16_g_LH_MotorRPM;
EXTERN U16  u16_g_RH_MotorRPM;
EXTERN U08  u08_g_PowerMoveInit1_NoHallData_LH;
EXTERN U08  u08_g_PowerMoveInit2_NoHallData_RH;
EXTERN U08  u08_g_LH_HallPatternErrSt;
EXTERN U08  u08_g_RH_HallPatternErrSt;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_Encoder_Scan(void);
EXTERN	void g_Encoder_Init(void);
EXTERN  void g_Get_Motor_RPM_1msPeriod(void);
EXTERN  void g_HallDirectionFaultCheck_10msPeriod(void);
EXTERN  U08 g_Get_LH_Hall( void );
EXTERN  U08 g_Get_RH_Hall( void );
#undef EXTERN

#endif
