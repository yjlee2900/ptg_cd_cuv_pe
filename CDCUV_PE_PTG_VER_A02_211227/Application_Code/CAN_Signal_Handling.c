/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-07-02 PM.  5:14:53                                   */
/*  Last update     : 2014-06-24 PM.  7:05:33                                   */
/*  File Name       : CAN_Signal_Handling.c                                     */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define CAN_SIGNAL_HANDLING_EXTERN
#include "Can_Signal_Handling.h"
#include "Control_Command.h"
#include "User_Logic_Control.h"
#include "EEPROM_Control.h"
#include "Spindle_Motor_Drive.h"
#include "CAN_Drive_App.h"
#include "Status_Detect.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Get_CAN_Vihicle_Speed( void );
static void s_Get_CAN_GSelDisP( void );
/*static void s_Get_CAN_InhibitP( void );*/
static void s_Get_CAN_InhibitRMT( void );
static void s_Get_CAN_USMReset( void );
/* static void s_Get_CAN_TgReleaseRlyAct( void ); */ /* CK Not use */
static void s_Get_CAN_AstUnlockState( void );
static void s_Get_CAN_AstDrSw( void );
static void s_Get_CAN_DrvSrSw( void );
static void s_Get_CAN_RLUnlockState( void );
static void s_Get_CAN_RLDrSw( void );
static void s_Get_CAN_TrunkTgSw( void );
static void s_Get_CAN_TG_ReleaseSW( void );
static void s_Get_CAN_RRUnlockState( void );
static void s_Get_CAN_RRDrSw( void );
static void s_Get_CAN_CrashUnlockStatus( void );
static void s_Get_CAN_DRVUnlockState( void );
/*static void s_Get_CAN_TgReleaseRly( void );*/
static void s_Get_CAN_BAState( void );
static void s_Get_CAN_IGNSW( void );
static void s_Get_CAN_TrunkOpenOption( void );
static void s_Get_CAN_PTGMNValueSet( void );
static void s_Get_CAN_PTGSpeedNValueSet( void );
static void s_Get_CAN_PassiveTrunk_TG( void );
static void s_Get_CAN_TailgateTrunkRKECMD( void );
/*static void s_Get_CAN_SMKRKECMD( void );*/
static void s_Get_CAN_RKECmd( void );
static void s_Get_CAN_SMKHazard( void ); 
static void s_Get_CAN_ConfTCU( void ); 
static void s_Get_eClutch( void );
static void s_Get_CAN_PTGHeightNValueSet( void ); 
static void s_Get_CAN_PTGCNTCmdAVN( void );
static void s_Tx_CAN_PTGM_St( void );
static void s_Tx_CAN_PTGReleaseRly( void );

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define LATCH_RELEASE_T  100  /* 10ms * 100 = 1Sec */ 

/********************************************************************************/
/*   name     : s_Get_CAN_Vihicle_Speed ,                                       */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_Vihicle_Speed( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_VehicleSpeedCLU_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_VehicleSpeedCLU_0DataChanged();
        u08_g_C_VehicleSpeed = IlGetRxC_VehicleSpeedCLU_0();
        CAN_Event.C_VehicleSpeed = TRUE;
        u08_g_CANsignal_Change = TRUE; 
    }    
    
    ON_Flag = IlGetC_VehicleSpeedCLU_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_VehicleSpeedCLU_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_VehicleSpeed = IlGetRxC_VehicleSpeedCLU_0();
            CAN_Event.C_VehicleSpeed = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_Signal_UpDate_check |= 0x10;   
        }
    }    
    
    ON_Flag = IlGetC_VehicleSpeedCLU_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_VehicleSpeedCLU_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 ) &&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_VehicleSpeed = IlGetRxC_VehicleSpeedCLU_0();
            CAN_Event.C_VehicleSpeed = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_GSelDisP ,                                            */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_GSelDisP( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_GSelDisp_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_GSelDisp_0DataChanged();
        u08_g_C_GselDisp = IlGetRxC_GSelDisp_0();
        CAN_Event.C_GselDisp = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_GSelDisp_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_GSelDisp_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_GselDisp = IlGetRxC_GSelDisp_0();
            CAN_Event.C_GselDisp = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_GSelDisp_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_GSelDisp_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_GselDisp = IlGetRxC_GSelDisp_0();
            CAN_Event.C_GselDisp = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_InhibitP ,                                            */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
/*
static void s_Get_CAN_InhibitP( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_InhibitP_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_InhibitP_0DataChanged();
        u08_g_C_InhinitP = IlGetRxC_InhibitP_0();
        CAN_Event.C_InhibitP = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_InhibitP_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_InhibitP_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_InhinitP = IlGetRxC_InhibitP_0();
            CAN_Event.C_InhibitP = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_InhibitP_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_InhibitP_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_InhinitP = IlGetRxC_InhibitP_0();
            CAN_Event.C_InhibitP = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
}
*/
/********************************************************************************/
/*   name     : s_Get_CAN_InhibitRMT ,                                            */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_InhibitRMT( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_InhibitRMT_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_InhibitRMT_0DataChanged();
        u08_g_C_InhibitRMT = IlGetRxC_InhibitRMT_0();
        CAN_Event.C_InhibitRMT = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_InhibitRMT_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_InhibitRMT_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_InhibitRMT = IlGetRxC_InhibitRMT_0();
            CAN_Event.C_InhibitRMT = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_InhibitRMT_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_InhibitRMT_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_InhibitRMT = IlGetRxC_InhibitRMT_0();
            CAN_Event.C_InhibitRMT = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }        
}
/********************************************************************************/
/*   name     : s_Get_CAN_USMReset ,                                            */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_USMReset( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_USMReset_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_USMReset_0DataChanged();
        u08_g_C_USMReset = IlGetRxC_USMReset_0();
        CAN_Event.C_USMReset = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /*ON_Flag = IlGetC_USMReset_0_FirstFirstvalue();                                                                                        */
    /*if( ON_Flag != FALSE )                                                                                                                */
    /*{                                                                                                                                     */
    /*    IlClrC_USMReset_0_FirstFirstvalue();                                                                                              */
    /*    if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*    {                                                                                                                                 */
    /*        u08_g_C_USMReset = IlGetRxC_USMReset_0();                                                                                     */
    /*        CAN_Event.C_USMReset = TRUE;                                                                                                  */
    /*        u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*    }                                                                                                                                 */
    /*}                                                                                                                                     */
    /*                                                                                                                                      */
    /*ON_Flag = IlGetC_USMReset_0_TimeOutRxTimeout();                                                                                       */
    /*if( ON_Flag != FALSE )                                                                                                                */
    /*{                                                                                                                                     */
    /*    IlClrC_USMReset_0_TimeOutRxTimeout();                                                                                             */
    /*    if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*    {                                                                                                                                 */
    /*        u08_g_C_USMReset = IlGetRxC_USMReset_0();                                                                                     */
    /*        CAN_Event.C_USMReset = TRUE;                                                                                                  */
    /*        u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*    }                                                                                                                                 */
    /*}                                                                                                                                     */
}
/********************************************************************************/
/*   name     : s_Get_CAN_TgReleaseRlyAct ,                                     */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
#if 0 
static void s_Get_CAN_TgReleaseRlyAct( void ) /* CK Not use */
{
    U08 ON_Flag;
    
    u08_g_C_TgReleaseRlyAct = 0;
    
   
    ON_Flag = IlGetC_TgReleaseRlyAct_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TgReleaseRlyAct_0DataChanged();
        u08_g_C_TgReleaseRlyAct = IlGetRxC_TrunkTgReleaseRlyAct_0();
        CAN_Event.C_TgReleaseRlyAct = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_TgReleaseRlyAct_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_TgReleaseRlyAct_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TgReleaseRlyAct = IlGetRxC_TrunkTgReleaseRlyAct_0();
            CAN_Event.C_TgReleaseRlyAct = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_TgReleaseRlyAct_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_TgReleaseRlyAct_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TgReleaseRlyAct = IlGetRxC_TrunkTgReleaseRlyAct_0();
            CAN_Event.C_TgReleaseRlyAct = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     

}
#endif

/********************************************************************************/
/*   name     : s_Get_CAN_AstUnlockState ,                                      */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_AstUnlockState( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_AstUnlockState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_AstUnlockState_0DataChanged();
        u08_g_C_AstUnlockState = IlGetRxC_AstUnlockState_0();
        CAN_Event.C_AstUnlockState = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_AstUnlockState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_AstUnlockState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_AstUnlockState = IlGetRxC_AstUnlockState_0();
            CAN_Event.C_AstUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_Signal_UpDate_check |= 0x02;  
        }    
    }    
    
    ON_Flag = IlGetC_AstUnlockState_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_AstUnlockState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_AstUnlockState = IlGetRxC_AstUnlockState_0();
            CAN_Event.C_AstUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_AstDrSw ,                                             */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_AstDrSw( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_AstDrSw_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_AstDrSw_0DataChanged();
        u08_g_C_AstDrSw = IlGetRxC_AstDrSw_0();
        CAN_Event.C_AstDrSw = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_AstDrSw_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_AstDrSw_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_AstDrSw = IlGetRxC_AstDrSw_0();
            CAN_Event.C_AstDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_AstDrSw_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_AstDrSw_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_AstDrSw = IlGetRxC_AstDrSw_0();
            CAN_Event.C_AstDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_DrvSrSw ,                                             */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_DrvSrSw( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_DrvSrSw_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_DrvSrSw_0DataChanged();
        u08_g_C_DrvDrSw = IlGetRxC_DrvDrSw_0();
        CAN_Event.C_DrvDrSw = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_DrvSrSw_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_DrvSrSw_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_DrvDrSw = IlGetRxC_DrvDrSw_0();
            CAN_Event.C_DrvDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_DrvSrSw_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_DrvSrSw_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_DrvDrSw = IlGetRxC_DrvDrSw_0();
            CAN_Event.C_DrvDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_RLUnlockState ,                                       */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_RLUnlockState( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_RLUnlockState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_RLUnlockState_0DataChanged();
        u08_g_C_RLUnlockState = IlGetRxC_RLUnlockState_0();
        CAN_Event.C_RLUnlockState = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_RLUnlockState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_RLUnlockState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RLUnlockState = IlGetRxC_RLUnlockState_0();
            CAN_Event.C_RLUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_Signal_UpDate_check |= 0x04;  
        }   
           
    }    
    
    ON_Flag = IlGetC_RLUnlockState_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_RLUnlockState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RLUnlockState = IlGetRxC_RLUnlockState_0();
            CAN_Event.C_RLUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_RLDrSw ,                                              */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_RLDrSw( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_RLDrSw_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_RLDrSw_0DataChanged();
        u08_g_C_RLDrSw = IlGetRxC_RLDrSw_0();
        CAN_Event.C_RLDrSw = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_RLDrSw_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_RLDrSw_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RLDrSw = IlGetRxC_RLDrSw_0();
            CAN_Event.C_RLDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_RLDrSw_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_RLDrSw_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RLDrSw = IlGetRxC_RLDrSw_0();
            CAN_Event.C_RLDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_TrunkTgSw ,                                           */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_TrunkTgSw( void )
{
   
    U08 ON_Flag;
    
    ON_Flag = IlGetC_TrunkTgSw_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TrunkTgSw_0DataChanged();
        u08_g_C_TrunkTgSW = IlGetRxC_TrunkTgSw_0();
        CAN_Event.C_TrunkTgSW = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_TrunkTgSw_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_TrunkTgSw_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TrunkTgSW = IlGetRxC_TrunkTgSw_0();
            CAN_Event.C_TrunkTgSW = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_TrunkTgSw_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_TrunkTgSw_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TrunkTgSW = IlGetRxC_TrunkTgSw_0();
            CAN_Event.C_TrunkTgSW = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
   
}
/********************************************************************************/
/*   name     : s_Get_CAN_TG_ReleaseSW ,                                        */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_TG_ReleaseSW( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_TrunkTgOtrReleaseSW_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TrunkTgOtrReleaseSW_0DataChanged();
        u08_g_C_TgOtrReleaseSW = IlGetRxC_TrunkTgOtrReleaseSW_0();
        CAN_Event.C_TgOtrReleaseSW = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /* ON_Flag = IlGetC_TG_ReleaseSW_0_FirstFirstvalue();                                                                                    */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_TG_ReleaseSW_0_FirstFirstvalue();                                                                                          */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_TgOtrReleaseSW = IlGetRxC_TrunkTgOtrReleaseSW_0();                                                                    */
    /*         CAN_Event.C_TgOtrReleaseSW = TRUE;                                                                                            */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
    
    ON_Flag = IlGetC_TrunkTgOtrReleaseSW_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_TrunkTgOtrReleaseSW_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TgOtrReleaseSW = IlGetRxC_TrunkTgOtrReleaseSW_0();
            CAN_Event.C_TgOtrReleaseSW = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    if( u08_g_C_TgOtrReleaseSW == 1 )
    {
        if( u16_g_ReleaswSW_On_t < 1000 )
        {    
            u16_g_ReleaswSW_On_t++;
        }
    }
    else
    {
        u16_g_ReleaswSW_On_t = 0;
    }         
}

/********************************************************************************/
/*   name     : s_Get_CAN_RRUnlockState ,                                       */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_RRUnlockState( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_RRUnlockState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_RRUnlockState_0DataChanged();
        u08_g_C_RRUnlockState = IlGetRxC_RRUnlockState_0();
        CAN_Event.C_RRUnlockState = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_RRUnlockState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_RRUnlockState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RRUnlockState = IlGetRxC_RRUnlockState_0();
            CAN_Event.C_RRUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_Signal_UpDate_check |= 0x08;
        }    
    }    
    
    ON_Flag = IlGetC_RRUnlockState_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_RRUnlockState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RRUnlockState = IlGetRxC_RRUnlockState_0();
            CAN_Event.C_RRUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_RRDrSw ,                                              */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_RRDrSw( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_RRDrSw_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_RRDrSw_0DataChanged();
        u08_g_C_RRDrSw = IlGetRxC_RRDrSw_0();
        CAN_Event.C_RRDrSw = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_RRDrSw_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_RRDrSw_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RRDrSw = IlGetRxC_RRDrSw_0();
            CAN_Event.C_RRDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_RRDrSw_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_RRDrSw_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_RRDrSw = IlGetRxC_RRDrSw_0();
            CAN_Event.C_RRDrSw = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_CrashUnlockStatus ,                                   */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_CrashUnlockStatus( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_CrashUnlockState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_CrashUnlockState_0DataChanged();
        u08_g_C_CrashUnlockState = IlGetRxC_CrashUnlockState_0();
        CAN_Event.C_CrashUnlockState = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_CrashUnlockState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_CrashUnlockState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_CrashUnlockState = IlGetRxC_CrashUnlockState_0();
            CAN_Event.C_CrashUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_CrashUnlockState_0_TimeOutRxTimeout();
    if( ON_Flag != FALSE )   
    {
        IlClrC_CrashUnlockState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_CrashUnlockState = IlGetRxC_CrashUnlockState_0();
            CAN_Event.C_CrashUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_DRVUnlockState ,                                      */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_DRVUnlockState( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_DrvUnlockState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_DrvUnlockState_0DataChanged();
        u08_g_C_DrvUnlockState = IlGetRxC_DrvUnlockState_0();
        CAN_Event.C_DrvUnlockState = TRUE;
        u08_g_CANsignal_Change = TRUE;
        u08_g_DDM_Timeout_Flag = FALSE;
    }    
    
    ON_Flag = IlGetC_DrvUnlockState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_DrvUnlockState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_DrvUnlockState = IlGetRxC_DrvUnlockState_0();
            CAN_Event.C_DrvUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_DDM_Timeout_Flag = FALSE;
            u08_g_Signal_UpDate_check |= 0x01;    
        }    
    }    
    
    ON_Flag = IlGetC_DrvUnlockState_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_DrvUnlockState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_DrvUnlockState = IlGetRxC_DrvUnlockState_0();
            CAN_Event.C_DrvUnlockState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_DDM_Timeout_Flag = TRUE;
        }    
    }     
}

/********************************************************************************/
/*   name     : s_Get_CAN_TgReleaseRly ,                                        */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
#if 0  
static void s_Get_CAN_TgReleaseRly( void )
{
    
  
    U08 ON_Flag;
    
    ON_Flag = IlGetC_TgReleaseRly_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TgReleaseRly_0DataChanged();
        u08_g_C_TgReleaseRly = IlGetRxC_TrunkTgReleaseRly_0();
        CAN_Event.C_TgReleaseRly = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_TgReleaseRly_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_TgReleaseRly_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TgReleaseRly = IlGetRxC_TrunkTgReleaseRly_0();
            CAN_Event.C_TgReleaseRly = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_TgReleaseRly_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_TgReleaseRly_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TgReleaseRly = IlGetRxC_TrunkTgReleaseRly_0();
            CAN_Event.C_TgReleaseRly = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
   
}
#endif 
/********************************************************************************/
/*   name     : s_Get_CAN_BAState ,                                             */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_BAState( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_BAState_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_BAState_0DataChanged();
        u08_g_C_BAState = IlGetRxC_BAState_0();
        CAN_Event.C_BAState = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    ON_Flag = IlGetC_BAState_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_BAState_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_BAState = IlGetRxC_BAState_0();
            CAN_Event.C_BAState = TRUE;
            u08_g_CANsignal_Change = TRUE;
            u08_g_Signal_UpDate_check |= 0x20;
        }    
    }    
    
    ON_Flag = IlGetC_BAState_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_BAState_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_BAState = 1; /* Timeout value : Arming */ /* IlGetRxC_BAState_0(); */
            CAN_Event.C_BAState = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }     
}


/********************************************************************************/
/*   name     : s_Get_CAN_IGNSW ,                                               */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_IGNSW( void )
{
    U08 ON_Flag;
    U08 IGN_buff;
    
    ON_Flag = IlGetC_IGNSW_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_IGNSW_0DataChanged();
        IGN_buff = IlGetRxC_IGNSw_0();
        if( IGN_buff <= 4 ) /* Invalid value check */
        {    
            u08_g_C_IGNSw = IGN_buff;
            CAN_Event.C_IGNSw = TRUE;
        }
        u08_g_CANsignal_Change = TRUE;
    }    
 
    ON_Flag = IlGetIGNSW_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrIGNSW_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            IGN_buff = IlGetRxC_IGNSw_0();
            if( IGN_buff <= 4 )  /* Invalid value check */   
            {    
                u08_g_C_IGNSw = IGN_buff;
                CAN_Event.C_IGNSw = TRUE;
                u08_g_C_IGN_Fail = 0;
            }
            else
            {
                u08_g_C_IGN_Fail = 1;
            }
            
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetIGNSW_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrIGNSW_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            IGN_buff = IlGetRxC_IGNSw_0();
            if( IGN_buff <= 4 ) /* Invalid value check */   
            {    
                u08_g_C_IGNSw = IGN_buff;
                CAN_Event.C_IGNSw = TRUE;
            }
            u08_g_CANsignal_Change = TRUE;
        }    
    } 
    
    if( u08_g_Recheck_IGNSt_flag == TRUE )
    {
        u08_g_Recheck_IGNSt_flag = FALSE;
        u08_g_C_IGNSw = IlGetRxC_IGNSw_0();
    }        


    if( (u16_g_EEP_Diag.InLine_Mode == TRUE) && (u08_g_C_IGNSw == KEY_CRANK) ) /* inline mode cranking status check */
    {
        u08_g_C_IGNSw = KEY_IGN;
    }
         
}

/********************************************************************************/
/*   name     : s_Get_CAN_TrunkOpenOption ,                                     */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_TrunkOpenOption( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_TrunkOpenDiagSet_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TrunkOpenDiagSet_0DataChanged();
        u08_g_C_TrunkOpenOption = IlGetRxC_TrunkOpenDiagSet_0();
        CAN_Event.C_TrunkOpenOption = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
 
    ON_Flag = IlGetC_TrunkOpenDiagSet_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_TrunkOpenDiagSet_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TrunkOpenOption = IlGetRxC_TrunkOpenDiagSet_0();
            CAN_Event.C_TrunkOpenOption = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_TrunkOpenDiagSet_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_TrunkOpenDiagSet_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TrunkOpenOption = IlGetRxC_TrunkOpenDiagSet_0();
            CAN_Event.C_TrunkOpenOption = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
}

/********************************************************************************/
/*   name     : s_Get_CAN_PTGMNValueSet ,                                       */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_PTGMNValueSet( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_PTGMNValueSet_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_PTGMNValueSet_0DataChanged();
        u08_g_C_PTGMNValueSet = IlGetRxC_PTGMNValueSet_0();
        CAN_Event.C_PTGMNValueSet = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /* ON_Flag = IlGetC_PTGMNValueSet_0_FirstFirstvalue();                                                                                   */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_PTGMNValueSet_0_FirstFirstvalue();                                                                                         */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_PTGMNValueSet = IlGetRxC_PTGMNValueSet_0();                                                                           */
    /*         CAN_Event.C_PTGMNValueSet = TRUE;                                                                                             */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
    /*                                                                                                                                       */
    /* ON_Flag = IlGetC_PTGMNValueSet_0_TimeOutRxTimeout();                                                                                  */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_PTGMNValueSet_0_TimeOutRxTimeout();                                                                                        */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_PTGMNValueSet = IlGetRxC_PTGMNValueSet_0();                                                                           */
    /*         CAN_Event.C_PTGMNValueSet = TRUE;                                                                                             */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
}

/********************************************************************************/
/*   name     : s_Get_CAN_PTGSpeedNValueSet ,                                   */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_PTGSpeedNValueSet( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_PTGSpeedNValueSet_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_PTGSpeedNValueSet_0DataChanged();
        u08_g_C_PTGSpeedNValueSet = IlGetRxC_PTGSpeedNValueSet_0();
        CAN_Event.C_PTGSpeedNValueSet = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
}

/********************************************************************************/
/*   name     : s_Get_CAN_PassiveTrunk_TG ,                                     */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_PassiveTrunk_TG( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_PassiveTrunkTg_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_PassiveTrunkTg_0DataChanged();
        u08_g_C_PassiveTrunkTg = IlGetRxC_PassiveTrunkTg_0();
        CAN_Event.C_PassiveTrunkTg = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /* ON_Flag = IlGetC_PassiveTrunk_TG_0_FirstFirstvalue();                                                                                  */
    /* if( ON_Flag != FALSE )                                                                                                                 */
    /* {                                                                                                                                      */
    /*     IlClrC_PassiveTrunk_TG_0_FirstFirstvalue();                                                                                        */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))   */
    /*     {                                                                                                                                  */
    /*         u08_g_C_PassiveTrunkTg = IlGetRxC_PassiveTrunkTg_0();                                                                          */
    /*         CAN_Event.C_PassiveTrunkTg = TRUE;                                                                                             */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                 */
    /*     }                                                                                                                                  */
    /* }                                                                                                                                      */
    /*                                                                                                                                        */
    /* ON_Flag = IlGetC_PassiveTrunk_TG_0_TimeOutRxTimeout();                                                                                 */
    /* if( ON_Flag != FALSE )                                                                                                                 */
    /* {                                                                                                                                      */
    /*     IlClrC_PassiveTrunk_TG_0_TimeOutRxTimeout();                                                                                       */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))   */
    /*     {                                                                                                                                  */
    /*         u08_g_C_PassiveTrunkTg = IlGetRxC_PassiveTrunkTg_0();                                                                          */
    /*         CAN_Event.C_PassiveTrunkTg = TRUE;                                                                                             */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                 */
    /*     }                                                                                                                                  */
    /* }                                                                                                                                      */
}

/********************************************************************************/
/*   name     : s_Get_CAN_SMKRKECMD ,                                           */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
/*static void s_Get_CAN_SMKRKECMD( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_SMKRKECmd_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_SMKRKECmd_0DataChanged();
        u08_g_C_SMKRKECmd = IlGetRxC_SMKRKECmd_0();
        CAN_Event.C_SMKRKECmd = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
}*/
/********************************************************************************/
/*   name     : s_Get_CAN_RKECmd ,                                              */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_RKECmd( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_RKECmd_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_RKECmd_0DataChanged();
        u08_g_C_RkeCmd = IlGetRxC_RKECmd_0();
        CAN_Event.C_RkeCmd = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /* ON_Flag = IlGetC_RKECmd_0_FirstFirstvalue();                                                                                          */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_RKECmd_0_FirstFirstvalue();                                                                                                */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_RkeCmd = IlGetRxC_RKECmd_0();                                                                                         */
    /*         CAN_Event.C_RkeCmd = TRUE;                                                                                                    */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
    /*                                                                                                                                       */
    /* ON_Flag = IlGetC_RKECmd_0_TimeOutRxTimeout();                                                                                         */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_RKECmd_0_TimeOutRxTimeout();                                                                                               */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_RkeCmd = IlGetRxC_RKECmd_0();                                                                                         */
    /*         CAN_Event.C_RkeCmd = TRUE;                                                                                                    */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
}

/********************************************************************************/
/*   name     : s_Get_CAN_SMKHazard ,                                           */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_SMKHazard( void ) 
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_HazardFromSMK_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_HazardFromSMK_0DataChanged();
        u08_g_C_HazardfromSMK = IlGetRxC_HazardFromSMK_0();
        CAN_Event.C_HazardfromSMK = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
    
    /* ON_Flag = IlGetC_HazardFromSMK_0Firstvalue();                                                                                          */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClrC_HazardFromSMK_0FirstFirstvalue();                                                                                                */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_HazardfromSMK = IlGetRxC_HazardFromSMK_0();                                                                                        */
    /*         CAN_Event.C_HazardfromSMK = TRUE;                                                                                                    */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
    /*                                                                                                                                       */
    /* ON_Flag = IlGet_HazardFromSMK_0TimeOutRxTimeout();                                                                                         */
    /* if( ON_Flag != FALSE )                                                                                                                */
    /* {                                                                                                                                     */
    /*     IlClr_HazardFromSMK_0TimeOutRxTimeout();                                                                                               */
    /*     if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))  */
    /*     {                                                                                                                                 */
    /*         u08_g_C_HazardfromSMK = IlGetRxC_HazardFromSMK_0();                                                                                         */
    /*         CAN_Event.C_HazardfromSMK = TRUE;                                                                                                        */
    /*         u08_g_CANsignal_Change = TRUE;                                                                                                */
    /*     }                                                                                                                                 */
    /* }                                                                                                                                     */
}

/********************************************************************************/
/*   name     : s_Get_CAN_ConfTCU ,                                             */
/*   function : TCU Type information                                            */
/********************************************************************************/
static void s_Get_CAN_ConfTCU( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_ConfTCU_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_GSelDisp_0DataChanged();
        u08_g_C_ConfTCU = IlGetRxC_ConfTCU_0();
        CAN_Event.C_ConfTCU = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
 
    ON_Flag = IlGetC_ConfTCU_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_ConfTCU_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_ConfTCU = IlGetRxC_ConfTCU_0();
            CAN_Event.C_ConfTCU = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_ConfTCU_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_ConfTCU_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_ConfTCU = IlGetRxC_ConfTCU_0();
            CAN_Event.C_ConfTCU = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
}

/********************************************************************************/
/*   name     : s_Get_CAN_ConfTCU ,                                             */
/*   function : TCU Type information                                            */
/********************************************************************************/
static void s_Get_CAN_TailgateTrunkRKECMD( void )
{
    U08 ON_Flag;

    ON_Flag = IlGetC_TailgateTrunkRKECMD_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_TailgateTrunkRKECMD_0DataChanged();
        u08_g_C_TailgateTrunkRKECMD = IlGetRxC_TailgateTrunkRKECMD_0();
        CAN_Event.C_TailgateTrunkRKECMD = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
 
    ON_Flag = IlGetC_TailgateTrunkRKECMD_0Firstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_TailgateTrunkRKECMD_0Firstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_TailgateTrunkRKECMD = IlGetRxC_TailgateTrunkRKECMD_0();
            CAN_Event.C_TailgateTrunkRKECMD = TRUE;
            u08_g_CANsignal_Change = TRUE;
        } 
        
        if( u08_g_C_TailgateTrunkRKECMD == TAILGATE_TRUN_ON )
        {	
            u16_g_RKE_TimeOut_t = TAILGATE_TRUNK_TIMEOUT;
        }
    }	

    if( u16_g_RKE_TimeOut_t != 0 )
    {
        u16_g_RKE_TimeOut_t--;	
    }    
}

/********************************************************************************/
/*   name     : s_Get_eClutch ,                                                 */
/*   function : eclutch Gear Position information                               */
/********************************************************************************/
static void s_Get_eClutch( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_G_SEL_DISP_eClutch_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_G_SEL_DISP_eClutch_0DataChanged();
        u08_g_C_eClutch = IlGetRxG_SEL_DISP_eClutch_0();
        CAN_Event.C_eClutch = TRUE;
        u08_g_CANsignal_Change = TRUE;
    }    
 
    ON_Flag = IlGetC_G_SEL_DISP_eClutch_0_FirstFirstvalue();
    if( ON_Flag != FALSE )  
    {
        IlClrC_G_SEL_DISP_eClutch_0_FirstFirstvalue();   
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_eClutch = IlGetRxG_SEL_DISP_eClutch_0();
            CAN_Event.C_eClutch = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
    
    ON_Flag = IlGetC_G_SEL_DISP_eClutch_0_TimeOutRxTimeout();     
    if( ON_Flag != FALSE )   
    {
        IlClrC_G_SEL_DISP_eClutch_0_TimeOutRxTimeout();  
        if(( NmStateBusSleep( NmGetStatus() ) == 0 )&&(PtlTrans.CanBusOff != TRUE)&&(PtlTrans.kDescCommControlStateDisableFlag != TRUE))
        {
            u08_g_C_eClutch = IlGetRxG_SEL_DISP_eClutch_0();
            CAN_Event.C_eClutch = TRUE;
            u08_g_CANsignal_Change = TRUE;
        }    
    }    
}

/********************************************************************************/
/*   name     : s_Get_CAN_PTGHeightNValueSet ,                                  */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_PTGHeightNValueSet( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_PTGHeightNValueSet_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_PTGHeightNValueSet_0DataChanged();
        u08_g_C_PTGHeightNValueSet = IlGetRxC_PTGHeightNValueSet_0();
        if( u08_g_C_PTGHeightNValueSet != 0)
        {
            CAN_Event.C_PTGHeightNValueSet = TRUE;
        }
        u08_g_CANsignal_Change = TRUE;
    }	
}

/********************************************************************************/
/*   name     : s_Get_CAN_PTGCNTCmdAVN ,                                        */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Get_CAN_PTGCNTCmdAVN( void )
{
    U08 ON_Flag;
    
    ON_Flag = IlGetC_PTGCNTLCmdAVN_0DataChanged();
    if( ON_Flag != FALSE )
    {
        IlClrC_PTGCNTLCmdAVN_0DataChanged();
        u08_g_C_PTGCNTCmdAVN = IlGetRxC_PTGCNTLCmdAVN_0();
        CAN_Event.C_PTGCNTCmdAVN = TRUE;
        u08_g_CANsignal_Change = TRUE;
        c6_CGWmsg04_0_c = 0x30; // Set 'default' again because it is a event message.
    }	
}

/********************************************************************************/
/*   name     : s_Tx_CAN_PTGM_St ,                                              */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Tx_CAN_PTGM_St( void )
{
    if (NmStateActive(NmGetStatus()) != 0)
    {    
        IlPutTxC_PTGMOpt_0(TRUE); /* 1:On, 0:Off */
        IlPutTxC_TrnkTlgtOption_0(2);
        IlPutTxC_PTGAntiPinchSnsr_0(2);
        IlPutTxC_PTGOpnClsState_0(u08_g_C_PTGOpenClsState);
        IlPutTxC_PTGCmdWarn_0(u08_g_CmdWarn);

        if(u08_g_PTGM_Power_Mode == TRUE)
        {
            IlPutTxC_PTGMode_0(0);  /* 0:PowerMode, 1:ManualMode  */
            IlPutTxC_PTGMRValue_0(1);
            IlPutTxC_PTGSpeedRValue_0((U08)u16_g_EEP_Diag.Speed_Set_Value);
            IlPutTxC_PTGHeightRValue_0((U08)u16_g_EEP_Para.User_Hight_SetValue);
        }
        else
        {
            IlPutTxC_PTGMode_0(1);
            IlPutTxC_PTGMRValue_0(2);
            IlPutTxC_PTGSpeedRValue_0(3); /* if power mode is manual Mode then Speedvalue transmitt invalid value */
            IlPutTxC_PTGHeightRValue_0(7);
        }
    }
}

/********************************************************************************/
/*   name     : s_Tx_CAN_PTGReleaseRly ,                                        */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Tx_CAN_PTGReleaseRly( void )
{
    static U08 u08_s_PTGReleaseRly_t = 0;      /* PTG Release Relay Control Time */
    static U08 u08_s_PTGReleaseRly_Wake_t = 0; /* PTG Release Relay CAN wake delay Time */
        
    if( (u08_g_Tx_ReleaseRly == 1) && (u08_g_Moving_Delay_T == 0) )
    {
        u08_g_Tx_ReleaseRly = 0;
                     
        if( NmStateBusSleep(NmGetStatus()) == 0 )
        {
            IlPutTxC_PTGReleaseRly_0(1);
            u08_s_PTGReleaseRly_Wake_t = 0;
            u08_s_PTGReleaseRly_t = LATCH_RELEASE_T;
        }
        else
        {
            GotoMode (Awake);
            u08_g_CanSleep_flag = FALSE;
            u16_g_InitSleepDelTm_Att = INIT_SLEEP_DEL_T;
            u08_s_PTGReleaseRly_Wake_t = 10; /* 100ms wait */
            u08_s_PTGReleaseRly_t = (LATCH_RELEASE_T + 10);
        }        
    }    
    
    if( u08_s_PTGReleaseRly_t > 0 )
    {
        u08_s_PTGReleaseRly_t--;    
        if( u08_s_PTGReleaseRly_Wake_t > 0 )
        {
            u08_s_PTGReleaseRly_Wake_t--;
            if( u08_s_PTGReleaseRly_Wake_t == 0 )
            {
                IlPutTxC_PTGReleaseRly_0(1);
            }
        }
    }  
    else
    {
        if( NmStateActive(NmGetStatus()) != 0 )
        {
            IlPutTxC_PTGReleaseRly_0(0); 
        } 
    }    
}

/********************************************************************************/
/*   name     : g_Tx_CAN_Hazard                                                 */
/*   function : CAN Signal Tailgate Hazard Lamp Control                         */
/*                                                                              */
/********************************************************************************/
void g_Tx_CAN_Hazard(U08 St)
{
    IlPutTxC_PTGHazard_0(St);
}


/********************************************************************************/
/*   name     : g_CAN_Signal_Handling1_10msPeriod_TX                              */
/*   function : CAN Signal Tx/Rx Handling, 10ms period task                     */
/*                                                                              */
/********************************************************************************/
void g_CAN_Signal_Handling1_10msPeriod_TX(void)
{
    s_Tx_CAN_PTGM_St();
    s_Tx_CAN_PTGReleaseRly();
}
/********************************************************************************/
/*   name     : g_CAN_Signal_Handling1_10msPeriod_1                              */
/*   function : CAN Signal Tx/Rx Handling, 10ms period task                     */
/*                                                                              */
/********************************************************************************/
void g_CAN_Signal_Handling1_10msPeriod_1(void)
{                    
    s_Get_CAN_TrunkOpenOption();
    s_Get_CAN_Vihicle_Speed();
    s_Get_CAN_GSelDisP();
    s_Get_eClutch();
    /*s_Get_CAN_InhibitP();*/
    s_Get_CAN_InhibitRMT();
    s_Get_CAN_USMReset(); 
    /* s_Get_CAN_TgReleaseRlyAct(); */ /* CK Not use */
    s_Get_CAN_AstUnlockState();
    s_Get_CAN_AstDrSw();
    s_Get_CAN_ConfTCU();
}

/********************************************************************************/
/*   name     : g_CAN_Signal_Handling2_10msPeriod_2                              */
/*   function : CAN Signal Tx/Rx Handling, 10ms period task                     */
/*                                                                              */
/********************************************************************************/
void g_CAN_Signal_Handling2_10msPeriod_2(void)
{
    s_Get_CAN_DrvSrSw();
    s_Get_CAN_RLUnlockState();
    s_Get_CAN_RLDrSw();
    s_Get_CAN_TrunkTgSw();
    s_Get_CAN_TG_ReleaseSW();
    s_Get_CAN_RRUnlockState();
    s_Get_CAN_RRDrSw();
    s_Get_CAN_CrashUnlockStatus();
}

/********************************************************************************/
/*   name     : g_CAN_Signal_Handling3_10msPeriod_3                              */
/*   function : CAN Signal Tx/Rx Handling, 10ms period task                     */
/*                                                                              */
/********************************************************************************/
void g_CAN_Signal_Handling3_10msPeriod_3(void)
{
    s_Get_CAN_DRVUnlockState();
    /*s_Get_CAN_TgReleaseRly();*/
    s_Get_CAN_BAState();
    s_Get_CAN_IGNSW();
    s_Get_CAN_PTGMNValueSet();
    s_Get_CAN_PTGHeightNValueSet();
    s_Get_CAN_PTGSpeedNValueSet();
    s_Get_CAN_PassiveTrunk_TG();
    /*s_Get_CAN_SMKRKECMD();*/
    s_Get_CAN_TailgateTrunkRKECMD();
    s_Get_CAN_RKECmd();
    s_Get_CAN_SMKHazard();
    s_Get_CAN_PTGCNTCmdAVN();
}
