/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-14 AM.  11:00:07                                  */
/*  Last update     : 2013-05-14 AM.  11:00:12                                  */
/*  File Name       : Analog_conversion.h                                       */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                        MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef     ANALOG_CON_C_H_EXTERN
#define     ANALOG_CON_C_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef     ANALOG_CONVERSION_EXTERN
#define     EXTERN     extern
#else
#define     EXTERN   
#endif

/********************************************************************************/
/*  Macro                                                                       */
/********************************************************************************/
/* #define    u08_XXXXXXXX_XXX    3000 */  /* 100us task count */
/********************************************************************************/
/*  Global Variable                                                             */
/********************************************************************************/


typedef struct  /* adc data buffer frame */
{
    U08     Count;                     /* data conversion count */
    U08     Valid;                     /* Value (averaged data) is valid */
    U16     Read;                      /* conversion value */
    U16     Value;                     /* averaged value */
} stAdcSignal;

typedef struct  /* adc input signal collection */
{
    stAdcSignal BattMon;              /* battery voltage monitoring signal */
    stAdcSignal TempMon;              /* temperature sensor input signal */
    stAdcSignal DriveVoltage;         /* Motor Dirve IC input Voltage signal */
    stAdcSignal SpindleCur_LH;        /* lid driving motor current monitoring signal */
    stAdcSignal SpindleCur_RH;        /* lid driving motor current monitoring signal */
    stAdcSignal PCUCurrent;           /* cinching motor current monitoring signal */
    stAdcSignal HallPower_LH;         /* trunk position sensor - not used */
    stAdcSignal HallPower_RH;         /* trunk position sensor - not used */
    stAdcSignal Antipinch_LH;         /* trunk position sensor - not used */
    stAdcSignal Antipinch_RH;         /* trunk position sensor - not used */    
} stAnalogSignal;


EXTERN stAnalogSignal st_g_ADCData;

/*
EXTERN U08 u08_g_Convert_Batt;
EXTERN U08 u08_g_Convert_SpLH;
EXTERN U08 u08_g_Convert_SpRH;
EXTERN U08 u08_g_Convert_PCU;
EXTERN U08 u08_g_Convert_Hall_LH;
EXTERN U08 u08_g_Convert_Hall_RH;
EXTERN U08 u08_g_Convert_Anti_RH;
EXTERN U08 u08_g_Convert_Anti_RH;
*/
/********************************************************************************/
/*  extern function                                                             */
/********************************************************************************/
EXTERN  void g_ADC_ConversionPeriod5ms( void );
EXTERN  void g_ADC_value_Init( void );

#undef EXTERN

#endif
