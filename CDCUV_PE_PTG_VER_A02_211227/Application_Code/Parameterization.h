/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-20 PM.  2:22:59                                   */
/*  Last update     : 2013-05-20 PM.  2:23:02                                   */
/*  File Name       : Parameterization.c                                        */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                       */
/********************************************************************************/
#ifndef		PARAMETERIZATION_H_EXTERN
#define		PARAMETERIZATION_H_EXTERN

#include "AP_DataType.h"
#include "Basic_Type_Define.h"

#ifndef  	PARAMETERIZAITON_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define MECHANICAL_END_CONST    1510 /* PTG Spindle Hall Count - ON :952 , CD WGN : 1242 , CD SB : 1017  CD CUV : 1358.6*/   
#define SPINDLE_MAX_MECH_LIMIT  1500 /* Tunning point */
#define SPINDLE_MIN_MECH_LIMIT  1200 /* Tunning point */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/

EXTERN enumParam u08_g_ParamStatus;
EXTERN U16 u16_g_ParamerInitTimer;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN    void g_Param_Init( void );
EXTERN    void g_User_Open_Position( void );
EXTERN    void g_Parameterization_10msPeriod( void );
EXTERN    void g_Parameter_Reset( void );

#undef EXTERN

#endif
