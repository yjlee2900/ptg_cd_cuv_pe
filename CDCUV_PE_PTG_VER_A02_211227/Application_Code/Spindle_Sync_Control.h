/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-31 PM.  3:24:11                                   */
/*  Last update     : 2013-05-31 PM. 3:24:13                                    */
/*  File Name       : Spindle_Sync_Control.h                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		SPINDLE_SYNC_H_EXTERN
#define		SPINDLE_SYNC_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	SPINDLE_SYNC_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/

/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN S08 	s08_g_RH_Sync_Ratio; /* Correction control RH Only */
EXTERN U08 	u08_g_Sync_Fail_Stop; /* Correction control RH Only */
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_Spindle_SyncControl_20msPeriod( void );
EXTERN  void g_Spindle_Sync_Init( void );

#undef EXTERN

#endif
