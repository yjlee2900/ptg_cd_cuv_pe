/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-16 PM.  2:17:39                                   */
/*  Last update     : 2014-03-11 PM.  2:58:00                                   */
/*  File Name       : Status_Detect.c                                           */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define STATUS_DETECT_EXTERN
#include "Basic_Type_Define.h"
#include "Status_Detect.h"
#include "Digital_Input_Chattering.h"
#include "EEPROM_Control.h"
#include "Analog_conversion.h"  
#include "Read_Hall_Sensor.h"
#include "Parameterization.h" 
#include "Fail_Safe.h"
#include "Obstacle_Detect.h"
#include "CAN_Signal_Handling.h"
#include "CAN_Drive_App.h"
#include "Spindle_Motor_Drive.h"
#include "Sensor_Power_Control.h"  
#include "Read_Hall_Sensor.h"
#include "Control_Command.h"
#include "User_Logic_Control.h"
#include "Thermal_Protect.h"
#include "Sleep_Wakeup_Control.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
 

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_GetSystemBattSt( void );
static void s_Get_CAN_DoorSt( void );
static void s_Get_CAN_VehicleSpeed( void );
static void s_Get_CAN_GearPos( void );
static void s_GetVehilcleStCAN( void ); 
static void s_UnLatchStCheck( void );
static void s_LatchStCheck( void );
static void s_GetTempValue( void );  
static void s_TailGate_Positon_LatchSt_Check( void );
static void s_BatteryVoltageCompare( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*  System Tunning Constants                        */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*#define ADC_SENSE_BATTERY_20V           631*/ /*750*/   /* 20.00v */
                                            /*   */
/*#define ADC_SENSE_BATTERY_19V           618*/ /*708*/   /* 18.00v */
/*#define ADC_SENSE_BATTERY_18_5V         610*/ /*689*/   /* 17.50v */
                                            /*   */
#define ADC_SENSE_BATTERY_18_2V         695 /*669*/   /* 18.00v */
#define ADC_SENSE_BATTERY_18V           687 /*(608+5)*/ /* 650 */ /* 17.50v */
#define ADC_SENSE_BATTERY_17V           648 /*630    */   /* 17.00v */
/*#define ADC_SENSE_BATTERY_16_8V         578 *//* 588   */ /*610*/   /* 16.80v */
#define ADC_SENSE_BATTERY_16_5V         638 /* 588   */ /*610*/   /* 16.50v */
/*#define ADC_SENSE_BATTERY_16_3V         567*/ /*(571+5) */ /*596*/ /* 16.30v */
#define ADC_SENSE_BATTERY_16V           609 /*(571+5) */ /*596*/ /* 16.00v */
/*#define ADC_SENSE_BATTERY_15_5V         560*/ /*577*/   /* 15.50v */
/*#define ADC_SENSE_BATTERY_15V           543*/ /*558*/   /* 15.00v */
/*#define ADC_SENSE_BATTERY_14_5V         527*/ /*539*/   /* 14.50v */
/*#define ADC_SENSE_BATTERY_14V           512*/ /*519*/   /* 14.00v */
/*#define ADC_SENSE_BATTERY_13_5V         596*/ /*500*/   /* 13.50v */
/*#define ADC_SENSE_BATTERY_13V           480*/ /* 13.00v */

/*#define ADC_SENSE_BATTERY_12_5V         462*/ /* 12.50v */
/*#define ADC_SENSE_BATTERY_12V           443*/ /* 12.00v */
/*#define ADC_SENSE_BATTERY_11_5V         423*/ /* 11.50v */
/*#define ADC_SENSE_BATTERY_11V           403*/ /* 11.00v */
/*#define ADC_SENSE_BATTERY_10_5V         384*/ /* 10.50v */
/*#define ADC_SENSE_BATTERY_10V           365*/ /* 10.00v */
/*#define ADC_SENSE_BATTERY_9_5V          346*/ /* 9.50 v */
#define ADC_SENSE_BATTERY_9V            330 /*(326-5)*/   /* 9.00 v */

#define ADC_SENSE_BATTERY_8_5V          311 /* 8.50 v */
/*#define ADC_SENSE_BATTERY_8V            286*/ /* 8.00 v */
/*#define ADC_SENSE_BATTERY_7_5V          266*/ /*(269-9)*/   /* 7.50 v */
#define ADC_SENSE_BATTERY_7V            252 /* 7.00 v */
#define ADC_SENSE_BATTERY_6_8V          244 /* 6.00 v */

/*#define ADC_SENSE_BATTERY_5V            170*/ /* 4.98 v */
/*#define ADC_SENSE_BATTERY_LOW           150*/ /* 4.45 v */

#define BATVOLT_DIV                     37  
/*#define BATVOLT_DELTA                   1*/


/********************************************************************************/
/*   name     : s_MCU_Voltage_St ,                                              */
/*   function : get MCU Power voltage status                                    */
/********************************************************************************/
static void s_MCU_Voltage_St( void )
{
    if(CPMULVCTL_LVDS==TRUE)
    {
        u08_g_MCU_Voltage_Error = 1U;
    }
    else
    {
        u08_g_MCU_Voltage_Error = 0U;
    }
}

/********************************************************************************/
/*   name     : s_GetSystemBattSt ,                                             */
/*   function : Low/High Battery Status detect, CAN&PTGM Enable/Disable         */
/********************************************************************************/
static void s_GetSystemBattSt( void )
{                         
    static U08 u08_s_CAN_Enable_Delay_T = 0;
    static U08 u08_s_PTG_Disable_Bat_T = 0;
    U16 Compensation_Value = 0;
    
    if( st_g_ADCData.BattMon.Valid == ON )
    {
        /******** Battery voltage check for CAN Enable/Disable ***************/
        if( (st_g_ADCData.BattMon.Value > ADC_SENSE_BATTERY_18_2V ) ||
            (st_g_ADCData.BattMon.Value < ADC_SENSE_BATTERY_6_8V )    )
        {
            st_g_Vehiclest.bBatVoltCAN_Disable = 1;
            u08_s_CAN_Enable_Delay_T = 18; /*  dleay Disable -> Enable */
        }
        else
        {
            if( (st_g_ADCData.BattMon.Value < ADC_SENSE_BATTERY_18V ) &&
                (st_g_ADCData.BattMon.Value > ADC_SENSE_BATTERY_7V )     )
            {
                if( u08_s_CAN_Enable_Delay_T == 0 )
                {    
                    st_g_Vehiclest.bBatVoltCAN_Disable = 0;  
                }
            }    
        }  
        /***********************************************************************/
        
        /********** Battery voltage check for PTG Enable/Disable ***************/      
        if( (st_g_ADCData.BattMon.Value > ADC_SENSE_BATTERY_17V ) ||
            (st_g_ADCData.BattMon.Value < ADC_SENSE_BATTERY_8_5V ) ||
            ( ((st_g_ADCData.DriveVoltage.Value < 340) || (st_g_ADCData.DriveVoltage.Value > 910)) && (st_g_ADCData.DriveVoltage.Valid == ON) )   )
        {
        	  if( u08_s_PTG_Disable_Bat_T > 10 )
        	  {	
                st_g_Vehiclest.bBatVoltPTG_Disable = 1;
            }
            else
            { 
            	 u08_s_PTG_Disable_Bat_T++;
            }
        }
        else
        {
            if( (st_g_ADCData.BattMon.Value < ADC_SENSE_BATTERY_16_5V ) &&
                (st_g_ADCData.BattMon.Value > ADC_SENSE_BATTERY_9V ) &&
                (st_g_ADCData.DriveVoltage.Value < 900 ) &&
                (st_g_ADCData.DriveVoltage.Value > 350 )     )
            {
                st_g_Vehiclest.bBatVoltPTG_Disable = 0;  
                u08_s_PTG_Disable_Bat_T = 0;
            }    
        }
        /***********************************************************************/
        
        /********** Battery voltage check for DTC Check Enable/Disable**********/   
        if( (st_g_ADCData.BattMon.Value > ADC_SENSE_BATTERY_16V ) ||
            (st_g_ADCData.BattMon.Value < ADC_SENSE_BATTERY_9V )   )
        {
            st_g_Vehiclest.bBatVoltDTC_Disable = 1;
        }
        else
        {
            st_g_Vehiclest.bBatVoltDTC_Disable = 0;
        }         
        /***********************************************************************/
#if 0        
        if( st_g_ADCData.BattMon.Value > 500 ) /* over 14V Compensation */
        {
            Compensation_Value = ((st_g_ADCData.BattMon.Value - 500)/2); 
            if( st_g_ADCData.BattMon.Value > 620)
            {
                Compensation_Value += 25;
            }    
        }
#endif        
        u08_g_clacBatteryVolt = (U08)((st_g_ADCData.BattMon.Value+Compensation_Value) / BATVOLT_DIV); 
        
        if( u08_g_clacBatteryVolt > 20 )
        {
            u08_g_clacBatteryVolt = 20;
        }
        
        if( u08_s_CAN_Enable_Delay_T > 0 )
        {
            u08_s_CAN_Enable_Delay_T--;
        }
    }  
}

/********************************************************************************/
/*   name     : s_GetTempValue ,                                                */
/*   function : thermister voltage converting Temp vlaue                        */
/********************************************************************************/
static void s_GetTempValue( void )
{
	 U08 i; 
     static U16 u16_s_preTempAD_value = 0; 
     const U16 u16_s_TempTable_Value[23] = {
           /* -30, -25, -20, -15, -10,  -5,  -0,   5,  10,  15       */
               90, 125, 150, 180, 215, 255, 295, 340, 380, 425, 
           /*  20,  25,  30,  35,  40,  45,  50,  55,  60,  65       */    
              475, 530, 580, 625, 665, 700, 735, 770, 790, 815, 
           /*  70,  75,  80     */       
              840, 860, 885
              };  /* -30 ~ 80 */  /* 547 : 20 , 590 : 33 */
              
     if( (st_g_ADCData.TempMon.Valid == ON) && (u16_s_preTempAD_value != st_g_ADCData.TempMon.Value) )
     {      
	     if( (st_g_ADCData.TempMon.Value < u16_s_TempTable_Value[0]) || (st_g_ADCData.TempMon.Value > u16_s_TempTable_Value[22]) )
	     {
	        s08_g_Convert_Temp = 50;
	     }
	     else
	     {
	         for( i=0; i<22; i++ )
	         {
	             if( (st_g_ADCData.TempMon.Value >= u16_s_TempTable_Value[i]) &&
	                 (st_g_ADCData.TempMon.Value < u16_s_TempTable_Value[i+1])  )
	             {
	                 s08_g_Convert_Temp = \
	                 (S08)((i*5) + (U08)((st_g_ADCData.TempMon.Value - u16_s_TempTable_Value[i]) / ((u16_s_TempTable_Value[i+1] - u16_s_TempTable_Value[i]) / 5)));
	                 break;
	             }
	         }
	     }
	     
	     s08_g_Convert_Temp = (s08_g_Convert_Temp - 30);
	     
	     u16_s_preTempAD_value = st_g_ADCData.TempMon.Value;
	 }      
}

/********************************************************************************/
/*   name     : s_Get_CAN_DoorSt ,                                              */
/*   function : Vehicle Status Detect by CAN Rx Signal                          */
/********************************************************************************/
static void s_Get_CAN_DoorSt( void )
{
    U08 Door_St_buff = 0;
             
    if( (CAN_Event.C_DrvUnlockState == 1) || (CAN_Event.C_AstUnlockState == 1) ||
        (CAN_Event.C_RLUnlockState == 1) || (CAN_Event.C_RRUnlockState == 1) )
    {    
        CAN_Event.C_DrvUnlockState = 0;
        CAN_Event.C_AstUnlockState = 0;
        CAN_Event.C_RLUnlockState = 0;
        CAN_Event.C_RRUnlockState = 0;
        CAN_Event.C_CrashUnlockState = 0;
        
        Door_St_buff += ((u08_g_C_DrvUnlockState == DOOR_ULOCK)?  1U : 0U);
        Door_St_buff += ((u08_g_C_AstUnlockState == DOOR_ULOCK)?  1U : 0U);         
        Door_St_buff += ((u08_g_C_RLUnlockState == DOOR_ULOCK)?  1U : 0U);   
        Door_St_buff += ((u08_g_C_RRUnlockState == DOOR_ULOCK)?  1U : 0U); 
            
        if( Door_St_buff == 0 )
        {                         
            st_g_Vehiclest.DoorLockSt = ALL_DOOR_LOCK; 
        }       
        else if( Door_St_buff == 4 )  
        {
            st_g_Vehiclest.DoorLockSt = ALL_DOOR_UNLOCK;
        }
        else
        {    
            st_g_Vehiclest.DoorLockSt = ANY_DOOR_LOCK;
        }
    }    
}

/********************************************************************************/
/*   name     : s_Get_CAN_VehicleSpeed ,                                        */
/*   function : Vehicle Status Detect by CAN Rx Signal                          */
/********************************************************************************/
static void s_Get_CAN_VehicleSpeed( void )
{
    static U08 u08_s_Pre_IGN = 0;
    static U08 u08_s_Update_Delay = 0;
    
    if( (u08_s_Pre_IGN != u08_g_C_IGNSw) && (u08_g_C_IGNSw >= KEY_IGN) )
    {
        u08_s_Update_Delay = 60; /* 600ms update Delay */
    }    
    
    /* gateway validity fail - (ign off && speed signal timeout value(oxFF)) speed = 0 */
    if( ( (u08_g_C_VehicleSpeed == 0xFF) && ((u08_s_Update_Delay != 0) || (u08_g_C_IGNSw < KEY_IGN)) ) ||
        ( u16_g_EEP_Diag.InLine_Mode == TRUE ) ) 
    {                                                                 
       st_g_Vehiclest.bSpeedOver3Km = FALSE;
    }                                                                 
    else
    {
        st_g_Vehiclest.bSpeedOver3Km = (u08_g_C_VehicleSpeed >= 3)  ?  1U : 0U;    
    }   
     
    if( u08_s_Update_Delay != 0 )
    {
        u08_s_Update_Delay--;
    } 
    
    u08_s_Pre_IGN = u08_g_C_IGNSw;
}

/********************************************************************************/
/*   name     : s_Get_CAN_GearPos ,                                             */
/*   function : Vehicle Status Detect by CAN Rx Signal                          */
/********************************************************************************/
static void s_Get_CAN_GearPos( void )
{
    if(u08_g_C_IGNSw >= KEY_IGN)
    {
        if( (( u08_g_C_GselDisp == 0) && (u08_g_C_ConfTCU != 0xF) && (u08_g_C_ConfTCU != 0x9)) ||
            ((st_g_swinput.swGearN_Position.swInput == enON) && (u08_g_C_InhibitRMT == 0) && (u08_g_C_ConfTCU == 0xF)) ||
            ((u08_g_C_ConfTCU == 0x9) && (u08_g_C_eClutch == 0)) )
        {
            st_g_Vehiclest.bGearPositionEn = 1;
        }
        else
        {
            st_g_Vehiclest.bGearPositionEn = 0;
        }
    }
    else
    {
        st_g_Vehiclest.bGearPositionEn = 1;
    } 
}

/********************************************************************************/
/*   name     : g_GetPTG_PositionRate                                           */
/*   function : User intput Operating tatail gate Open Positon Rate calculation */
/*                                                                              */
/********************************************************************************/
void g_GetUser_PositionRate(void)
{
    
    if( (u08_g_LatchSt != LATCH_OPEN) && (u08_g_Latch_Unexpect == OFF)  ) /* full close */
    {
        u08_g_User_Position = TAILGATE_ZONE_0;
    }
    else if( s16_g_Tailgate_Position > u16_g_EEP_Para.MaxPowerOpenPos ) /* Over Open */
    {
        u08_g_User_Position = TAILGATE_ZONE_4;
    }
    else if( s16_g_Tailgate_Position > u16_g_EEP_Para.ZONE3Pos ) /* 10% over Open */
    {
        u08_g_User_Position = TAILGATE_ZONE_3;
    }
    else if( s16_g_Tailgate_Position > u16_g_EEP_Para.ZONE2Pos )
    {
        u08_g_User_Position = TAILGATE_ZONE_2;     /* 10% under */
    }
    else
    {
        u08_g_User_Position = TAILGATE_ZONE_1;     /* 5% under */
    }
}

/********************************************************************************/
/*   name     : s_UnLatchStCheck                                                */
/*   function : UnLatch status Detect 10ms polling                              */
/*                                                                              */
/********************************************************************************/
static void s_UnLatchStCheck(void)
{
#if 0
    if( u08_g_Latch_Releasewait == 1 ) 
    {
        if( st_g_swinput.inLatchSecondary.swInput == enOFF )
        {
            if( (u08_Latch_releaseWait_time == 0)  )
            {
                SpindleCmd = tailgate_Open;
                u08_g_Latch_Releasewait = 0;
            }
        }
        else
        {
            u08_Latch_releaseWait_time = 200ms;
            
            if( u08_g_SJB_LatchReleaseEnd == 1 )
            {
                u08_g_SJB_LatchReleaseEnd = 0;
                /* unlatch fail error */
            }
            
            /* no Unlatch command */
            /* ERROR */
        }    
    }    
#endif        
}

/********************************************************************************/
/*   name     : s_LatchStCheck                                                  */
/*   function : Latch status Detect 10ms polling                                */
/*                                                                              */
/********************************************************************************/
static void s_LatchStCheck(void)
{
    U08 latch_Buff = 0;
    static U08 u08_s_LatchFaultCnt = 0;
    
    latch_Buff += ( st_g_swinput.inLatchSecondary.swInput == enON ) ? 0x01U : 0x00U ;
    latch_Buff += ( st_g_swinput.inLatchLock.swInput == enON )      ? 0x02U : 0x00U ;
    /* latch_Buff |= ( st_g_swinput.inLatchPrimary.swInput == enON )   ? 0x04 : 0x00 ; */
    
    switch( latch_Buff )
    {
        case 0x00U :
            u08_g_LatchSt = LATCH_OPEN; 
            break;
        case 0x01U :
            u08_g_LatchSt = LATCH_SECONDARY; 
            /*u08_g_Power_OnReset = 0; */
            break;
        case 0x02U :
            u08_g_LatchSt = LATCH_LOCK_ONLY;  
            /*u08_g_Power_OnReset = 0; */
            break;
        case 0x03U :
            u08_g_LatchSt = LATCH_LOCK;
            /*u08_g_Power_OnReset = 0;*/
            break;        
        default :
            u08_g_LatchSt = LATCH_OPEN; 
            break;
    }        
    
    if( u16_g_EEP_Diag.InLine_Mode != TRUE )
    {	
        if( (( u08_g_C_TrunkTgSW == 0 ) && (u08_g_LatchSt != LATCH_LOCK)) || 
        	  (( u08_g_C_TrunkTgSW == 1 ) && (u08_g_LatchSt == LATCH_LOCK)) ||
        	  (u08_g_LatchSt == LATCH_LOCK_ONLY	)  )
        {
            if( u08_s_LatchFaultCnt < 50 ) /* 500ms */
            {	
                u08_s_LatchFaultCnt++;	
                if( u08_s_LatchFaultCnt >= 50 )
                {
                    u08_g_LatchFaultSt = TRUE;
                }	
            }    	
        }
        else
        {
        	  if( ( u08_g_C_TrunkTgSW == 1 ) && (u08_g_LatchSt == LATCH_OPEN) )
        	  {	
                u08_g_LatchFaultSt = 0;	
            }
            u08_s_LatchFaultCnt = 0;
        }
    }
    else
    {
        u08_g_LatchFaultSt = 0;
        u08_s_LatchFaultCnt = 0;	
    }		
}
               
/********************************************************************************/
/*   name     : s_TailGate_Positon_LatchSt_Check                                */
/*   function : tailgate position check & latch status check                    */
/********************************************************************************/                                             
static void s_TailGate_Positon_LatchSt_Check( void )
{                    
    static U08 u08_s_pre_Latchst = 0xFF; 
       
    if( u08_s_pre_Latchst != u08_g_LatchSt )
    {
        if( u08_g_LatchSt != LATCH_OPEN )  
        {    
            if( (s16_g_Tailgate_Position < CLOSE_INIT_POSITION) || (u08_g_Init_Param == OFF) || 
                (u08_g_Power_OnReset == 1) || (u08_g_Hall_Sensor_Error == ON) || 
                (u08_g_Trunk_Position_Error == ON) || (u08_g_HallPowerOnErr_LH == 1) || (u08_g_LH_HallPatternErrSt == 1) )    /* check -> pcu sequence cinching start */
            {
                s16_g_Tailgate_Position = 0;
                s16_g_LH_Position_Cnt = 0;
                /*s16_g_RH_Position_Cnt = 0;*/
                u08_g_Hall_Sensor_Error = OFF;
                u08_g_Trunk_Position_Error = OFF;                 
                PtlTrans.HallError_LH = FALSE;
                /*PtlTrans.HallError_RH = FALSE;*/
                u08_g_LH_HallPatternErrSt = OFF; 
                /*u08_g_RH_HallPatternErrSt = OFF; */ 
                u08_g_SpindleCurrentFailCnt = 0;
                if( (u08_g_Power_OnReset == 0) && (u08_g_LatchSt == LATCH_LOCK) && (u08_g_HallPowerOnErr_LH == 0) )
                {	
                    u08_g_Strain_Cmd = 1;
                    u08_g_HallPwrOffCheckCmd_LH = 1;
                    /*u08_g_HallPwrOffCheckCmd_RH = 1;*/
                }
                u08_g_Power_OnReset = 0;
            }
            else
            {
                if( u08_g_Hall_Sensor_Error == OFF )
                {    
                    u08_g_Latch_Unexpect = ON;
                }
            }           
        }
        else /* full Open detect */
        {
            u08_g_Latch_Unexpect = OFF;    
        }            
    }
    
    if( (u08_g_LatchSt == LATCH_OPEN ) || (u08_g_Latch_Unexpect == ON) )
    {        
        s16_g_Tailgate_Position = s16_g_LH_Position_Cnt;    
        
#if 0        
        if( s16_g_LH_Position_Cnt > s16_g_RH_Position_Cnt )
        {    
            s16_g_Tailgate_Position = s16_g_LH_Position_Cnt;    
        }
        else
        {
            s16_g_Tailgate_Position = s16_g_RH_Position_Cnt; 
        }    
#endif        
    }

    /******** trunk postion Error Mode **************************************************************/
    /*if( (u08_g_Init_Param == ON) && (u08_g_Power_OnReset == 0) && 
        ( ((s16_g_LH_Position_Cnt > SPINDLE_MAX_MECH_LIMIT) || (s16_g_LH_Position_Cnt < -70)) || 
          ((s16_g_RH_Position_Cnt > SPINDLE_MAX_MECH_LIMIT) || (s16_g_RH_Position_Cnt < -70)) )  )*/
    if( (u08_g_Init_Param == ON) && (u08_g_Power_OnReset == 0) && 
        ((s16_g_LH_Position_Cnt > SPINDLE_MAX_MECH_LIMIT) || (s16_g_LH_Position_Cnt < -70))  )
    {
        u08_g_Trunk_Position_Error = ON;
        
        if( u08_g_LatchSt != LATCH_OPEN )
        {
            s16_g_LH_Position_Cnt = 0;
            /*s16_g_RH_Position_Cnt = 0;*/
        }
        else
        {
            u08_g_Latch_Unexpect = OFF;    
            s16_g_LH_Position_Cnt = 1300;
            /*s16_g_RH_Position_Cnt = 900;*/
        }    
    }    
    /*************************************************************************************************/   
    
    u08_s_pre_Latchst = u08_g_LatchSt;
}

/********************************************************************************/
/*   name     : s_BatteryVoltageCompare ,                                       */
/*   function : Battery Voltage & Dirve IC Input Voltage Compare                */
/********************************************************************************/
static void s_BatteryVoltageCompare( void )
{
    S16 s16_t_ErrValue = 0;  
    S16 s16_t_mDtctCmpVal = 0;
    
    const U16 u16_t_MtrVoltCmpnstnTable[21] = {
           /*  0V    1V    2V    3V    4V    5V    6V    7V    8V    9V */
               0U,   0U,   0U,   0U,   0U,   0U,  93U, 104U, 116U, 129U,
           /* 10V   11V   12V   13V   14V   15V   16V   17V   18V   19V */
             140U, 152U, 152U, 170U, 180U, 200U, 200U, 220U, 230U, 240U,
           /*  20V */
             250U };
             
    if( (u08_g_SpindlePowerMov_Direction == STOP_TAILGATE) && (st_g_ADCData.DriveVoltage.Valid == 1) &&
    	  (st_g_ADCData.BattMon.Valid == 1) && (u08_g_SpindlePowerMov_Direction == STOP_TAILGATE) )
    {	
        s16_t_mDtctCmpVal = st_g_ADCData.DriveVoltage.Value - (u16_t_MtrVoltCmpnstnTable[u08_g_clacBatteryVolt] + st_g_ADCData.BattMon.Value);
        s16_t_ErrValue = (st_g_ADCData.BattMon.Value / 7);
        if((s16_t_mDtctCmpVal >= s16_t_ErrValue) || (s16_t_mDtctCmpVal <= (s16_t_ErrValue * -1)))
        {
            u08_g_DetectVoltCmprErrSts = 1U;
        }
        else
        {
            s16_t_ErrValue = st_g_ADCData.BattMon.Value / 10;
            if( (s16_t_mDtctCmpVal < s16_t_ErrValue) || (s16_t_mDtctCmpVal > (s16_t_ErrValue * -1)) )
            {
                u08_g_DetectVoltCmprErrSts = 0U;
            }
        }
    }
    else
    {
    	 u08_g_DetectVoltCmprErrSts = 0;
    }	
}

/********************************************************************************/
/*   name     : s_OpenStatusMotorBreakSet                                       */
/*   function : Break set on Vehicle speed 3km/h Over and Open Direction Detected*/
/********************************************************************************/
static void s_OpenStatusMotorBreakSet( void )
{
	  static U08 u08_s_OpenDetec_T = 0;
	  
	  if( st_g_Vehiclest.bSpeedOver3Km == TRUE )
	  {
        if( u08_g_LH_Motor_Dir == u08_MOTOR_CW ) 
        {
            if( u08_s_OpenDetec_T < 50 )
            {
                u08_s_OpenDetec_T++;	
            }	
        }
        else
        {
            u08_s_OpenDetec_T = 0;	
        }
        
        if( ((u08_s_OpenDetec_T >= 50) || (u08_g_LatchSt == LATCH_OPEN)) && 
        	  (u08_g_Tailgate_Moving_Cmd != CLOSE_TAILGATE) ) 
        {	
            u08_g_BreakSetCmd = 1;
        }    
    }
    
    if( (st_g_Vehiclest.bSpeedOver3Km != TRUE) || (u08_g_LatchSt != LATCH_OPEN) || 
    	  (u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) )
    {
        u08_s_OpenDetec_T = 0;
        u08_g_BreakSetCmd = 0;
    }
}

/********************************************************************************/
/*   name     : s_GetVehilcleStCAN ,                                            */
/*   function : Vehicle Status Detect by CAN Rx Signal                          */
/********************************************************************************/
static void s_GetVehilcleStCAN( void )
{
    s_Get_CAN_DoorSt();
    s_Get_CAN_VehicleSpeed();
    s_Get_CAN_GearPos();
}

/********************************************************************************/
/*   name     : g_Past_Failure_Record ,                                         */
/*   function : Past Operation Failure Record Function                          */
/********************************************************************************/
void g_Past_Failure_Record( U08 ErrorCode )
{
    if( u16_g_EEP_Diag.InLine_Mode != TRUE )
    {    
        u16_g_EEP_Diag.PastFailure[u16_g_EEP_Diag.PastFailure_Index] = ErrorCode;
        u16_g_EEP_Diag.PastFailure_Index++;
        if( u16_g_EEP_Diag.PastFailure_Index >= 3 )
        {
            u16_g_EEP_Diag.PastFailure_Index = 0;
        }    
        
        u08_g_EEP_Write_Event = TRUE;
    }
}
/********************************************************************************/
/*   name     : g_DTC_Clear_by_IGN                                              */
/*   function : IGN ON/OFF count 30 DTC Clear, 100ms polling                     */
/*                                                                              */
/********************************************************************************/
void g_DTC_Clear_by_IGN( void )
{
    static U08 u08_s_Pre_IGN_ST = KEY_OFF;
    
    if( (PtlTrans.CanBusOffB == TRUE) || (PtlTrans.CanLineErrorB == TRUE)|| (u16_g_EEP_Diag.HallError_LH == TRUE) ||
        /*(u16_g_EEP_Diag.HallError_RH == TRUE) ||*/ (u16_g_EEP_Diag.APS_Error_LH == TRUE) || (u16_g_EEP_Diag.APS_Error_RH == TRUE) || 
        (u16_g_EEP_Diag.UnLatch_Error == TRUE) /*|| (PtlTrans.Pre_Inline_Mode == TRUE)*/ )
    {
        if( (PtlTrans.CanBusOff == FALSE) && (PtlTrans.CanLineError == FALSE) && (PtlTrans.HallError_LH == FALSE) &&
            /*(PtlTrans.HallError_RH == FALSE) &&*/ (PtlTrans.APS_Error_LH == FALSE) && (PtlTrans.APS_Error_RH == FALSE) && 
            (PtlTrans.UnLatch_Error == FALSE) && ( u16_g_EEP_Diag.InLine_Mode == FALSE) )
        {
           if( (u08_s_Pre_IGN_ST == KEY_OFF) && (u08_g_C_IGNSw == KEY_IGN) )
           {
               u08_g_IGN_Count++;
               
               if( u08_g_IGN_Count >= 30 )
               {
                   u08_g_IGN_Count = 0;
                   PtlTrans.CanBusOffB = FALSE;
                   PtlTrans.CanLineErrorB = FALSE;
                   u16_g_EEP_Diag.HallError_LH = FALSE;
                   /*u16_g_EEP_Diag.HallError_RH = FALSE;*/
                   u16_g_EEP_Diag.APS_Error_LH = FALSE;
                   u16_g_EEP_Diag.APS_Error_RH = FALSE;
                   u16_g_EEP_Diag.UnLatch_Error = FALSE;
                   /*PtlTrans.Pre_Inline_Mode = FALSE;*/
                   u08_g_EEP_Write_Event = TRUE;
               } 
               
               u08_s_Pre_IGN_ST = KEY_IGN;
           }
           
           if( (u08_s_Pre_IGN_ST == KEY_IGN) && (u08_g_C_IGNSw == KEY_OFF) )
           {
               u08_s_Pre_IGN_ST = KEY_OFF;
           } 
        }    
    }
    else
    {
        u08_g_IGN_Count = 0;
    }        
}

/********************************************************************************/
/*   name     : g_PTGOpnClsStUpdate_Period100ms                                 */
/*   function : PTG Status Detect FOR CAN tx Signal C_PTGOpnClsState            */
/********************************************************************************/
void g_PTGOpnClsStUpdate_Period100ms( void )
{
    static U16 u16_s_prePosition = 0xFFFFU;
	  U08 u08_t_5perPos = 0;
	  
	if( (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) || (u08_g_Tailgate_Moving_Cmd == BREAK_TAILGATE) )
	{
	    if( (s16_g_Tailgate_Position == 0) && ( u08_g_LatchSt != LATCH_OPEN ) )
	  	{	
	        u08_g_C_PTGOpenClsState = 1; /* closed */
	    }
	    else 
	    {   
	        if( s16_g_Tailgate_Position != u16_s_prePosition )
	      	{	
	      	    u08_t_5perPos = (u16_g_EEP_Para.LearnedMechEndPos/20);
	      	    if( s16_g_Tailgate_Position >= (u08_t_5perPos*18) )
	      	    {
	      	        u08_g_C_PTGOpenClsState = 6; /* 90% ~ Full Open */
	      	    }
	      	    else if( s16_g_Tailgate_Position >= (u08_t_5perPos*15) )
	      	    {
	      	        u08_g_C_PTGOpenClsState = 5; /* 75% ~ 90% */
	      	    }
	      	    else if( s16_g_Tailgate_Position >= (u08_t_5perPos*10) ) 
	      	    {
	      	        u08_g_C_PTGOpenClsState = 4; /* 50% ~ 75% */	 	
	      	    }
	      	    else if( s16_g_Tailgate_Position >= (u08_t_5perPos*5) ) 
	      	    {
	      	        u08_g_C_PTGOpenClsState = 3; /* 25% ~ 50% */	 	
	      	    }
	      	    else
	      	    {
	      	        u08_g_C_PTGOpenClsState = 2; /* 1% ~ 25% */	 		
	      	    }		
	            u16_s_prePosition = s16_g_Tailgate_Position;   
	        }

	      	if( (u08_g_Latch_Unexpect == 1) || (u08_g_Hall_Sensor_Error == ON) || (u08_g_Power_OnReset == 1) || (u08_g_Init_Param == OFF) )
	      	{
	      	    u08_g_C_PTGOpenClsState = 0xA;    
	      	}	
	      	else
	      	{
	      	    if( u08_g_C_PTGOpenClsState == 0xA )
	      	    {
	      	        u16_s_prePosition = 0xFFFFU;	
	      	    }	  
	      	}	      	            
	    }	
	}
	else
	{
	    if( u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE )
	    {
	        u08_g_C_PTGOpenClsState = 9; /* Opening operation */    	
	    }	
	    else
	    {
	        u08_g_C_PTGOpenClsState = 8; /* Closing operation */    	
	    }	 
	}	

	if( u08_g_PTGM_Power_Mode != TRUE )
	{
	    u08_g_CmdWarn = 0x04;	
	}	
    else if(u08_g_ReverseMovSet == 1)
    {
        u08_g_CmdWarn = 0x02;
    }	  
	else if( (u08_g_Latch_Unexpect == TRUE) || (u08_g_Spindle_Theraml_St == THERMAL_PROTECT_SET) || 
	         (u08_g_PCU_Theraml_St == THERMAL_PROTECT_SET) || (u08_g_Power_OnReset == 1) || 
             (u08_g_Hall_Sensor_Error == ON ) || (u08_g_Init_Param == OFF) ||
             (st_g_Vehiclest.bBatVoltPTG_Disable == 1) || (PtlTrans.UnLatch_Error == TRUE) )
	{
	    u08_g_CmdWarn = 0x03;
	}		
	else if( (st_g_Vehiclest.bGearPositionEn == 0) || (st_g_Vehiclest.bSpeedOver3Km == TRUE) )
	{
	    u08_g_CmdWarn = 0x01;
	}	 
	else
	{
	    u08_g_CmdWarn = 0x00; 	
	}	
}

/********************************************************************************/
/*   name     : g_StatusDetet_Period100ms                                       */
/*   function : status Detect 100ms polling                                      */
/*                                                                              */
/********************************************************************************/
void g_StatusDetet_Period100ms(void)
{
    s_BatteryVoltageCompare();
}


/********************************************************************************/
/*   name     : g_StatusDetet_Period500ms                                       */
/*   function : status Detect 50ms polling                                      */
/*                                                                              */
/********************************************************************************/
void g_StatusDetet_Period500ms(void)
{
    s_MCU_Voltage_St();
}

/********************************************************************************/
/*   name     : g_StatusDetect_Period10ms                                       */
/*   function : status Detect 10ms polling                                      */
/*                                                                              */
/********************************************************************************/
void g_StatusDetect_Period10ms(void)
{
    s_GetSystemBattSt();
    s_GetVehilcleStCAN();
    s_UnLatchStCheck();
    s_LatchStCheck();
    s_OpenStatusMotorBreakSet();
    s_GetTempValue();
}

/********************************************************************************/
/*   name     : g_StatusDetect_Period5ms                                       */
/*   function : status Detect 5ms polling                                      */
/*                                                                              */
/********************************************************************************/
void g_StatusDetect_Period5ms(void)
{
    s_TailGate_Positon_LatchSt_Check();
}

/********************************************************************************/
/*   name     : g_StatusDetect_Init                                             */
/*   function : status Detect value initialize                                  */
/*                                                                              */
/********************************************************************************/
void g_StatusDetect_Init(void)
{
    st_g_Vehiclest.bBatVoltCAN_Disable = 0;
    st_g_Vehiclest.bBatVoltPTG_Disable = 0;
    st_g_Vehiclest.DoorLockSt = ALL_DOOR_LOCK;
    u08_g_User_Position = 0;
    u08_g_LatchSt = LATCH_OPEN; 
    u08_g_clacBatteryVolt = 0;
    s08_g_Convert_Temp = 20;
    s16_g_Tailgate_Position = 0;
    u08_g_Latch_Error = 0;
    u08_g_Latch_On_Spindle_Stop = 0;
    u08_g_Power_OnReset = 1;
    u08_g_Result_call = 0;
    u08_g_Trunk_Position_Error = OFF;   
    u08_g_SpeedSetWarning_T = 0; 
    u08_g_Strain_Cmd = 0;
    u08_g_MCU_Voltage_Error = 0;
    u08_g_DetectVoltCmprErrSts = 0;
    u08_g_HallPowerOnErr_LH = 0;
    u08_g_HallPowerOffErr_LH = 0;
    u08_g_HallPwrOffCheckCmd_LH = 0;
    /*u08_g_HallPowerOnErr_RH = 0;*/
    /*u08_g_HallPowerOffErr_RH = 0;*/
    /*u08_g_HallPwrOffCheckCmd_RH = 0;*/
    u08_g_BreakSetCmd = 0;
        
#ifdef NONCAN_TEST_MODE    
    st_g_Vehiclest.bGearPositionEn = 1;
    st_g_Vehiclest.bSpeedOver3Km = 0;
    u08_g_C_VehicleSpeed = 0;
#else
    st_g_Vehiclest.bGearPositionEn  = 0;
    st_g_Vehiclest.bSpeedOver3Km    = 1;
    u08_g_C_VehicleSpeed            = 3;
    u08_g_C_BAState                 = 1; /* arming */
    u08_g_C_RkeCmd	                = 0;        
    u08_g_C_TgReleaseRly            = 0;
    u08_g_C_PTGMNValueSet           = 0;
    u08_g_C_InhinitP                = 0;
    u08_g_C_GselDisp                = 15;
    u08_g_C_InhibitRMT              = 0;
    u08_g_C_USMReset                = 0;
    u08_g_C_PTGSpeedNValueSet       = 0;
    u08_g_C_CrashUnlockState        = 0;
    u08_g_C_DrvUnlockState          = 0;
    u08_g_C_AstUnlockState          = 0;
    u08_g_C_RLUnlockState           = 0;
    u08_g_C_RRUnlockState           = 0;
    u08_g_C_TgOtrReleaseSW          = 0;
    u08_g_C_PassiveTrunkTg          = 0;
    u08_g_C_SMKRKECmd               = 0;
    u08_g_C_TrunkOpenOption         = 0;
    u08_g_C_ConfTCU                 = 0;
    u08_g_C_TailgateTrunkRKECMD     = 0;
    u08_g_C_TrunkTgSW               = 1; /* open */
    u08_g_LatchFaultSt              = 0;
    u08_g_C_eClutch                 = 0xF;
    u08_g_C_PTGOpenClsState         = 0;
    PtlTrans.kDescCommControlStateDisableFlag = FALSE;
    u08_g_C_PTGHeightNValueSet     = 0;
    u08_g_C_IGN_Fail               = 0;
#endif

    u08_g_ECU_Power_Mode           = ECU_NORAM_MODE;
    
    
}
                                 