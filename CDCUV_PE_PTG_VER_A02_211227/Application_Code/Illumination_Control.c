/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-22 PM.  5:18:18                                   */
/*  Last update     : 2013-05-22 PM.  5:18:20                                   */
/*  File Name       : Illumination_Control.c                                    */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define ILLUMINATION_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Illumination_Control.h"
#include "Digital_Input_Chattering.h"
#include "Status_Detect.h"
#include "CAN_Drive_App.h"
#include "CAN_Signal_Handling.h"
#include "EEPROM_Control.h"
#include "User_Logic_Control.h"

/* Auto generate code */
#include "Illumination_Ctl_Out.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/


/********************************************************************************/
/*   name     : g_Illumination_Control_10msPeriod                               */
/*   function :                                                                 */
/*   illumination control 10ms task                                             */
/********************************************************************************/
void g_Illumination_Control_10msPeriod( void )
{
    switch( u08_g_Illumi_St )
    {
        case ON :
            if( (st_g_Vehiclest.bBatVoltPTG_Disable == ON) || (u08_g_PTGM_Power_Mode == OFF) || (u08_g_LatchSt != LATCH_OPEN) ) 
            {
                Illumination_Ctl_Out_PutVal( OFF );
                u08_g_Illumi_St = OFF;
            }
            break;
        case OFF :
            if( u08_g_LatchSt == LATCH_OPEN )
            {
                if((st_g_Vehiclest.bBatVoltPTG_Disable == OFF) && (u08_g_PTGM_Power_Mode != 0))
                {     
                    Illumination_Ctl_Out_PutVal( ON );  
                    u08_g_Illumi_St = ON;
                }
            }    
            break;
        case SLEEP_OFF :
            break;  
        default :
            u08_g_Illumi_St = OFF;
            break;          
    }
    
    u08_g_CANsignal_Change = OFF;
}
