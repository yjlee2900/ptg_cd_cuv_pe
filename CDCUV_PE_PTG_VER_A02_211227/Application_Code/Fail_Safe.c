/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-05 PM.  6:52:49                                   */
/*  Last update     : 2013-06-05 PM.  6:53:03                                   */
/*  File Name       : Fail_Safe.c                                               */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define FAIL_SAFE_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Fail_Safe.h"
#include "Status_Detect.h"
#include "Hazard_Buzzer_Control.h"
#include "Read_Hall_Sensor.h"
#include "Obstacle_Detect.h"
#include "User_Logic_Control.h"
#include "CAN_Signal_Handling.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/


/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/


/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
/*#define u08_XXX_TABLE_CNT 4*/


/********************************************************************************/
/*   name     : g_Latch_failSafe_St_Out                                         */
/*   function : Latch Fail safe Error status out control                        */
/*                                                                              */
/********************************************************************************/
void g_Latch_failSafe_St_Out( void )
{
    if( ( u08_g_LatchSt != LATCH_OPEN ) || (u08_g_C_TrunkTgSW == 0) )
    {
        u08_g_Tx_ReleaseRly = 1;
        g_Past_Failure_Record( UNEXPECT );
    }        
    
    /*g_SetBuzzHazardControl(BUZZONLY_ON, 6);*/
}

