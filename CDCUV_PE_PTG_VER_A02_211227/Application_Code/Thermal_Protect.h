/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-24 AM.  9:47:08                                   */
/*  Last update     : 2013-05-24 AM.  9:47:12                                   */
/*  File Name       : Thermal_Protect.c                                         */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		THERMAL_H_EXTERN
#define		THERMAL_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	THERMAL_PROTECT_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define    THERMAL_PROTECT_SET      1
#define    THERMAL_PROTECT_CLEAR    0
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_Spindle_Theraml_St; 
EXTERN U08 	u08_g_PCU_Theraml_St;
EXTERN U16  u16_g_Spindle_Thermal_Cnt;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_Spindle_Thermal_10msPeriod(void);
EXTERN	void g_PCU_Thermal_10msPeriod(void);
EXTERN  void g_Thermal_Status_Init(void);

#undef EXTERN

#endif
