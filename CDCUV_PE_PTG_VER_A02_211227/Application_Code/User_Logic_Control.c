/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-07 AM.  9:20:25                                   */
/*  Last update     : 2014-03-11 PM.  2:57:39                                   */
/*  File Name       : User_Logic_Control.c                                      */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/
#define USER_LOGIC_EXTERN
#include "Basic_Type_Define.h"
#include "User_Logic_Control.h"
#include "Status_Detect.h"
#include "Thermal_Protect.h"
#include "Fail_Safe.h"
#include "Obstacle_Detect.h"
#include "PCU_Sequence.h"
#include "Hazard_Buzzer_Control.h"
#include "Spindle_Motor_Drive.h"
#include "Control_Command.h"
#include "CAN_Drive_App.h"
#include "CAN_Signal_Handling.h"
#include "Digital_Input_Chattering.h"
#include "Read_Hall_Sensor.h"
#include "Parameterization.h"
#include "EEPROM_Control.h"
#include "PID_Control.h"
#include "Sensor_Power_Control.h"
#include "Obstacle_Detect.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U08 u08_s_Pre_User_Tailgate_Cmd = 0;
static U16 u16_s_watit_Passive_t = 0; 
static U08 u08_s_CAN_St_Update_Delay = 0;
static U08 u08_s_CAN_St_Update_Delay_for_Passive = 0;
static U08 s_u08_Lonbuzzer = 0;
static U08 u08_s_UserSpeed_Update_Set = 0;

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_User_Command_Check( U08 u08_t_Sw );
static U08  u08_s_Get_VehicleSt_TG_Open( U08 u08_t_Swst );
static U08  u08_s_Get_VehicleSt_TG_Moving( U08 u08_t_SwMov );
static void s_User_CAN_Command( void );
static void s_User_IO_Command( void );
static void s_Parmeter_Reset_Command( void );
static void s_Inline_Mode_UserClear( void );
static void s_InnerSwitch_Error_Check( void );
static void s_Switch_Short_Check( void );
static void s_Serial_Write_Check( void );
static void s_UserHighSet_Conrtol( void );
static void s_UserSpeedSet_Control( void );
static void s_USM_ReSet( void );
static void s_PowerMode_Set( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/

#define DISABLE                0
#define POWER_OPEN             1

#define CONTROL_ENABLE         1
#define MOVING_AND_BUZZER      2  
#define AVNCMD_OPEN            3
//#define AVNCMD_CLOSE           4

#define PASSIVE_WAIT_T              100  /* 10ms * 100 = 1000ms */
#define UNLATCH_WAIT_TIME           200  /* 10ms * 200 = 2000ms */  /*  more then MOVING_DELAY_BUZZ + 100 */
#define MOVING_DELAY_BUZZ           100  /* 10ms * 100 = 1000ms */     
#define USER_MOVING_DETECT_RPM      300 /* 300 RPM */ 

/********************************************************************************/
/*   name     : u08_s_Get_VehicleSt_TG_Open                                     */
/*   function : check The vehicle Status to                                     */ 
/*              tailgate open possible by user input command                    */
/********************************************************************************/
static U08 u08_s_Get_VehicleSt_TG_Open( U08 u08_t_Swst )
{
    U08 Ret_value = 0;

    
    if( (st_g_Vehiclest.bSpeedOver3Km != TRUE) && (u08_g_C_IGNSw < KEY_CRANK ) && 
    	  (u08_g_PCU_Mode == STANDBY) && (u08_g_C_IGN_Fail == 0) )
    {
        switch( u08_t_Swst )
        {
            case TG_OUTER_SW :
                if( u08_g_C_BAState == ARMING )
                {
                    if(u08_g_C_IGNSw >= KEY_ACC)
                    {
                        Ret_value = DISABLE;
                    }
                    else
                    {
                        u16_s_watit_Passive_t = PASSIVE_WAIT_T;
                        Ret_value = DISABLE;
                    }   
                }
                else if( st_g_Vehiclest.DoorLockSt == ALL_DOOR_UNLOCK )
                {
                    if( st_g_Vehiclest.bGearPositionEn == ON )
                    {    
                        Ret_value = MOVING_AND_BUZZER;
                    }
                }
                else
                {
                    if( st_g_Vehiclest.bGearPositionEn == ON )
                    {
                        u16_s_watit_Passive_t = PASSIVE_WAIT_T;
                    }
                    Ret_value = DISABLE;
                }  
#if 0                
                /* Inline Mode */ /* Key off, all door lock , all door open */
                if( (u08_g_C_IGNSw == KEY_OFF) && (st_g_Vehiclest.DoorLockSt == ALL_DOOR_LOCK) &&
                    (u08_g_C_AstDrSw == 1) && (u08_g_C_DrvDrSw == 1) && (u08_g_C_RLDrSw == 1) && (u08_g_C_RRDrSw == 1) )
                {
                    Ret_value = DISABLE;
                    u08_g_Tx_ReleaseRly = 1;  
                }    
#endif                         
                break;
            case TG_INNER_SW :
                Ret_value = DISABLE;
                break;
            case TG_CPAD_SW :
                if( (u08_g_C_BAState != ARMING) && (st_g_Vehiclest.bGearPositionEn == ON) )
                {
                    Ret_value = MOVING_AND_BUZZER;
                }
                else
                {
                    Ret_value = DISABLE;
                } 
                break;
            case TG_RKE_SW :
                if( u08_g_C_IGNSw == KEY_OFF )
                {
                    if( u08_g_C_TrunkOpenOption == 1 )
                    {   
                        /* Drive Door Unlock or Any Door Open */
                        if( (u08_g_C_DrvUnlockState == DOOR_ULOCK) || (u08_g_C_DrvDrSw == 1) ||  (u08_g_C_AstDrSw == 1) || 
                            (u08_g_C_RRDrSw == 1) || (u08_g_C_RLDrSw == 1) )
                        {
                            Ret_value = MOVING_AND_BUZZER; 
                        }     
                    } 
                    else
                    {   
                        Ret_value = MOVING_AND_BUZZER; 
                    }                    
                }    
                break;
            case TG_PASSIVE :
                if( (u16_s_watit_Passive_t > 0) || 
                	((st_g_Vehiclest.DoorLockSt == ALL_DOOR_LOCK)&&(u08_g_C_IGNSw == KEY_OFF )) )
                {   
                    Ret_value = MOVING_AND_BUZZER;  
                }    
                break;    
            case TG_AVNCMD :
            	if( (u08_g_C_BAState != ARMING) && (st_g_Vehiclest.bGearPositionEn == ON) && ( u08_g_C_PTGCNTCmdAVN == 1 ) )
            	{
            	    Ret_value = MOVING_AND_BUZZER;  	
            	}	
            	break;           
            default :
                break;                                                    
        }    
    }
    
    /*********** in line mode *************************************************/
    if( u16_g_EEP_Diag.InLine_Mode == TRUE )
    {
        if( (u08_t_Swst == TG_OUTER_SW) || (u08_t_Swst == TG_CPAD_SW) )
        {
            Ret_value = MOVING_AND_BUZZER;
        }
        else
        {
            Ret_value = DISABLE;
        }        
    }    
    /**************************************************************************/
    
    if( (u08_t_Swst == TG_OUTER_SW) && (u08_g_Signal_UpDate_check != 0x3F) && (Ret_value == DISABLE) && (u16_g_EEP_Diag.InLine_Mode != TRUE))
    {
         u08_s_CAN_St_Update_Delay = 32; /*25; 20;*/ /* 120ms : CAN status update Delay time */
    }    
   
    return( Ret_value );
}

/********************************************************************************/
/*   name     : u08_s_Get_VehicleSt_TG_Moving                                   */
/*   function : check The vehicle Status to                                     */ 
/*              tailgate Moving possible by user input command                  */
/********************************************************************************/
static U08 u08_s_Get_VehicleSt_TG_Moving( U08 u08_t_SwMov )
{
    U08 Ret_value_M = OFF;
    
    if( (u08_g_C_IGNSw < KEY_CRANK ) && (u08_g_PCU_Mode == STANDBY) && (u08_g_PTGM_Power_Mode == TRUE) && 
        (u08_g_Init_Param == ON) && ((u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) || (u16_g_LH_MotorRPM < USER_MOVING_DETECT_RPM))   )
    {    
        switch( u08_t_SwMov )
        {
            case TG_OUTER_SW :
                Ret_value_M = MOVING_AND_BUZZER;  
                break;
            case TG_INNER_SW :
                Ret_value_M = MOVING_AND_BUZZER;
                break;
            case TG_CPAD_SW :
                Ret_value_M = MOVING_AND_BUZZER;
                break;
            case TG_RKE_SW :                
                if( (u08_g_C_IGNSw == KEY_OFF)&& (u16_g_EEP_Diag.InLine_Mode != TRUE) )
                {
                    Ret_value_M = MOVING_AND_BUZZER;
                }
                break;
            case TG_PASSIVE :  
                Ret_value_M = OFF;
                break;    
            case TG_AVNCMD :
            	if(u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE)
            	{
            	    if( u08_g_C_PTGCNTCmdAVN == 1 )
            	  	{	
            	  	    if(st_g_Vehiclest.bSpeedOver3Km != TRUE)
            	        {
            	            Ret_value_M = AVNCMD_OPEN;  	
            	        }
            	    }
                    else
                    {
                        Ret_value_M = DISABLE;
                    }
                    
            	}	
            	break;                            
            default :
                break;                                                    
        }
    }
    
    if(  (u08_g_Init_Param == OFF) && (u08_t_SwMov != TG_PASSIVE) )
    {
        if( u08_g_ParamStatus == PARAM_RUN )
        {
            Ret_value_M = CONTROL_ENABLE; /* for parameter run stop */
        }    
        else
        {    
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6;           
        }
    }    
    
    return( Ret_value_M );
}

/********************************************************************************/
/*   name     : s_User_Command_Check                                            */
/*   function : tailgate Status check for user input command                    */
/********************************************************************************/
static void s_User_Command_Check( U08 u08_t_Sw )
{
    U08 u08_t_Enable_Control = OFF;
    
    if( (u08_g_Spindle_Theraml_St != THERMAL_PROTECT_SET) && (u08_g_Power_OnReset == 0) && 
    	  (u08_g_C_CrashUnlockState != 1) && (u08_g_HallPowerOnErr_LH == 0) && (u08_g_HallPowerOffErr_LH == 0) && 
    	  (u08_g_LH_HallPatternErrSt == 0) && (u08_g_SpindleCurrentFailCnt != 3) )
    {    
        if( (st_g_Vehiclest.bBatVoltPTG_Disable == ON) || (u08_g_DetectVoltCmprErrSts == 1) )
        {
            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6;   

            g_Past_Failure_Record(LOWBAT);  
        }
        else if( (u08_g_Latch_Unexpect == ON) || (u08_g_Hall_Sensor_Error == ON ) || (u08_g_LatchFaultSt == 1) )
        {
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6; 
            
            if( st_g_Vehiclest.bSpeedOver3Km != TRUE )
            {
                if( (u08_t_Sw == TG_PASSIVE) || (u08_t_Sw == TG_RKE_SW) ||
                    ( (u08_g_C_BAState != ARMING) && ((u08_t_Sw == TG_OUTER_SW) || (u08_t_Sw == TG_CPAD_SW)) ) )
                {
                    if( u08_g_Latch_Unexpect == ON )
                    {	 
                        g_Latch_failSafe_St_Out();
                    }
                    else
                    {
                        u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Open( u08_t_Sw );
                        if( u08_t_Enable_Control >= POWER_OPEN )
                        {
                        	g_Latch_failSafe_St_Out();
                        }		
                    }	
                }    
            }     
        }
        else 
        {
            g_GetUser_PositionRate();
            switch( u08_g_User_Position )
            {
                case TAILGATE_ZONE_0 : /* Latch Close Status */
                    u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Open( u08_t_Sw );
                    if( (u08_t_Enable_Control >= POWER_OPEN) && (u08_g_C_TgReleaseRlyAct != 1) ) 
                    {
                        u16_s_watit_Passive_t = 0;
                        u08_g_HallPwrOffCheckCmd_LH = 0;
                        
                        if( u08_g_PTGM_Power_Mode == TRUE )
                        {    
                            u08_g_Tx_ReleaseRly = 1;  
                            u08_g_Wait_Unlatch_T = UNLATCH_WAIT_TIME;
                            
                            u08_g_User_Tailgate_Cmd = OPEN_TAILGATE;  /* control command -> check Latch Release */
                            u08_s_Pre_User_Tailgate_Cmd = u08_g_User_Tailgate_Cmd;   
                        }
                    }
                    
                    if(u08_t_Sw == TG_OUTER_SW)
                    {
                        if( (u08_g_PCU_Mode == CINCHING_RUN) || (u08_g_PCU_Mode == CINCHING_START) )
                        {
                            u08_g_Tx_ReleaseRly = 1;          
                            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                            u08_g_PCU_Antipinch_St = ANTIPINCH_PCU_UNLATCH; /* 140828 */
                            if( u08_g_PTGM_Power_Mode == TRUE )
                            {    
                                u08_g_StopToMove_T = CINCHING_ANTI_OPEN_DELAY_T;
                                u08_g_Antipinch_Reverse_Mode = OPEN_TAILGATE;
                            }
                        }

                        if(u08_g_PCU_Mode == STANDBY)
                        {
                            u08_g_CinchingStartDelaySet = 1;
                        }
                    }     

                    s_u08_Lonbuzzer = 0;

                    break;
                case TAILGATE_ZONE_1 : /* ZONE_1 ~ 4 = Tailgate Open Status */  
                    u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Moving( u08_t_Sw );   
                    if( u08_t_Enable_Control >= CONTROL_ENABLE )
                    {    
                        if( (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) && ( st_g_Vehiclest.bSpeedOver3Km != TRUE ) )
                        {
                            u08_g_User_Tailgate_Cmd = OPEN_TAILGATE;
                        }
                        if( u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE )
                        {
                            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;  /* control command -> zone 3 stop */
                        }
                    }     

                    s_u08_Lonbuzzer = 0;

                    break;
                case TAILGATE_ZONE_2 :
                    u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Moving( u08_t_Sw );   
                    if( u08_t_Enable_Control >= CONTROL_ENABLE )
                    {
                        if( u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE)
                        {
                            u08_g_User_Tailgate_Cmd = STOP_TAILGATE; /* control command -> zone 3 stop */
                        }
                        else 
                        {
                            if( st_g_Vehiclest.bSpeedOver3Km != TRUE )
                            {                       
                                u08_g_User_Tailgate_Cmd = OPEN_TAILGATE; /* control command -> close status : stop to Open , idle status : open */
                                
                                if( ((u08_t_Sw == TG_CPAD_SW) && (u08_g_CPAD_Close_St == 2 )) || 
                                	  ((u08_t_Sw == TG_RKE_SW) && (u08_g_RKE_Close_St == 2)) )
                                {
                                	  /*u08_g_CPAD_Close_St = 0;*/
                                    u08_g_Zone2_Open = 1;
                                    
                                }
                                else /* for Other switch Reverse Open Buzzer Set */
                                {
                                    u08_g_CPAD_Close_St = 0;	
                                    u08_g_RKE_Close_St = 0;
                                }	
                            }    
                        }
                    }     
                    break;
                case TAILGATE_ZONE_3 :
                    u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Moving( u08_t_Sw );   
                    if( u08_t_Enable_Control >= CONTROL_ENABLE )
                    {       
                        if( (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) && (u08_g_Tailgate_Moving_Cmd != BREAK_TAILGATE)  )
                        {
                           u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                        }
                        else
                        {
                            if( st_g_Vehiclest.bSpeedOver3Km == TRUE )
                            {    
                                u08_g_User_Tailgate_Cmd = CLOSE_TAILGATE;
                            }
                            else
                            {
                                if( u08_t_Enable_Control == AVNCMD_OPEN )
                                {
                                    u08_g_User_Tailgate_Cmd = OPEN_TAILGATE;
                                    u08_t_Enable_Control = MOVING_AND_BUZZER; /* for Start Buzzer */
                                }	
                                else
                                {	                                	
                                    if( u08_s_Pre_User_Tailgate_Cmd == CLOSE_TAILGATE )
                                    {
                                        u08_g_User_Tailgate_Cmd = OPEN_TAILGATE;
                                    }
                                    else
                                    {
                                        u08_g_User_Tailgate_Cmd = CLOSE_TAILGATE;
                                    }         
                                }
                            }
                        }
                    }          
                    break;
                case TAILGATE_ZONE_4 :  /* MAX Open Position Over */
                    u08_t_Enable_Control = u08_s_Get_VehicleSt_TG_Moving(u08_t_Sw);   
                    if(u08_t_Enable_Control >= CONTROL_ENABLE)
                    {       
                        if( (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) && (u08_g_Tailgate_Moving_Cmd != BREAK_TAILGATE) )
                        {
                           u08_s_Pre_User_Tailgate_Cmd = u08_g_Tailgate_Moving_Cmd;
                           u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                        }
                        else
                        {
                            if(u08_t_Enable_Control != AVNCMD_OPEN)
                            {
                                u08_g_User_Tailgate_Cmd = CLOSE_TAILGATE;
                            }
                        }
                    }
                    break;
                default :
                    break;
            }
#if 0            
            /********************* Tail gate Close Anti Sensor Fail Safe ***********************/
            if( (u08_g_User_Tailgate_Cmd == CLOSE_TAILGATE) && (u08_g_Antipinch_Sensor_St == ON) )
            {
                u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                /*g_SetBuzzHazardControl(BUZZONLY_ON, 2);*/
                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                u08_g_Buzzer_Hazard_Count = 6;           
            }    
            /**********************************************************************************/
#endif            
            if( (u08_g_User_Tailgate_Cmd != STOP_TAILGATE) && 
                ((u08_g_User_Tailgate_Cmd != u08_g_Tailgate_Moving_Cmd) || (u08_g_Strain_Operation_Motor_On_T != 0)) )
            {                                 
                u08_g_Antipinch_Reverse_Mode = 0;  
                if( (u08_t_Enable_Control == MOVING_AND_BUZZER) || (u08_t_Enable_Control == AVNCMD_OPEN) ) 
                {
                    if( (u08_g_SMK_Buzzer_T == 0) && (st_g_BuzzHaza.Mode != LONG_BUZZER) )
                    {    
                        u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;  
                        u08_g_SMK_Buzzer_T = 50;     
                        u08_g_Buzzer_Hazard_Count = 4;  
                    }
                    
                    if( (u08_g_User_Tailgate_Cmd == CLOSE_TAILGATE) && (( u08_t_Sw == TG_RKE_SW )||( u08_t_Sw == TG_CPAD_SW)) )
                    {
                    	if( u08_t_Sw == TG_RKE_SW )
                    	{	 
                            u08_g_RKE_Close_St = 1;
                        }
                        if( u08_t_Sw == TG_CPAD_SW)
                        {
                        	  u08_g_CPAD_Close_St = 1;
                        }	
                        u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;
                        u08_g_Buzzer_Hazard_Count = 20;  
                    }                        
                    u08_g_Moving_Delay_T = MOVING_DELAY_BUZZ;
                }
            }
        }
    }
    else
    {
        /* Thermal status */
        if( u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE )
        {
            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
        }
        else
        {        
            if( (u08_g_Power_OnReset == 1) && ( u08_t_Sw == TG_INNER_SW ) && 
                ( u16_g_EEP_Diag.InLine_Mode == TRUE ) && (u08_g_LatchSt == LATCH_OPEN) ) /* Inline Close Mode */
            {     
                u08_g_User_Tailgate_Cmd = CLOSE_TAILGATE;
                u08_g_Trunk_Position_Error = ON;
                s16_g_LH_Position_Cnt = 1300;
            }
            else
            {    
                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;    
                u08_g_Buzzer_Hazard_Count = 6;             
            }             
        }      

        if( u08_g_Spindle_Theraml_St == THERMAL_PROTECT_SET)
        {    
            g_Past_Failure_Record(SPD_THER);  
        }
        if( (u08_g_C_CrashUnlockState != 1) && ( u08_g_Power_OnReset == 1 ) )
        {
            g_Past_Failure_Record(INIT_POS);  
        }      
    }    
}

/********************************************************************************/
/*   name     : s_PowerMode_Set                                                 */
/*   function : Power/manual mode Set                                           */
/*                                                                              */
/********************************************************************************/
static void s_PowerMode_Set(void)
{
    U08 err_st = 0;
    
    if(CAN_Event.C_PTGMNValueSet == TRUE)
    {
    	CAN_Event.C_PTGMNValueSet = FALSE;
        if( (u08_g_C_PTGMNValueSet == 1) && (u08_g_PTGM_Power_Mode == 0) )
        {
            u08_g_PTGM_Power_Mode = 1;
            u16_g_EEP_Diag.PGTM_Enable = u08_g_PTGM_Power_Mode; 

            err_st = u08_g_Write_Diag_Sector(); 
        } 	
        
        if( (u08_g_C_PTGMNValueSet == 2) && (u08_g_PTGM_Power_Mode == 1) )
        {
            u08_g_PTGM_Power_Mode = 0;
            u16_g_EEP_Diag.PGTM_Enable = u08_g_PTGM_Power_Mode; 

            err_st = u08_g_Write_Diag_Sector();         	
        }	
    }
}

/********************************************************************************/
/*   name     : s_USM_ReSet                                                     */
/*   function : USM Reset Command                                               */
/*                                                                              */
/********************************************************************************/
static void s_USM_ReSet(void)
{
    if(CAN_Event.C_USMReset == TRUE)
    {
    	CAN_Event.C_USMReset = FALSE;
        if(u08_g_C_IGNSw == KEY_IGN)
        {
            if(u08_g_C_USMReset == 1)
            {
            	if( (u08_g_PTGM_Power_Mode == 0) || (u16_g_EEP_Diag.Speed_Set_Value == 2) )
            	{
            	    u08_g_PTGM_Power_Mode = 1; /* Power Mode */	
            	    u16_g_EEP_Diag.Speed_Set_Value = 1; /* Fast Mode */
            	    u16_g_EEP_Diag.PGTM_Enable = u08_g_PTGM_Power_Mode; 
            	    u08_s_UserSpeed_Update_Set = 1;
            	}	 
                
              if(u16_g_EEP_Para.User_Hight_SetValue != 4)
              {	
                  u16_g_EEP_Para.User_Hight_SetValue = 4; /* 100% */
                  u08_g_User_Position_set = 1;
              }
            }    
        }
    }   
}

/********************************************************************************/
/*   name     : s_User_CAN_Command                                              */
/*   function : User logic control function                                     */
/*                                                                              */
/********************************************************************************/
static void s_User_CAN_Command( void )
{
    /************ BCM & SKM RKE Command Error Protect Mode ************/    
    static U08 u08_s_RKE_Delay_T = 0;
    /******************************************************************/
    static U08 s_RKE_Stat_Ready_t = 0;
    
    /* sleep timer , can sleep time initialization */
    if( ( (CAN_Event.C_TgOtrReleaseSW == ON) && (u08_g_C_TgOtrReleaseSW == ON) && (u16_g_EEP_Diag.OS_HDLSw_Short != TRUE) && 
          ((st_g_swinput.swInnerOpenClose.swInput == OFF)||(u16_g_InnerSW_On_t == 0xFFFFU))) ||
          ((u08_g_Signal_UpDate_check == 0x3F) && (u08_s_CAN_St_Update_Delay != 0))  )
    {    
        u08_s_CAN_St_Update_Delay = 0;
        CAN_Event.C_TgOtrReleaseSW = OFF;
        u08_g_User_Input_On = ON;

        s_User_Command_Check(TG_OUTER_SW);
    }


    if(CAN_Event.C_TailgateTrunkRKECMD == ON)
    {
    	  CAN_Event.C_TailgateTrunkRKECMD = OFF;
        if( (u08_g_C_TailgateTrunkRKECMD == 0x01) && (u08_s_RKE_Delay_T == 0) && (s_RKE_Stat_Ready_t != 0) )
        {
            u08_g_User_Input_On = ON;
            u08_s_RKE_Delay_T = 10;
            s_RKE_Stat_Ready_t = 0;

            s_User_Command_Check(TG_RKE_SW);
        }    	  
    }	

    if( CAN_Event.C_RkeCmd == ON )
    {
        CAN_Event.C_RkeCmd = OFF;
        if( u08_g_C_RkeCmd == PANIC_STOP )
        {
        	  if( (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) )
        	  {	
                s_User_Command_Check(TG_RKE_SW); 
            }
            s_RKE_Stat_Ready_t = 120;
        }       
    }       
    
#if 0
    if( CAN_Event.C_SMKRKECmd == ON )
    {
        CAN_Event.C_SMKRKECmd = OFF;
        if( (u08_g_C_SMKRKECmd == SMK_TAILGATE) && (u08_s_RKE_Delay_T == 0) )
        {    
            u08_g_User_Input_On = ON;
            u08_s_RKE_Delay_T = 10;                                                               
            s_User_Command_Check(TG_RKE_SW);                                
        } 
        if( (u08_g_C_SMKRKECmd == SMK_PANIC_STOP) && (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) )
        {
            s_User_Command_Check(TG_RKE_SW); 
        }                                                                  
    } 
#endif
    if( CAN_Event.C_PassiveTrunkTg == ON )
    {
        CAN_Event.C_PassiveTrunkTg = OFF;
        if( u08_g_C_PassiveTrunkTg == ON )
        {
            u08_g_User_Input_On = ON;
            if( (u08_g_Signal_UpDate_check & 0x30 ) == 0x30 ) /* vehicle speed , Globe Box Signal update check */
            {
                u08_s_CAN_St_Update_Delay = 0;

                s_User_Command_Check(TG_PASSIVE); 
            }
            else
            {
                u08_s_CAN_St_Update_Delay_for_Passive = 20;
            }              
        }    
    }    

    if( (u08_g_RKE_Close_St == 2) || (u08_g_Crank_RKEnCPAD_CloseStop == 1) )
    {	
        if( (u08_g_C_TailgateTrunkRKECMD == TAILGATE_TRUN_OFF) || ( u16_g_RKE_TimeOut_t == 0 ) )
        {
           u16_g_RKE_TimeOut_t = 0;
                if( st_g_BuzzHaza.CountTick > 17 ) /* check for start Buzzer&Hazard ending */
                {	
                    st_g_BuzzHaza.CountTick -= 16; 
                }
                else
                {
                	 u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF_T;
                }	
                
            u08_g_Crank_RKEnCPAD_CloseStop = 0;
            u08_g_Craking_Stop = 0;
            s_u08_Lonbuzzer = 1;
            s_User_Command_Check(TG_RKE_SW);
            u08_g_RKE_Close_St = 0;
        }	
    }

    if(CAN_Event.C_PTGCNTCmdAVN == ON)
    {	
    	CAN_Event.C_PTGCNTCmdAVN = OFF;
        if( (u08_g_C_PTGCNTCmdAVN == 1) && (u08_g_C_IGNSw == KEY_IGN) )
        {
            s_User_Command_Check(TG_AVNCMD); 
        }	
    }

    /******************************************************************/
    if( u08_s_CAN_St_Update_Delay != 0 )
    {
        u08_s_CAN_St_Update_Delay--;
    } 
    if( u08_s_RKE_Delay_T != 0 )
    {
        u08_s_RKE_Delay_T--;
    }
    if( s_RKE_Stat_Ready_t !=0 )
    {
    	  s_RKE_Stat_Ready_t--;
    }	    
    if( u08_s_CAN_St_Update_Delay_for_Passive > 0 )
    {
        u08_s_CAN_St_Update_Delay_for_Passive--;
        if( ( u16_s_watit_Passive_t > 0 ) && ((u08_g_Signal_UpDate_check & 0x30 ) == 0x30) )
        {
            u08_s_CAN_St_Update_Delay_for_Passive = 0;
            s_User_Command_Check(TG_PASSIVE); 
        }    
    }  
    
    if(s_u08_Lonbuzzer == 1)
    {
        if (u08_g_LatchSt != LATCH_OPEN)
        {
            s_u08_Lonbuzzer = 0;
        }
        else
        {
            if((st_g_BuzzHaza.CountTick == 0) && (st_g_BuzzHaza.TimeTick == 0))
            {
                s_u08_Lonbuzzer = 0;   
                u08_g_Buzzer_Hazard_Mode  = LONG_BUZZER;  
                u08_g_Buzzer_Hazard_Count = 2;
            }
        }
    }	
}


/********************************************************************************/
/*   name     : s_User_IO_Command                                               */
/*   function : User logic control function                                     */
/*                                                                              */
/********************************************************************************/
static void s_User_IO_Command( void )
{
    static U08 u08_s_Consol_Recheck_t = 0;
    static U08 u08_s_Inner_Recheck_t = 0; 
   
    U08 u08_t_Consol_SW_Check = 0;
    
    
    if( st_g_swinput.swCPadTailgate.swEvent == enON ) 
    {
        st_g_swinput.swCPadTailgate.swEvent = enOFF;   
        if( st_g_swinput.swCPadTailgate.swInput == enON )
        {   
            if( (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) && (u08_g_Tailgate_Moving_Cmd != BREAK_TAILGATE) &&
            	(u16_g_EEP_Diag.ConsoleSw_Short != TRUE) && (u08_g_Strain_Operation_Motor_On_T == 0) )
            {    
                u08_t_Consol_SW_Check = TRUE;
                u08_g_Consol_SW_On_t = 0xFFU; /* stop  */
            }
            u08_g_User_Input_On = ON;
        }
        else
        {  
        	if( ((u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) && (u08_g_CPAD_Close_St == 2)) ||
                ((u08_g_Moving_Delay_T > 0) && (u08_g_CPAD_Close_St == 1)) ||    /* moving start wait */ 
        	    (u08_g_Crank_RKEnCPAD_CloseStop == 2) )
        	{
        	    /*u08_t_Consol_SW_Check = TRUE;*/ /* FOR STOP */
                if(st_g_BuzzHaza.CountTick > 17) /* check for start Buzzer&Hazard ending */
                {	
                    st_g_BuzzHaza.CountTick -= 16; 
                }
                else
                {
               	    u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF_T;
                }  
                u08_g_Crank_RKEnCPAD_CloseStop = 0;
                u08_g_Craking_Stop = 0;
        	  	s_u08_Lonbuzzer = 1;

        	  	if( (u08_g_Moving_Delay_T > 0) && (u08_g_CPAD_Close_St == 1) ) /* moving start wait */
        	  	{	 
                    u08_g_Moving_Delay_T = 0;
        	  	    u08_g_User_Tailgate_Cmd = STOP_TAILGATE;        	  	     
        	  	}
        	  	else
        	  	{
        	  	    s_User_Command_Check(TG_CPAD_SW);	
        	  	}
        	  	u08_g_CPAD_Close_St = 0;
        	}	
            u08_g_Consol_SW_On_t = 0;
        }        
    }    
     
    /* consol switch 900ms On check  */
    if( (st_g_swinput.swCPadTailgate.swInput == enON) && (u08_g_Consol_SW_On_t < 90) &&
        (u16_g_EEP_Diag.ConsoleSw_Short != TRUE) && (u08_g_IO_InputFault != TRUE)   ) 
    {
        u08_g_Consol_SW_On_t++;
        if( u08_g_Consol_SW_On_t == 90 )
        {
            u08_t_Consol_SW_Check = TRUE;   
            u08_g_Consol_SW_On_t = 0xFFU;
        }    
    }    
            
    if( u08_t_Consol_SW_Check == TRUE )
    {          
        u08_t_Consol_SW_Check = FALSE;
        
        if( (NmStateBusSleep(NmGetStatus()) == 0) && (NmStateWaitBusSleep(NmGetStatus()) == 0) )
        {
            u08_s_Consol_Recheck_t = 0;
            GotoMode (Awake);   /* TNM003 : sleep indication flag : wake */
            u16_g_InitSleepDelTm_Att = 100;     /* 100MS */
            s_User_Command_Check(TG_CPAD_SW); 
            
        }
        else
        {
            GotoMode (Awake);                              
            u08_g_CanSleep_flag = FALSE;                   
            u16_g_InitSleepDelTm_Att = 100;     /* 100MS */
            u08_s_Consol_Recheck_t = 30; /* 300ms after Recheck */   
        }
    }        
            
    if( st_g_swinput.swInnerOpenClose.swEvent == enON )
    {
        st_g_swinput.swInnerOpenClose.swEvent = enOFF;
        
        if( st_g_swinput.swInnerOpenClose.swInput == enOFF ) 
        {    
            if( ((u08_g_LatchSt == LATCH_OPEN) || (u08_g_Latch_Unexpect == ON)) && 
                (u16_g_InnerSW_On_t < 600) && (u16_g_EEP_Diag.InnerSw_Short != TRUE) && 
                (u08_g_IO_InputFault != TRUE) )
            {
                if( NmStateBusSleep(NmGetStatus()) == 0 )
                {                      
                    u08_s_Inner_Recheck_t = 0;
                    s_User_Command_Check(TG_INNER_SW);
                }
                else
                {
                    GotoMode (Awake);                        
                    u08_g_CanSleep_flag = FALSE;                   
                    u16_g_InitSleepDelTm_Att = 100;     /* 100MS */
                    u08_s_Inner_Recheck_t = 30;
                }    
            }
            u16_g_InnerSW_On_t = 0;
        }
        else
        {
            if( u08_g_LatchSt == LATCH_OPEN  )
            {
                if( (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) && (u08_g_Tailgate_Moving_Cmd != BREAK_TAILGATE) &&
                	  (u16_g_EEP_Diag.InnerSw_Short != TRUE) )
                {    
                    u16_g_InnerSW_On_t = 0xFFFFU;
                    s_User_Command_Check(TG_INNER_SW);    
                }    
            
                u08_g_User_Input_On = ON;
            }
        }    
    }
    
    if( u08_s_Inner_Recheck_t != 0 )
    {
        u08_s_Inner_Recheck_t--;  
        if( u08_s_Inner_Recheck_t == 0 )
        {
            s_User_Command_Check(TG_INNER_SW);
        }
    } 
    
    if( u08_s_Consol_Recheck_t != 0 )
    {
        u08_s_Consol_Recheck_t--; 
        if( u08_s_Consol_Recheck_t == 0 )
        {
            s_User_Command_Check(TG_CPAD_SW);     
        }         
    }
}

/********************************************************************************/
/*   name     : s_Parmeter_Reset_Command                                        */
/*   function : Parameter Reset control command                                 */
/*                                                                              */
/********************************************************************************/

static void s_Parmeter_Reset_Command( void )
{
    if( (u16_g_InnerSW_On_t >= 600) && (u16_g_InnerSW_On_t != 0xFFFFU) ) /* 5ms count tick, 5ms * 600 = 3Sec */
    {    
        if( (u16_g_ReleaswSW_On_t >= 300) && (u16_g_ReleaswSW_On_t != 0xFFFFU))
        {
            u16_g_InnerSW_On_t = 0xFFFFU;     
            u16_g_ReleaswSW_On_t = 0xFFFFU;
            CAN_Event.C_TgOtrReleaseSW = OFF;
            if( u08_g_LatchSt == LATCH_OPEN )
            {  
                g_Parameter_Reset();
            }
        }
    
        if( u16_g_ReleaswSW_On_t == 0 )
        {
            if( (u08_g_LatchSt == LATCH_OPEN) && (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) )
            { 
                u16_g_InnerSW_On_t = 0xFFFFU;     
                u16_g_ReleaswSW_On_t = 0xFFFFU;
                u08_s_Pre_User_Tailgate_Cmd = OPEN_TAILGATE;

                g_User_Open_Position();
            }
        }    
    }
}

/********************************************************************************/
/*   name     : s_Switch_Short_Check                                            */
/*   function : O/S HDL Switch , Inner Switch Short Check                       */
/*                                                                              */
/********************************************************************************/
static void s_Switch_Short_Check( void )
{
    static U08 u08_s_Inner_SW_Off_Cnt = 0;
    static U16 u16_s_Inner_SW_On_Cnt = 0;
    static U08 u08_s_OS_HDL_SW_Off_Cnt = 0;
    static U08 u08_s_Console_SW_Off_Cnt = 0;
    static U16 u16_s_Console_SW_On_Cnt = 0;
    
    /* if Keep On Press Inner_Sw 10Sec then Switch Short set*/
    if( u16_g_EEP_Diag.InnerSw_Short != TRUE ) 
    {
        if( st_g_swinput.swInnerOpenClose.swInput == enON )
        {   
            u16_s_Inner_SW_On_Cnt++; 
            if( u16_s_Inner_SW_On_Cnt >= 1000 )
            {
                u08_s_Inner_SW_Off_Cnt = 0;
                u16_g_InnerSW_On_t = 0xFFFFU;
                u16_g_EEP_Diag.InnerSw_Short = TRUE;
                u08_g_Switch_Short_Event = TRUE; 
            }
        }
        else
        {
            u16_s_Inner_SW_On_Cnt = 0;
        }    
    }     
    else
    {    
        if( st_g_swinput.swInnerOpenClose.swInput == enOFF )
        {
            u08_s_Inner_SW_Off_Cnt++;
            if( u08_s_Inner_SW_Off_Cnt >= 200 ) /* 200 * 10ms = 2Sec off */
            {
                u08_s_Inner_SW_Off_Cnt = 0;
                u16_g_InnerSW_On_t = 0;
                u16_g_EEP_Diag.InnerSw_Short = FALSE;
                u08_g_Switch_Short_Event = TRUE; 
            }    
        }
        else
        {
            u08_s_Inner_SW_Off_Cnt = 0;
        }    
    }     
    
    if( u16_g_EEP_Diag.OS_HDLSw_Short != TRUE ) 
    {
        if( (u16_g_ReleaswSW_On_t >= 1000) && (u16_g_ReleaswSW_On_t != 0xFFFFU) )
        {  
            u08_s_OS_HDL_SW_Off_Cnt = 0;  
            u16_g_ReleaswSW_On_t = 0xFFFFU;
            u16_g_EEP_Diag.OS_HDLSw_Short = TRUE;
            u08_g_Switch_Short_Event = TRUE; 
        }
    }       
    else
    {    
        if( u08_g_C_TgOtrReleaseSW == OFF )
        {
            u08_s_OS_HDL_SW_Off_Cnt++;
            if( u08_s_OS_HDL_SW_Off_Cnt >= 200 ) /* 200 * 10ms = 2Sec off */
            {
                u08_s_OS_HDL_SW_Off_Cnt = 0;
                u16_g_ReleaswSW_On_t = 0;
                CAN_Event.C_TgOtrReleaseSW = OFF;
                u08_g_Switch_Short_Event = TRUE; 
                u16_g_EEP_Diag.OS_HDLSw_Short = FALSE;
            }  
        }
        else
        {
            u08_s_OS_HDL_SW_Off_Cnt = 0;
        }    
    }    
    
    if( u16_g_EEP_Diag.ConsoleSw_Short != TRUE )
    {
        if( st_g_swinput.swCPadTailgate.swInput == enON)
        {
            u16_s_Console_SW_On_Cnt++;
            if( u16_s_Console_SW_On_Cnt >= 1000 )
            {
                u08_s_Console_SW_Off_Cnt = 0;
                u16_g_EEP_Diag.ConsoleSw_Short = TRUE;
                u08_g_Consol_SW_On_t = 0xFF;
                u08_g_Switch_Short_Event = TRUE; 
            }    
        }
        else
        {
            u16_s_Console_SW_On_Cnt = 0;
        }        
    }
    else
    {
        if( st_g_swinput.swCPadTailgate.swInput == enOFF )
        {
            u08_s_Console_SW_Off_Cnt++;
            if( u08_s_Console_SW_Off_Cnt >= 200 )
            {
                u16_g_EEP_Diag.ConsoleSw_Short = FALSE;
                u08_g_Switch_Short_Event = TRUE; 
                u08_s_Console_SW_Off_Cnt = 0;
                u08_g_Consol_SW_On_t = 0;
            }    
        }
        else
        {
            u08_s_Console_SW_Off_Cnt = 0;
        }        
    }        
}





/********************************************************************************/
/*   name     : s_Serial_Write_Check                                            */
/*   function : ECU Serial numner EEPROM Write control                          */
/*                                                                              */
/********************************************************************************/
static void s_Serial_Write_Check( void )
{
    U08 err_st = 0;
    
    if( (u08_g_Diag_Write_Cmd == TRUE) && (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE)  )
    {
        u08_g_Diag_Write_Cmd = 0x00;
        err_st = u08_g_Write_Diag_Sector();   
    }    
}

/********************************************************************************/
/*   name     : s_Inline_Mode_UserClear ,                                       */
/*   function : Inline Mode User Clear mode detect                              */
/********************************************************************************/
static void s_Inline_Mode_UserClear( void )
{
    if( u16_g_EEP_Diag.InLine_Mode == TRUE )
    {    
        if( (u08_g_C_VehicleSpeed != 0xFF) && (u08_g_C_VehicleSpeed >= 5) ) 
        {
            u16_g_EEP_Diag.InLine_Mode = FALSE; 
            u08_g_Diag_Write_Cmd = TRUE;
        }
    }    
}

/********************************************************************************/
/*   name     : s_UserHighSet_Conrtol ,                                         */
/*   function : Trunk Open HightSet Control                                     */
/********************************************************************************/
static void s_UserHighSet_Conrtol(void)
{
	S16 Temp = 0;
	
	if(CAN_Event.C_PTGHeightNValueSet == TRUE)
	{
	    CAN_Event.C_PTGHeightNValueSet = 0;
          /* 0:Default, 1:40% , 2:67%, 3:85%, 4:100% 5:6: Reserved, 7:Invalid */
        if( (u16_g_EEP_Para.User_Hight_SetValue != u08_g_C_PTGHeightNValueSet) && (u08_g_C_PTGHeightNValueSet != 0) &&
      	    (u08_g_C_PTGHeightNValueSet != 5) && (u08_g_C_PTGHeightNValueSet < 7) && (u08_g_C_IGNSw == KEY_IGN) )
        {
            u16_g_EEP_Para.User_Hight_SetValue = u08_g_C_PTGHeightNValueSet;
            u08_g_User_Position_set = 1;
        }
    }
    
    if( (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) && (u08_g_User_Position_set == 1) && (u08_g_Init_Param == ON) )
    {
        Temp = (u16_g_EEP_Para.MaxBasicPos/20);
        switch(u16_g_EEP_Para.User_Hight_SetValue)
        {
            case 1 :
            	u16_g_EEP_Para.MaxPowerOpenPos = (Temp*8 + 15); /*40%*/
                break;
            case 2 :
            	u16_g_EEP_Para.MaxPowerOpenPos = (Temp*12 + 30); /*67% -> 60%*/
            	break;   
            case 3 :
            	u16_g_EEP_Para.MaxPowerOpenPos = (Temp*16 + 15);  /*85% -> 80%*/
            	break;
            case 4 :
            	u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.MaxBasicPos; /*100%*/
            	break;	
            case 6 :	 
            	u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.UserSetPosition; /* User Set position */ 
            	break;
            default :
            	/*u16_g_EEP_Para.MaxPowerOpenPos = u16_g_EEP_Para.MaxBasicPos;*/ /*100%*/
            	break;	 	 
        }
        u08_g_ParamStatus = PARAM_SAVE;
    }	    
}

/********************************************************************************/
/*   name     : s_UserHighSet_Conrtol ,                                         */
/*   function : Trunk Open HightSet Control                                     */
/********************************************************************************/
static void s_UserSpeedSet_Control(void)
{
	U08 err_st = 0;
	U08 u08_t_SpeedMode = 0;
	  
    if(CAN_Event.C_PTGSpeedNValueSet == ON)
    {
        CAN_Event.C_PTGSpeedNValueSet = OFF;
        if(u08_g_C_PTGSpeedNValueSet == 1)
        {	
            u08_t_SpeedMode = 1;
        }
        else
        {
            if(u08_g_C_PTGSpeedNValueSet == 2)
            {	
                u08_t_SpeedMode = 2;
            }    
        }
    }
     
    if( (u08_t_SpeedMode == 2) || (u08_t_SpeedMode == 1) )
    {     
        if(u08_g_C_IGNSw == KEY_IGN)
        {
            if(u08_t_SpeedMode != (U08)u16_g_EEP_Diag.Speed_Set_Value)
            {
                u16_g_EEP_Diag.Speed_Set_Value = u08_t_SpeedMode;
                u08_s_UserSpeed_Update_Set = 1;
            }    
        }
    }        
        
    if( (u08_g_User_Tailgate_Cmd == STOP_TAILGATE) &&  (u08_s_UserSpeed_Update_Set == 1) )
    {
    	  u08_s_UserSpeed_Update_Set = 0;

        g_PID_Tartget_Position_Init();
        err_st = u08_g_Write_Diag_Sector();
    }
}

/********************************************************************************/
/*   name     : s_InnerSwitch_Error_Check ,                                     */
/*   function : Inner Switch On check in Latch Close Status                     */
/********************************************************************************/
static void s_InnerSwitch_Error_Check( void )
{
    if( st_g_swinput.swInnerOpenClose.swInput == enON ) 
    {
        if( (u08_g_LatchSt == LATCH_LOCK) && (u08_g_Latch_Unexpect != ON)  )
        {
            u16_g_InnerSW_On_t = 0xFFFFU;
        }
    }
}

/********************************************************************************/
/*   name     : g_User_Logic_10msPeriod                                         */
/*   function : User logic control function                                     */
/*                                                                              */
/********************************************************************************/
void g_User_Logic_10msPeriod(void)
{
    s_User_CAN_Command();
    s_User_IO_Command();
    s_Parmeter_Reset_Command();
    s_Inline_Mode_UserClear();
    s_Switch_Short_Check();
    s_Serial_Write_Check();
    s_USM_ReSet();
    s_PowerMode_Set();
    s_UserHighSet_Conrtol();    
    s_UserSpeedSet_Control();
    s_InnerSwitch_Error_Check();
    
    if( u08_g_User_Tailgate_Cmd != STOP_TAILGATE )
    {
        u08_s_Pre_User_Tailgate_Cmd = u08_g_User_Tailgate_Cmd;
    } 
    
    if( u16_s_watit_Passive_t > 0 )
    {
        u16_s_watit_Passive_t--;
    }    
}

/********************************************************************************/
/*   name     : g_User_Logic_Init                                               */
/*   function : user logic value init                                           */
/*                                                                              */
/********************************************************************************/
void g_User_Logic_Init(void)
{
    u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
    u08_g_Tx_ReleaseRly = 0;
    u08_g_Moving_Delay_T = 0;  
    u08_g_User_Input_On = 0;
    u08_g_Signal_UpDate_check = 0;
    u08_g_Diag_Write_Cmd = 0x00;
    u08_g_Switch_Short_Event = FALSE;
    u08_g_RKE_Close_St = 0;
    u08_g_CPAD_Close_St = 0;
    u08_g_Zone2_Open = 0;
    u08_g_User_Position_set = 0;
    
    if( u16_g_EEP_Diag.OS_HDLSw_Short == TRUE )
    {    
        u16_g_ReleaswSW_On_t = 0xFFFFU;
    }
    else
    {
        u16_g_ReleaswSW_On_t = 0;
    }    
    
    if( u16_g_EEP_Diag.InnerSw_Short == TRUE )
    {     
        u16_g_InnerSW_On_t = 0xFFFFU;
    }
    else
    {
        u16_g_InnerSW_On_t = 0;
    }    
    
    if( u16_g_EEP_Diag.ConsoleSw_Short == TRUE )
    {
        u08_g_Consol_SW_On_t = 0xFF;
    }
    else
    {
        u08_g_Consol_SW_On_t = 0;
    }        
}