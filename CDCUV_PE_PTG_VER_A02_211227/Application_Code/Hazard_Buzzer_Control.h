/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-21 PM.  5:39:35                                   */
/*  Last update     : 2013-12-12 AM. 11:22:55                                   */
/*  File Name       : Hazard_Buzzer_Control.h                                   */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		HAZARD_BUZZER_H_EXTERN
#define		HAZARD_BUZZER_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	HAZARD_BUZZER_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
 #define    BUZZONLY_ON     1  /* Only Buzzer out control */
 #define    BUZZHAZA_ON     2  /* Buzzer and Hazard out control */
 #define    BUZZHAZA_OFF    3  /* Buzzer and Hazard out Off */ 
 #define    SHORT_BUZZER    4  /* 250ms On/Off time Buzzer */
 #define    HAZAONLY_ON     5  /* Only Hazard out control */
 #define    LONG_BUZZER     6  /* Long Time Buzzer */
 #define    BUZZHAZA_OFF_T  7
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
typedef struct
{
    U16 TimeTick;    
    U08 CountTick;
    U08 Mode;
    U08 BuzzerSt;
    U08 HazardSt;             
}stControlBuzzHaza;

EXTERN stControlBuzzHaza st_g_BuzzHaza;
EXTERN U08 u08_g_Buzzer_Hazard_Mode;
EXTERN U08 u08_g_Buzzer_Hazard_Count;
EXTERN U08 u08_g_BuzzFeedBackSt;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_BuzzHazardOut10msPeriod(void);

#undef EXTERN

#endif
