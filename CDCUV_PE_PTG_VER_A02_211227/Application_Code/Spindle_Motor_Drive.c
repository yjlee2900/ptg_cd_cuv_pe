/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-30 PM.  6:31:47                                   */
/*	Last update     : 2013-05-23 PM.  2:53:45                                   */
/*	File Name       : Spindle_Motor_Drive.c                                     */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define LID_MOTOR_DRIVE_EXTERN
#include "Basic_Type_Define.h"
#include "Spindle_Motor_Drive.h"
#include "SPD_LH_EN_A.h"
#include "SPD_LH_EN_B.h"
#include "SPD_RH_EN_A.h"
#include "SPD_RH_EN_B.h"
#include "SPD_LH_IN_A.h"
#include "SPD_LH_IN_B.h"
#include "SPD_RH_IN_A.h"
#include "SPD_RH_IN_B.h"
#include "SPD_LH_PWM.h"
#include "SPD_RH_PWM.h"
#include "SPD_LH_Multisense_EN.h"
#include "SPD_RH_Multisense_EN.h"
#include "Status_Detect.h"


/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/

/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/

/********************************************************************************/
/*   name     : g_Spind_MTDrive_Out                                  */
/*   function : full bridge IC Direction initial                                */
/********************************************************************************/
void g_Spind_MTDrive_Out( U08 u08_MotorOut )
{
    /* init value : Motor Stop */
    U08 u08_LH_EN_A = u08_FULL_BRIT_OFF;
    U08 u08_LH_EN_B = u08_FULL_BRIT_OFF;
    /*U08 u08_RH_EN_A = u08_FULL_BRIT_OFF;
    U08 u08_RH_EN_B = u08_FULL_BRIT_OFF;*/
    U08 u08_LH_IN_A = u08_FULL_BRIT_OFF;
    U08 u08_LH_IN_B = u08_FULL_BRIT_OFF;
    /*U08 u08_RH_IN_A = u08_FULL_BRIT_OFF;
    U08 u08_RH_IN_B = u08_FULL_BRIT_OFF;*/
    U08 u08_LH_MT_EN = u08_FULL_BRIT_OFF;
    /*U08 u08_RH_MT_EN = u08_FULL_BRIT_OFF;*/
        
    switch( u08_MotorOut )
    {
        case OPEN_TAILGATE :
             u08_LH_MT_EN = u08_FULL_BRIT_ON;
             u08_LH_IN_A = u08_FULL_BRIT_ON;  
             /*u08_RH_IN_A = u08_FULL_BRIT_ON; */
             u08_LH_EN_A = u08_FULL_BRIT_ON; /* SEL_0 Set */
             /*u08_RH_EN_A = u08_FULL_BRIT_ON;*/ /* SEL_0 Set */
            break;
        case CLOSE_TAILGATE :
        	  u08_LH_MT_EN = u08_FULL_BRIT_ON;
            u08_LH_IN_B = u08_FULL_BRIT_ON;  
            /*u08_RH_IN_B = u08_FULL_BRIT_ON; */
            
            /*u08_LH_EN_A = u08_FULL_BRIT_ON;*/ /* SEL_0 Set */
            /*u08_RH_EN_A = u08_FULL_BRIT_ON;*/ /* SEL_0 Set */
            break;
        case BREAK_TAILGATE :    
            break;               
        case STOP_TAILGATE : 
        	  u08_LH_EN_A = u08_FULL_BRIT_ON;
        	  u08_LH_EN_B = u08_FULL_BRIT_ON;
        	  u08_LH_MT_EN = u08_FULL_BRIT_ON;
            break; 
        case INIT_TAILGATE : 
        	  break;                 
        default :
            break;                          
    }
    
    SPD_LH_Multisense_EN_PutVal( u08_LH_MT_EN );
    /*SPD_RH_Multisense_EN_PutVal( u08_RH_MT_EN );*/
    SPD_LH_EN_A_PutVal( u08_LH_EN_A ); /* Sel 0 */
    SPD_LH_EN_B_PutVal( u08_LH_EN_B ); /* Sel 1 */
    /*SPD_RH_EN_A_PutVal( u08_RH_EN_A );*/ /* Sel 0 */
    /*SPD_RH_EN_B_PutVal( u08_RH_EN_B );*/ /* Sel 1 */
    SPD_LH_IN_A_PutVal( u08_LH_IN_A );
    SPD_LH_IN_B_PutVal( u08_LH_IN_B );
    /*SPD_RH_IN_A_PutVal( u08_RH_IN_A );
    SPD_RH_IN_B_PutVal( u08_RH_IN_B );     */
    
    u08_g_SpindlePowerMov_Direction = u08_MotorOut;   
    
}

/********************************************************************************/
/*   name     : g_Spindle_MT_OutPWM                                               */
/*   function : LID Motor LH,RH Out Control                                     */
/*   time  task                                                                 */
/********************************************************************************/
void g_Spindle_MT_OutPWM(/*U08 u08_RH_Ratio,*/ U08 u08_LH_Ratio ) 
{
    u08_g_Result_call = SPD_RH_PWM_SetRatio8( 255U ); 
    u08_g_Result_call = SPD_LH_PWM_SetRatio8( 255U - u08_LH_Ratio );
        
    u08_g_LH_Ratio = u08_LH_Ratio; 
    /*u08_g_RH_Ratio = u08_RH_Ratio;*/
}

