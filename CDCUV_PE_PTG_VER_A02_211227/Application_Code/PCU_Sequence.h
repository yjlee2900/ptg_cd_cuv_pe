/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-24 PM.  4:43:16                                   */
/*  Last update     : 2013-05-24 PM.  4:43:19                                   */
/*  File Name       : PCU_Sequence.h                                            */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		PCU_SEQUENCE_H_EXTERN
#define		PCU_SEQUENCE_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	PCU_SEQUENCE_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/

 #define    STANDBY           0U
 #define    CINCHING_START    1U
 #define    CINCHING_RUN      2U
 #define    CINCHING_STOP     3U   
 #define    REWIND_RUN        4U
 #define    PCU_STOP          5U
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_PCU_Mode;
EXTERN U08 u08_g_CinchingStartDelaySet;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_PCU_SequenceControl_10msPeriod( void );
EXTERN  void g_PCU_Sequenc_Init( void );
EXTERN  void g_PCU_Rewind_Break_1msperiod( void );

#undef EXTERN

#endif
