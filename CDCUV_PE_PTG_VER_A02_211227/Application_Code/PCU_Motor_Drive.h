/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-30 PM.  6:31:47                                   */
/*	Last update     : 2013-04-30 PM.  6:31:51                                   */
/*	File Name       : PCU_Motor_Drive.c                                         */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		PCU_MOTOR_DRIVE_H
#define		PCU_MOTOR_DRIVE_H

#include "Basic_Type_Define.h"

#ifndef  	PCU_MOTOR_DRIVE_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define OFF_PCU        0U
#define CINCHING_PCU   1U
#define REWIND_PCU     2U
#define BREAK_PCU      3U

/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_PCU_MotorSt; 

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_PCU_MTDrive_Out( U08 u08_MotorOut );

#undef EXTERN

#endif
