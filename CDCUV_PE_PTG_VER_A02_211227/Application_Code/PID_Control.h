/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-29 AM.  10:11:04                                  */
/*  Last update     : 2014-06-24 PM. 7:05:58                                    */
/*  File Name       : PID_Control.h                                             */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		PID_CONTROL_H_EXTERN
#define		PID_CONTROL_H_EXTERN
                         
#include "Basic_Type_Define.h"                         
                                  
#ifndef  	PID_CONTROL_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
/*************************** Tuning point ***************************************/ 
#define OPEN_TARGET_RPM_START     1800
#define OPEN_TARGET_RPM_ZONE1     2600
#define OPEN_TARGET_RPM_ZONE2     2600
#define OPEN_TARGET_RPM_ZONE3     1500
#define OPEN_TARGET_RPM_ZONE4     1500

#define CLOSE_TARGET_RPM_START    1800
#define CLOSE_TARGET_RPM_ZONE1    2400
#define CLOSE_TARGET_RPM_ZONE2    2400
#define CLOSE_TARGET_RPM_ZONE3    2000
#define CLOSE_TARGET_RPM_ZONE4    2000

#define FAST_SPEED_TARGET_RPM     900

#define OPEN_NOT_PARA_RPM         2600
#define CLOSE_NOT_PARA_RPM        2600
/********************************************************************************/

#define PID_ZONE1                    0U
#define PID_ZONE2                    1U
#define PID_ZONE3                    2U
#define PID_ZONE4                    3U
#define RE_SERCHING_ZONE             0xFFU
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U16 	u16_g_Target_RPM; 
EXTERN U08  u08_g_PID_Zone;
EXTERN U08  u08_g_Kp;
EXTERN U08  u08_g_Ki;
EXTERN U08  u08_g_Kd;
EXTERN S08  s08_g_PID_Radio;
EXTERN S08  s08_g_Propoti_Ratio;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN  void g_PID_Value_Init( void );
EXTERN  void g_Proporti_PWM_Calculation( void );
EXTERN	void g_PID_Calculation( void );
EXTERN  void g_PID_Tartget_Position_Init( void );

#undef EXTERN

#endif
