/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-21 PM.  5:39:35                                   */
/*  Last update     : 2013-12-12 AM. 11:22:25                                   */
/*  File Name       : Hazard_Buzzer_Control.c                                   */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define HAZARD_BUZZER_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Hazard_Buzzer_Control.h"
#include "Status_Detect.h"
#include "BuzzerPWM.h"
#include "CAN_Signal_Handling.h"
#include "Buzzer_FeedBackIn.h"
#include "cpu.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U16 u16_s_Timer_Tick = 0; 

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_BuzzerOutPWM( U08 On_Off );
static void s_BuzzHazaOut( void );
static void s_SetBuzzHazardControl(U08 u08_t_OutSel, U08 u08_t_Count);
static void s_BuzzerFeedbackCheck( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define BUZZHAZA_PERIOD_TICK      50   /* 10ms * 50 = 500ms */
#define BUZZHAZA_PERIOD_SHOT_TIME 25   /* 10ms * 25 = 250ms */
#define BUZZHAZA_INACTIVE_TICK    50   /* 10ms * 50 = 500ms */
#define BUZZHAZA_PERIOD_LONG_TIME 500  /* 10ms * 500 = 5000ms */

/********************************************************************************/
/*   name     : s_BuzzerOutPWM                                                  */
/*   function : buzzer pwm control                                              */
/********************************************************************************/
static void s_BuzzerOutPWM( U08 On_Off )
{
    if( On_Off == ON )
    {       
        /*Buzzer_OnOff_PutVal( 1 );*/
        u08_g_Result_call = BuzzerPWM_SetDutyUS(375);    /* 375 = 0xBC , CHECK s_BuzzerFeedbackCheck VALUE !!! */
        u08_g_Result_call = BuzzerPWM_Enable(); 
    }
    else
    {
        /*Buzzer_OnOff_PutVal( 0 ); */
        u08_g_Result_call = BuzzerPWM_Disable(); 
    }        
}

/********************************************************************************/
/*   name     : s_BuzzerOutPWM                                                  */
/*   function : buzzer pwm control                                              */
/********************************************************************************/
static void s_BuzzHazaOut( void )
{
    if( (st_g_BuzzHaza.Mode == BUZZHAZA_ON) || (st_g_BuzzHaza.Mode == BUZZONLY_ON) || 
    	  (st_g_BuzzHaza.Mode == SHORT_BUZZER) || (st_g_BuzzHaza.Mode == LONG_BUZZER ) )
    {    
        st_g_BuzzHaza.BuzzerSt = (st_g_BuzzHaza.BuzzerSt != ON) ? ON : OFF ;
        s_BuzzerOutPWM(st_g_BuzzHaza.BuzzerSt);
    }
    else
    {
        st_g_BuzzHaza.BuzzerSt = OFF;
        s_BuzzerOutPWM(st_g_BuzzHaza.BuzzerSt); 
    }    
    
    if( (st_g_BuzzHaza.Mode == BUZZHAZA_ON) || (st_g_BuzzHaza.Mode == HAZAONLY_ON) )     
    {
        st_g_BuzzHaza.HazardSt = (st_g_BuzzHaza.HazardSt != ON) ? ON : OFF ;
        g_Tx_CAN_Hazard( st_g_BuzzHaza.HazardSt ); 
    }
    else
    {
        st_g_BuzzHaza.HazardSt = OFF;
        g_Tx_CAN_Hazard( 2 ); 
    }        
}

/********************************************************************************/
/*   name     : s_SetBuzzHazardControl                                          */
/*   function : buzzer & hazard out control setting                             */
/*                                                                              */
/********************************************************************************/
static void s_SetBuzzHazardControl(U08 u08_t_OutSel, U08 u08_t_Count)
{
    U08 s_temp;
    
    /*if( u08_t_OutSel == LONG_BUZZER )
    {
        st_g_BuzzHaza.CountTick = 2;
        st_g_BuzzHaza.BuzzerSt = OFF;
        st_g_BuzzHaza.HazardSt = ON; 
    }*/	
    if( (u08_t_OutSel == BUZZHAZA_OFF) || (u08_t_OutSel == BUZZHAZA_OFF_T) )
    {
        /* (CountTick = 1 and BuzzerSt = ON, HazardSt = ON) ==> next reverse out OFF */
        st_g_BuzzHaza.CountTick = 1;
        st_g_BuzzHaza.BuzzerSt = ON;
        st_g_BuzzHaza.HazardSt = ON;        
    }
    else
    {
        if( st_g_BuzzHaza.TimeTick > 0 ) /* run check */
        {
            s_temp = (st_g_BuzzHaza.CountTick % 2 );  /* 1 or 0 , 1 = On state , 0 = Off state */
            st_g_BuzzHaza.CountTick = (u08_t_Count + s_temp ); /* when pre run status == On , Off time end waiting and On state flow */
            
            if( s_temp == 1 ) /* for Buzzer & Hazard Sync Output value */
            {    
                st_g_BuzzHaza.BuzzerSt = ON;
                st_g_BuzzHaza.HazardSt = ON;
            }
            
        }
        else
        {    
             st_g_BuzzHaza.CountTick = u08_t_Count;  
             st_g_BuzzHaza.BuzzerSt = OFF;
             st_g_BuzzHaza.HazardSt = OFF;
        }
        
        if( u08_t_OutSel == SHORT_BUZZER )
        {
            u16_s_Timer_Tick = BUZZHAZA_PERIOD_SHOT_TIME;
        }
        else if ( u08_t_OutSel == LONG_BUZZER )
        {
        	  u16_s_Timer_Tick = BUZZHAZA_PERIOD_LONG_TIME;
        }	
        else
        {
            u16_s_Timer_Tick = BUZZHAZA_PERIOD_TICK;
        }    
    }
    
    st_g_BuzzHaza.Mode = u08_t_OutSel; 
}

static void s_BuzzerFeedbackCheck( void )
{
	  static U08 BuzzerFailCnt = 0;
	  U08 BuzzerSt = 0;
	  
	  BuzzerSt = Buzzer_FeedBackIn_GetVal();
	  
	  if( (BuzzerSt == 0) || (PWMDTY0 != 0xBC) )
	  {
	      if( BuzzerFailCnt >= 5 )
	      {
	          u08_g_BuzzFeedBackSt = 1;
	      }
	      else
	      {
            BuzzerFailCnt++;	     	
        }
	  }
	  else
	  {
	      u08_g_BuzzFeedBackSt = 0;
	      BuzzerFailCnt = 0;
	  }		
}

/********************************************************************************/
/*   name     : g_BuzzHazardOut10msPeriod                                       */
/*   function : buzzer & hazard out put                                         */
/*                                                                              */
/********************************************************************************/
void g_BuzzHazardOut10msPeriod(void)
{
    if( u08_g_Buzzer_Hazard_Mode != 0 )
    {    
        s_SetBuzzHazardControl(u08_g_Buzzer_Hazard_Mode, u08_g_Buzzer_Hazard_Count);
        u08_g_Buzzer_Hazard_Mode = 0;
    }
    
    if( st_g_BuzzHaza.CountTick > 0 )
    {
        if( st_g_BuzzHaza.TimeTick == 0 )
        {
            st_g_BuzzHaza.CountTick--;  
            if( st_g_BuzzHaza.CountTick > 0 )
            {
                st_g_BuzzHaza.TimeTick = u16_s_Timer_Tick;
            }
            else
            {
                if( st_g_BuzzHaza.Mode != BUZZHAZA_OFF )
                {    
                    st_g_BuzzHaza.TimeTick = BUZZHAZA_INACTIVE_TICK;
                }
                
                st_g_BuzzHaza.Mode = 0;
            }
            s_BuzzHazaOut(); /* reverse output */            
        }
        else
        {
            st_g_BuzzHaza.TimeTick--;
            if( (st_g_BuzzHaza.TimeTick <= 15) && (st_g_BuzzHaza.TimeTick > 10) && (st_g_BuzzHaza.BuzzerSt == ON ) )
            {
                s_BuzzerFeedbackCheck();
            }	 
        }        
    }
    else  /* hazard inactive */
    {
        if( st_g_BuzzHaza.TimeTick > 0 )
        {
            st_g_BuzzHaza.TimeTick--;
        }
        else   /* hazard signal default value 2 : inactive */ 
        {
            g_Tx_CAN_Hazard( 2 );  /* inactive status */
            s_BuzzerOutPWM( OFF ); /*Buzzer Off */
            st_g_BuzzHaza.BuzzerSt = OFF;
            st_g_BuzzHaza.HazardSt = OFF;  
            st_g_BuzzHaza.Mode = 0;
            u08_g_BuzzFeedBackSt = 0;
        }           
    }    
}
