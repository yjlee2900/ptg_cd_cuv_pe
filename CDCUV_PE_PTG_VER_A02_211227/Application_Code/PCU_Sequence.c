/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-24 PM.  4:42:32                                   */
/*  Last update     : 2013-05-28 PM.  8:35:50                                   */
/*  File Name       : PCU_Sequence.c                                            */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define PCU_SEQUENCE_EXTERN
#include "Basic_Type_Define.h"
#include "PCU_Sequence.h"
#include "Digital_Input_Chattering.h"
#include "Status_Detect.h"
#include "User_Logic_Control.h"
#include "Thermal_Protect.h"
#include "PCU_Motor_Drive.h"
#include "Hazard_Buzzer_Control.h"
#include "Parameterization.h"
#include "Obstacle_Detect.h"
#include "EEPROM_Control.h"
#include "PCU_HomePos_SW.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
static U16 u16_s_Cinching_OnLimit_Time = 0;
static U16 u08_s_Rewind_OnLimit_Time = 0;
static U08 u08_s_Rewind_delay_Time = 0;
static U08 u08_s_Rewind_Break_Time = 0;
static U08 u08_s_pre_PCU_Mode = 0xFF;
static U08 u08_s_Cinching_Error_Cnt = 0;
static U08 u08_s_Rewind_Error_Cnt = 0;
static U08 u08_s_Cinching_Start_Event = 0;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Cinchign_Start_Check( void );
static void s_Cinchign_start_Set( void );
static void s_Cinching_Stop_Check( void );
static void s_Rewind_Start_check( void );
static void s_Rewind_Stop_Check( void );
static void s_PCU_CinchingStart_Position( void );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define CINCHING_START_DELAY_TIME    20  /* 20 * 10ms = 200ms */
#define CINCHING_RUN_LIMIT_T         300  /* 300 * 10ms = 3000ms */ 
#define REWIND_START_DELAY_TIME      10  /* 10 * 10ms = 100ms */
#define REWIND_RUN_LIMIT_T           200 /* 200 * 10ms = 2000ms */
#define CINCHING_ERR_LIMIT           10
#define REWIND_ERR_LIMIT             10
/********************************************************************************/
/*   name     : s_Cinchign_Start_Check                                          */
/*   function : cinching start condition check                                  */
/********************************************************************************/
static void s_Cinchign_Start_Check( void )
{
    static U08 u08_s_Secondary_On_Time = 0;
    static U08 u08_s_ThermalBuzzer_Set = 0;
    
    if( u08_s_Cinching_Start_Event == ON )
    {
        u08_s_Cinching_Start_Event = OFF;
        if( (u08_s_Cinching_Error_Cnt < CINCHING_ERR_LIMIT) && (u08_s_Rewind_Error_Cnt < REWIND_ERR_LIMIT) )
        {    
            u08_s_Secondary_On_Time = CINCHING_START_DELAY_TIME;
        }
        else
        {
            u08_g_PCU_Mode = CINCHING_STOP;
        }    
    }    
    
    if( u08_s_Secondary_On_Time > 0 )
    {
        if( (st_g_swinput.inLatchSecondary.swInput == enON) && (st_g_swinput.inLatchLock.swInput == enOFF) )
        {
            u08_s_Secondary_On_Time--;

            if(u08_g_CinchingStartDelaySet == 1)
            {
                u08_g_CinchingStartDelaySet = 0;
                u08_s_Secondary_On_Time = CINCHING_START_DELAY_TIME;
            }

            if( u08_s_Secondary_On_Time == 0 )
            {
                if( u08_g_PCU_Theraml_St != THERMAL_PROTECT_SET )
                {
                    u08_g_PCU_Mode = CINCHING_START;     
                    u08_s_ThermalBuzzer_Set = 0;  
                    u08_g_PCU_Antipinch_St = 0;
                }
                else if( u08_s_ThermalBuzzer_Set == 0 )
                {
                    /*g_SetBuzzHazardControl(BUZZONLY_ON, 6 );*/
                    u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    u08_g_Buzzer_Hazard_Count = 6;           
                    u08_s_ThermalBuzzer_Set = 1; 
                    u08_s_Secondary_On_Time = CINCHING_START_DELAY_TIME;

                    g_Past_Failure_Record(PCU_THER);
                }
                else
                {
                    u08_s_Secondary_On_Time = CINCHING_START_DELAY_TIME;
                }        
            }
        }
        else
        {
            u08_s_Secondary_On_Time = 0;
            u08_s_ThermalBuzzer_Set = 0;
            u08_g_CinchingStartDelaySet = 0;
        }
    }        
}

/********************************************************************************/
/*   name     : s_Cinchign_start_Set                                            */
/*   function : ciching start value set, thermal protect check                  */
/********************************************************************************/
static void s_Cinchign_start_Set( void )
{                                  
    u16_s_Cinching_OnLimit_Time = CINCHING_RUN_LIMIT_T;
    u08_g_PCU_Mode = CINCHING_RUN;
    g_PCU_MTDrive_Out(CINCHING_PCU);
}

/********************************************************************************/
/*   name     : s_Cinching_Stop_Check                                           */
/*   function : cinching stop condition check                                   */
/********************************************************************************/
static void s_Cinching_Stop_Check( void )
{
    if( u16_s_Cinching_OnLimit_Time > 0 )
    {
        u16_s_Cinching_OnLimit_Time--;
        if( u16_s_Cinching_OnLimit_Time == 0 )
        {                      
            g_PCU_MTDrive_Out(OFF_PCU);
            u08_g_PCU_Mode = CINCHING_STOP;
            u08_s_Rewind_delay_Time = REWIND_START_DELAY_TIME;
            u08_s_Cinching_Error_Cnt++;
            u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;  
            u08_g_Buzzer_Hazard_Count = 20; 
        }
        else
        {
            if( (u08_g_LatchSt == LATCH_LOCK) || (u08_g_PCU_Antipinch_St >= ON) )
            {
                g_PCU_MTDrive_Out(OFF_PCU);
                u08_g_PCU_Mode = CINCHING_STOP;
                u16_s_Cinching_OnLimit_Time = 0;
                u08_s_Rewind_delay_Time = REWIND_START_DELAY_TIME;
                if(u08_g_PCU_Antipinch_St == ON) 
                {
                    u08_s_Cinching_Error_Cnt++;
                    u08_g_PCU_Antipinch_St = 0;
                    g_Past_Failure_Record(PCU_CURR);
                }
                else if( u08_g_PCU_Antipinch_St == ANTIPINCH_PCU_UNLATCH )
                {
                    u08_g_Tx_ReleaseRly = 1;
                    u08_g_PCU_Antipinch_St = 0;
                }
                else
                {
                    u08_s_Cinching_Error_Cnt = 0;
                }            
            }    
        }        
    }    
}      

/********************************************************************************/
/*   name     : s_Rewind_Start_check                                            */
/*   function : Rewind start value set, value check  , Init Position Rewind     */
/********************************************************************************/
static void s_Rewind_Start_check( void )
{
    if( u08_s_Rewind_delay_Time > 0 )
    {
        u08_s_Rewind_delay_Time--;
        if( u08_s_Rewind_delay_Time == 0 )
        {
           if( st_g_swinput.inHomePosition.swInput != enON ) 
           {
               g_PCU_MTDrive_Out( REWIND_PCU );
               u08_s_Rewind_OnLimit_Time = REWIND_RUN_LIMIT_T;
               u08_g_PCU_Mode = REWIND_RUN;
           }
           else
           {
               u08_g_PCU_Mode = STANDBY;
           } 
        }    
    }    
}
      
/********************************************************************************/
/*   name     : s_Rewind_Stop_Check                                             */
/*   function : Rewind stop condition check ,                                   */
/*              g_PCU_Rewind_Break_1msperiod : rewind break set                 */
/********************************************************************************/
static void s_Rewind_Stop_Check( void )
{
    if( u08_s_Rewind_OnLimit_Time > 0 )
    {
        u08_s_Rewind_OnLimit_Time--;
        if( u08_s_Rewind_OnLimit_Time == 0 )
        {
            g_PCU_MTDrive_Out(OFF_PCU);
            u08_g_PCU_Mode = PCU_STOP;
            u08_s_Rewind_Error_Cnt++;
        }
        else
        {
            if( u08_s_Rewind_Break_Time > 0 )
            {
                u08_s_Rewind_Break_Time--;
                if( u08_s_Rewind_Break_Time == 0 )
                {
                    g_PCU_MTDrive_Out(OFF_PCU);
                    u08_g_PCU_Mode = PCU_STOP;
                    u08_s_Rewind_Error_Cnt = 0;
                    u08_g_Latch_Unexpect = 0;  /* Clear latch Unexpect */ 
                    u08_g_Hall_Sensor_Error = OFF;
                    u08_g_Trunk_Position_Error = OFF;                
                }    
            }
            else
            {
                if( u08_g_PCU_Antipinch_St == ON )
                {
                    u08_g_PCU_Antipinch_St = 0;
                    g_PCU_MTDrive_Out(OFF_PCU);
                    u08_g_PCU_Mode = PCU_STOP;
                    u08_s_Rewind_Error_Cnt++;
                }    
            }        
        }        
    }    
}

/********************************************************************************/
/*   name     : s_PCU_CinchingStart_Position                                    */
/*   function : PCU Cinching start position check                               */
/********************************************************************************/                                            
static void s_PCU_CinchingStart_Position( void )
{
    if( (st_g_swinput.inLatchSecondary.swEvent == enON) && (st_g_Vehiclest.bBatVoltPTG_Disable != ON) )
    {
        st_g_swinput.inLatchSecondary.swEvent = enOFF;
        
        if( st_g_swinput.inLatchSecondary.swInput == enON )
        {    
            if( (u08_g_Init_Param == OFF) || (u08_g_Power_OnReset == 1) || 
                (s16_g_Tailgate_Position < CLOSE_INIT_POSITION) || (u08_g_Hall_Sensor_Error == ON) || (u08_g_Trunk_Position_Error == ON) )
            {
                u08_s_Cinching_Start_Event = ON; 
            }
            u08_g_CinchingStartDelaySet = 0;
        }
        else
        {
            u08_s_Cinching_Start_Event = OFF;
        }    
    }
}



/********************************************************************************/
/*   name     : g_PCU_SequenceControl_10msPeriod                                */
/*   function :                                                                 */
/*   xxxus time event task                                                      */
/********************************************************************************/
void g_PCU_SequenceControl_10msPeriod(void)
{
     switch( u08_g_PCU_Mode )
     {
         case STANDBY :    
             s_PCU_CinchingStart_Position();            
             s_Cinchign_Start_Check();
             break;
         case CINCHING_START :
             s_Cinchign_start_Set();
             break;
         case CINCHING_RUN :
             s_Cinching_Stop_Check();
             break; 
         case CINCHING_STOP :
             s_Rewind_Start_check();
             break;
         case REWIND_RUN :
             s_Rewind_Stop_Check();
             break;    
         case PCU_STOP :
             u08_g_PCU_Mode = STANDBY;
             break;
         default :
             u08_g_PCU_Mode = STANDBY;
             break;                  
     }
}

/********************************************************************************/
/*   name     : g_PCU_Rewind_Break_1msperiod                                    */
/*   function : Rewind stop condition check , 1ms scheduling task               */
/*              when homeposition On rewind break set                           */
/********************************************************************************/
void g_PCU_Rewind_Break_1msperiod( void )
{
    if( u08_g_PCU_Mode == REWIND_RUN )
    {
        if( (st_g_swinput.inHomePosition.swInput == enON) && (st_g_swinput.inHomePosition.swEvent == enON) )
        {
            st_g_swinput.inHomePosition.swEvent = enOFF;
            g_PCU_MTDrive_Out(BREAK_PCU);
            u08_s_Rewind_Break_Time = 5; /* 50ms Motor Break On */
            u08_s_Rewind_OnLimit_Time = (u08_s_Rewind_OnLimit_Time + 5);
        }    
    }    
}

/********************************************************************************/
/*   name     : g_PCU_Sequenc_Init                                              */
/*   function : PCU Sequenc grobal value Init                                   */
/********************************************************************************/
void g_PCU_Sequenc_Init( void )
{
    U08 u08_t_home_sw = 0;
    
    u08_s_Cinching_Start_Event = 0;
    u08_g_PCU_Antipinch_St = 0;
    u08_s_Cinching_Error_Cnt = 0;
    u08_s_Rewind_Error_Cnt = 0;
    u08_g_CinchingStartDelaySet = 0;
    u08_t_home_sw = PCU_HomePos_SW_GetVal();
    if( u08_t_home_sw == 0 )
    {    
        u08_g_PCU_Mode = CINCHING_STOP; /* for pcu init Position is not homeposition : rewind try */ 
        u08_s_Rewind_delay_Time = REWIND_START_DELAY_TIME;
    }
    else
    {
        u08_g_PCU_Mode = STANDBY; 
        u08_s_Rewind_delay_Time = 0;
    }
}
