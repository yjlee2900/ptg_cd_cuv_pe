/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-05-09 PM.  6:31:01                                   */
/*	Last Update     : 2013-05-09 PM.  6:31:04                                   */
/*	File Name       : Digital_Input_Chattering.h                                */
/*	Processor       : MC9S12P64                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		DIGITAL_IN_CHATT_H
#define		DIGITAL_IN_CHATT_H

#include "Basic_Type_Define.h"
#include "AP_DataType.h"

#ifndef  	DIGITAL_INPUT_CHATTERING_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/

/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/

typedef	struct	/* standard input debouncing data */
{
	U08			nCount;	                /* filtering time counter : typically use 10mS sampling */
	enumSw      swInput;	            /* debounced input value */
	enumSw      swPrev;	                /* previous input value */
	enumSw      swRead;	                /* current input value */
	enumSw      swEvent;	                /* input change Event */
} stSwInput;

typedef	struct	/* this value will store debounded input values */
{
	
    stSwInput    swCPadTailgate;	      /* driver side tailgate open/close switch */
    stSwInput    swInnerOpenClose;	    /* inside tailgate open/close switch  */
    stSwInput    swGearN_Position;      /* Gear 'N' Position Switch */
                 
    stSwInput    inLatchPrimary;		    /* Primary switch input from trunk latch module */
    stSwInput    inLatchSecondary;	    /* Secondary switch input from trunk latch module */
    stSwInput    inLatchLock;            /* Lock switch input from trunk latch module */
    stSwInput    inHomePosition;		    /* power on switch input from cinch module */

	/* event */
    U08          onChanged;
} stPtlSwitch;

EXTERN stPtlSwitch st_g_swinput;
EXTERN U16 u16_g_InnerSW_On_t;
EXTERN U08 u08_g_IO_InputFault;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_Switch_value_Init(void); 
EXTERN  void g_IO_InputHandlerPeriod1ms(void);
EXTERN  void g_IO_InputHandlerPeriod5ms(void);

#undef EXTERN

#endif
