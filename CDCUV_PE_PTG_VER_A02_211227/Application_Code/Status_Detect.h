/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-05-16 PM.  2:17:39                                   */
/*  Last update     : 2014-03-11 PM.  2:59:54                                   */
/*  File Name       : Status_Detect.h                                           */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		STATUS_DETEC_H_EXTERN
#define		STATUS_DETEC_H_EXTERN

#include "AP_DataType.h"
#include "Basic_Type_Define.h"

#ifndef  	STATUS_DETECT_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define TAILGATE_ZONE_0    0U
#define TAILGATE_ZONE_1    1U
#define TAILGATE_ZONE_2    2U
#define TAILGATE_ZONE_3    3U
#define TAILGATE_ZONE_4    4U

    
#define LATCH_LOCK          0  
#define LATCH_SECONDARY     1
#define LATCH_OPEN          2
#define LATCH_LOCK_ONLY     3 
          
#define CLOSE_INIT_POSITION  100         

#define KEY_OFF   0
#define KEY_IN    1
#define KEY_ACC   2
#define KEY_IGN   3
#define KEY_CRANK 4

#define ARMING 1

#define UNEXPECT   1
#define LOWBAT     2
#define INIT_POS   3
#define PCU_THER   4
#define PCU_CURR   5
#define ANT_PINCH  6
#define SPD_THER   7
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/

typedef enum
{
    ALL_DOOR_LOCK   =  0,
    ANY_DOOR_LOCK   =  1,
    ALL_DOOR_UNLOCK =  2
}enumDoor;

typedef struct
{
    U16 bBatVoltDTC_Disable  :  1;    
    U16 bBatVoltCAN_Disable  :  1;
    U16 bBatVoltPTG_Disable  :  1;
    U16 bSpeedOver3Km        :  1;
    U16 bGearPositionEn      :  1;
    enumDoor DoorLockSt;    
            
}stVehiclestatus;


EXTERN stVehiclestatus st_g_Vehiclest;
EXTERN U08 u08_g_User_Position;
EXTERN U08 u08_g_LatchSt;
EXTERN U08 u08_g_clacBatteryVolt;
EXTERN S08 s08_g_Convert_Temp;
EXTERN S16 s16_g_Tailgate_Position;
EXTERN U08 u08_g_Latch_Error;
EXTERN U08 u08_g_Latch_On_Spindle_Stop;                         
EXTERN U08 u08_g_Power_OnReset;
EXTERN U08 u08_g_Result_call;  
EXTERN U08 u08_g_Latch_Unexpect; 
EXTERN U08 u08_g_Trunk_Position_Error;
EXTERN U08 u08_g_IGN_Count;    
EXTERN U08 u08_g_SpeedSetWarning_T;
EXTERN U08 u08_g_Strain_Cmd;       
EXTERN U08 u08_g_C_PTGOpenClsState;
EXTERN U08 u08_g_CmdWarn;
EXTERN U08 u08_g_MCU_Voltage_Error;      
EXTERN U08 u08_g_DetectVoltCmprErrSts;  
EXTERN U08 u08_g_HallPwrOffCheckCmd_LH;   
/*EXTERN U08 u08_g_HallPwrOffCheckCmd_RH;  */
EXTERN U08 u08_g_LatchFaultSt;
EXTERN U08 u08_g_BreakSetCmd;
/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN void g_GetUser_PositionRate(void);
EXTERN void g_StatusDetect_Init(void);
EXTERN void g_DTC_Clear_by_IGN(void);
EXTERN void g_Past_Failure_Record( U08 ErrorCode );
EXTERN void g_StatusDetect_Period10ms(void);
EXTERN void g_StatusDetect_Period5ms(void);
EXTERN void g_StatusDetet_Period100ms(void);
EXTERN void g_StatusDetet_Period500ms(void);
EXTERN void g_PTGOpnClsStUpdate_Period100ms(void);

#undef EXTERN

#endif
