/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*	Copyright       : MOTOTECH CO. LTD.                                         */
/*	Author          : Jae-Young Park                                            */
/*	Created date    : 2013-04-09 PM.  2:22:24                                   */
/*	Last Update     : 2013-04-09 PM.  2:22:24                                   */
/*	File Name       : code_H_template.h                                         */
/*	Processor       : MC9S12XXX                                                 */
/*	Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		TASK_SCHEDULING_H_EXTERN
#define		TASK_SCHEDULING_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	TASK_SCHEDULING_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
 /* #define    u08_XXXXXXXX_XXX    3000 */ /* 100us task count */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/

/* #ifdef TASK_SCHEDULING_EXTERN */

typedef	struct
{
	U08	nCount1ms;		                            /* 1ms timer counter for 1ms periodic event */
	U08	nCount5ms;                                  /* 1ms timer counter for 5ms periodic event */
	U08	nCount10ms;		                            /* 1ms timer counter for 10ms periodic event */
	U08	nCount20ms;		                            /* 1ms timer counter for 20ms periodic event */
} stTimer1ms;

typedef	struct
{
	U08	nCount100ms;		                        /* 100ms timer counter for 100ms periodic event */
	U08	nCount500ms;		                        /* 100ms timer counter for 200ms periodic event */
	U08	nCount1S;		                            /* 100ms timer counter for 1S periodic event */
	U08	nCount10S;		                            /* 100ms timer counter for 10S periodic event */
} stTimer100ms;

typedef	struct	/* time based task events */
{
	U16	bOn1ms		: 1;		                    /* 1ms timer event for 1ms periodic task event */
	U16	bOn5ms		: 1;
	U16	bOn10ms		: 1;		                    /* 10mS timer event */
	U16	bOn20ms		: 1;
	U16	bOn100ms	: 1;		                    /* cinch rleasing condition */
	U16	bOn500ms	: 1;		                    /* cinching condition */
	U16	bOn1S		: 1;		                    /* 100ms timer x 10 = 1S timer event */
} stTimeEvent;

/* #endif */

EXTERN  stTimer1ms		st_g_Timer1ms;
EXTERN	stTimer100ms	st_g_Timer100ms;
EXTERN	stTimeEvent		st_g_TimeEvent;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN	void g_TASK_Init( void );
EXTERN  void g_Task_Scheduling( void );
EXTERN  void g_Task_Timer_Count_1msEvent( void );

#undef EXTERN

#endif
