/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-07-02 PM.  5:14:53                                   */
/*  Last update     : 2014-06-24 PM.  7:05:45                                   */
/*  File Name       : CAN_Signal_Handling.h                                     */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		CAN_SIGNAL_HAND_H_EXTERN
#define		CAN_SIGNAL_HAND_H_EXTERN

#include "CAN_Drive_App.h"

#ifndef  	CAN_SIGNAL_HANDLING_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define    DOOR_LOCK     0 
#define    DOOR_ULOCK    1      

#define    RKE_TAILGATE   5
#define    PANIC_STOP     4

#define    SMK_TAILGATE   3
#define    SMK_PANIC_STOP 5

#define    TAILGATE_TRUN_ON  1
#define    TAILGATE_TRUN_OFF 2

/*#define    TAILGATE_TRUNK_TIMEOUT 100*/  /* RKE cyclic time 600ms  -> 10ms * 100 : 1000ms */
#define    TAILGATE_TRUNK_TIMEOUT 260 /* RKE cyclic time 1000ms -> 10ms * 150 : 1500ms */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
typedef	struct
{
    U16	C_RkeCmd			                  : 1;     
    U16 C_BAState                             : 1;     
    U16 C_IGNSw                               : 1;     
    U16 C_TgReleaseRly                        : 1;     
    U16 C_PTGMNValueSet                       : 1;     
    U16 C_InhibitP                            : 1;     
    U16 C_USMReset                            : 1;     
    U16 C_VehicleSpeed                        : 1;     
    U16 C_CrashUnlockState                    : 1;     
    U16 C_DrvUnlockState                      : 1;     
    U16 C_AstDrSw                             : 1;     
    U16 C_AstUnlockState                      : 1;     
    U16 C_DrvDrSw                             : 1;     
    U16 C_RLDrSw                              : 1;     
    U16 C_RLUnlockState                       : 1;     
    U16 C_RRDrSw                              : 1;     
    U16 C_RRUnlockState                       : 1;     
    U16 C_TgOtrReleaseSW                      : 1;     
    U16 C_TrunkTgSW                           : 1;     
    U16 C_TrunkOpenOption                     : 1;
    U16 C_TgReleaseRlyAct                     : 1;     
    U16 C_PassiveTrunkTg                      : 1;     
    U16 C_SMKRKECmd                           : 1;     
    U16 C_HazardfromSMK                       : 1;  
    U16 C_GselDisp                            : 1;
    U16 C_InhibitRMT                          : 1; 
    U16 C_PTGSpeedNValueSet                   : 1;
    U16 C_PTGHeightNValueSet                  : 1;
    U16 C_PTGCNTCmdAVN                        : 1;
    U16 C_ConfTCU                             : 1;
    U16 C_TailgateTrunkRKECMD                 : 1;	
    U16 C_eClutch                             : 1;
                
} stBCanInputEvent;

EXTERN stBCanInputEvent  CAN_Event;
EXTERN U08 u08_g_CANsignal_Change; 
EXTERN U08 u08_g_C_RkeCmd;	          
EXTERN U08 u08_g_C_BAState;           
EXTERN U08 u08_g_C_IGNSw;             
EXTERN U08 u08_g_C_TgReleaseRly;      
EXTERN U08 u08_g_C_PTGMNValueSet;     
EXTERN U08 u08_g_C_InhinitP;   
EXTERN U08 u08_g_C_GselDisp; 
EXTERN U08 u08_g_C_InhibitRMT;
EXTERN U08 u08_g_C_USMReset;        
EXTERN U08 u08_g_C_VehicleSpeed;      
EXTERN U08 u08_g_C_CrashUnlockState;  
EXTERN U08 u08_g_C_DrvUnlockState;    
EXTERN U08 u08_g_C_AstDrSw;           
EXTERN U08 u08_g_C_AstUnlockState;    
EXTERN U08 u08_g_C_DrvDrSw;           
EXTERN U08 u08_g_C_RLDrSw;            
EXTERN U08 u08_g_C_RLUnlockState;     
EXTERN U08 u08_g_C_RRDrSw;            
EXTERN U08 u08_g_C_RRUnlockState;     
EXTERN U08 u08_g_C_TgOtrReleaseSW;    
EXTERN U08 u08_g_C_TrunkOpenOption;
EXTERN U08 u08_g_C_TrunkTgSW;         
EXTERN U08 u08_g_C_TgReleaseRlyAct;   
EXTERN U08 u08_g_C_PassiveTrunkTg;    
EXTERN U08 u08_g_C_SMKRKECmd;         
EXTERN U08 u08_g_C_HazardfromSMK;     
EXTERN U08 u08_g_C_PTGSpeedNValueSet;
EXTERN U08 u08_g_C_PTGHeightNValueSet;
EXTERN U08 u08_g_C_PTGCNTCmdAVN;
EXTERN U08 u08_g_C_ConfTCU; 
EXTERN U08 u08_g_C_TailgateTrunkRKECMD;
EXTERN U08 u08_g_C_eClutch;
EXTERN U16 u16_g_ReleaswSW_On_t;
EXTERN U16 u16_g_RKE_TimeOut_t;
EXTERN U08 u08_g_ADM_Timeout_Flag;
EXTERN U08 u08_g_DDM_Timeout_Flag;
EXTERN U08 u08_g_C_IGN_Fail;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
EXTERN void g_CAN_Signal_Handling1_10msPeriod_TX( void );
EXTERN void g_CAN_Signal_Handling1_10msPeriod_1( void );   
EXTERN void g_CAN_Signal_Handling2_10msPeriod_2( void );
EXTERN void g_CAN_Signal_Handling3_10msPeriod_3( void );
EXTERN void g_Tx_CAN_Hazard(U08 St);

#undef EXTERN

#endif
