/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-03 AM.  11:17:06                                  */
/*  Last update     : 2013-06-03 AM.  11:18:34                                  */
/*  File Name       : PID_Control.c                                             */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                   	  MODULE DEFINITION                                     */
/********************************************************************************/
#ifndef		CONTROL_COMMAND_H_EXTERN
#define		CONTROL_COMMAND_H_EXTERN

#include "Basic_Type_Define.h"

#ifndef  	CONTROL_COMMAND_EXTERN
#define  	EXTERN     extern
#else
#define  	EXTERN   
#endif

/********************************************************************************/
/*	Macro                                                                       */
/********************************************************************************/
#define CINCHING_ANTI_OPEN_DELAY_T   50  /* 10ms * 50  = 500ms  */
/********************************************************************************/
/*	Global Variable                                                             */
/********************************************************************************/
EXTERN U08 	u08_g_Tailgate_Moving_Cmd;
EXTERN U08  u08_g_Tailgate_Stop_Cmd;
EXTERN U08  u08_g_StopToMove_T;
EXTERN U08  u08_g_Antipinch_Reverse_Mode;
EXTERN U08  u08_g_SMK_Buzzer_T;
EXTERN U08  u08_g_Crank_RKEnCPAD_CloseStop;
EXTERN U08  u08_g_Craking_Stop; 
EXTERN U08 u08_g_ReverseMovSet;
EXTERN U08 u08_g_Strain_Operation_Motor_On_T;

/********************************************************************************/
/*	extern function                                                             */
/********************************************************************************/
/*EXTERN	void g_TASK_TEMP( void );*/
EXTERN void g_Spindle_Moving_1msPeriod( void );
EXTERN void g_Spindle_MoveStart_10msPeriod( void );
EXTERN void g_Tailgate_Control_10msPeriod( void );
EXTERN void g_Control_Command_Init( void );
#undef EXTERN

#endif
