/*=============================================================================*/
/*                                                                             */
/* Project         : PTG(Power Tailgate ) for CD                               */
/* Copyright       : MOTOTECH CO. LTD.                                         */
/* Author          : HeeSub-Song                                               */
/* Created date    : 2018-01-11 PM.  09:15:07                                  */
/* Last update     : 2018-01-11 PM.  09:15:07                                  */
/* File Name       : DrvCore.c                                                 */
/* Processor       : MC9S12P64                                                 */
/* Version         :                                                           */
/* Compiler        : HI-WIRE                                                   */
/* IDE             : CodeWarrior IDE VERSION 5.1                               */
/*                                                                             */
/*=============================================================================*/


/*=============================================================================*/
/*                                INCLUDE FILES                                */
/*=============================================================================*/
#define DRV_CORE_EXTERN

#include "Basic_Type_Define.h"

#include "MCU_Fail_Check.h"
#include "Analog_Conversion.h"
#include "EEPROM_Control.h"
#include "Read_Hall_Sensor.h"
#include "CAN_Drive_App.h"
/*=============================================================================*/
/*                            macro define                                     */
/*=============================================================================*/

#define CORE_REG_COMP_PWM				  	0x0CU
#define CORE_REG_COMP_ADC01					0x0F2FU
#define CORE_REG_COMP_ADC23					0x4288U

#define CORE_ADC_SENSE_MAX					1024U
#define CORE_ADC_SENSE_MIN					0U			

/*=============================================================================*/
/*                                Local value                                  */
/*=============================================================================*/

static U08 u08_s_CoreFlashFaultCnt 		  = 0U;
static U08 u08_s_CoreHealthFaultCnt 		= 0U;
static U08 u08_s_CoreRegStrFaultCnt 		= 0U;
static U08 u08_s_CoreSequenceFaultCnt   = 0U;
static U08 u08_s_CoreRngFaultCnt        = 0U;

/*=============================================================================*/
/*                      function prototype                                     */
/*=============================================================================*/
static void s_MCU_Fail_Set(void);
static void s_ReadFlashDFault(void);
static void s_MCU_HealthSwapCheck(void);
static void s_DrvCoreHealthSwap(U08* x, U08* y);
static void s_DrvCoreAdcRngCheck(void);

/*---------------------- Global Funciton Definition ---------------------------*/


/********************************************************************************/
/*   name     : g_DrvCoreRegCheck                                               */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
void g_DrvCoreInit(void)
{
	setReg8Bits(FCNFG, 0x01);	/** - ECC Init */

	u08_s_CoreFlashFaultCnt 		= 0U;
	u08_s_CoreHealthFaultCnt 	= 0U;
	u08_s_CoreRegStrFaultCnt 	= 0U;
	u08_s_CoreRngFaultCnt 		= 0U;
	u08_g_CoreFaultSts = 0U;
}

/********************************************************************************/
/*   name     : g_DrvCoreRegCheck                                               */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
void g_DrvCoreRegCheck(void)
{
	if( (PWMCTL != CORE_REG_COMP_PWM) ||
		  (ATDCTL01 != CORE_REG_COMP_ADC01) ||
		  (ATDCTL23 != CORE_REG_COMP_ADC23)    )
	{
		u08_s_CoreRegStrFaultCnt++;
	}
}
/********************************************************************************/
/*   name     : g_MCU_FailCheckPeriod100ms                                       */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
void g_MCU_FailCheckPeriod100ms(void)
{
    s_MCU_HealthSwapCheck();
    s_DrvCoreAdcRngCheck();
}

/********************************************************************************/
/*   name     : g_MCU_FailCheckPeriod500ms                                       */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
void g_MCU_FailCheckPeriod500ms(void)
{
    s_ReadFlashDFault();
    s_MCU_Fail_Set();		
}

/********************************************************************************/  
/*   name     : g_DrvCoreSquenceCheck                                           */ 
/*   function :                                                                 */  
/*                                                                              */  
/********************************************************************************/  
void g_DrvCoreSquenceCheck(U08 Sequence_Num )
{
	  U08 u08_t_i =0;
    static U08 u08_s_SequenceCheckData[6] = {0xffU,0xffU,0xffU,0xffU,0xffU,0xffU};
    
    u08_s_SequenceCheckData[Sequence_Num] = Sequence_Num;
    
    if( Sequence_Num == CORE_SEQUENCE_6 )
    {
        if( (u08_s_SequenceCheckData[CORE_SEQUENCE_1] != CORE_SEQUENCE_1) ||
        	  (u08_s_SequenceCheckData[CORE_SEQUENCE_2] != CORE_SEQUENCE_2) ||
        	  (u08_s_SequenceCheckData[CORE_SEQUENCE_3] != CORE_SEQUENCE_3) ||
        	  (u08_s_SequenceCheckData[CORE_SEQUENCE_4] != CORE_SEQUENCE_4) ||
        	  (u08_s_SequenceCheckData[CORE_SEQUENCE_5] != CORE_SEQUENCE_5) ||
        	  (u08_s_SequenceCheckData[CORE_SEQUENCE_6] != CORE_SEQUENCE_6)    )
        {
            u08_s_CoreSequenceFaultCnt++;	
        }
        
        for( u08_t_i=0U; u08_t_i<6U; u08_t_i++)
        {
            u08_s_SequenceCheckData[u08_t_i] = 0xFFU;
        }
    }	
}


/********************************************************************************/
/*   name     : s_MCU_Fail_Set                                                  */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
static void s_MCU_Fail_Set(void)
{
	if( (u08_s_CoreFlashFaultCnt >= 10U)  	||
  	  (u08_s_CoreSequenceFaultCnt >= 3U)  ||
      (u08_s_CoreHealthFaultCnt >= 3U) 	  ||
      (u08_s_CoreRegStrFaultCnt >= 3U)		||
    	(u08_s_CoreRngFaultCnt >= 3U)       ||
    	(u08_g_CANFatalError > 3U)    )
	{
		u08_g_CoreFaultSts = 1U; 
	}
}

/********************************************************************************/
/*   name     : g_DrvCoreAdcRngCheck                                            */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
static void s_DrvCoreAdcRngCheck(void)
{
    if(st_g_ADCData.BattMon.Valid != 0U)										/* System Input Voltage Range Check*/
    {
        if( (st_g_ADCData.BattMon.Value > CORE_ADC_SENSE_MAX) ||
    		    (st_g_ADCData.BattMon.Value < CORE_ADC_SENSE_MIN)   )
        {
    	      u08_s_CoreRngFaultCnt++;
        }
    }

    if(st_g_ADCData.TempMon.Valid != 0U)										/* Thermister Input Voltage Range Check*/
    {
        if( (st_g_ADCData.TempMon.Value > CORE_ADC_SENSE_MAX) ||
    		    (st_g_ADCData.TempMon.Value < CORE_ADC_SENSE_MIN)    )
        {
    	      u08_s_CoreRngFaultCnt++;
        }
    }

    if(st_g_ADCData.PCUCurrent.Valid != 0U)									/* PCU Motor Current Range Check*/
    {
        if( (st_g_ADCData.PCUCurrent.Value > CORE_ADC_SENSE_MAX) ||
    		    (st_g_ADCData.PCUCurrent.Value < CORE_ADC_SENSE_MIN)    )
        {
    	      u08_s_CoreRngFaultCnt++;
        }
    }

    if(st_g_ADCData.HallPower_LH.Valid != 0U)									/* Hall Power Feedback Input Voltage Range Check*/
    {
        if( (st_g_ADCData.HallPower_LH.Value > CORE_ADC_SENSE_MAX) ||
    		    (st_g_ADCData.HallPower_LH.Value < CORE_ADC_SENSE_MIN)   )
        {
            u08_s_CoreRngFaultCnt++;
        }
    }

    if(st_g_ADCData.DriveVoltage.Valid != 0U )							/* Spindle Motor Input Voltage Range Check*/
    {
        if( (st_g_ADCData.DriveVoltage.Value > CORE_ADC_SENSE_MAX) ||
            (st_g_ADCData.DriveVoltage.Value < CORE_ADC_SENSE_MIN)   )
        {
            u08_s_CoreRngFaultCnt++;
        }
    }

    if(st_g_ADCData.SpindleCur_LH.Valid != 0U )								/* Spindle Motor Current Range Check*/
    {
        if( (st_g_ADCData.SpindleCur_LH.Value > CORE_ADC_SENSE_MAX) ||
            (st_g_ADCData.SpindleCur_LH.Value < CORE_ADC_SENSE_MIN)   )
        {
            u08_s_CoreRngFaultCnt++;
        }
    }
}


/********************************************************************************/
/*   name     : s_ReadFlashDFault                                               */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
static void s_ReadFlashDFault(void)
{
    U08 u08_t_IsDoubleFault = FALSE;

    if(FSTAT_MGBUSY==0U)
    {
        u08_t_IsDoubleFault = (getReg8(FERSTAT) & 0x02);

        if(u08_t_IsDoubleFault != 0U)
        {
            u08_s_CoreFlashFaultCnt++;
            setReg8Bits(FERSTAT, 0x00);
        }
    }
}

/********************************************************************************/
/*   name     : s_DrvCoreHealthSwap                                             */
/*   function : DATA SWAP                                                       */
/*                                                                              */
/********************************************************************************/
static void s_DrvCoreHealthSwap(U08* x, U08* y)
{
    U08 u08_t_uTmpBuf;

    u08_t_uTmpBuf = *x;
    *x = *y;
    *y = u08_t_uTmpBuf;
}

/********************************************************************************/
/*   name     : s_MCU_HealthSwapCheck                                           */
/*   function : RAM Check                                                       */
/*                                                                              */
/********************************************************************************/
static void s_MCU_HealthSwapCheck(void)
{
    static U08 u08_s_SwapConfirm = 0;
    static U08 u08_s_SwapDataA = 0;
    static U08 u08_s_SwapDataB = 0;
    
    if( u08_s_SwapConfirm == TRUE )
    {
        u08_s_SwapConfirm = FALSE;
        if( (u08_s_SwapDataA != 0x55) || (u08_s_SwapDataB != 0xAA) )
        {
            u08_s_CoreHealthFaultCnt++;	
        }	
    }
    else
    {
    	  u08_s_SwapDataA = 0xAA;
    	  u08_s_SwapDataB = 0x55;
        s_DrvCoreHealthSwap(&u08_s_SwapDataA , &u08_s_SwapDataB ); 
        u08_s_SwapConfirm = TRUE;
    }		
}
