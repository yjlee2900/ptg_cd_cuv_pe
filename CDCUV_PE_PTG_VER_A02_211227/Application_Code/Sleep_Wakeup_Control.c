/********************************************************************************/   
/*                                                                              */   
/*  Project         : PTG(Power Tailgate ) for CK                               */   
/*  Copyright       : MOTOTECH CO. LTD.                                         */   
/*  Author          : Jae-Young Park                                            */   
/*  Created date    : 2013-06-17 PM.  8:16:38                                   */   
/*  Last update     : 2013-06-17 PM.  8:16:44                                   */   
/*  File Name       : Sleep_Wakeup_Control.c                                    */   
/*  Processor       : MC9S12P64                                                 */   
/*  Version         :                                                           */   
/*  Compiler        : HI-WIRE                                                   */   
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */   
/*                                                                              */   
/********************************************************************************/   

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define SLEEP_WAKEUP_EXTERN
#include "Basic_Type_Define.h"
#include "Task_Scheduling.h"
#include "CAN_Drive_App.h"      
#include "CAN_Signal_Handling.h"
#include "Sleep_Wakeup_Control.h"
#include "Watchdog_Out.h"
#include "Internal_WatchDog.h"
#include "Sensor_Power_Control.h"
#include "EEPROM_Control.h"
#include "CAN_Drive_App.h"
#include "VNQ500_CE_Out.h"
#include "Timer100us.h"
#include "Timer1ms.h"
#include "User_Logic_Control.h"
#include "Status_Detect.h"
#include "Read_Hall_Sensor.h"
#include "Illumination_Control.h"
#include "Illumination_Ctl_Out.h"
#include "PullUp_Control.h"
#include "Digital_Input_Chattering.h"
#include "RTI1.h"
#include "AD1.h"
#include "Spindle_Motor_Drive.h"     
#include "MCU_Fail_Check.h"
#include "Hazard_Buzzer_Control.h"
/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/
/* static U08 u08_s_local_value; */

/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_Check_EEPROM_Diag_Write( void );
static U08 u08s_Input_Change_Check( U08 Check_Cmd );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/

#define PULL_UP_ON_CMD    0U /* pnp Control : OFF(low) -> pull up on  */ 
#define PULL_UP_OFF_CMD   1U /* pnp Control : ON(high) -> pull up off */ 

/* 10ms * 123000 = 20.5min */ /* SJB CAN Sleep wait 0.5min 3000 */
/* Change Requirement Stand By Mode : CAN Sleep & Trunk Optn -> Stand By Mode within 5sec */
#define u32_STANDBY_MODE_TIME    500U /* 10ms * 500 = 5sec */

/********************************************************************************/
/*   name     : s_Check_EEPROM_Diag_Write                                       */
/*   function : XXXXXXXXXXXXXXXXXXXXXXX                                         */
/********************************************************************************/
static void s_Check_EEPROM_Diag_Write( void )
{
    U08 err_st = 0;
    
    /* sleep  eeprom writting check */
    if( (PtlTrans.CanLineErrorB != PtlTrans.CanLineErrorB_Old) ||
        (PtlTrans.CanBusOffB != PtlTrans.CanBusOffB_Old)       ||
        ( u08_g_IGN_Count != u16_g_EEP_Diag.EEPIGNCount)       ||
        ( u08_g_PTGM_Power_Mode != u16_g_EEP_Diag.PGTM_Enable) ||
        /*( PtlTrans.Pre_Inline_Mode != u16_g_EEP_Diag.Pre_InLine_Mode ) || */
        ( u08_g_Switch_Short_Event == TRUE) ||
        ( u08_g_IO_InputFault != u16_g_EEP_Diag.IO_InputFault ) ||
        ( u08_g_EEP_Write_Event == TRUE)                              )
    {
        /* init value 0xFF , writting value invert */
        if(PtlTrans.CanLineErrorB == TRUE)
        {
               u16_g_EEP_Diag.EEPCanLineErrorB = FALSE; 
        }
        else 
        {
            u16_g_EEP_Diag.EEPCanLineErrorB = TRUE;
        }
            
        if(PtlTrans.CanBusOffB == TRUE)
        {
               u16_g_EEP_Diag.EEPCanBusOffB = FALSE; 
        }
        else 
        {
            u16_g_EEP_Diag.EEPCanBusOffB = TRUE;
        }
        
        u08_g_EEP_Write_Event = FALSE;
        u08_g_Switch_Short_Event = FALSE;
        /*u16_g_EEP_Diag.Pre_InLine_Mode = PtlTrans.Pre_Inline_Mode;*/
        u16_g_EEP_Diag.PGTM_Enable = u08_g_PTGM_Power_Mode;
        u16_g_EEP_Diag.EEPIGNCount = u08_g_IGN_Count;
        u08_g_IO_InputFault = (U08)u16_g_EEP_Diag.IO_InputFault;
        err_st = u08_g_Write_Diag_Sector();
    }
    
    u08_g_Shutdown_Delay = 20; /* 1000ms delay */
}


/********************************************************************************/
/*   name     : u08s_Input_Change_Check                                         */
/*   function : Stand by mode I0/CAN Input change cheak                         */
/*             ( OFF : check Buffer set , ON : Value change check )             */
/********************************************************************************/
static U08 u08s_Input_Change_Check( U08 Check_Cmd )
{
    static enumSw u08_s_Pre_Latch_Secondary_Sw = enOFF;
    static enumSw u08_s_Pre_Latch_Lock_Sw      = enOFF;
    static enumSw u08_s_Pre_Inner_Sw           = enOFF;
    static enumSw u08_s_Pre_console_Sw         = enOFF;
    static U08    u08_s_Pre_LH_Hall_Sensor     = 0;
    static U08    u08_s_LH_Hall_Change_Check   = 0;
    /*static U08    u08_s_Pre_RH_Hall_Sensor     = 0;*/
    /*static U08    u08_s_RH_Hall_Change_Check   = 0;*/
    
    U08 u08_t_Result = OFF;
    U08 u08_t_Now_LH_Hall_Value;
    /*U08 u08_t_Now_RH_Hall_Value;*/
    
    u08_t_Now_LH_Hall_Value = g_Get_LH_Hall();
    /*u08_t_Now_RH_Hall_Value = g_Get_RH_Hall();*/
    
    if( Check_Cmd == ON )
    {
        u08_t_Result += (u08_s_Pre_Latch_Secondary_Sw   != st_g_swinput.inLatchSecondary.swRead) ? ON : OFF;
        u08_t_Result += (u08_s_Pre_Latch_Lock_Sw        != st_g_swinput.inLatchLock.swRead)      ? ON : OFF;
        u08_t_Result += (u08_s_Pre_Inner_Sw             != st_g_swinput.swInnerOpenClose.swRead) ? ON : OFF;
        u08_t_Result += (u08_s_Pre_console_Sw           != st_g_swinput.swCPadTailgate.swRead)   ? ON : OFF;
        
        u08_s_LH_Hall_Change_Check |= (u08_s_Pre_LH_Hall_Sensor ^ u08_t_Now_LH_Hall_Value);	
        u08_t_Result += (	u08_s_LH_Hall_Change_Check == 3 )                                      ? ON : OFF;
      /*u08_s_RH_Hall_Change_Check |= (u08_s_Pre_RH_Hall_Sensor ^ u08_t_Now_RH_Hall_Value);	
        u08_t_Result += (	u08_s_RH_Hall_Change_Check == 3 )                                      ? ON : OFF; */
#if 0        	
        /*u08_t_Result += (u08_s_Pre_LH_Hall_Sensor       != u08_t_Now_LH_Hall_Value)              ? ON : OFF;*/
        /*u08_t_Result += (u08_s_Pre_RH_Hall_Sensor       != u08_t_Now_RH_Hall_Value)              ? ON : OFF;*/
#endif        
        u08_t_Result += (NmStateBusSleep(NmGetStatus()) == 0 )                                   ? ON : OFF; 
    }
    else
    {
        u08_s_LH_Hall_Change_Check = 0;
        /*u08_s_RH_Hall_Change_Check = 0;*/
    }	
    
    u08_s_Pre_Latch_Secondary_Sw = st_g_swinput.inLatchSecondary.swRead;
    u08_s_Pre_Latch_Lock_Sw      = st_g_swinput.inLatchLock.swRead;
    u08_s_Pre_Inner_Sw           = st_g_swinput.swInnerOpenClose.swRead;     
    u08_s_Pre_console_Sw         = st_g_swinput.swCPadTailgate.swRead;
    u08_s_Pre_LH_Hall_Sensor     = u08_t_Now_LH_Hall_Value;
    /*u08_s_Pre_RH_Hall_Sensor     = u08_t_Now_RH_Hall_Value;*/
   
    return( u08_t_Result );
}

/********************************************************************************/
/*   name     : g_Sleep_Control_10msPeriod                                      */
/*   function :                                                                 */
/*                                                                              */
/********************************************************************************/
void g_SleepWake_Control_10msPeriod( void )
{
    static U08 u08_s_ECU_Pre_Power_Mode = ECU_NORAM_MODE;
    static U32 u32_t_Sleep_Timer = 0;  

    U08 Control_Result = 0;
    
    switch( u08_g_ECU_Power_Mode )
    {
        case ECU_NORAM_MODE : 
            if( (u08_g_C_IGNSw == KEY_OFF) && (u08_g_User_Input_On != ON) && (NmStateBusSleep(NmGetStatus()) != 0 ) &&
            	  ((u08_g_LatchSt != LATCH_LOCK) || (u08_g_Latch_Unexpect == ON)) && (st_g_BuzzHaza.TimeTick == 0) )
            {
                if( u32_t_Sleep_Timer < u32_STANDBY_MODE_TIME )
                {
                    u32_t_Sleep_Timer++;
                    if( u32_t_Sleep_Timer >= u32_STANDBY_MODE_TIME )
                    {
                        u08_g_ECU_Power_Mode = ECU_STANDBY_MODE;
                        Control_Result = u08s_Input_Change_Check(OFF); /* OFF : check Buffer set */
                    }
                }
            }
            else
            {
                u32_t_Sleep_Timer = 0;
                u08_g_User_Input_On = OFF;
            }        
            
            if( u08_s_ECU_Pre_Power_Mode != ECU_NORAM_MODE )
            {
                g_Sleep_Wakeup_Drive( WAKE_UP_MODE );
                Control_Result = AD1_Enable(); 
                if( Control_Result != ERR_OK )
                {
                   Control_Result = AD1_Enable(); 
                }
                u08_g_Illumi_St = OFF; /* OFF : check g_Illumination_Control_10msPeriod Latch Open/Close  */
            }    
            
            u08_s_ECU_Pre_Power_Mode = ECU_NORAM_MODE;
            break;
            
        case ECU_STANDBY_MODE :
            
            Control_Result = u08s_Input_Change_Check(ON); /* ON : Value change check */
            
            if( Control_Result != 0 )
            {
                u08_g_ECU_Power_Mode = ECU_NORAM_MODE;
                u32_t_Sleep_Timer = 0; 
                
                Control_Result = Timer100us_Enable(); // In the ECU standby mode, Hall count error. (feat. MQ4)
            }
            else
            {            
                if( u08_s_ECU_Pre_Power_Mode == ECU_NORAM_MODE )
                {     
                    Illumination_Ctl_Out_PutVal( OFF ); 
                    u08_g_Illumi_St = SLEEP_OFF;
                    Control_Result = AD1_Disable();
                    g_Spind_MTDrive_Out(INIT_TAILGATE);
                }    
                    
                __DI(); 
                /*Control_Result = Timer1ms_Disable();*/
                Control_Result = Timer100us_Disable();
                CPMUAPICTL_APIE = 0;   
                RTI1_Init();
                __EI();    
                             
                g_Sleep_Wakeup_Drive(SLEEP_MODE);             
                                
                Cpu_SetStopMode();     
                
                /******** CPU_Sleep **********/
                /******** CPU_Wake_up ********/

                g_Sleep_Wakeup_Drive(WAKE_SERCH_MODE);
                
                __DI();
                CPMUAPICTL_APIE = 1;
                /*Control_Result = Timer1ms_Enable();*/
                /*Control_Result = Timer100us_Enable();*/
                __EI();
                
                st_g_TimeEvent.bOn1ms  = 1;
                st_g_TimeEvent.bOn5ms  = 1;
                st_g_TimeEvent.bOn10ms = 1;   
            }
            
            u08_s_ECU_Pre_Power_Mode = ECU_STANDBY_MODE;
            break;

        default :
            break;
    }    
    
    
    
}


/********************************************************************************/
/*   name     : g_ECU_Sleep                                                     */
/*   function :                                                                 */
/*              Sleep Drive set                                                 */
/********************************************************************************/
void g_ECU_Sleep( void )
{
    /* sleep */
    s_Check_EEPROM_Diag_Write();
    g_Sleep_Wakeup_Drive(OFF);
    /*g_AntiSensor_Power_Out(OFF);*/
    g_Hall_Power_Out_LH(NO_RETRY_OFF);
    /*g_Hall_Power_Out_RH(NO_RETRY_OFF);*/
}

/********************************************************************************/
/*   name     : g_Sleep_Wakeup_Drive                                            */
/*   function :                                                                 */
/*              Drvie set                                                       */
/********************************************************************************/
void g_Sleep_Wakeup_Drive( U08 t_Mode )
{
    switch( t_Mode )
    {
        case SLEEP_MODE :
             g_Hall_Power_Out_LH(OFF);
             /*g_Hall_Power_Out_RH(OFF);*/
             /*g_AntiSensor_Power_Out(OFF);   */
             PullUp_Control_PutVal(PULL_UP_OFF_CMD); 
             /*Driver_Enable_Pullup_Control_PutVal(PULL_UP_OFF_CMD); */
             VNQ500_CE_Out_PutVal(OFF);
            break;
        case WAKE_UP_MODE :
        	   //u08_g_ECU_Power_Mode = ECU_NORAM_MODE; /* Init Power Mode */ Hall Count Error!! When ECU mode change Standby-Mode to WakeUp mode by CAN Bus Online.
             VNQ500_CE_Out_PutVal(ON);
             g_Hall_Power_Out_LH(ON);
             /*g_Hall_Power_Out_RH(ON);*/
             /*g_AntiSensor_Power_Out(ON);   */
             PullUp_Control_PutVal(PULL_UP_ON_CMD);
             /*Driver_Enable_Pullup_Control_PutVal(PULL_UP_ON_CMD); */
             /* retry */
             VNQ500_CE_Out_PutVal(ON);                           
             g_Hall_Power_Out_LH(ON);                            
             /*g_Hall_Power_Out_RH(ON);                            */
             /*g_AntiSensor_Power_Out(ON);                         */
             PullUp_Control_PutVal(PULL_UP_ON_CMD);              
             /*Driver_Enable_Pullup_Control_PutVal(PULL_UP_ON_CMD);*/
            break;
        case WAKE_SERCH_MODE :
            VNQ500_CE_Out_PutVal(ON);
            g_Hall_Power_Out_LH(ON);
            /*g_Hall_Power_Out_RH(ON);*/
            PullUp_Control_PutVal(PULL_UP_ON_CMD);
            break;            
        default :
            break;        
    }
}



/********************************************************************************/
/*   name     : g_TriggerWDT                                                    */
/*   function :                                                                 */
/*   xxxus time event task                                                      */
/********************************************************************************/
void g_TriggerWDT(void)
{
    static U08 u08_s_watch_out_st = 0;
    U08 u08_t_Err_check = 0;
    
    if( u08_g_CoreFaultSts == 0 )
    {	
        u08_s_watch_out_st ^= 1;
        Watchdog_Out_PutVal( u08_s_watch_out_st);
        u08_t_Err_check = Internal_WatchDog_Clear(); /* Clear Internal watchdog */
        if( u08_t_Err_check != ERR_OK )
        {
        	  u08_t_Err_check = Internal_WatchDog_Clear();
        }
    }	
}


