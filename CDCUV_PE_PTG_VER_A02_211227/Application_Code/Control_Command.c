/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-03 AM.  11:17:06                                  */
/*  Last update     : 2013-12-27 PM.  7:38:27                                   */
/*  File Name       : PID_Control.c                                             */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

/********************************************************************************/
/*                                INCLUDE FILES                                 */
/********************************************************************************/

#define CONTROL_COMMAND_EXTERN
#include "AP_DataType.h"
#include "Basic_Type_Define.h"
#include "Control_Command.h"
#include "Status_Detect.h"
#include "User_Logic_Control.h"
#include "Spindle_Motor_Drive.h"
#include "Sensor_Power_Control.h"
#include "PID_Control.h"
#include "Obstacle_Detect.h"
#include "Spindle_Sync_Control.h"
#include "Hazard_Buzzer_Control.h"
#include "Parameterization.h"
#include "Read_Hall_Sensor.h"
#include "EEPROM_Control.h"
#include "CAN_Signal_Handling.h"
#include "PCU_Sequence.h"
#include "Digital_Input_Chattering.h"

/********************************************************************************/
/*                                Local value                                   */
/********************************************************************************/

static U16 u16_s_PID_Start_Delay_Time = 0;
static U08 u08_s_Pre_Calc_St = ON;
static U08 u08_s_SoftStop_cmd = OFF;
static U16 u16_s_MAX_Operating_Time = 0;
static U08 u08_s_OverSpeed_BuzzerSt = 0;
static U08 u08_s_OverSpeed_Buzzer_Delay = 0;
static U08 u08_s_zone3_UserSoftStop_Set = 0;
/********************************************************************************/
/*                      function prototype                                      */
/********************************************************************************/
static void s_MotorBreakControl( void );
static U08 s_Ratio_Limit_Check( S16 Calc_Buff );
static void s_Spindle_Stop_Cmd( void );
static void s_Over_Speed_Check( void );
static void s_Key_Crank_Check(U08 u08_t_Move_St );
static U08 u08_s_Tailgate_Soft_Stop( void );
static void s_Antipinch_Reverse_Control( void );
static void s_TG_StopToMoving_Control( void );      
static void s_SMK_Hazard_Buzzer( void );
static void s_Strain_Resist_Control( void );
static void s_Continuous_Power_Close_Off_Control( U08 check_st );
/********************************************************************************/
/*                            macro define                                      */
/********************************************************************************/
#define STOP_DUTY             0
#define MAX_DUTY              255
#define PID_START_DELAY       500   /* 1ms * 500 : 500ms */
#define ANTI_REVERSE_CONTROL_DELAY  20   /* 10ms * 20 : 200ms */
/*#define STOP_TO_OPEN_DELAY    20*/   /* 10ms * 20 : 200ms */
#define SOFTSTOP_DEC_VALUE    1   
/*#define SYNC_STOP_BUZZER_END  2*/
#define MAX_MANUAL_RPM_LIMIT  13000 /* 12000 RPM */
#define MIN_MANUAL_RPM_LIMIT  10000 /* 10000 RPM */
#define POWER_RPM_LIMIT       5500  /* 5500 RPM */
#define SPINDLE_BREAK_TIME    10
#define PWM_MIN_RATIO         30
#define MAX_OPERATION_TIME    1000 /* 10ms * 1000 = 10S */
#define ZONE3_STOP_GAP        30
/*#define LOW_VOLT_STOP_RECOVER_LIMIT   50  */ /* 10ms * 50 = 500ms */
/********************************************************************************/
/*   name     : s_MotorBreakControl ,                                            */
/*   function : out pwm ratio min/max limit                                     */
/********************************************************************************/
static void s_MotorBreakControl( void )
{
    static U08 u08_s_Break_T = 0;
    
    if( u08_s_Break_T > 0 ) /* RPM OVER Break Mode check */
    {
        u08_s_Break_T--;
    }    
    
    if( (u16_g_LH_MotorRPM > MAX_MANUAL_RPM_LIMIT) || (u08_g_BreakSetCmd == 1) )
    {
    	  if( u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE ) 
    	  {	 
            u08_g_Tailgate_Moving_Cmd = BREAK_TAILGATE;
            u08_s_Break_T = SPINDLE_BREAK_TIME;
        }
    }
    else
    {
        if( (u08_g_Tailgate_Moving_Cmd == BREAK_TAILGATE) && ( u08_s_Break_T == 0 ) &&
            (u16_g_LH_MotorRPM < MIN_MANUAL_RPM_LIMIT) && (u08_g_BreakSetCmd == 0)  )
        {                       
            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
        }    
    }    
}

/********************************************************************************/
/*   name     : s_Ratio_Limit_Check ,                                           */
/*   function : out pwm ratio min/max limit                                     */
/********************************************************************************/
static U08 s_Ratio_Limit_Check( S16 Calc_Buff )
{
    U08 Ret_Value;
    
    if( Calc_Buff >= 255 )
    {
        Ret_Value = 255;
    }    
    else if( Calc_Buff <= PWM_MIN_RATIO )
    {
        Ret_Value = PWM_MIN_RATIO;
    }    
    else    
    {
        Ret_Value = (U08)Calc_Buff;
    }    
    
    return( Ret_Value );    
}

/********************************************************************************/
/*   name     : s_Spindle_Stop_Cmd                                   */
/*   function : Spindle Motor Run to Stop condition check & stop control        */
/*              10ms scheduling task                                            */
/********************************************************************************/
static void s_Spindle_Stop_Cmd(void)
{
    static U08 u08_s_preBusOffSt = 1;  /* PtlTrans.CanBusOff */
    static U08 u08_s_preInputFault = 1;
    U08 u08_t_Reverse_Check = OFF;
    
    if( (PtlTrans.CanBusOff == TRUE) && (u08_s_preBusOffSt != TRUE) )
    {
    	 u08_g_Tailgate_Stop_Cmd = ON;
    }	     
    u08_s_preBusOffSt = PtlTrans.CanBusOff;
    
    if( (u08_g_IO_InputFault == TRUE) && (u08_s_preInputFault != TRUE) )
    {
        u08_g_Tailgate_Stop_Cmd = ON;	
    }	
    u08_s_preInputFault = u08_g_IO_InputFault;  
    
    if( u08_g_HallPowerOnErr_LH == 1 )
    {
    	  u08_g_Tailgate_Stop_Cmd = ON;
    }	
    
    if( u08_g_LH_HallPatternErrSt == 1 )
    {
        u08_g_Tailgate_Stop_Cmd = ON;	
    }	
    
    if( u08_g_LatchFaultSt == 1 )
    {
    	 u08_g_Tailgate_Stop_Cmd = ON;
    }	
    
    if( u08_g_SpindleOverUnderCurrentSt == 1 )
    {
    	 u08_g_SpindleOverUnderCurrentSt = 0;
    	 u08_g_Tailgate_Stop_Cmd = ON; 
    }	
    
    if( u08_g_Spindle_Antipinch_St == ON )
    {
        u08_g_Spindle_Antipinch_St = OFF;
        
        if( u08_s_SoftStop_cmd == OFF )
        {    
            u08_g_Tailgate_Stop_Cmd = ON;    
            u08_t_Reverse_Check = ON;
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 6;

            if( u08_g_Init_Param == ON )
            {    
                g_Past_Failure_Record(ANT_PINCH);
            }
        }
    } 
    
    if( u08_g_C_CrashUnlockState == 1 )
    {
        u08_g_Tailgate_Stop_Cmd = ON;            
        u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
        u08_g_Buzzer_Hazard_Count = 6;
    }    
    
    if( (u08_g_Hall_Sensor_Error == ON) && (u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) )
    {
        u08_g_StopToMove_T = 0;
        u08_g_Tailgate_Stop_Cmd = ON;

        if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (u16_g_EEP_Diag.InLine_Mode != TRUE) )
        {    
            if( u08_g_PowerMoveInit1_NoHallData_LH == ON )  
            {
                if( u16_g_EEP_Diag.HallError_LH != TRUE )
                {    
                    u16_g_EEP_Diag.HallError_LH = TRUE;
                    u08_g_EEP_Write_Event = TRUE; 
                }
                PtlTrans.HallError_LH = TRUE;
            }
        }
    }    
    
    if( u08_g_LatchSt != LATCH_OPEN ) /* Latch Error detect spindle stop */
    {
        u08_g_Tailgate_Stop_Cmd = ON;
    }    

    if( (u16_g_LH_MotorRPM > POWER_RPM_LIMIT) && (st_g_Vehiclest.bSpeedOver3Km != ON) && ( s16_g_LH_Position_Cnt > 10) )
    {
        u08_g_Tailgate_Stop_Cmd = ON;
        
        if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (u16_g_EEP_Diag.InLine_Mode != TRUE) )
        {    
            if( u08_g_PowerMoveInit1_NoHallData_LH == ON )  
            {
                if( u16_g_EEP_Diag.HallError_LH != TRUE )
                {    
                    u16_g_EEP_Diag.HallError_LH = TRUE;
                    u08_g_EEP_Write_Event = TRUE; 
                }
                PtlTrans.HallError_LH = TRUE;
            }
        }        
    }    
    
    if( u16_s_MAX_Operating_Time > MAX_OPERATION_TIME )
    {
        u08_g_Tailgate_Stop_Cmd = ON;
    }
    else
    {
        u16_s_MAX_Operating_Time++;
    }        
    
    if( (u08_g_User_Position < TAILGATE_ZONE_4) && (u08_g_Hall_Sensor_Error == OFF) && 
        (u08_t_Reverse_Check == ON ) && ( u08_g_Init_Param == ON ) && ( u08_g_StopToMove_T == 0) )
    {
        if( u08_g_Antipinch_Reverse_Mode == 0 ) /* first Anti-pinch -> Spindle Reverse */
        {    
            if( u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE ) /* all zone reverse 140507 */
            {    
                u08_g_StopToMove_T = ANTI_REVERSE_CONTROL_DELAY;
                u08_g_Antipinch_Reverse_Mode = CLOSE_TAILGATE;
            }
            if( u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE )
            {
                u08_g_StopToMove_T = ANTI_REVERSE_CONTROL_DELAY;
                u08_g_Antipinch_Reverse_Mode = OPEN_TAILGATE;
            }
        }
        else  /* Anti-pinch Spindle Reverse status -> Second Anti-pinch -> Spindle stop & Second Anti-pinch flag clear */ 
        {
            u08_g_Antipinch_Reverse_Mode = 0;
            u08_g_ReverseMovSet = 0;
        }
    }        
}

/********************************************************************************/
/*   name     : s_Over_Speed_Check                                              */
/*   function : 3km/h over Status Buzzer warning                                */
/*                                                                              */
/********************************************************************************/
static void s_Over_Speed_Check(void)
{                              
    if( (u08_g_LatchSt == LATCH_OPEN) && (u08_s_OverSpeed_Buzzer_Delay == 0) )
    {    
        if( (st_g_Vehiclest.bSpeedOver3Km == TRUE) && (u08_s_OverSpeed_BuzzerSt == FALSE) )
        {
            /*g_SetBuzzHazardControl(BUZZONLY_ON, 20);    */
            u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
            u08_g_Buzzer_Hazard_Count = 20; 
            u08_s_OverSpeed_BuzzerSt = TRUE;
        }
    }
    
    if( u08_s_OverSpeed_BuzzerSt == TRUE )       
    {
        if( (st_g_Vehiclest.bSpeedOver3Km == FALSE) || (u08_g_LatchSt != LATCH_OPEN) )
        {
            u08_s_OverSpeed_BuzzerSt = FALSE;  
            if( st_g_BuzzHaza.CountTick > 4 )
            {    
                u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF; 
            }
        }
        else
        {
            if( (u08_g_BuzzFeedBackSt == 1) && ( st_g_BuzzHaza.Mode != BUZZHAZA_ON) )
            {
                u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;
                u08_g_Buzzer_Hazard_Count = 20; 	
            }	
        }	         
    }
    
    if( u08_s_OverSpeed_Buzzer_Delay != 0 )
    {
        u08_s_OverSpeed_Buzzer_Delay--;
    }    
}
                         
/********************************************************************************/ 
/*   name     : u08_s_Tailgate_Soft_Stop                                        */ 
/*   function : Tailgate Soft stop                                              */ 
/*                                                                              */ 
/********************************************************************************/ 
static U08 u08_s_Tailgate_Soft_Stop(void)
{
    U08 ret_value = 0;
    static U08 u08_t_Delay_count = 0;
    
    u08_t_Delay_count++;
    
    if( (u08_t_Delay_count >= 8) || 
    	  ((u08_s_zone3_UserSoftStop_Set==1) && (u08_t_Delay_count >= 7)) ) /* Tunning Point */
    {
        if( u08_g_LH_Ratio > (SOFTSTOP_DEC_VALUE + PWM_MIN_RATIO ) )
        {
        	  if( u08_s_zone3_UserSoftStop_Set == 1 ) /* zone 3 softstop */
        	  {
        	  	  ret_value = ( u08_g_LH_Ratio - 2 ); /* zone3 softstop decriment value */
        	  }
        	  else /* zone 4 softstop */
        	  {		
        	  	 if( s16_g_Tailgate_Position < (u16_g_EEP_Para.MaxBasicPos - 300) ) 
        	  	 {
        	  	     if( u08_g_LH_Ratio > 60 ) 
        	  	     {	
        	  	         ret_value = ( u08_g_LH_Ratio/2 );	
        	  	 	   }
        	  	 	   else
        	  	 	   {
        	  	 	   	   ret_value = ( u08_g_LH_Ratio - SOFTSTOP_DEC_VALUE );
        	  	 	   }
        	  	 }	
               else
               {
               	  ret_value = ( u08_g_LH_Ratio - SOFTSTOP_DEC_VALUE );
               }
            }
        }
        else
        {        
            u08_s_SoftStop_cmd = OFF;
            u08_s_zone3_UserSoftStop_Set = 0;
            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE; 
            u08_g_User_Tailgate_Cmd   = STOP_TAILGATE; 
        }
        
        u08_t_Delay_count = 0;
    }
    else
    {
        ret_value = u08_g_LH_Ratio;
    }    
    
    return( ret_value );
}
                         
/********************************************************************************/
/*   name     : s_TG_StopToMoving_Control                                       */
/*   function : Tailgate Stop to Moving status & Control Command check          */
/*                                                                              */
/********************************************************************************/                         
static void s_TG_StopToMoving_Control( void )
{
	if( u08_g_Moving_Delay_T != 0 )
	{
	    u08_g_Moving_Delay_T--;	
	}	
	
    if( (u08_g_LatchSt == LATCH_OPEN) && (u08_g_Moving_Delay_T == 0) )
    {    
        u08_g_Tailgate_Moving_Cmd = u08_g_User_Tailgate_Cmd;
        u08_g_Wait_Unlatch_T = 0;
        PtlTrans.UnLatch_Error = FALSE;
    }
    else
    {
        if( u08_g_Wait_Unlatch_T > 0 )
        {
            u08_g_Wait_Unlatch_T--;
            if( u08_g_Wait_Unlatch_T == 0 )
            {
                u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
                if( u08_g_LatchSt != LATCH_OPEN )
                {
                    u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                    u08_g_Buzzer_Hazard_Count = 6;
                    if( (st_g_Vehiclest.bBatVoltDTC_Disable == 0) && (u16_g_EEP_Diag.InLine_Mode != TRUE) )
                    {    
                        if( u16_g_EEP_Diag.UnLatch_Error != TRUE )
                        {    
                            u16_g_EEP_Diag.UnLatch_Error = TRUE;
                            u08_g_EEP_Write_Event = TRUE; 
                        }
                        PtlTrans.UnLatch_Error = TRUE;
                    }
                } 
            }	
        }
    } 
}                         
       
       
/********************************************************************************/
/*   name     : s_Antipinch_Reverse_Control                                     */
/*   function : anti-pinch full open control                                    */
/*                                                                              */
/********************************************************************************/
static void s_Antipinch_Reverse_Control( void )
{
    if( u08_g_StopToMove_T > 0 )
    {
        u08_g_StopToMove_T--;
        if( (u08_g_StopToMove_T == 0) && (u08_g_User_Position < TAILGATE_ZONE_4) && (u08_g_LatchSt == LATCH_OPEN) )
        {
            u08_g_User_Tailgate_Cmd = u08_g_Antipinch_Reverse_Mode;
            u08_g_ReverseMovSet = 1;
            u08_g_Moving_Delay_T = 0; /* no wait delay */
        }
    }
}

/********************************************************************************/
/*   name     : s_Key_Crank_Check                                               */
/*   function : crank stop check                                                */
/*                                                                              */
/********************************************************************************/
static void s_Key_Crank_Check(U08 u08_t_Move_St )
{                   
       
    static U08 u08_S_pre_Crank_Moving_St = 0;
    
    if( u08_t_Move_St == ON )
    {    
        if(u08_g_C_IGNSw == KEY_CRANK)
        {
            u08_S_pre_Crank_Moving_St = u08_g_Tailgate_Moving_Cmd;
            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;             
            u08_g_Craking_Stop = ON;  
            
            if( (u08_g_RKE_Close_St == 2) || (u08_g_CPAD_Close_St == 2) )
            {
                u08_g_Crank_RKEnCPAD_CloseStop = (u08_g_RKE_Close_St == 2) ? 0x01U : 0x02U;
            }
            else
            {
            	u08_g_Crank_RKEnCPAD_CloseStop = 0;
            }		
        }
    }
    else
    {         
        if( u08_g_Craking_Stop == ON )
        {
            if(u08_g_C_IGNSw != KEY_CRANK)
            {
                u08_g_Craking_Stop = OFF;
                if(u08_g_LatchSt == LATCH_OPEN)
                {    
                    u08_g_User_Tailgate_Cmd = u08_S_pre_Crank_Moving_St;
                    if( u08_g_Crank_RKEnCPAD_CloseStop != 0 )
                    {
                        if( u08_g_Crank_RKEnCPAD_CloseStop == 0x01U )
                        {
                            u08_g_RKE_Close_St = 1;	
                        }
                        else
                        {
                            u08_g_CPAD_Close_St = 1;   	
                        }	
                        u08_g_Crank_RKEnCPAD_CloseStop = 0;	
                        u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;
                        u08_g_Buzzer_Hazard_Count = 20;	
                    }	
                }
            }    
        }
    }        
}

/********************************************************************************/
/*   name     : s_SMK_Hazard_Buzzer                                             */
/*   function : Hazard from skm signal control PTG Buzzer                       */
/*                                                                              */
/********************************************************************************/
static void s_SMK_Hazard_Buzzer( void )
{
    static U08 u08_s_SMK_Buzzer_On = 0;
    
    if( CAN_Event.C_HazardfromSMK == TRUE )
    {
        CAN_Event.C_HazardfromSMK = OFF;
        if( (u08_g_C_HazardfromSMK != 0) && (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) )        
        {
            if( (st_g_BuzzHaza.TimeTick > 0) && (u08_g_SMK_Buzzer_T == 0) )
            {
                st_g_BuzzHaza.TimeTick = 1;
            }              
            
            if( u08_g_C_HazardfromSMK == 1 )
            {    
                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                u08_g_Buzzer_Hazard_Count = 2; 
                u08_s_SMK_Buzzer_On = 1;
            }
            if( (u08_g_C_HazardfromSMK == 2) && (u08_g_SMK_Buzzer_T == 0) )
            {
                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                u08_g_Buzzer_Hazard_Count = 4;
                u08_g_SMK_Buzzer_T = 50; 
            }    
        }
        if( (u08_g_C_HazardfromSMK == 0) && (u08_s_SMK_Buzzer_On == 1) )    
        {
            u08_s_SMK_Buzzer_On = 0;
            u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF;  
            u08_g_Buzzer_Hazard_Count = 0;
            st_g_BuzzHaza.TimeTick = 0;
        }    
    }    
    
    if( u08_g_SMK_Buzzer_T != 0 )
    {
        u08_g_SMK_Buzzer_T--;
    }    
        
}

/********************************************************************************/
/*   name     : s_Strain_Resist_Control                                         */
/*   function : strain Resist Control when latch close , spindle control        */
/*              10ms scheduling task                                             */
/********************************************************************************/
static void s_Strain_Resist_Control( void )
{
   static U08 u08_s_Wait_Delay = 0; 
	 
   if( (u08_g_Strain_Cmd == 1) && (u08_g_PCU_Mode == STANDBY) )
   {
       u08_g_Strain_Cmd = 0;
       u08_s_Wait_Delay = 10; /* 100ms wait (cinching start dealy check)  */	
   }	
   
   if( u08_s_Wait_Delay != 0 )
   {
       u08_s_Wait_Delay--;
       if( u08_s_Wait_Delay == 0 )
       {
          u08_g_Strain_Operation_Motor_On_T = 2; /* 200ms -> 20ms */	
          u08_g_Tailgate_Moving_Cmd = OPEN_TAILGATE;
          /* CLOSE_TAILGATE OPEN_TAILGATE */
       }	
   }
   
   if( u08_g_Strain_Operation_Motor_On_T != 0 )
   {
       u08_g_Strain_Operation_Motor_On_T--;
       if( (u08_g_Strain_Operation_Motor_On_T == 0) || (u08_g_User_Tailgate_Cmd == OPEN_TAILGATE) ) 
       {
           u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;    	
       }	
   }	
   
   if((u08_g_Tailgate_Moving_Cmd != STOP_TAILGATE) || (u08_g_LatchSt == LATCH_OPEN) || (u08_g_User_Tailgate_Cmd == OPEN_TAILGATE))
   {	
       u08_s_Wait_Delay = 0; 
       u08_g_Strain_Cmd = 0;
   }   
   	
}

/********************************************************************************/
/*   name     : s_Continuous_Power_Close_Off_Control                            */
/*   function : countnue Close Status change , Buzzer / Hazard off              */
/*              10ms scheduling task                                            */
/********************************************************************************/
static void s_Continuous_Power_Close_Off_Control( U08 check_st )
{
	if( check_st == OFF )
	{	
	    if( (u08_g_RKE_Close_St == 2) || (u08_g_CPAD_Close_St == 2) )
	    {
	        u08_g_RKE_Close_St = 0;	
	        u08_g_CPAD_Close_St = 0;
	    	if( (st_g_BuzzHaza.CountTick > 3) && (st_g_BuzzHaza.Mode == BUZZHAZA_ON) )
	    	{
	    	    u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF;
	    	}	
	    }   
	}
	else
	{	
	    if(u08_g_RKE_Close_St == 1) 
	    {
	    	u08_g_RKE_Close_St = 2;
	    }
	    if( u08_g_CPAD_Close_St == 1 )
	    {
	    	u08_g_CPAD_Close_St = 2;
	    }
	}		
}

/********************************************************************************/
/*   name     : g_Tailgate_Control_10msPeriod                                   */
/*   function : Spindle Motor PID or Propoti Control                            */
/*              1ms scheduling task                                             */
/********************************************************************************/
void g_Tailgate_Control_10msPeriod(void)
{
    if( (u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE) || (u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) )  /* Tailgate Moving Status */
    {
        s_Spindle_Stop_Cmd(); 
        if( (u08_g_Tailgate_Stop_Cmd == ON) && ( u08_g_Strain_Operation_Motor_On_T == 0) )
        {                   
            u08_g_Tailgate_Stop_Cmd = OFF;
            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
        }
        else
        {    
            g_GetUser_PositionRate();
            
            switch( u08_g_User_Position )
            {
                case TAILGATE_ZONE_0 :
                    if( (u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) && (u08_g_Strain_Operation_Motor_On_T == 0) )
                    {    
                        u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE; 
                        u08_g_User_Tailgate_Cmd   = STOP_TAILGATE;
                        u08_g_StopToMove_T = 0;
                    }
                    break;
                case TAILGATE_ZONE_1 :
                    /* Continue Run */
                    break;
                case TAILGATE_ZONE_2 :
                    if( ( st_g_Vehiclest.bSpeedOver3Km != TRUE ) && (u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) && 
                        (u08_g_User_Tailgate_Cmd == OPEN_TAILGATE ) )
                    {    
                        u08_g_Moving_Delay_T = 20; /* 200ms delay */
                        u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE; /* stop & delay over -> Open */
                    }
                    break;
                case TAILGATE_ZONE_3 :
                	
                    if((u08_g_User_Tailgate_Cmd == STOP_TAILGATE) || (u08_g_Zone2_Open == 1 ))
                    {	
                    	  if( u08_g_Init_Param == ON )
                    	  {	
                            u08_s_zone3_UserSoftStop_Set = 1;
                            u08_s_SoftStop_cmd = ON;
                        }
                        else
                        {
                            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;	
                        }	  
                        
                        u08_g_Zone2_Open = 0;
                    }

                    if( u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE )
                    {   
                        /* Power Open -> stop, position GAP Value apply */                                   
                        if( (s16_g_Tailgate_Position > (u16_g_EEP_Para.ZONE3Pos+ ZONE3_STOP_GAP)) && 
                            ((st_g_Vehiclest.bSpeedOver3Km == TRUE ) || (u08_g_PTGM_Power_Mode == OFF)))
                        {    
                            if(st_g_Vehiclest.bSpeedOver3Km == TRUE )
                            {
                                /*g_SetBuzzHazardControl(BUZZONLY_ON, 20); */
                                u08_g_Buzzer_Hazard_Mode = BUZZONLY_ON;  
                                u08_g_Buzzer_Hazard_Count = 20; 
                                u08_s_OverSpeed_BuzzerSt = TRUE;
                            } 

                            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
                            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                        }
                    }
                    else
                    {
                        if( (u08_g_PTGM_Power_Mode == OFF) && (st_g_Vehiclest.bSpeedOver3Km != TRUE) ) 
                        {
                            u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
                            u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                        }   
                    } 

                    s_Key_Crank_Check(ON);
                    
                    break;
                case TAILGATE_ZONE_4 :
                    u08_g_Tailgate_Moving_Cmd = u08_g_User_Tailgate_Cmd;
                    if( u08_g_Init_Param == ON )
                    {	
                        if( u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE )
                        {
                            u08_s_SoftStop_cmd = ON;  /* open to stop moving soft */
                        }
                        else
                        {
                            if( (u08_g_PTGM_Power_Mode == OFF) && (st_g_Vehiclest.bSpeedOver3Km != TRUE) ) 
                            {
                                u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
                                u08_g_User_Tailgate_Cmd = STOP_TAILGATE;
                            }

                            s_Key_Crank_Check(ON);
                        }        
                    }
                    break; 
                default : 
                    u08_g_Tailgate_Moving_Cmd = STOP_TAILGATE;
                    u08_g_User_Tailgate_Cmd   = STOP_TAILGATE;
                    break;               
            }   
        }    
    }
    else /* Tailgate Stop Status */
    {
        s_Antipinch_Reverse_Control();
        s_Key_Crank_Check(OFF);
        s_TG_StopToMoving_Control();
        s_Over_Speed_Check();
        s_MotorBreakControl();
        s_Continuous_Power_Close_Off_Control(OFF);
        u08_g_Tailgate_Stop_Cmd = OFF;
        u16_s_MAX_Operating_Time = 0;
        u08_s_zone3_UserSoftStop_Set = 0;
    }        
    
    s_Strain_Resist_Control();
    s_SMK_Hazard_Buzzer();
}
/********************************************************************************/
/*   name     : g_Spindle_Moving_1msPeriod                                      */
/*   function : Spindle Motor PID or Propoti Control                            */
/*              1ms scheduling task                                             */
/********************************************************************************/
void g_Spindle_Moving_1msPeriod(void)
{
    /*S16 RH_PWM_Buff;*/
    S16 LH_PWM_Buff;
/*    U08 RH_Out;*/
    U08 LH_Out;
    
    if((u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) || (u08_g_SpindlePowerMov_Direction == OPEN_TAILGATE))
    {          
        if( u16_s_PID_Start_Delay_Time != 0 )
        {
            if( u08_g_Init_Param == ON )
            { 
                u16_s_PID_Start_Delay_Time--;
            }
                
            g_Proporti_PWM_Calculation();

            /* LH spindle : now out pwm ratio + propoti calc pwm ratio */
            LH_PWM_Buff = (u08_g_LH_Ratio + s08_g_Propoti_Ratio);   

            /* RH spindle : now out pwm ratio + propoti calc pwm ratio + sync calc pwm ratio */
            /*RH_PWM_Buff = (u08_g_RH_Ratio + s08_g_Propoti_Ratio);   */
        }
        else if( u08_s_SoftStop_cmd == OFF )
        {                       
            g_PID_Calculation();

            /* LH spindle : now out pwm ratio + PID calc pwm ratio */
            LH_PWM_Buff = (u08_g_LH_Ratio + s08_g_PID_Radio);   

            /* RH spindle : now out pwm ratio + PID calc pwm ratio + sync calc pwm ratio */
            /*RH_PWM_Buff = (LH_PWM_Buff + s08_g_RH_Sync_Ratio );*/
        }
        else
        {
            LH_PWM_Buff = (U08)u08_s_Tailgate_Soft_Stop();
            /*RH_PWM_Buff = (LH_PWM_Buff + s08_g_RH_Sync_Ratio);*/
        }            
        
        if((u08_g_SpindlePowerMov_Direction == CLOSE_TAILGATE) && ( s16_g_LH_Position_Cnt <= 10))
        {
            /*RH_Out = s_Ratio_Limit_Check( 255 );*/ /* 100% */
            LH_Out = s_Ratio_Limit_Check( 255 ); /*80% -> 100% 190816 */  
        } 
        else 
        {
            /*RH_Out = s_Ratio_Limit_Check( RH_PWM_Buff );*/
            LH_Out = s_Ratio_Limit_Check( LH_PWM_Buff );  
        }  

        g_Spindle_MT_OutPWM( /*RH_Out,*/ LH_Out );
    }
    else
    {
        if( u08_s_Pre_Calc_St == ON )
        {
            u08_s_Pre_Calc_St = OFF;
            g_PID_Value_Init();
        }    
    }        
}

/********************************************************************************/
/*   name     : g_Spindle_MoveStart_10msPeriod                                */
/*   function : Spindle Motor Moving start value Setup                          */
/*              stop motor Init -> moving motor Init -> stop motor Init         */
/********************************************************************************/
void g_Spindle_MoveStart_10msPeriod(void)
{                                
    const U08 Start_Duty_Tbl_Open[21] = {
    /* Battery Volt                   0     1     2     3     4     5     6     7     8     9     */
                                    117,  117,  117,  117,  117,  117,  135,  210,  210,  195,
    /* Battery Volt                  10    11    12    13    14    15    16    17    18    19     */
                                    175,  160,  140,  120,  110,   95,   81,   81,   72,   72,                                    
    /* Battery Volt                  20                                                  */
                                    76 };     
    const U08 Start_Duty_Tbl_Close[21] = {
    /* Battery Volt                   0     1     2     3     4     5     6     7     8     9     */
                                     100, 100,  100,  100,  100,  100,  100,  100,  110,   110,
    /* Battery Volt                  10    11    12    13    14    15    16    17    18    19     */
                                     105, 100,   90,   85,   75,   65,   60,   55,   50,   50,
    /* Battery Volt                  20                                                  */
                                     50 };     
                    
    static U08 u08_s_Pre_Setup_Value = INIT_TAILGATE;
    /* Init value : Motor stop */
    U08 t_MTDrive_Out = STOP_TAILGATE;
    U08 t_PWM_Duty = STOP_DUTY;
    
    if( u08_g_Tailgate_Moving_Cmd != u08_s_Pre_Setup_Value )
    {
        if( (u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) || (u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE) )
        {
             g_PID_Value_Init();
             g_Encoder_Init();
             g_Hall_Power_Out_LH(ON);
             /*g_Hall_Power_Out_RH(ON);*/
             s_Continuous_Power_Close_Off_Control( ON );
        }
        
        switch( u08_g_Tailgate_Moving_Cmd )
        {
            case STOP_TAILGATE  :
                u16_s_PID_Start_Delay_Time = 0;
                u08_g_ReverseMovSet = 0;
                break;
            case CLOSE_TAILGATE  :
                t_MTDrive_Out = CLOSE_TAILGATE;
                t_PWM_Duty = Start_Duty_Tbl_Close[u08_g_clacBatteryVolt];       
                u08_s_Pre_Calc_St = ON;
                u08_s_SoftStop_cmd = OFF;
                u16_s_PID_Start_Delay_Time = PID_START_DELAY;  
                u08_g_AntiPinch_Start_Delay_T = ANTIPINCH_START_DELAY;
                if( u08_g_Init_Param == ON )
                {
                    u16_g_Target_RPM = CLOSE_TARGET_RPM_START;
                }
                else
                {
                    u16_g_Target_RPM = CLOSE_NOT_PARA_RPM;                
                }    
                break;
            case OPEN_TAILGATE  :
                t_MTDrive_Out = OPEN_TAILGATE;
                if( s08_g_Convert_Temp >= 60 )
                {
                    t_PWM_Duty = (Start_Duty_Tbl_Open[u08_g_clacBatteryVolt] + 20); 
                    u16_s_PID_Start_Delay_Time = (PID_START_DELAY + 150); /* const control + 200ms */
                }
                else
                {        
                	  if( s16_g_Tailgate_Position > u16_g_EEP_Para.ZONE3Pos )
                	  {
                	  	  t_PWM_Duty = (Start_Duty_Tbl_Open[u08_g_clacBatteryVolt] - 40); 
                	  }
                	  else 
                	  {
                	      t_PWM_Duty = Start_Duty_Tbl_Open[u08_g_clacBatteryVolt]; 
                	  }		
                    
                    u16_s_PID_Start_Delay_Time = PID_START_DELAY;
                }
                /* test out duty */
                /*t_PWM_Duty = 255;*/
                /****************/
                u08_g_AntiPinch_Start_Delay_T = ANTIPINCH_START_DELAY + 20;
                u08_s_Pre_Calc_St = ON;
                u08_s_SoftStop_cmd = OFF;
                if( u08_g_Init_Param == ON )
                {
                    u16_g_Target_RPM = OPEN_TARGET_RPM_START;
                }
                else
                {
                    u16_g_Target_RPM = OPEN_NOT_PARA_RPM;                
                }
                break;
            case BREAK_TAILGATE  :
                t_MTDrive_Out = BREAK_TAILGATE;
                t_PWM_Duty = MAX_DUTY; 
                u16_s_PID_Start_Delay_Time = 0;
                u08_s_SoftStop_cmd = OFF;
                break;
            case INIT_TAILGATE :
                u16_s_PID_Start_Delay_Time = 0;
                u08_s_SoftStop_cmd = OFF;
                break;    
            default  :
                u16_s_PID_Start_Delay_Time = 0;
                u08_s_SoftStop_cmd = OFF;
                break;
        }
        
        /************************************** in line mode ******************************************/  
        if( u16_g_EEP_Diag.InLine_Mode == TRUE ) 
        {
            if( ((u08_g_Tailgate_Moving_Cmd == CLOSE_TAILGATE) || (u08_g_Tailgate_Moving_Cmd == OPEN_TAILGATE)) &&
            	  ( u08_g_Strain_Operation_Motor_On_T == 0) )
            {    
                u08_g_Buzzer_Hazard_Mode = BUZZHAZA_ON;  
                u08_g_Buzzer_Hazard_Count = 20;       
            }
            
            if( (u08_g_Tailgate_Moving_Cmd == STOP_TAILGATE) && (st_g_BuzzHaza.Mode == BUZZHAZA_ON) && (st_g_BuzzHaza.CountTick > 0) )
            {
                u08_g_Buzzer_Hazard_Mode = BUZZHAZA_OFF;
            }    
        }
        /**********************************************************************************************/                
                
        g_Spind_MTDrive_Out(t_MTDrive_Out);  
        g_Spindle_MT_OutPWM(/*t_PWM_Duty,*/ t_PWM_Duty );
        u08_s_Pre_Setup_Value = u08_g_Tailgate_Moving_Cmd;
    }    
}
                
/********************************************************************************/
/*   name     : g_Control_Command_Init                                          */  
/*   function : Control command value init                                      */
/********************************************************************************/
void g_Control_Command_Init( void )
{
     u08_g_Tailgate_Moving_Cmd  =  STOP_TAILGATE;
     u08_g_Tailgate_Stop_Cmd = OFF;
     u08_g_StopToMove_T = 0;
     u08_g_Antipinch_Reverse_Mode = 0;  
     u08_g_SMK_Buzzer_T = 0;    
     u08_s_OverSpeed_Buzzer_Delay = 60; /* 600ms wake up vehicle speed over Buzzer delay CK */
     u08_g_Crank_RKEnCPAD_CloseStop = 0;
     u08_g_Craking_Stop = 0;
     u08_g_ReverseMovSet = 0;
     u08_g_Strain_Operation_Motor_On_T = 0;
}
