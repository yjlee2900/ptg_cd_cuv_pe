/********************************************************************************/
/*                                                                              */
/*  Project         : PTG(Power Tailgate ) for CK                               */
/*  Copyright       : MOTOTECH CO. LTD.                                         */
/*  Author          : Jae-Young Park                                            */
/*  Created date    : 2013-06-13 PM.  6:41:23                                   */
/*  Last update     : 2013-06-13 PM.  6:41:25                                   */
/*  File Name       : Basic_Type_Define.h                                       */
/*  Processor       : MC9S12P64                                                 */
/*  Version         :                                                           */
/*  Compiler        : HI-WIRE                                                   */
/*  IDE             : CodeWarrior IDE VERSION 4.7.0                             */
/*                                                                              */
/********************************************************************************/

#if	!defined(FW_DATATYPE_BASE_H__)
#define	FW_DATATYPE_BASE_H__

/* #define NONCAN_TEST_MODE */
/****************************************************************/
/*	base type expansion set	: base types						*/
/****************************************************************/
typedef	signed char				        S08;		
typedef	unsigned char				    U08;
typedef	signed int				        S16;		
typedef	unsigned int					U16;
typedef	signed long int				    S32;
typedef	unsigned long int				U32;

/* logic symbols */
#ifndef		LOW
	#define	LOW				0U
#endif
#ifndef		HIGH
	#define	HIGH			1U
#endif

#ifndef		OFF
	#define	OFF				0U
#endif
#ifndef		ON
	#define	ON		    	1U
#endif

#ifndef		FALSE
	#define	FALSE			0
#endif
#ifndef		TRUE
	#define	TRUE			1
#endif
/* digital I/O symbols */
#ifndef		SWITCH_OFF
	#define	SWITCH_OFF		0U
#endif

#ifndef		SWITCH_ON
	#define	SWITCH_ON		1U
#endif

#ifndef     EEP_OK
    #define EEP_OK          0U
#endif
#ifndef     EEP_NG
    #define EEP_NG          1U
#endif


#endif	/* FW_DATATYPE_BASE_H__ */


/* ******************** EOF : _DataTypeCommon.h ******************/


