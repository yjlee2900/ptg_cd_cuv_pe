/******   STARTSINGLE_OF_MULTIPLE_COMMENT    ******/


/*****************************************************************************/
/**
 * \file  SIP_VERS.C
 * \brief Vector SIP (Software Integration Package) version check
 *
 *  \pre 
 *  The purpose of this file is to:
 *  1) Check that all currently compiled files in the system have the correct 
 *     versions according to the delivered software integration package (SIP).
 *  2) Provide the SIP version as ROM constants for e.g. diagnostic purposes.
 *
 *-----------------------------------------------------------------------------
 *
 *  Usage hints: What to do when a compiler error occurs within this file?
 *               If a compiler error occurs within this file, a wrong version
 *               for the notified module has been detected. 
 *               Please check, if
 *               a) The module is part of your project and the include path is 
 *                  set correct (if not, the compiler may assume 0 for the 
 *                  version number comparison)
 *               b) The used module may have an incorrect version for this
 *                  project. Proof carefully, if a newer (or older) version
 *                  of the notified component is already available on your site
 *                  and an erroneous version mixup had occurred. See the required
 *                  module's main and bugfix version above of the error message.
 *               c) This may be the wrong SIP version check file for the project.
 *                  Proof carefully, if the file has been exchanged together with
 *                  the latest change of a component (e.g. an bugfix delivery), 
 *                  too.
 *               In case none of the above mention points is true, please contact 
 *               your Vector's contact person for further help.
 *
 *****************************************************************************
 *
 *  Background information: Each Vector software component checks its own header
 *               and code files for version consistency. With this, a version  
 *               mixup within each component could be detected during compilation.
 *               To detect the mixup of different components in a project, too,
 *               this version check file has been introduced.
 *                                
 *****************************************************************************
 *               C O P Y R I G H T
 *****************************************************************************
 * Copyright (c) 2006 by Vector Informatik GmbH.           All rights reserved.
 *
 *                    Alle Rechte an der Software verbleiben bei der
 *                    Vector Informatik GmbH.
 *                    Vector Informatik GmbH r�umt dem Lizenznehmer das unwider-
 *                    rufliche, geographisch und zeitlich nicht beschr�nkte,
 *                    jedoch nicht ausschlie�liche Nutzungsrecht innerhalb des
 *                    Lizenznehmers ein. Die Weitergabe des Nutzungsrechts
 *                    durch den Lizenznehmers ist auf dessen Zulieferer
 *                    beschr�nkt. Die Zulieferer sind zu verpflichten, die
 *                    Software nur im Rahmen von Projekten f�r den Lizenznehmer
 *                    zu verwenden; weitere Rechte der Zulieferer sind
 *                    auszuschlie�en.
 *
 *     Use of this source code is subject to the License Agreement "license.txt"
 *     distributed with this file. You may not use this source code unless you
 *     agree to the terms of the License Agreement.
 *
 *     REMOVAL OF THESE COMMENTS IS A VIOLATION OF THE LICENSE AGREEMENT.
 *
 *****************************************************************************
 *               A U T H O R   I D E N T I T Y
 *****************************************************************************
 * Initials     Name                      Company
 * ********     *********************     ************************************
 * Ra           Andreas Raisch            Vector Informatik GmbH
 * Hgo          Hua Guo                   Vector Informatik GmbH
 * Abc          Alexander Becker          Vector Informatik GmbH
 * Bwr          Bernhard Wissinger        Vector Informatik GmbH
 * Fr           Markus Feninger           Vector Informatik GmbH
 * Dbr          Daniela Brenner           Vector Informatik GmbH
 * Agh          Alexander Gronbach        Vector Informatik GmbH
 * Stu          Bernd Stumpf              Vector Informatik GmbH
 *****************************************************************************
 *               R E V I S I O N   H I S T O R Y
 *****************************************************************************
 * Date         Version Author  Description
 * ********     ************** ***********************************************
 * 02-11-14     1.00.00 Ra      First template version, based on proposal by Hp
 * 07-11-07     2.00.00 Hgo     use v_ver.h to automatically check SIP Version
 * 07-11-16     2.01.00 Abc     sip_vers OEM independent
 * 07-11-19     2.01.01 Abc     different bugfixes
 * 08-01-31     2.01.02 Bwr     adaption for TMS470HeccScc
 * 08-03-10     2.01.03 Hgo     correct diag
 * 08-03-12     2.01.04 Hgo     correct Tp_ISO_SCS bugfix version check
 * 08-03-12     2.01.05 Fr      SipVersion declaration included
 * 08-03-13     2.01.06 Hgo     COMMENT rule for GW_AS_DC added
 * 08-03-13     2.01.07 Et      GenTool (release => bugfix version) + DIAG modifications
 *                              _GENTOOL_CANGEN_VERSION / _GENTOOL_CANGEN_RELEASE_VERSION
 * 08-03-14     2.01.08 Hgo     DBKomGEN version check added
 *                              both release and bugfix version names supported
 * 08-03-17     2.01.09 Et      corrected GenTool CANgen/DBKOMgen version support
 * 08-04-04     2.01.10 Hgo     ESCAN00025852
 *                              added the check for some components, if it is used
 * 08-04-17     2.01.11 Dbr     adaption for TMS470HeccScc ARM
 * 08-04-23     2.01.12 Agh     added Mpc55xx_Flexcan2 with GHS and corrected VWTP20
 *                              COMMENT rule for GENy, CANgen and DBKOMgen added
 * 08-04-28     2.01.13 Hgo     added H8S RCAN and HCAN1 support
 * 08-04-28     2.01.14 visbwr  fixed GMLAN and FJ16FX support
 * 08-05-07     2.01.15 visbwr  added TP_MCAN support
 * 08-05-07     2.01.16 Dbr     added LIN for MPC55XX GHS Flexcan2
 * 08-05-08     2.01.17 Dbr     added CanDrv R32C/Renesas
 * 08-05-08     2.01.18 Hgo     added gw_sig, gw_msg
 * 08-05-13     2.01.19 Stu     added CanDrv Stm8a/Cosmic
 * 08-06-18     2.01.20 Agh     avoid compiler warnings for M32C with Mitsubishi
 * 08-08-05     2.01.21 viset   support C_CLIENT_MMC (OSEK-NM)
 * 08-10-22     2.01.22 visbwr  changed include order for GGDA
 *****************************************************************************/


#include "v_cfg.h"
#include "v_def.h"

/* ---------------------------------------------------------------------------
 * Can
 * --------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_CAN_DRV )
#include "can_inc.h"
#endif

/* ---------------------------------------------------------------------------
 * IL
 * --------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_IL_VECTOR )
#include "il_inc.h"
#include "il_def.h"
#endif
/* ---------------------------------------------------------------------------
 * TP
 * --------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_TP_ISO_MC )
#include "tpmc.h"
#endif


/*----------------------------------------------------------------------------
  DIAG_candesc_basic_kwp
  ---------------------------------------------------------------------------*/
#if defined(VGEN_ENABLE_DIAG_CANDESC_BASIC_KWP)
#include "desc.h"
#endif
/* ---------------------------------------------------------------------------
 * NM_basic
 * --------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_NM_BASIC )
#include "nm_basic.h"
#endif

/* ---------------------------------------------------------------------------
 * NM_osek
 * --------------------------------------------------------------------------*/
#if defined VGEN_ENABLE_NM_OSEK_D
  #include "nm_osek.h"
#endif


/* ---------------------------------------------------------------------------
 * CCP
 * --------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_CCP )
#include "ccp.h"
#endif


/*
 * Description: VSTDLIB.H includes the Vector Standard Library header.
 */
#if defined ( VGEN_ENABLE_VSTDLIB )
  #include "vstdlib.h"
#endif


#if defined( VGEN_GENY )
 #include "v_inc.h"
#endif
/*
 * Description: This file is the own header of the SIP version check code file.
 *              It MUST always be included last due to the usage of MEMORY_ROM
 *              which is defined in V_DEF.H.
 */
#include "sip_vers.h"

/*
 * Description: This file is generated by Almplus
 */
#include "v_ver.h"

/******************************************************************************
| version check
|*****************************************************************************/
/* Used to check source against header. This is NOT the SIP version! */
#if ( SIP_VERS_VERSION != _COMMON_SIPVERSIONCHECK_VERSION )
  #error "SipCheck: Different main/sub version in header and source!"
#endif
#if ( SIP_VERS_RELEASE_VERSION != _COMMON_SIPVERSIONCHECK_RELEASE_VERSION )
  #error "SipCheck: Different release versions header and source!"
#endif

/******************************************************************************
| local defines
|*****************************************************************************/

/******************************************************************************
| check components (presence, versions, functionalities)
|*****************************************************************************/

/*----------------------------------------------------------------------------
  Common_VStdLib
  ---------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_VSTDLIB )
#else
 #error "Common_VStdLib is not part of the project although it should be."
#endif

#if defined( C_COMP_MTRWRKS_MCS12X_MSCAN12 ) 
 #if( VSTDLIB_MCS12X_VERSION != _VSTDLIB_HW_VERSION )
  #error "VStdLib: Wrong main/sub version detected for this SIP delivery!"
 #endif
 #if( VSTDLIB_MCS12X_RELEASE_VERSION != _VSTDLIB_HW_RELEASE_VERSION )
  #error "VStdLib: Wrong release version detected for this SIP delivery!"
 #endif
#endif

/*----------------------------------------------------------------------------
  DrvCan 
  ---------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_CAN_DRV )
#else
 #error "DrvCan is not part of the project although it should be."
#endif

#if defined( C_COMP_MTRWRKS_MCS12X_MSCAN12 ) 
 #define CAN_DRIVER_OK
 #if( DRVCAN_MCS12XMSCANHLL_VERSION != _DRVCAN_HW_VERSION )
  #error "CanDrv: Wrong version detected for this SIP delivery!"
 #endif
 #if( DRVCAN_MCS12XMSCANHLL_RELEASE_VERSION != _DRVCAN_HW_RELEASE_VERSION )
  #error "CanDrv: Wrong version detected for this SIP delivery!"
 #endif
#endif
#if !defined( CAN_DRIVER_OK )
 #error "Unsupported �C platform detected!"
#endif

/*----------------------------------------------------------------------------
  Il_Vector
  ---------------------------------------------------------------------------*/ 
#if defined( VGEN_ENABLE_IL_VECTOR)
 #if( IL_VECTOR_VERSION != _IL_VECTOR_VERSION )
  #error "IL: Wrong main/sub version detected for this SIP delivery!"
 #endif
 #if( IL_VECTOR_RELEASE_VERSION != _IL_VECTOR_RELEASE_VERSION )
  #error "IL: Wrong release version detected for this SIP delivery!"
 #endif
#else
 #error "IL_Vector is not part of the project although it should be."
#endif

/*----------------------------------------------------------------------------
  NM_OSEK_D
  ---------------------------------------------------------------------------*/ 
#if defined (VGEN_ENABLE_NM_OSEK_D)
 #if(( NM_DIROSEK_VERSION != _NM_DIROSEK_VERSION ) && \
	( NM_OSEK_VERSION != _NM_DIROSEK_VERSION ))
  #error "NM: Wrong version detected for this SIP delivery!"
 #endif
 #if(( NM_DIROSEK_RELEASE_VERSION != _NM_DIROSEK_RELEASE_VERSION ) && \
	( NM_OSEK_BUGFIX_VERSION != _NM_DIROSEK_RELEASE_VERSION ))
  #error "NM: Wrong version detected for this SIP delivery!"
 #endif
#else
 #error "NM_OSEK_D is not part of the project although it should be."
#endif


/*----------------------------------------------------------------------------
  NM_BASIC
  ---------------------------------------------------------------------------*/ 
#if defined( VGEN_ENABLE_NM_BASIC )
 #if( NM_PWRTRAINBASIC_VERSION != _NM_PWRTRAINBASIC_VERSION )
  #error "Nm_PowertrainBasic: Wrong main/sub version detected for this SIP delivery!"
 #endif
 #if( NM_PWRTRAINBASIC_RELEASE_VERSION != _NM_PWRTRAINBASIC_RELEASE_VERSION )
  #error "Nm_PowertrainBasic: Wrong release version detected for this SIP delivery!"
 #endif
#else
#if	defined(__DISABLE_BY_GONG__)
 #error "Nm_PowertrainBasic is not part of the project although it should be."
#endif	/* __DISABLE_BY_GONG__ */
#endif

/*----------------------------------------------------------------------------
  Tp_ISO_MC
  ---------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_TP_ISO_MC )
 #if( TP_ISO15765_VERSION != _TP_ISO15765_VERSION )
  #error "Tp_Iso15765: Wrong main/sub version detected for this SIP delivery!"
 #endif
 #if( TP_ISO15765_RELEASE_VERSION != _TP_ISO15765_RELEASE_VERSION )
  #error "Tp_Iso15765: Wrong release version detected for this SIP delivery!"
 #endif
#else
#if	defined(__DISABLE_BY_GONG__)
 #error "TP_ISO_MC is not part of the project although it should be."
#endif	/* __DISABLE_BY_GONG__ */
#endif

/*----------------------------------------------------------------------------
  DIAG_candesc_basic_kwp
  ---------------------------------------------------------------------------*/
#if defined ( VGEN_ENABLE_DIAG_CANDESC_BASIC_KWP )
  #if ( DIAG_CANDESC__CORE_VERSION != _DIAG_CANDESC__COREBASE_VERSION )
    #error "CANdescBasic KWP: Wrong version detected for this SIP delivery!"
  #endif
  #if ( DIAG_CANDESC__CORE_RELEASE_VERSION != _DIAG_CANDESC__COREBASE_RELEASE_VERSION )
    #error "CANdescBasic KWP: Wrong release version detected for this SIP delivery!"
  #endif
#else
#if	defined(__DISABLE_BY_GONG__)
 #error "Diag_Candesc_Basic_Kwp is not part of the project although it should be."
#endif	/* __DISABLE_BY_GONG__ */
#endif


/*----------------------------------------------------------------------------
  Ccp
  ---------------------------------------------------------------------------*/
#if defined( VGEN_ENABLE_CCP)
# if (CP_CCP_VERSION != _CP_CCP_VERSION)
#  error "CCP: Wrong version detected for this SIP delivery!"
# endif
# if (CP_CCP_RELEASE_VERSION != _CP_CCP_RELEASE_VERSION)
#  error "CCP: Wrong version detected for this SIP delivery!"
# endif
#endif

/******************************************************************************
| Store additional SIP information in ROM constants
|*****************************************************************************/
/* ROM CATEGORY 4 START*/
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kSipMainVersion   = (vuint8)(VGEN_DELIVERY_VERSION_BYTE_0);
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kSipSubVersion    = (vuint8)(VGEN_DELIVERY_VERSION_BYTE_1);
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kSipBugFixVersion = (vuint8)(VGEN_DELIVERY_VERSION_BYTE_2);
/* ROM CATEGORY 4 END*/

/************   Organi, Version 3.8.0 Vector-Informatik GmbH  ************/
