/****************************************************************************
| Project Name: Flash Bootloader
|    File Name: APPLVECT.h
|
|  Description: Application vectortable for Motorola MC9S12X
|               This file is usually compiled and linked with 
|               the bootloader and is located  O U T S I D E  the 
|               protected area of the FBL. So it could be 
|               compiled, linked and downloaded with the 
|               application after adapting the interrupt
|               entry poits to the application interrupt vectors.
|               The application service routines are still interrupts!!
|
|               This file must be compiled and linked in the bootloader
|               and the application to the 
|               S A M E  M E M O R Y   L O C A T I O N !!!!
|
|            
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2000-2009 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains example source code used by the
| Flash Bootloader. This code may influence the behaviour of the 
| bootloader in principle. Therefore, great care must be taken to verify
| the correctness of the implementation.
|
| The contents of the originally delivered files are only examples resp. 
| implementation proposals. With regard to the fact that these functions 
| are meant for demonstration purposes only, Vector Informatik�s 
| liability shall be expressly excluded in cases of ordinary negligence, 
| to the extent admissible by law or statute.
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------------------
| WM            Marco Wierer           Vector Informatik GmbH
| Hp            Armin Happel           Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2004-09-07  01.00.00  WM      ESCAN00009414: Initial version for S12X
| 2005-02-09  01.01.00  WM      ESCAN00015298: Corrected macro encapsulation
|                                for C_COMP_MWERKS_MCS12X_MSCAN12
|                               Added version scan support
| 2006-08-10  01.02.00  Hp      Using general typedef for IntVect-pointer
| 2007-01-20  01.03.00  WM      ESCAN00019209: C_COMP_MWERKS_MCS12X_MSCAN12
|                                to C_COMP_MTRWRKS_MCS12X_MSCAN12 renamed
| 2008-07-10  01.04.00  WM      ESCAN00029131: Added support for MCS12P
| 2009-12-15  01.05.00  AWh     ESCAN00039789: Inconsistent pragma settings in 
|                                .c/.h file
|*****************************************************************************/
#ifndef APPL_VECT_H
#define APPL_VECT_H

/* ##V_CFG_MANAGEMENT ##CQProject : FblVtabAppl_Hc12 CQComponent : Implementation */
#define FBLVTABAPPL_HC12_VERSION             0x0105
#define FBLVTABAPPL_HC12_RELEASE_VERSION     0x00

#define APPLVECT_SIZE            64
#define RSTVECT_INDEX            (APPLVECT_SIZE-1)
#define APPLVECT_FROM_BOOT       0x0000

/* Reset vector pointer definition - compiler specific */
#define RESET_ENTRY_POINT _Startup

typedef void V_API_NEAR (*V_API_NEAR const tIntVectPtr)(void);


typedef struct
{
  tIntVectPtr _vectab;
} tIntJmpTable;

#pragma CONST_SEG APPLVECT
extern const tIntJmpTable ApplIntJmpTable[APPLVECT_SIZE];
#pragma CONST_SEG DEFAULT

#endif
