/*****************************************************************************
| Project Name: CANfbl (KWP2000) for SYMC
|    File Name: FblIf.H
|
|  Description: Interface routines for application for interaction with the FBL
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2006 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of callback 
| functions to be used with the Flash Bootloader. These functions may 
| influence the behaviour of the bootloader in principle. Therefore, 
| great care must be taken to verify the correctness of the 
| implementation.
|
| The contents of the originally delivered files are only examples resp. 
| implementation proposals. With regard to the fact that these functions 
| are meant for demonstration purposes only, Vector Informatik�s 
| liability shall be expressly excluded in cases of ordinary negligence, 
| to the extent admissible by law or statute.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| CB           Christian Baeuerle        Vector Informatik GmbH
|
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Ver       Author  Description
| ---------   --------  ------  ----------------------------------------------
| 2006-12-20  01.00.00  CB      First implementation
|****************************************************************************/
#ifndef __FB_IF_h__
#define __FB_IF_h__

#define FBLIF_SYMC_VERSION           0x0100u
#define FBLIF_SYMC_RELEASE_VERSION   0x00u


/* Includes *******************************************************************/

/* Typedefs ******************************************************************/

typedef struct
{
   /* Software identification information */
   vuint8 apInfEcuOrigin;
   vuint8 apInfSupplierId;
   vuint8 apInfDiagnosticInfo[2];
   vuint8 apInfReserved;
   vuint8 apInfSwVersion[3];
   vuint8 apInfPartNo[10];
   
   /* Interface version for consistency check */
   vuint8 apInfVersion[2];
}tModuleInfoHeader;

/* Prototypes *****************************************************************/
void ApplFblInitPowerOn( void );
void ApplFblReset( void );
void ApplFblExtProgRequest( void );




/******************************************************************************
******************************************************************************/
#endif 
