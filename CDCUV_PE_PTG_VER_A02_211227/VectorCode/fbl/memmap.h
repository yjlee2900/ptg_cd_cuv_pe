/****************************************************************************
| Project Name: Flash Bootloader
|    File Name: memmap.H
|
|  Description: Section definition to locate data or code in memory
|               This file contains compiler specific section definition
|               for every segment that can be mapped within the bootloader
|               to a specific location.
|
|-----------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------
| Copyright (c) 2006 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of definitions
| to be used with the Flash Bootloader. These functions may
| influence the behaviour of the bootloader in principle. Therefore,
| great care must be taken to verify the correctness of the
| implementation.
|
| The contents of the originally delivered files are only examples resp.
| implementation proposals. With regard to the fact that these definitions
| are meant for demonstration purposes only, Vector Informatiks
| liability shall be expressly excluded in cases of ordinary negligence,
| to the extent admissible by law or statute.
|-----------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------
|  HP            Armin Happel          Vector Informatik GmbH
|-----------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------
| Date          Ver    Author   Description
| ---------   -------  ------   ------------------------------------
| 2006-10-05  1.00.00    Hp     Creation.
|*****************************************************************************/


/*-- Locate block descriptor (LBT) ----------*/
#if defined(LBT_START_SEC_CODE_EXPORT) || \
    defined(LBT_START_SEC_CODE)
#undef LBT_START_SEC_CODE
#undef LBT_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
/* if a re-programmable LBT shall be used. */
#endif

#if defined(LBT_STOP_SEC_CODE_EXPORT) || \
    defined(LBT_STOP_SEC_CODE)
#undef LBT_STOP_SEC_CODE
#undef LBT_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
/* if a re-programmable LBT shall be used. */
#endif


/*-- Locate NEAR data in FBL_DIAG ----------*/
#if defined(FBLDIAG_START_SEC_NEARDATA_EXPORT) || \
    defined(FBLDIAG_START_SEC_NEARDATA)
#undef FBLDIAG_START_SEC_NEARDATA
#undef FBLDIAG_START_SEC_NEARDATA_EXPORT
/* Insert memory section definition here   */
#endif

#if defined(FBLDIAG_STOP_SEC_NEARDATA_EXPORT) || \
    defined(FBLDIAG_STOP_SEC_NEARDATA)
#undef FBLDIAG_STOP_SEC_NEARDATA
#undef FBLDIAG_STOP_SEC_NEARDATA_EXPORT
/* Insert memory section definition here   */
#endif


/*-- Locate DiagBuffer to a specific RAM-location if necessary --*/
#if defined(DIAGBUFFER_START_SEC_DATA) || \
    defined(DIAGBUFFER_START_SEC_DATA_EXPORT)
#undef DIAGBUFFER_START_SEC_DATA
#undef DIAGBUFFER_START_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif

#if defined(DIAGBUFFER_STOP_SEC_DATA) || \
    defined(DIAGBUFFER_STOP_SEC_DATA_EXPORT)
#undef DIAGBUFFER_STOP_SEC_DATA
#undef DIAGBUFFER_STOP_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif


/*---- Locate CanInitTable ----*/
#if defined(CANPARAM_START_SEC_DATA) || \
    defined(CANPARAM_START_SEC_DATA_EXPORT)
#undef CANPARAM_START_SEC_DATA
#undef CANPARAM_START_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif

#if defined(CANPARAM_STOP_SEC_DATA) || \
    defined(CANPARAM_STOP_SEC_DATA_EXPORT)
#undef CANPARAM_STOP_SEC_DATA
#undef CANPARAM_STOP_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif


/*---- Locate FblHeader ----*/
#if defined(FBLHEADER_START_SEC_CONST) || \
    defined(FBLHEADER_START_SEC_CONST_EXPORT)
#undef FBLHEADER_START_SEC_CONST
#undef FBLHEADER_START_SEC_CONST_EXPORT
/* Insert memory section definition here   */
# pragma CONST_SEG FBLHEADER
#endif

#if defined(FBLHEADER_STOP_SEC_CONST) || \
    defined(FBLHEADER_STOP_SEC_CONST_EXPORT)
#undef FBLHEADER_STOP_SEC_CONST
#undef FBLHEADER_STOP_SEC_CONST_EXPORT
/* Insert memory section definition here   */
#pragma CONST_SEG DEFAULT
#endif


/*---- Locate FblLookForWatchdog function ----*/
#if defined(LOOKFORWATCHDOG_START_SEC_CODE) || \
    defined(LOOKFORWATCHDOG_START_SEC_CODE_EXPORT)
#undef LOOKFORWATCHDOG_START_SEC_CODE
#undef LOOKFORWATCHDOG_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG __NEAR_SEG NON_BANKED
#endif

#if defined(LOOKFORWATCHDOG_STOP_SEC_CODE) || \
    defined(LOOKFORWATCHDOG_STOP_SEC_CODE_EXPORT)
#undef LOOKFORWATCHDOG_STOP_SEC_CODE
#undef LOOKFORWATCHDOG_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG DEFAULT
#endif


/*---- Locate FblLookForWatchdogEnd function ----*/
#if defined(LOOKFORWATCHDOGEND_START_SEC_CODE) || \
    defined(LOOKFORWATCHDOGEND_START_SEC_CODE_EXPORT)
#undef LOOKFORWATCHDOGEND_START_SEC_CODE
#undef LOOKFORWATCHDOGEND_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG __NEAR_SEG NON_BANKED
#endif

#if defined(LOOKFORWATCHDOGEND_STOP_SEC_CODE) || \
    defined(LOOKFORWATCHDOGEND_STOP_SEC_CODE_EXPORT)
#undef LOOKFORWATCHDOGEND_STOP_SEC_CODE
#undef LOOKFORWATCHDOGEND_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG DEFAULT
#endif


/*---- Locate ApplFblWDTrigger function ----*/
#if defined(WDTRIGGER_START_SEC_CODE) || \
    defined(WDTRIGGER_START_SEC_CODE_EXPORT)
#undef WDTRIGGER_START_SEC_CODE
#undef WDTRIGGER_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG __NEAR_SEG NON_BANKED
#endif

#if defined(WDTRIGGER_STOP_SEC_CODE) || \
    defined(WDTRIGGER_STOP_SEC_CODE_EXPORT)
#undef WDTRIGGER_STOP_SEC_CODE
#undef WDTRIGGER_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG DEFAULT
#endif


/*---- Locate ApplFblWDTriggerEnd function ----*/
#if defined(WDTRIGGEREND_START_SEC_CODE) || \
    defined(WDTRIGGEREND_START_SEC_CODE_EXPORT)
#undef WDTRIGGEREND_START_SEC_CODE
#undef WDTRIGGEREND_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG __NEAR_SEG NON_BANKED
#endif

#if defined(WDTRIGGEREND_STOP_SEC_CODE) || \
    defined(WDTRIGGEREND_STOP_SEC_CODE_EXPORT)
#undef WDTRIGGEREND_STOP_SEC_CODE
#undef WDTRIGGEREND_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
# pragma CODE_SEG DEFAULT
#endif


