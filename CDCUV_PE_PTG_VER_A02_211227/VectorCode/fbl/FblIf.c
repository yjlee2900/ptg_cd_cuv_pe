/*****************************************************************************
| Project Name: CANfbl (KWP2000) for SYMC
|    File Name: FblIf.C
|
|  Description: Interface routines for application
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2006 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of callback 
| functions to be used with the Flash Bootloader. These functions may 
| influence the behaviour of the bootloader in principle. Therefore, 
| great care must be taken to verify the correctness of the 
| implementation.
|
| The contents of the originally delivered files are only examples resp. 
| implementation proposals. With regard to the fact that these functions 
| are meant for demonstration purposes only, Vector Informatik�s 
| liability shall be expressly excluded in cases of ordinary negligence, 
| to the extent admissible by law or statute.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| CB           Christian Baeuerle        Vector Informatik GmbH
|
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2006-12-20  01.00.00  CB      First implementation
|****************************************************************************/

/* Includes *******************************************************************/


/* EEPROM driver interface */
//#include "iotypes.h"
//#include "EepIo.h"
//#include "EepInc.h"

/* CAN-driver interface */
#include "can_inc.h"

#include "fbl_def.h"
#include "FblIf.h"
//#include "Variable.h"

/* Defines *******************************************************************/
/* Reset vector */
#define RESETVECT    ((vuint16)0xFFFE)   
#define STARTUP_CODE (*(volatile vuint16*)(RESETVECT))

#define JSR(x) (*((void(*V_CALLBACK_NEAR)(void))(x)))()
#define JSR_RESET()       JSR(STARTUP_CODE)

/* Constants ******************************************************************/
#if 0
#pragma CONST_SEG MODULEHDR


MEMORY_ROM tModuleInfoHeader ApplInfoHeader = 
{
   ECU_Origin,                /* Ecu Origin */
   SupplyIndentify,                /* Supplier ID */
   {DiagnoVersion_0, DiagnoVersion_1},        /* Diagnostic Info */
   ReservedValue,                /* Reserved */
   {SoftwareVersion_0, SoftwareVersion_1, SoftwareVersion_2},  /* Software version */
   {PartNumber_0, PartNumber_1, PartNumber_2, PartNumber_3, PartNumber_4, PartNumber_5, PartNumber_6, PartNumber_7, PartNumber_8, PartNumber_9}, /* Part-No */
   {0x01, 0x00}         /* Interface version */
};

#pragma CONST_SEG DEFAULT
#endif
/*
#define FBLHEADER_START_SEC_CONST
#include "memmap.h"
V_MEMROM0 V_MEMROM1 tFblHeader V_MEMROM2 FbltestHeader =
{
    0x02          
   ,0x00          
   ,0x03       
   ,0xff                           
   ,(V_MEMRAM0 V_MEMRAM1_FAR vuint8 V_MEMRAM2_FAR *)0x38DD
#if defined( FBL_ENABLE_FBL_START )
   ,(V_MEMRAM0 V_MEMRAM1_FAR tCanInitTable V_MEMRAM2_FAR *)0x38E1
   ,0xE35a
#endif
};
#define FBLHEADER_STOP_SEC_CONST
#include "memmap.h"

*/

void ApplCallFbl( void )
{
#if defined( FBL_ENABLE_FBL_START )
  CallFblStart(FBL_START_PARAM);
#else
ApplSetFblStartMagicFlag();
/* Only works with Watchdog enabled */
while(1);
#endif
}
