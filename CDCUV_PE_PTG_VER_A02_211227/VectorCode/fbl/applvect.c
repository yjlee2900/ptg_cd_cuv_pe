/*****************************************************************************
| Project Name: Flash Bootloader
|    File Name: APPLVECT.C
|
|  Description: Application vectortable for Motorola MC9S12X
|               This file is usually compiled and linked with
|               the bootloader and is located  O U T S I D E  the
|               protected area of the FBL. So it could be
|               compiled, linked and downloaded with the
|               application after adapting the interrupt
|               entry poits to the application interrupt vectors.
|               The application service routines are still interrupts!!
|
|               This file must be compiled and linked in the bootloader
|               and the application to the
|               S A M E  M E M O R Y   L O C A T I O N !!!!
|
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2000-2009 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains example source code used by the
| Flash Bootloader. This code may influence the behaviour of the
| bootloader in principle. Therefore, great care must be taken to verify
| the correctness of the implementation.
|
| The contents of the originally delivered files are only examples resp.
| implementation proposals. With regard to the fact that these functions
| are meant for demonstration purposes only, Vector Informatik�s
| liability shall be expressly excluded in cases of ordinary negligence,
| to the extent admissible by law or statute.
|------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------------------
| WM            Marco Wierer           Vector Informatik GmbH
| Hp            Armin Happel           Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2004-09-07  01.00.00  WM      ESCAN00009414: Initial version for S12X
| 2005-02-09  01.01.00  WM      ESCAN00015298: Corrected macro encapsulation
|                                for C_COMP_MWERKS_MCS12X_MSCAN12
|                               Added version scan support
| 2006-08-10  01.02.00  Hp      Using general typedef for IntVect-pointer
| 2007-01-20  01.03.00  WM      ESCAN00019209: C_COMP_MWERKS_MCS12X_MSCAN12
|                                to C_COMP_MTRWRKS_MCS12X_MSCAN12 renamed
| 2008-07-10  01.04.00  WM      ESCAN00029131: Added support for MCS12P
| 2009-12-15  01.05.00  AWh     ESCAN00039789: No changes
|*****************************************************************************/
#include "Cpu.h"
#include "v_cfg.h"
#include "v_def.h"
#include "applvect.h"
#include "Events.h"

/* --- Version --- */
#if ( (FBLVTABAPPL_HC12_VERSION != 0x0105) || \
      (FBLVTABAPPL_HC12_RELEASE_VERSION != 0x00) )
#error "Error in APPLVECT.C: Source and header file are inconsistent!"
#endif

extern void V_CALLBACK_NEAR RESET_ENTRY_POINT(void);   /*lint -e526 */


#pragma CONST_SEG APPLVECT
const tIntJmpTable ApplIntJmpTable[APPLVECT_SIZE] = {
  Cpu_Interrupt,                        /* 0x40  0xFF80   ivVsi         unused by PE */
  Cpu_Interrupt,                        /* 0x41  0xFF82   ivVReserved62 unused by PE */
  Cpu_Interrupt,                        /* 0x42  0xFF84   ivVatdcompare unused by PE */
  Cpu_Interrupt,                        /* 0x43  0xFF86   ivVhti        unused by PE */
  Timer100us_Interrupt,                 /* 0x44  0xFF88   ivVapi        used by PE */
  Cpu_Interrupt,                        /* 0x45  0xFF8A   ivVlvi        unused by PE */
  Cpu_Interrupt,                        /* 0x46  0xFF8C   ivVpwmesdn    unused by PE */
  Cpu_Interrupt,                        /* 0x47  0xFF8E   ivVportp      unused by PE */
  Cpu_Interrupt,                        /* 0x48  0xFF90   ivVReserved55 unused by PE */
  Cpu_Interrupt,                        /* 0x49  0xFF92   ivVReserved54 unused by PE */
  Cpu_Interrupt,                        /* 0x4A  0xFF94   ivVReserved53 unused by PE */
  Cpu_Interrupt,                        /* 0x4B  0xFF96   ivVReserved52 unused by PE */
  Cpu_Interrupt,                        /* 0x4C  0xFF98   ivVReserved51 unused by PE */
  Cpu_Interrupt,                        /* 0x4D  0xFF9A   ivVReserved50 unused by PE */
  Cpu_Interrupt,                        /* 0x4E  0xFF9C   ivVReserved49 unused by PE */
  Cpu_Interrupt,                        /* 0x4F  0xFF9E   ivVReserved48 unused by PE */
  Cpu_Interrupt,                        /* 0x50  0xFFA0   ivVReserved47 unused by PE */
  Cpu_Interrupt,                        /* 0x51  0xFFA2   ivVReserved46 unused by PE */
  Cpu_Interrupt,                        /* 0x52  0xFFA4   ivVReserved45 unused by PE */
  Cpu_Interrupt,                        /* 0x53  0xFFA6   ivVReserved44 unused by PE */
  Cpu_Interrupt,                        /* 0x54  0xFFA8   ivVReserved43 unused by PE */
  Cpu_Interrupt,                        /* 0x55  0xFFAA   ivVReserved42 unused by PE */
  Cpu_Interrupt,                        /* 0x56  0xFFAC   ivVReserved41 unused by PE */
  Cpu_Interrupt,                        /* 0x57  0xFFAE   ivVReserved40 unused by PE */
  CanTxInterrupt_0,                     /* 0x58  0xFFB0   ivVcantx      used by PE */
  CanRxInterrupt_0,                     /* 0x59  0xFFB2   ivVcanrx      used by PE */
  CanErrorInterrupt_0,                  /* 0x5A  0xFFB4   ivVcanerr     used by PE */
  CanWakeUpInterrupt_0,                 /* 0x5B  0xFFB6   ivVcanwkup    used by PE */
  Cpu_Interrupt,                        /* 0x5C  0xFFB8   ivVflash      unused by PE */
  Cpu_Interrupt,                        /* 0x5D  0xFFBA   ivVflashfd    unused by PE */
  Cpu_Interrupt,                        /* 0x5E  0xFFBC   ivVReserved33 unused by PE */
  Cpu_Interrupt,                        /* 0x5F  0xFFBE   ivVReserved32 unused by PE */
  Cpu_Interrupt,                        /* 0x60  0xFFC0   ivVReserved31 unused by PE */
  Cpu_Interrupt,                        /* 0x61  0xFFC2   ivVReserved30 unused by PE */
  Cpu_Interrupt,                        /* 0x62  0xFFC4   ivVReserved29 unused by PE */
  Cpu_Interrupt,                        /* 0x63  0xFFC6   ivVcpmuplllck unused by PE */
  Cpu_Interrupt,                        /* 0x64  0xFFC8   ivVcpmuocsns  unused by PE */
  Cpu_Interrupt,                        /* 0x65  0xFFCA   ivVReserved26 unused by PE */
  Cpu_Interrupt,                        /* 0x66  0xFFCC   ivVReserved25 unused by PE */
  Cpu_Interrupt,                        /* 0x67  0xFFCE   ivVportj      unused by PE */
  Cpu_Interrupt,                        /* 0x68  0xFFD0   ivVReserved23 unused by PE */
  AD1_Interrupt,                        /* 0x69  0xFFD2   ivVatd        used by PE */
  Cpu_Interrupt,                        /* 0x6A  0xFFD4   ivVReserved21 unused by PE */
  Cpu_Interrupt,                        /* 0x6B  0xFFD6   ivVsci        used by PE */
  Cpu_Interrupt,                        /* 0x6C  0xFFD8   ivVspi        unused by PE */
  Cpu_Interrupt,                        /* 0x6D  0xFFDA   ivVtimpaie    unused by PE */
  Cpu_Interrupt,                        /* 0x6E  0xFFDC   ivVtimpaaovf  unused by PE */
  Cpu_Interrupt,                        /* 0x6F  0xFFDE   ivVtimovf     unused by PE */
  Cpu_Interrupt,                        /* 0x70  0xFFE0   ivVtimch7     unused by PE */
  Cpu_Interrupt,                        /* 0x71  0xFFE2   ivVtimch6     unused by PE */
  Cpu_Interrupt,                        /* 0x72  0xFFE4   ivVtimch5     unused by PE */
  Cpu_Interrupt,                        /* 0x73  0xFFE6   ivVtimch4     unused by PE */
  Cpu_Interrupt,                        /* 0x74  0xFFE8   ivVtimch3     used by PE */
  Cpu_Interrupt,                        /* 0x75  0xFFEA   ivVtimch2     used by PE */
  Cpu_Interrupt,                        /* 0x76  0xFFEC   ivVtimch1     unused by PE */
  Timer1ms_Interrupt,                   /* 0x77  0xFFEE   ivVtimch0     unused by PE */
  RTI_ISR,                              /* 0x78  0xFFF0   ivVrti        unused by PE */
  Cpu_Interrupt,                        /* 0x79  0xFFF2   ivVirq        used by PE */
  Cpu_Interrupt,                        /* 0x7A  0xFFF4   ivVxirq       unused by PE */
  Cpu_Interrupt,                        /* 0x7B  0xFFF6   ivVswi        unused by PE */
  Cpu_Interrupt,                         /* 0x7C  0xFFF8   ivVtrap       unused by PE */
  _EntryPoint,                  /* 0xFFFA Not used */
  _EntryPoint,                  /* 0xFFFC Not used */   
   _EntryPoint  /* 0xFFFE Not used */
};

#pragma CONST_SEG DEFAULT

