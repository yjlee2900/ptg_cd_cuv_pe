/* -----------------------------------------------------------------------------
  Filename:    v_cfg.h
  Description: Toolversion: 04.00.00.00.90.01.41.00.00.00
                
                Serial Number: CBD0900141
                Customer Info: Freescale Semiconductor Korea Inc.
                               Hyundai / Freescale MC9S12XE / Metrowerks 5.0.30 / MC9S12XEQ384
                
                
                Generator Fwk   : GENy 
                Generator Module: GenTool_GenyVcfgNameDecorator
                
                Configuration   : E:\LKG\CD_PTG\ⓒEngineering\ⓒ소프트웨어 개발\ⓔ소프트웨어 소스\CAN_Bedded\Geny_V0.8_CD_PE_PTG_WGN_200324\GENy_V0.8_CD_PE_PTG_200324.gny
                
                ECU: 
                        TargetSystem: Hw_Mcs12xCpu
                        Compiler:     Metrowerks
                        Derivates:    MCS12X
                
                Channel "Channel0":
                        Databasefile: E:\LKG\CD_PE_PTG\Trunk\ⓒEngineering\ⓒ소프트웨어 개발\ⓔ소프트웨어 소스\CAN_Bedded\Geny_V0.8_CD_PE_PTG_WGN_200324\CANDB\20200306_CD PE_2021_Body_v3.0__MTEdit.dbc
                        Bussystem:    CAN
                        Manufacturer: HMC
                        Node:         PTGM

  Generated by , 2021-07-26  10:33:16
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2008 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__V_CFG_H__)
#define __V_CFG_H__

#ifndef VGEN_GENY
#define VGEN_GENY
#endif

#ifndef GENy
#define GENy
#endif

#ifndef SUPPLIER_CANBEDDED
#define SUPPLIER_CANBEDDED                   30
#endif

/* -----------------------------------------------------------------------------
    &&&~ Version
 ----------------------------------------------------------------------------- */

#ifndef VERSIONNUMBER
#define VERSIONNUMBER                        0x30
#endif



/* -----------------------------------------------------------------------------
    &&&~ General Switches
 ----------------------------------------------------------------------------- */

#ifndef V_OSTYPE_NONE
#define V_OSTYPE_NONE
#endif



/* -----------------------------------------------------------------------------
    &&&~ Processor specific
 ----------------------------------------------------------------------------- */

#ifndef C_CPUTYPE_16BIT
#define C_CPUTYPE_16BIT
#endif


#ifndef V_CPUTYPE_BITARRAY_16BIT
#define V_CPUTYPE_BITARRAY_16BIT
#endif


#ifndef C_CPUTYPE_BIGENDIAN
#define C_CPUTYPE_BIGENDIAN
#endif


#ifndef C_CPUTYPE_BITORDER_LSB2MSB
#define C_CPUTYPE_BITORDER_LSB2MSB
#endif


#ifndef V_DISABLE_USE_DUMMY_FUNCTIONS
#define V_DISABLE_USE_DUMMY_FUNCTIONS
#endif


#ifndef V_ENABLE_USE_DUMMY_STATEMENT
#define V_ENABLE_USE_DUMMY_STATEMENT
#endif


#ifndef C_COMP_MTRWRKS_MCS12X_MSCAN12
#define C_COMP_MTRWRKS_MCS12X_MSCAN12
#endif


#ifndef V_CPU_MCS12X
#define V_CPU_MCS12X
#endif

#ifndef V_COMP_METROWERKS
#define V_COMP_METROWERKS
#endif

#ifndef V_COMP_METROWERKS_MCS12X
#define V_COMP_METROWERKS_MCS12X
#endif

#ifndef V_PROCESSOR_MCS12X
#define V_PROCESSOR_MCS12X
#endif


#ifndef C_PROCESSOR_MCS12X
#define C_PROCESSOR_MCS12X
#endif




/* -----------------------------------------------------------------------------
    &&&~ Used Modules
 ----------------------------------------------------------------------------- */

#define VGEN_ENABLE_DIAG_CANDESC_BASIC_KWP
#define VGEN_ENABLE_CANDESC_BASIC_KWP
#define VGEN_ENABLE_CCP
#ifndef VGEN_ENABLE_VSTDLIB
#define VGEN_ENABLE_VSTDLIB
#endif

#ifndef VSTD_ENABLE_DEFAULT_INTCTRL
#define VSTD_ENABLE_DEFAULT_INTCTRL
#endif

#ifndef VSTD_ENABLE_GLOBAL_LOCK
#define VSTD_ENABLE_GLOBAL_LOCK
#endif

#ifndef VSTD_DISABLE_DEBUG_SUPPORT
#define VSTD_DISABLE_DEBUG_SUPPORT
#endif

#ifndef VSTD_ENABLE_LIBRARY_FUNCTIONS
#define VSTD_ENABLE_LIBRARY_FUNCTIONS
#endif


#define VGEN_ENABLE_CAN_DRV
#define C_ENABLE_CAN_CHANNELS
#define V_BUSTYPE_CAN
#define VGEN_ENABLE_IL_VECTOR
#define VGEN_ENABLE_NM_OSEK_D
#define VGEN_ENABLE_TP_ISO_MC


#ifndef kVNumberOfIdentities
#define kVNumberOfIdentities                 1
#endif

#ifndef tVIdentityMsk
#define tVIdentityMsk                        vuint8
#endif

#ifndef kVIdentityIdentity_0
#define kVIdentityIdentity_0                 (vuint8) 0
#endif

#ifndef VSetActiveIdentity
#define VSetActiveIdentity(identityLog)
#endif

#ifndef V_ACTIVE_IDENTITY_MSK
#define V_ACTIVE_IDENTITY_MSK                1
#endif

#ifndef V_ACTIVE_IDENTITY_LOG
#define V_ACTIVE_IDENTITY_LOG                0
#endif


#define DIAG_API_CALL_TYPE
#define DIAG_API_CALLBACK_TYPE
#define DIAG_INTERNAL_CALL_TYPE
#define DRV_API_CALL_TYPE
#define DRV_API_CALLBACK_TYPE
#define NM_API_CALL_TYPE
#define NM_API_CALLBACK_TYPE
#define NM_INTERNAL_CALL_TYPE
#define TP_API_CALL_TYPE
#define TP_API_CALLBACK_TYPE
#define TP_INTERNAL_CALL_TYPE

#ifndef V_REG_BLOCK_ADR
#define V_REG_BLOCK_ADR                      0x00u
#endif

#define VGEN_ENABLE_VSTDLIB

/* -----------------------------------------------------------------------------
    &&&~ Versions of Preconfig Files
 ----------------------------------------------------------------------------- */

#define VGEN_OEM_PRECONFIG_HMC_SLP1_Generic
#define VGEN_OEM_PRECONFIG_VERSION           0x0101u
#define VGEN_OEM_PRECONFIG_RELEASE_VERSION   0x00u
#define VGEN_USER_PRECONFIG_HMC_SLP4
#define VGEN_USER_PRECONFIG_VERSION          0x0000u
#define VGEN_USER_PRECONFIG_RELEASE_VERSION  0x00u



#ifndef VGEN_ENABLE_VSTDLIB
/* Diag_CanDesc_Basic requires VSTDLIB */
#define VGEN_ENABLE_VSTDLIB
#endif

#ifndef C_CLIENT_HMC
#define C_CLIENT_HMC
#endif

#ifndef __PTGM__
#define __PTGM__
#endif

/* -----------------------------------------------------------------------------
    &&&~ Compatibility defines for ComSetCurrentECU
 ----------------------------------------------------------------------------- */

#define kComNumberOfNodes                    kVNumberOfIdentities
#define ComSetCurrentECU                     VSetActiveIdentity
#define comMultipleECUCurrent                vActiveIdentityLog


#define C_VERSION_REF_IMPLEMENTATION         0x150u


#ifndef VGEN_ENABLE_VSTDLIB
/* DrvCan__baseRi15 requires VSTDLIB */
#define VGEN_ENABLE_VSTDLIB
#endif

#ifndef VGEN_ENABLE_VSTDLIB
#define VGEN_ENABLE_VSTDLIB
#endif


#ifndef VGEN_ENABLE_VSTDLIB
/* TpMC requires VSTDLIB */
#define VGEN_ENABLE_VSTDLIB
#endif


/* begin Fileversion check */
#ifndef SKIP_MAGIC_NUMBER
#ifdef MAGIC_NUMBER
  #if MAGIC_NUMBER != 386834908
      #error "The magic number of the generated file <E:\LKG\CD_PE_PTG\Trunk\ⓒEngineering\ⓒ소프트웨어 개발\ⓔ소프트웨어 소스\CAN_Bedded\GENy_CD_PTG_CUV_PE\v_cfg.h> is different. Please check time and date of generated files!"
  #endif
#else
  #define MAGIC_NUMBER 386834908
#endif  /* MAGIC_NUMBER */
#endif  /* SKIP_MAGIC_NUMBER */

/* end Fileversion check */

#endif /* __V_CFG_H__ */
