/*****************************************************************************
| Project Name: C C P - Driver adapter
|    File Name: can_ccp.c
|
|  Description: 
|   CCP driver customization module
|   This module is just an example how to adapt the CCP driver
|   It may be modified
|       !!! Please contact Vector for Support !!!
|
|  Contains:
|   - Examples for indication, confirmation and pretransmit functions
|     for the vector CAN driver.
|   - Examples for FLASH programming, EEPROM programming, CalPage switching
|
|-----------------------------------------------------------------------------
|               D E M O
|-----------------------------------------------------------------------------
|
|       Please note, that the demo and example programs 
|       only show special aspects of the software. 
|       With regard to the fact that these programs are meant 
|       for demonstration purposes only,
|       Vector Informatik's liability shall be expressly excluded in cases 
|       of ordinary negligence, to the extent admissible by law or statute.
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2001-2008 by Vector Informatik GmbH.      All rights reserved.
|
|       This software is copyright protected and 
|       proprietary to Vector Informatik GmbH.
|       Vector Informatik GmbH grants to you only
|       those rights as set out in the license conditions.
|       All other rights remain with Vector Informatik GmbH.
| 
|       Diese Software ist urheberrechtlich geschuetzt. 
|       Vector Informatik GmbH raeumt Ihnen an dieser Software nur 
|       die in den Lizenzbedingungen ausdruecklich genannten Rechte ein.
|       Alle anderen Rechte verbleiben bei Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Eta          Edgar Tongoona            Vector Informatik GmbH
| Get          Matthias Gette            Vector Informatik GmbH
| Hp           Armin Happel              Vector Informatik GmbH
| Js           Bernd Jesse               Vector Informatik GmbH
| Mra          Marc Rauscher             Vector Informatik GmbH
| Si           Bernd Sigle               Vector Informatik GmbH
| Tri          Frank Triem               Vector Informatik GmbH
| Svh          Sven Hesselmann           Vector Informatik GmbH
| Stu          Bernd Stumpf              Vector Informatik GmbH
| Hr           Andreas Herkommer         Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
|  Date       Version  Author  Description
| ----------  -------  ------  -----------------------------------------------
| 2002-04-17  1.00.00  Hp      - Setting-up version and commonise the header 
|                                with CANbedded software modules.
|                      Js      - CCP_CAN_CHANNEL added, code switches only via
|                                positive defines changed
| 2002-05-15  1.01.00  Js      - CCP_CRO_Indication() dummy statement moved at
|                                the of the function
| 2003-02-11  1.02.00  Si      - support for C_COMP_ANSI_CANOE added
| 2003-04-11  1.03.00  Mra     - support for indexed CAN drivers
| 2003-08-12  1.03.01  Tri     - support for V_ENABLE_USE_DUMMY_STATEMENT added
| 2003-09-17  1.03.02  Si      - support for C_COMP_ANSI_CANOE and Windows NT added
| 2003-10-14  1.03.03  Tri     - corrected compiler switch CCP_BOOTLOADER for ccpFlashClear()
| 2003-11-17  1.04.00  Tri     - ESCAN00007927: Apply new naming for callbacks
|                              - ESCAN00010257: 16Bit access to misaligned data
| 2005-05-23  1.05.00  Tri     - ESCAN00012355: Memory access by application
| 2005-06-30  1.06.00  Tri     - ESCAN00012700: place ccpWrite in far page for Star12
| 2006-03-16  1.07.00  Tri     - ESCAN00014169: Support HCS12x with Metrowerks compiler
|                              - ESCAN00014184: Support compiler option +nowiden on Star12x/Cosmic
| 2006-06-12  1.08.00  Tri     - Double word access to word aligned memory area in function GetSeed
|                              - ESCAN00016157: Demo disclaimer added
|                              - ESCAN00014625: Warning because of missing return value
| 2006-08-31  1.09.00  Get     - ESCAN00017162: Big-Endian CPUs only: Wrong 'Seed and Key' handling
|                      Tri     - ESCAN00017497: Illegal combination of pointer in function 'CCP_CRO_Indication'
| 2006-11-06  1.10.00  Tri     - ESCAN00018251: Support of small memory model
| 2007-03-07  1.11.00  Eta     - ESCAN00019311: Remove initialized RAM vars: ccpResourceMask and ccpLastSeed
| 2008-04-18  1.12.00  Svh     - Cosmic compiler option -pck caused "truncating assignment"
| 2008-06-12  1.12.01  Stu     - support C_COMP_MICROCHIP_PIC18 with Microchip compiler
| 2009-04-02  1.12.02  Hr      - ESCAN00034342: Compile error due to version check mismatch.
|***************************************************************************/

#define CCP_CAN_INTERNAL


#include "can_inc.h"
#include "ccp.h"

#if defined ( C_ENABLE_CCP )

#define CCP_CAN_ADAPTER_VERSION  0x0112
#define CCP_CAN_ADAPTER_BUGFIX_VERSION 0x02

/******************************************************************************/
/* Version check                                                              */
/******************************************************************************/
#if ( CP_CCP_VERSION != 0x0152 )
  #error "Current Version of CCP is not matching with can_ccp Version!"
#endif


/*----------------------------------------------------------------------------*/

#if defined ( CCP_WRITE_EEPROM )
  #include "eeprom.h"
#endif

/*----------------------------------------------------------------------------*/

/* CCP_CAN_CHANNEL defines in ccppar.h (CANgen) the used CAN channel for CCP */

#if defined ( C_SINGLE_RECEIVE_BUFFER ) || \
    defined ( C_MULTIPLE_RECEIVE_BUFFER )
# if defined ( CCP_CAN_CHANNEL )

#  if ( CCP_CAN_CHANNEL == 0 )
#  define CanTransmit CanTransmit_0
#   if defined ( C_ENABLE_TRANSMIT_QUEUE_0 )
#    define C_ENABLE_TRANSMIT_QUEUE
#   endif
#  endif

#  if ( CCP_CAN_CHANNEL == 1 )
#  define CanTransmit CanTransmit_1
#   if defined ( C_ENABLE_TRANSMIT_QUEUE_1 )
#    define C_ENABLE_TRANSMIT_QUEUE
#   endif
#  endif

#  if ( CCP_CAN_CHANNEL == 2 )
#  define CanTransmit CanTransmit_2
#   if defined ( C_ENABLE_TRANSMIT_QUEUE_2 )
#    define C_ENABLE_TRANSMIT_QUEUE
#   endif
#  endif

#  if ( CCP_CAN_CHANNEL == 3 )
#  define CanTransmit CanTransmit_3
#   if defined ( C_ENABLE_TRANSMIT_QUEUE_3 )
#    define C_ENABLE_TRANSMIT_QUEUE
#  endif
#  endif

#  if ( CCP_CAN_CHANNEL == 4 )
#  define CanTransmit CanTransmit_4
#   if defined ( C_ENABLE_TRANSMIT_QUEUE_4 )
#    define C_ENABLE_TRANSMIT_QUEUE
#   endif
#  endif
# else

#  if (kCanNumberOfChannels > 1)
#   error "! CCP only available in one instance - use CCP_CAN_CHANNEL !"
#  endif

# endif /*CCP_CAN_CHANNEL*/

#endif /*C_SINGLE_RECEIVE_BUFFER || C_MULTIPLE_RECEIVE_BUFFER*/

/*----------------------------------------------------------------------------*/

#if defined ( CCP_SEED_KEY )
static CCP_BYTE   ccpResourceMask;
static CCP_DWORD  ccpLastSeed;
#endif

#if defined ( CCP_CALPAGE )
static CCP_BYTE   ccpCalPage;
#endif

#if defined ( CCP_WRITE_EEPROM )
void ccpCheckPendingEEPROM( void );
#endif

/******************************************************************************/
/* The following functions are the interface between CCP and the CAN driver   */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* Basic initialization of the CCP on CAN driver */
void ccpCanInit(void) 
{  
  #if defined ( CCP_CALPAGE )
  ccpCalPage = 0;
  #endif
  #if defined ( CCP_SEED_KEY )
  ccpResourceMask = 0;
  ccpLastSeed = 0;
  #endif
}

/*----------------------------------------------------------------------------*/
/* Indication function for rx message CRO */
void CCP_CRO_Indication(CanReceiveHandle rxObject)
{
  CCP_DWORD c[2];

  /* CCP message received, data has been copied */
  /* Argument is pointer to copied data */

  /* Handle CCP commands on application level in ccpBackground */
  #if defined ( CCP_CMD_NOT_IN_INTERRUPT )
  ccp.SendStatus |= CCP_CMD_PENDING;
  /* Handle CCP commands on CAN interrupt level */
  #else
  /* Copy to a CCP_DWORD aligned location */
  ((CCP_BYTEPTR)c)[0] = CCP_RX_DATA_PTR[0];
  ((CCP_BYTEPTR)c)[1] = CCP_RX_DATA_PTR[1];
  ((CCP_BYTEPTR)c)[2] = CCP_RX_DATA_PTR[2];
  ((CCP_BYTEPTR)c)[3] = CCP_RX_DATA_PTR[3];
  ((CCP_BYTEPTR)c)[4] = CCP_RX_DATA_PTR[4];
  ((CCP_BYTEPTR)c)[5] = CCP_RX_DATA_PTR[5];
  ((CCP_BYTEPTR)c)[6] = CCP_RX_DATA_PTR[6];
  ((CCP_BYTEPTR)c)[7] = CCP_RX_DATA_PTR[7];
  ccpCommand((CCP_BYTEPTR)c);
  #endif

  #if defined ( V_ENABLE_USE_DUMMY_STATEMENT )
  /* avoid compiler warning due to unused parameters */
  rxObject = rxObject;
  #endif
}

/*----------------------------------------------------------------------------*/
/* Confirmation function of tx message CCP_DTO is configured in generation tool */
void CCP_DTO_Confirmation(CanTransmitHandle txObject)
{
  #if defined ( V_ENABLE_USE_DUMMY_STATEMENT )
  /* avoid compiler warning due to unused parameters */
  txObject = txObject;
  #endif

  (void)ccpSendCallBack();
}

/*----------------------------------------------------------------------------*/
/* Transmit the CCP message */
/* Id is CCP_DTO_ID, which is configured at compile time */
void ccpSend(CCP_BYTEPTR msg)
{
  CCP_TX_DATA_PTR[0] = msg[0];
  CCP_TX_DATA_PTR[1] = msg[1];
  CCP_TX_DATA_PTR[2] = msg[2];
  CCP_TX_DATA_PTR[3] = msg[3];
  CCP_TX_DATA_PTR[4] = msg[4];
  CCP_TX_DATA_PTR[5] = msg[5];
  CCP_TX_DATA_PTR[6] = msg[6];
  CCP_TX_DATA_PTR[7] = msg[7];

  #if defined ( C_ENABLE_TRANSMIT_QUEUE )
  if(CanTransmit(CCP_TX_HANDLE) != kCanTxOk)
  {
    /* Fatal Error, should never fail */
    ccpInit();
  }
  #else
  if(CanTransmit(CCP_TX_HANDLE) != kCanTxOk)
  {
    /* Set transmission request flag */
    ccp.SendStatus |= CCP_TX_PENDING;
  }
  #endif
}

/*----------------------------------------------------------------------------*/
/* Perform backgound calculation if needed */

void ccpUserBackground(void)
{
  /* Try to retransmit if CAN driver transmit queue is not enabled */
  #if defined  ( C_ENABLE_TRANSMIT_QUEUE )
  #else
  if(ccp.SendStatus & CCP_TX_PENDING)
  {
    if(CanTransmit(CCP_TX_HANDLE) == kCanTxOk)
    {
      ccp.SendStatus &= (CCP_BYTE)~CCP_TX_PENDING;
    }
  }
  #endif

  /* Check if a pending EEPROM write access is finished */
  #if defined ( CCP_WRITE_EEPROM )
  ccpCheckPendingEEPROM();
  #endif

  /* ... */
  /* Insert any other user actions here */
  /* Call ccpSendCrm() to finish pending EEPROM or FLASH cycles */
}

/******************************************************************************/
/* The following functions must be individually implemented if required       */
/* There are samples available for C16x, HC12, SH7055                              */
/******************************************************************************/

/*----------------------------------------------------------------------------*/

/*
ccpGetPointer

Convert a memory address from CCP 8/32bit into a C pointer
used for memory transfers like DNLOAD, UPLOAD (MTA)
*/

#if defined ( ccpGetPointer )
#else
CCP_MTABYTEPTR ccpGetPointer(CCP_BYTE addr_ext, CCP_DWORD addr)
{
  #if defined ( V_ENABLE_USE_DUMMY_STATEMENT )
  /* avoid compiler warning due to unused parameters */
  addr_ext = addr_ext;
  #endif

  /* Example C16x: DDP1 used for CCP_RAM/CCP_ROM selection */
  #if defined ( CANBOX ) || defined ( PHYTEC_MM167 )
    #if defined ( CCP_CALPAGE )
    if((ccpCalPage == 1) && (addr >= 0x14000) && (addr < 0x18000))
    { /* CALRAM */
      return (CCP_MTABYTEPTR)(addr + 0x30000UL);
    }
    #endif
  #endif

  #if defined ( CCP_ENABLE_MEM_ACCESS_BY_APPL ) && ( defined ( C_COMP_MTRWRKS_MCS12X_MSCAN12 ) )
  if ( ( addr & (CCP_DWORD)0x00FF0000ul ) > (CCP_DWORD)0x00ul )
  {
    CCP_DWORD globalAddress;
    if ( (CCP_BYTE)(( addr & (CCP_DWORD)0x00FF0000ul ) >> 16 ) > 0x7F )
    {
      /* The paged address and the page are stored within xcp.MTA  */
      /* Convert from paged address to global address. */
      if ( ( addr & (CCP_DWORD)0x0000FFFFul ) >= 0x8000ul )
      {
        /* Paged Flash */
        /* Global address = 0x780000 + (PPAGE - 0xE0 - 0x02) * 0x4000 + paged address */
        globalAddress = 0x780000ul + ((CCP_BYTE)(( addr & (CCP_DWORD)0x00FF0000ul ) >> 16 ) - (CCP_BYTE)0xE2u ) * 0x4000 + (addr & (CCP_DWORD)0xFFFFul);
      }
      else
      {
        if ( ( addr & (CCP_DWORD)0x0000FFFFul ) >= 0x1000ul )
        {
          /* Paged RAM */
          /* Global address = 0x0F8000 + (RPAGE - 0xF9) * 0x1000 + paged address */
          globalAddress = 0x0F8000ul + ( (CCP_BYTE)(( addr & (CCP_DWORD)0x00FF0000ul ) >> 16 ) - (CCP_BYTE)0xF9u ) * 0x1000 + (addr & (CCP_DWORD)0xFFFFul);
        }
        else
        {
          if ( ( addr & (CCP_DWORD)0x0000FFFFul ) >= 0x0800ul )
          {
            /* Paged EEPROM */
            /* Global address = 0x13F000 + (EPAGE - 0xFB) * 0x400 + paged address */
            globalAddress = 0x013F000ul + (0xFBu - (CCP_BYTE)(( addr & (CCP_DWORD)0x00FF0000ul ) >> 16 ) ) * 0x0400ul + (addr & (CCP_DWORD)0xFFFFul);
          }
        }
      }
    }
    else
    {
      globalAddress = addr;
    }
    return (CCP_MTABYTEPTR) globalAddress;
  }
  else
  {
    /* Convert from local to global address. */
    if ( ( addr & (CCP_DWORD)0x0000FFFFu ) < 0x4000 )
    {
      /* RAM */
      return (CCP_MTABYTEPTR) (addr /*+ 0x0FC000u*/);   /* 20130320 pjy */
      /* return (CCP_MTABYTEPTR) (addr + 0x0FC000u); */
    }
    else
    {
      /* ROM */
      return (CCP_MTABYTEPTR) (addr + 0x7F0000u);
    }
  }
  #else
  return (CCP_MTABYTEPTR) addr;
  #endif
}
#endif


/*
ccpGetDaqPointer

Convert a memory address from CCP 8/32bit into a address
stored in the ODT entries for DAQ.

This is for reducing memory space used for DAQ lists.
For example on a 32 bit microcontroller, a DAQ pointer may be stored as 16 bit
value. DAQ will add the base address CCP_DAQ_BASE_ADDR before dereferencing the
pointer. This will limit data acquisition to a single 64K memory range, but it
will save 50% memory used for DAQ lists.

Note: It must be possible to calculate the final address for DAQ like
 value = * (CCP_DAQ_BASE_ADDR + addr);
*/

#if defined ( CCP_DAQ_BASE_ADDR )
CCP_DAQBYTEPTR ccpGetDaqPointer(CCP_BYTE addr_ext, CCP_DWORD addr)
{
  return (CCP_DAQBYTEPTR)(ccpGetPointer(addr_ext, addr) - CCP_DAQ_BASE_ADDR);
}
#endif


#if defined ( CCP_ENABLE_MEM_ACCESS_BY_APPL )
CCP_BYTE ccpRead( CCP_DWORD addr )
{
    #if defined ( C_COMP_MTRWRKS_MCS12X_MSCAN12 )
  return *((CCP_BYTEPTR __far)addr);
    #else
  return *((CCP_BYTEPTR)addr);
    #endif /*C_COMP_MTRWRKS_MCS12X_MSCAN12*/
}  

void CCP_FAR ccpWrite( CCP_DWORD addr, CCP_BYTE data )
{
    #if defined ( C_COMP_MTRWRKS_MCS12X_MSCAN12 )
  *((CCP_BYTEPTR __far)addr) = data;
    #else
  *((CCP_BYTEPTR)addr) = data;
    #endif
}

#endif


/*----------------------------------------------------------------------------*/
/* Check addresses for valid write access */
/* Used only if Write Protection is required */
/* Returns false if access denied */
#if defined ( CCP_WRITE_PROTECTION )
CCP_BYTE ccpCheckWriteAccess(CCP_MTABYTEPTR a, CCP_BYTE s)
{
  /* Protect CCP */
  if(a+s>=(CCP_MTABYTEPTR)&ccp && a<(CCP_MTABYTEPTR)&ccp+sizeof(ccp))
  {
    return 0;
  }

  return 1;
}
#endif


/*----------------------------------------------------------------------------*/
/* Flash Kernel Download */
/* Used only of Download of the Flash Kernel is required */
#if defined ( CCP_BOOTLOADER_DOWNLOAD )
CCP_BYTE ccpDisableNormalOperation(CCP_MTABYTEPTR a, CCP_WORD s)
{
  #if defined ( V_ENABLE_USE_DUMMY_STATEMENT )
  /* avoid compiler warning due to unused parameters */
  a = a;
  s = s;
  #endif

  /* CANape attempts to download the flash kernel to CCP_RAM */
  /* Address is a */
  /* Size is s bytes */

  /* ... */

  /* return 0; */ /* Refused */
  return 1; /* Ok */
}
#endif

/*----------------------------------------------------------------------------*/
/* Example: Flash Programming */
/* Used only if integrated Flash Programming is required */

#if defined ( CCP_PROGRAM )

# if defined ( CCP_BOOTLOADER )
# else

#include "flash.h"

void ccpFlashClear(CCP_MTABYTEPTR a, CCP_DWORD size)
{
  #if defined ( CANBOX ) || defined ( PHYTEC_MM167 )
    #if defined ( CCP_CALPAGE )
    if(a >= (CCP_MTABYTEPTR)0x40000)
    {
      a -= 0x30000; /* Compensate CCP_RAM/CCP_ROM mapping */
    }
    #endif
  #endif

  CCP_DISABLE_INTERRUPT;
  flashEraseBlock(a);
  CCP_ENABLE_INTERRUPT;
}

CCP_BYTE ccpFlashProgramm(CCP_BYTEPTR data, CCP_MTABYTEPTR a, CCP_BYTE size)
{
  #if defined ( CANBOX ) || defined ( PHYTEC_MM167 )
    #if defined ( CCP_CALPAGE )
    if(a >= (CCP_MTABYTEPTR)0x40000)
    {
      a -= 0x30000; /* Compensate CCP_RAM/CCP_ROM mapping */
    }
    #endif
  #endif

  if(size == 0)
  { /* End of programing sequence */
    /* Software Reset */
  }

  while(size > 0)
  {
    CCP_DISABLE_INTERRUPT;
    flashByteWrite(a, *data);
    CCP_ENABLE_INTERRUPT;
    data++;
    a++;
    size--;
  }

  return CCP_WRITE_OK;
}

# endif /* CCP_BOOTLOADER */
#endif /* CCP_PROGRAM */

/*----------------------------------------------------------------------------*/
/* Example: Calibration CCP_RAM/CCP_ROM Selection */
/* Used if Flash Programming is required */

#if defined ( CCP_CALPAGE ) || defined ( CCP_PROGRAM )
CCP_DWORD ccpGetCalPage(void)
{
  return (CCP_DWORD)ccpCalPage;
}

void ccpSetCalPage(CCP_DWORD a)
{
  ccpCalPage = (CCP_BYTE)a;

  #if defined ( CANBOX ) || defined ( PHYTEC_MM167 )
  if (ccpCalPage==1)
  {/* CCP_RAM */
    #pragma asm
    mov DPP1,#11h
    #pragma endasm
  }
  else
  {/* CCP_ROM */
    #pragma asm
    mov DPP1,#05h
    #pragma endasm
  }
  #endif

}

void ccpInitCalPage(void)
{
  #if defined ( CANBOX ) || defined ( PHYTEC_MM167 )

    #define CALROM_ADDR 0x14000
    #define CALRAM_ADDR 0x44000
    huge CCP_BYTE *p1 = (huge CCP_BYTE *)CALROM_ADDR;
    huge CCP_BYTE *p2 = (huge CCP_BYTE *)CALRAM_ADDR;
    unsigned int i;

    for(i = 0; i < 0x4000; i++)
    {
      *p2++ = *p1++;
    }

  #endif
}

#endif

/*----------------------------------------------------------------------------*/
/* Example: Seed&Key*/
/* Used only if Seed&Key is required */

#if defined ( CCP_SEED_KEY )

CCP_DWORD ccpGetSeed(CCP_BYTE resourceMask)
{
  ccpResourceMask = resourceMask;
  ccpLastSeed     = 0;

  /* Generate a seed */

  /* Example: */
  /* Optimum would be a number which never appears twice */
  #if defined ( CCP_TIMESTAMPING )
  ccpLastSeed = ccpGetTimestamp() * ccpGetTimestamp();
  #endif

  return ccpLastSeed;
}


CCP_BYTE ccpUnlock(CCP_BYTEPTR key)
{
  /* Check the key */
  CCP_DWORD dwKey;

  /* Since key is not aligned to dword the bytes are copied individually. */
  ((vuint8*)(&dwKey))[0] = key[0];
  ((vuint8*)(&dwKey))[1] = key[1];
  ((vuint8*)(&dwKey))[2] = key[2];
  ((vuint8*)(&dwKey))[3] = key[3];

  /* Example: */
  ccpLastSeed = (ccpLastSeed>>(vuint8)5u) | (ccpLastSeed<<(vuint8)23u);
  ccpLastSeed *= 7;
  ccpLastSeed ^= (vuint32)0x26031961u;

  if (dwKey != ccpLastSeed)
  {
    return 0;
  }
  else
  {
    /* Reset resource protection bit */
    return ccpResourceMask;
  }
}
#endif


/*----------------------------------------------------------------------------*/
/* Example: EEPROM write access */
/* Used only if required */

#if defined ( CCP_WRITE_EEPROM )

/* Pending EEPROM write cycle */
CCP_BYTE ccpEEPROMState = 0;

/* EEPROM write */
/* Return values for ccpCheckWriteEEPROM:
   CCP_WRITE_OK      - EEPROM written
   CCP_WRITE_DENIED  - This address is not in EEPROM
   CCP_WRITE_PENDING - EEPROM write in progress, call ccpSendCrm() when done
   CCP_WRITE_ERROR   - EEPROM write failed
*/
CCP_BYTE ccpCheckWriteEEPROM(CCP_MTABYTEPTR addr, CCP_BYTE size, CCP_BYTEPTR data)
{
  /* Check address for EEPROM */
  if(addr<EEPROM_START || addr>EEPROM_END)
  {
    /* Not EEPROM */
    /* Let the CCP driver perform a standard CCP_RAM write access */
    return CCP_WRITE_DENIED;
  }

  /* Alternative 1: */
  /* Initiate EEPROM write */
  /* When finished, call ccpSendCrm() */

  eeWrite(addr,data,size);
  return CCP_WRITE_OK;



}

/* Check for EEPROM write finished */
void ccpCheckPendingEEPROM( void )
{
}

#endif


#if defined ( CCP_READ_EEPROM )

/* EEPROM read */
/* Return values for ccpCheckReadEEPROM:
   0 (FALSE)    - This address is not in EEPROM
   1 (TRUE)     - EEPROM read
*/
CCP_BYTE ccpCheckReadEEPROM( CCP_MTABYTEPTR addr, CCP_BYTE size, CCP_BYTEPTR data )
{

  /* Read EEPROM */
  return 0;
}

#endif

#endif /* C_ENABLE_CCP */
/************   Organi, Version 3.8.0 Vector-Informatik GmbH  ************/
