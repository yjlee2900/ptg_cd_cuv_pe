@echo off
.\HexView\hexview.exe CD_CUV_PE_VER_A01_211223.glo /FA /AD2 /AL /CS5:@append /XS /s -o CD_CUV_PE_VER_A01_211223.hex



if "%1" == "" goto usage

rem USED HEXVIEW PARAMETERS
rem /FA ............ Create a single region file (fill all)
rem /AD2 ........... Align data
rem /AL ............ Align length
rem /CS5:@append ... Checksum calculation method
rem /XS ............ Exports in Motorola S-Record format
rem /s ............. Run HexView in silent modes

%~d0
cd %~dp0

rem Fill gaps with fill all
rem Align end to two. Otherwise appending checksum is not possible.
rem Append checksum

.\HexView\hexview.exe %1 /FA /AD2 /AL /CS5:@append /XS /s -o %~n1DL.hex
goto end

:usage
echo "Drag&Drop a hexfile (.hex .mhx) on this batch file"
echo "Then a single region hexfile with an appended checksum will be created"
pause
goto end

:end
